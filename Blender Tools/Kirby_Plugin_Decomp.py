# ------------------------------------------------------------------------
#    Header
# ------------------------------------------------------------------------

import bpy

from bpy.props import (
    StringProperty,
    BoolProperty,
    IntProperty,
    FloatProperty,
    FloatVectorProperty,
    EnumProperty,
    PointerProperty,
    IntVectorProperty,
    BoolVectorProperty,
    CollectionProperty,
)
from bpy.types import (
    Panel,
    Menu,
    Operator,
    PropertyGroup,
)

import os, struct, sys, math, importlib, re

from time import time
from functools import lru_cache
from shutil import copy
from pathlib import Path
from types import ModuleType
from mathutils import Vector, Euler, Matrix
from collections import namedtuple
from dataclasses import dataclass
from copy import deepcopy

# my own module I'll have to make a package for eventually
import F3DEX2_gbi as f3dex2

bl_info = {
    "name": "KCS Importer/Exporter",
    "description": "Import&Export levels for Kirby Crystal Shards",
    "author": "scuttlebug_raiser",
    "version": (1, 0, 0),
    "blender": (2, 81, 0),
    "location": "3D View > Tools",
    "warning": "",  # used for warning icon and text in addons panel
    "wiki_url": "",
    "tracker_url": "",
    "category": "Import-Export",
}

# ------------------------------------------------------------------------
#    Decorators
# ------------------------------------------------------------------------


# change the render engine to cycles and disable the device so things run faster
def DisableRender(context):
    context.scene.render.engine = "CYCLES"


# time a function
def time_func(func):
    # This function shows the execution time of
    # the function object passed
    def wrap_func(*args, **kwargs):
        t1 = time()
        result = func(*args, **kwargs)
        t2 = time()
        print(f"Function {func.__name__!r} executed in {(t2-t1):.4f}s")
        return result

    return wrap_func


# ------------------------------------------------------------------------
#    Classes
# ------------------------------------------------------------------------

# singleton for breakable block, I don't think this actually works across sessions
class BreakableBlockDat:
    _instance = None
    _Gfx_mat = None
    _Col_mat = None
    _Col_Mesh = None
    _Gfx_Mesh = None
    _Verts = [
        (-40.0, -40.0, -40.0),
        (-40.0, -40.0, 40.0),
        (-40.0, 40.0, -40.0),
        (-40.0, 40.0, 40.0),
        (40.0, -40.0, -40.0),
        (40.0, -40.0, 40.0),
        (40.0, 40.0, -40.0),
        (40.0, 40.0, 40.0),
    ]
    _GfxTris = [
        (1, 2, 0),
        (3, 6, 2),
        (7, 4, 6),
        (5, 0, 4),
        (6, 0, 2),
        (3, 5, 7),
        (1, 3, 2),
        (3, 7, 6),
        (7, 5, 4),
        (5, 1, 0),
        (6, 4, 0),
        (3, 1, 5),
    ]
    _ColTris = [
        (3, 6, 2),
        (5, 0, 4),
        (6, 0, 2),
        (3, 5, 7),
        (3, 7, 6),
        (5, 1, 0),
        (6, 4, 0),
        (3, 1, 5),
    ]

    def __new__(cls):
        if cls._instance is None:
            cls._instance = cls
        return cls._instance

    def Instance_Col(self):
        if self._Col_Mesh:
            return self._Col_Mesh
        else:
            self._Col_Mesh = bpy.data.meshes.new("KCS Block Col")
            self._Col_Mesh.from_pydata(self._Verts, [], self._ColTris)
            return self._Col_Mesh

    def Instance_Gfx(self):
        if self._Gfx_Mesh:
            return self._Gfx_Mesh
        else:
            self._Gfx_Mesh = bpy.data.meshes.new("KCS Block Gfx")
            self._Gfx_Mesh.from_pydata(self._Verts, [], self._GfxTris)
            return self._Gfx_Mesh

    def Instance_Mat_Gfx(self, obj):
        if self._Gfx_mat:
            return self._Gfx_mat
        bpy.context.view_layer.objects.active = obj
        bpy.ops.object.create_f3d_mat()
        self._Gfx_mat = self._Gfx_Mesh.materials[-1]
        self._Gfx_mat.f3d_mat.combiner1.D = "TEXEL0"
        self._Gfx_mat.f3d_mat.combiner1.D_alpha = "1"
        return self._Gfx_mat

    def Instance_Mat_Col(self, obj):
        if self._Col_mat:
            return self._Col_mat
        bpy.context.view_layer.objects.active = obj
        bpy.ops.object.create_f3d_mat()
        self._Col_mat = self._Col_Mesh.materials[-1]
        self._Col_mat.KCS_col.ColType = 9
        return self._Col_mat


# for selecting proper misc type of imp bin
class misc_bin:
    _vert_sig = (0x270F, 0x270F, 0x270F)

    def __new__(self, file):
        self.file = file
        sig_obj = self.upt(self, 4, ">3H", 6)
        if sig_obj == self._vert_sig:
            feature = "obj"
        else:
            sig_obj = self.upt(self, 16, ">3H", 6)
            if sig_obj == self._vert_sig:
                feature = "level"
            else:
                feature = "particle"
        subclass_map = {
            subclass.feature: subclass for subclass in self.__subclasses__()
        }
        subclass = subclass_map[feature]
        instance = super(misc_bin, subclass).__new__(subclass)
        return instance

    def upt(self, offset, type, len):
        return struct.unpack(type, self.file[offset : offset + len])


# for i/o of node data
class node_cls:
    def __init__(self, num):
        self.index = num

    def cam_imp(self, cam, vol, scale):
        Null = lambda x, y: x * (x != y)
        kcs_cam = cam.data.KCS_cam
        cn = self.cam_node
        kcs_cam.AxisLocks = [cn.LockX != 0, cn.LockY != 0, cn.LockZ != 0]
        kcs_cam.ProfileView = cn.Profile
        kcs_cam.PanH, kcs_cam.PanUPDown, kcs_cam.PanDown = (
            cn.PanYaw,
            cn.PanHiLo,
            cn.PanLo,
        )
        kcs_cam.Yaw = (cn.CamYaw1, cn.CamYaw2)
        kcs_cam.Radius = (cn.CamRadius1, cn.CamRadius2)
        kcs_cam.Pitch = (cn.CamPhi1, cn.CamPhi2)
        kcs_cam.Clips = (cn.NearClip, cn.FarClip)
        kcs_cam.Foc = (cn.FocusX, cn.FocusY, cn.FocusZ)
        kcs_cam.CamPitchBound = (cn.CamYawLock1, cn.CamYawLock2)
        kcs_cam.CamYawhBound = (cn.CamPitchLock1, cn.CamPitchLock2)
        x, y, z = (
            (Null(cn.CamXLock1, -9999) + Null(cn.CamXLock2, 9999)) / (2 * scale),
            (Null(cn.CamYLock1, -9999) + Null(cn.CamYLock2, 9999)) / (2 * scale),
            (Null(cn.CamZLock1, -9999) + Null(cn.CamZLock2, 9999)) / (2 * scale),
        )
        cam.location = Vector((x, y, z)) - cam.parent.location
        x, y, z = (
            (Null(cn.CamXLock2, 9999) - Null(cn.CamXLock1, -9999)) / (2 * scale),
            (Null(cn.CamYLock2, 9999) - Null(cn.CamYLock1, -9999)) / (2 * scale),
            (Null(cn.CamZLock2, 9999) - Null(cn.CamZLock1, -9999)) / (2 * scale),
        )
        vol.scale = (x, z, y)

    def path_imp(self, path, scale):
        kcs_node = path.data.KCS_node
        kcs_node.NodeNum = self.index + 1
        if self.num_connections == 1:
            kcs_node.NextNode = self.node_connections[2] + 1
            kcs_node.PrevNode = self.node_connections[2] + 1
            kcs_node.LockBackward = self.node_connections[0] != 0
            kcs_node.LockForward = self.node_connections[3] == 0
        elif self.num_connections == 2:
            kcs_node.NextNode = self.node_connections[2] + 1
            kcs_node.PrevNode = self.node_connections[6] + 1
            kcs_node.LockBackward = self.node_connections[0] != 0
            kcs_node.LockForward = self.node_connections[4] == 0
        num_pts = self.path_footer[1]
        path.data.splines.remove(path.data.splines[0])
        sp = path.data.splines.new("POLY")
        sp.points.add(num_pts - 1)
        first = self.path_matrix[0]
        path.location = [
            f / scale for f in (first[0], -first[2], first[1])
        ]  # rotation is not applied to location so I have to manually swap coords
        for i, s in enumerate(sp.points):
            coord = self.path_matrix[i]
            s.co = [*[(f - a) / scale for f, a in zip(coord, first)], 0]
        # warps
        shade = self.kirb_node[6:10]  # not implemented yet
        warp = self.kirb_node[2:6]
        # flag values to enums
        Locations = {
            0: "walk start",
            1: "walk end",
            2: "appear start",
            3: "appear end",
        }
        Actions = {
            0: "walk",
            1: "stand",
            2: "jump up",
            3: "jump forward",
            # 4 is unk
            5: "climb wall up",
            6: "climb wall down",
            7: "climb rope up",
            8: "climb rope down",
            9: "walking (unk)",
            10: "jumping (unk)",
            11: "fall from air",
        }
        kcs_node.EntranceLocation = Locations.get(self.kirb_node.EnterEnum >> 0xFF)
        kcs_node.EntranceAction = Actions.get(self.kirb_node.EnterEnum & 0xFF)
        kcs_node.Warp = warp[0:3]
        kcs_node.WarpDir = self.kirb_node.DestNode
        kcs_node.EnWarp = self.kirb_node.WarpFlag & 1

    def cam_out(self, cam, vol):
        pass

    def path_out(self, path):
        pass


# for i/o of level bin data
class level_bin(misc_bin):
    feature = "level"

    def __init__(self, file):
        self.main_header = self.upt(0, ">3L", 12)
        self.col_header = self.upt(self.main_header[0], ">17L", 68)
        self.node_header = self.upt(self.main_header[1], ">4L", 16)
        self.num_nodes = self.node_header[0]
        self.path_nodes = {}
        for i in range(self.num_nodes):
            self.decode_node(i)
        self.entities = []
        x = 0
        start = self.main_header[2]
        entity = namedtuple(
            "entity", "node bank id action res_flag spawn_flag eep pos rot scale"
        )
        vec3f = namedtuple("vec3", "x y z")
        if start:
            while True:
                sig = self.upt(start + x * 0x2C, ">L", 4)
                if sig[0] == 0x99999999:
                    break
                ent = self.upt(start + x * 0x2C, ">6BH9f", 0x2C)
                pos, rot, scale = [
                    vec3f._make(ent[3 * i + 7 : 3 * i + 10]) for i in range(3)
                ]
                self.entities.append(entity._make([*ent[:7], pos, rot, scale]))
                x += 1
        self.decode_col()

    def decode_node(self, num):
        node = node_cls(num)
        kirb = namedtuple(
            "kirb_node",
            "node EnterEnum w l a DestNode unused shad1 shad2 shad3 unused2 WarpFlag opt1 opt2 opt3 opt4 unused3",
        )
        cam = namedtuple(
            "cam_node",
            """Profile pad LockX LockY LockZ pad PanHiLo PanLo PanYaw pad pad FocusX FocusY FocusZ
        NearClip FarClip CamR1 CamR2 CamYaw1 CamYaw2 CamRadius1 CamRadius2 FOV1 FOV2 CamPhi1 CamPhi2
        CamXLock1 CamXLock2 CamYLock1 CamYLock2 CamZLock1 CamZLock2 CamYawLock1 CamYawLock2 CamPitchLock1 CamPitchLock2""",
            rename=True,
        )
        path_node = self.node_header[1] + (num * 16)
        node.path_node = self.upt(path_node, ">3L2H", 16)
        node.kirb_node = kirb._make(self.upt(node.path_node[0], ">2H4B4B4H2fL", 0x20))
        node.cam_node = cam._make(self.upt(node.path_node[0] + 0x20, ">10BH25f", 0x70))
        node.path_footer = self.upt(node.path_node[1], ">2HfLf2L", 0x18)
        node.num_connections = node.path_node[3]
        node.node_connections = self.upt(
            node.path_node[2],
            ">%dB" % (0x4 * node.num_connections),
            0x4 * node.num_connections,
        )
        node.is_loop = node.path_node[4]
        self.path_nodes[num] = node
        node.path_matrix = [
            self.upt(node.path_footer[3] + 12 * i, ">3f", 12)
            for i in range(node.path_footer[1])
        ]

    def decode_col(self):
        tri_num = self.col_header[1]
        vert_num = self.col_header[3]
        water_num = self.col_header[14]
        self.triangles = [
            self.upt(a * 20 + self.col_header[0], ">10H", 20) for a in range(1, tri_num)
        ]
        self.vertices = [
            self.upt(a * 6 + self.col_header[2], ">3h", 6) for a in range(1, vert_num)
        ]
        de_groups = self.col_header[11]
        de_indices = self.col_header[12]
        x = 0
        self.de_tris = {}
        pops = []
        while x + de_groups + 2 < de_indices:
            grp = self.upt(x + de_groups, ">3H", 6)
            if grp[0] != 0:
                indices = [
                    self.upt(2 * grp[1] + de_indices + 2 * i, ">H", 2)[0] - 1
                    for i in range(grp[0])
                ]
                self.de_tris[grp[2]] = [self.triangles[a] for a in indices]
                pops.extend(indices)
            x += 6
        if self.de_tris:
            pops.sort(reverse=True)
            [self.triangles.pop(a) for a in pops]


# for object bins. unused for now
class object_bin(misc_bin):
    feature = "obj"


# wont be used, will raise error
class particle_bin(misc_bin):
    feature = "particle"


class bpy_collision:
    # given a class full of formatted data, writes
    # data to blender. also gets data from blender
    # for class
    def __init__(self, rt, collection):
        self.rt = rt  # this is an empty with kcs object type collision
        self.collection = collection

    def write_bpy_col(self, cls, scene, scale):
        # start by formatting tri/vert data
        collection = self.rt.users_collection[0]
        main = self.create_pydata(cls, cls.triangles, scale)
        dynamic = {}
        for geo, dyn in cls.de_tris.items():
            dynamic[geo] = self.create_pydata(cls, dyn, scale)
            # make objs but no parenting
            dyn_mesh = MakeMeshData("kcd_dyn_mesh", dynamic[geo][0:3])
            dyn_obj = MakeMeshObj("kcs dyn obj", dyn_mesh, collection)
            self.write_mats(dyn_obj, dynamic[geo][3])
            Parent(self.rt, dyn_obj, 0)
            RotateObj(-90, dyn_obj)
            dyn_obj.KCS_mesh.MeshType = "Collision"
            dyn_obj.KCS_mesh.ColMeshType = "Breakable"
        # make objs and link
        main_mesh = MakeMeshData("kcs level mesh", main[0:3])
        main_obj = MakeMeshObj("kcs level obj col", main_mesh, collection)
        Parent(self.rt, main_obj, 0)  # get col rt from rt
        RotateObj(-90, main_obj)
        main_obj.KCS_mesh.MeshType = "Collision"
        self.write_mats(main_obj, main[3])
        # format node data
        for num, node in cls.path_nodes.items():
            AddNode(
                self.rt, collection
            )  # use my own operator so default settings are made
            path = self.rt.children[-1]
            RotateObj(-90, path)
            cam = path.children[0]
            vol = cam.children[0]
            node.path_imp(path, scale)
            node.cam_imp(cam, vol, scale)
            node.bpy_path = path
        # update view layer so that paths have accurate positions
        bpy.context.view_layer.update()
        # write entities
        for e in cls.entities:
            o = MakeEmpty("KCS entity", "ARROWS", collection)
            o.KCS_obj.KCS_obj_type = "Entity"
            ent = o.KCS_ent
            path = cls.path_nodes[e.node].bpy_path
            Parent(path, o, 1)  # parent to node, but remove transform
            rot = (
                e.rot[0],
                e.rot[1],
                e.rot[2],
            )  # only rotate root since tree will inherit transform
            o.rotation_euler = rot
            loc = Vector(e.pos) / scale
            o.location += Vector([loc[0], loc[1], loc[2]])
            o.scale = e.scale

            ent.BankNum = e.bank
            ent.IndexNum = e.id
            ent.Action = e.action
            ent.Respawn = e.res_flag
            ent.Eep = e.eep
            ent.Flags = e.spawn_flag

    def scale_verts(self, verts, scale):
        scaled = []
        for v in verts:
            scaled.append([a / scale for a in v])
        return scaled

    # make (verts,[],tris,mat_dat) properly formated given tri list
    def create_pydata(self, cls, tris, scale):
        verts = []
        triangles = []
        ind = set()
        mat_dat = []
        for i, t in enumerate(tris):
            tri = []
            for j, a in enumerate(t[0:3]):
                if a - 1 not in ind:
                    ind.add(a - 1)
                    tri.append(len(verts))
                    verts.append(cls.vertices[a - 1])
                else:
                    tri.append(verts.index(cls.vertices[a - 1]))
            mat_dat.append(t)
            triangles.append(tri)
        return (self.scale_verts(verts, scale), (), triangles, mat_dat)

    def write_mats(self, obj, dat):
        polys = obj.data.polygons
        mats = set()
        mat_dict = dict()
        for p, t in zip(polys, dat):
            if t[9] == 8:
                warp = t[5]
            else:
                warp = 0
            mat = (t[4], t[9], t[8], warp)
            if mat not in mats:
                mats.add(mat)
                bpy.data.materials.new("kcs mat")
                mat_dict[mat] = (len(bpy.data.materials), len(mat_dict))
                material = bpy.data.materials[-1]
                obj.data.materials.append(material)
                material.KCS_col.NormType = t[4]
                material.KCS_col.ColType = t[9]
                material.KCS_col.ColParam = t[8]
                material.KCS_col.WarpNum = warp
            p.material_index = mat_dict[mat][1]


# geo data

# just a base class that holds some binary processing
class BinProcess:
    def upt(self, offset, type, len):
        return struct.unpack(type, self.file[offset : offset + len])

    @staticmethod
    def seg2phys(num):
        if num >> 24 == 4:
            return num & 0xFFFFFF
        else:
            return num

    def get_BI_pairs(self, start, num=9999, stop=(0, 0)):
        pairs = []
        for i in range(num):
            BI = self.upt(start + 4 * i, ">HH", 4)
            pairs.append(BI)
            if stop == BI:
                break
        return pairs

    def get_referece_list(self, start, stop=0):
        x = 0
        start = self.seg2phys(start)
        ref = []
        r = self.upt(start, ">L", 4)[0]
        while r != stop:
            x += 4
            ref.append(r)
            r = self.upt(start + x, ">L", 4)[0]
        ref.append(r)
        return ref

    def write_arr(self, file, name, arr, BI=None, ptr_format="0x"):
        file.write(f"void *{name}[] = {{\n\t")
        vals = []
        for a in arr:
            if a == 0x99999999 or a == (0x9999, 0x9999):
                vals.append(f"ARR_TERMINATOR")
            elif BI:
                vals.append("BANK_INDEX(0x{:X}, 0x{:X})".format(*a))
            elif a:
                # ptrs have to be in bank 4 (really applied to entry pts)
                if a & 0x04000000 == 0x04000000:
                    vals.append(f"{ptr_format}{a:X}")
                else:
                    vals.append(f"0x{a:X}")
            else:
                vals.append("NULL")
        file.write(", ".join(vals))
        file.write("\n};\n\n")

    def SortDict(self, dictionary):
        return {k: dictionary[k] for k in sorted(dictionary.keys())}


# these aren't actually all ints
@dataclass
class layout(BinProcess):
    flag: int
    depth: int
    ptr: int
    translation: int
    rotation: int
    scale: int
    index: int = 0


# fake storage class that mirrors methods of layout
@dataclass
class faux_LY:
    ptr: int
    index: int = 0


class Vertices:
    _Vec3 = namedtuple("Vec3", "x y z")
    _UV = namedtuple("UV", "s t")
    _color = namedtuple("rgba", "r g b a")

    def __init__(self, scale):
        self.UVs = []
        self.VCs = []
        self.Pos = []
        self.scale = scale

    def _make(self, v):
        self.Pos.append(self.ScaleVerts(self._Vec3._make(v[0:3])))
        self.UVs.append(self._UV._make(v[4:6]))
        self.VCs.append(self._color._make(v[6:10]))

    def ScaleVerts(self, pos):
        v = [a / self.scale for a in pos]
        return v


# use for extraction,
TextureScrollStruct = {
    0x00: [">H", "field_0x0", 2],
    0x02: [">B", "fmt1", 1],
    0x03: [">B", "siz1", 1],
    0x04: [">L", "textures", 4],
    0x08: [">H", "stretch", 2],
    0x0A: [">H", "sharedOffset", 2],
    0x0C: [">H", "t0_w", 2],
    0x0E: [">H", "t0_h", 2],
    0x10: [">L", "halve", 4],
    0x14: [">f", "t0_xShift", 4],
    0x18: [">f", "t0_yShift", 4],
    0x1C: [">f", "xScale", 4],
    0x20: [">f", "yScale", 4],
    0x24: [">f", "field_0x24", 4],
    0x28: [">f", "field_0x28", 4],
    0x2C: [">L", "palettes", 4],
    0x30: [">H", "flags", 2],
    0x32: [">B", "fmt2", 1],
    0x33: [">B", "siz2", 1],
    0x34: [">H", "w2", 2],
    0x36: [">H", "h2", 2],
    0x38: [">H", "t1_w", 2],
    0x3A: [">H", "t1_h", 2],
    0x3C: [">f", "t1_xShift", 4],
    0x40: [">f", "t1_yShift", 4],
    0x44: [">f", "field_0x44", 4],
    0x48: [">f", "field_0x48", 4],
    0x4C: [">L", "field_0x4c", 4],
    0x50: [">4B", "prim_col", 4, "arr"],
    0x54: [">B", "primLODFrac", 1],
    0x55: [">B", "field_0x55", 1],
    0x56: [">B", "field_0x56", 1],
    0x57: [">B", "field_0x57", 1],
    0x58: [">4B", "env_col", 4, "arr"],
    0x5C: [">4B", "blend_col", 4, "arr"],
    0x60: [">4B", "light1_col", 4, "arr"],
    0x64: [">4B", "light2_col", 4, "arr"],
    0x68: [">L", "field_0x68", 4],
    0x6C: [">L", "field_0x6c", 4],
    0x70: [">L", "field_0x70", 4],
    0x74: [">L", "field_0x74", 4],
}

# this is the class that holds the actual individual scroll struct and textures
class tx_scroll:
    _scroll = namedtuple(
        "texture_scroll", " ".join([x[1] for x in TextureScrollStruct.values()])
    )

    def __init__(self, *args):
        self.scroll = self._scroll._make(*args)


# each texture scroll will start from an array of ptrs, and each ptr will reference
# tex scroll data
class Tex_Scroll(BinProcess):
    def extract_dict(self, start, dict):
        a = []
        for k, v in dict.items():
            try:
                if v[3]:
                    a.append(self.upt(start + k, v[0], v[2]))
            except:
                a.append(self.upt(start + k, v[0], v[2])[0])
        return a

    def __init__(self, scroll_ptrs, file, ptr):
        self.scroll_ptrs = scroll_ptrs
        self.scrolls = {}
        self.file = file
        self.ptr = ptr
        for p in scroll_ptrs:
            if p != 0x99999999 and p:
                # get struct
                scr = tx_scroll(
                    self.extract_dict(self.seg2phys(p), TextureScrollStruct)
                )
                self.scrolls[p] = scr
                # search for palletes
                if scr.scroll.palettes:
                    start = self.seg2phys(scr.scroll.palettes)
                    self.pal_start = scr.scroll.textures
                    scr.palettes = self.get_BI_pairs(start, stop=(0x9999, 0x9999))
                # search for textures
                if scr.scroll.textures:
                    start = self.seg2phys(scr.scroll.textures)
                    self.tx_start = scr.scroll.textures
                    scr.textures = self.get_BI_pairs(start, stop=(0x9999, 0x9999))

    def Write(self, file):
        # sort structs in case they aren't in order
        self.scrolls = self.SortDict(self.scrolls)
        for p, s in self.scrolls.items():
            # textures
            if hasattr(s, "textures"):
                self.write_arr(
                    file, f"tx_scroll_textures_{self.tx_start:X}", s.textures, BI=1
                )
            # palletes
            if hasattr(s, "palettes"):
                self.write_arr(
                    file, f"tx_scroll_palettes_{self.pal_start:X}", s.palettes, BI=1
                )
            # struct
            WriteDictStruct(
                s.scroll, TextureScrollW, file, "", f"tx_scroll scroll_{p:X}"
            )

    def WriteHeader(self, file):
        # write header, separate func because these are at the end of the file
        self.write_arr(
            file, f"tx_scroll_hdr_{self.ptr:X}", self.scroll_ptrs, ptr_format="&scroll_"
        )


class geo_bin(BinProcess):
    _Vec3 = namedtuple("Vec3", "x y z")
    _texture = namedtuple("texture", "fmt siz bank_index")

    def __init__(self, file, scale):
        self.file = file
        self.main_header = self.upt(0, ">8L", 32)
        self.scale = scale
        self.get_tex_scrolls()
        self.DLs = (
            dict()
        )  # this is also in layouts, but I want it raw here to print in RAM order, and in layouts to analyze in the processed order
        self.render_mode = self.main_header[2]
        self._render_mode_map[self.render_mode](self)
        # get vtx and img refs null terminated arrays
        self.get_refs()
        self.get_anims()

    def get_tex_scrolls(self):
        if self.main_header[1]:
            start = self.main_header[1]
            # get header of POINTERS
            self.tex_header = self.get_referece_list(start, stop=0x99999999)
            self.tex_scrolls = {}
            for p in self.tex_header:
                if p and p != 0x99999999:
                    self.tex_scrolls[p] = Tex_Scroll(
                        self.get_referece_list(p, stop=0x99999999), self.file, p
                    )
            # sort scrolls
            self.tex_scrolls = self.SortDict(self.tex_scrolls)

    # anims are bank indices
    def get_anims(self):
        num = self.main_header[5]
        start = self.seg2phys(self.main_header[6])
        self.anims = self.get_BI_pairs(start, num=num)

    # both types of refs are null terminated lists
    def get_refs(self):
        self.img_refs = self.get_referece_list(self.main_header[3])
        self.vtx_refs = self.get_referece_list(self.main_header[4])

    # no layout, just a single DL
    def decode_layout_13(self):
        L = faux_LY(self.seg2phys(self.main_header[0]))
        L.dl_ptrs = [L.ptr]
        self.layouts = [L]
        self.decode_f3d_bin(L)
        Vert_End = L.ptr[0]
        self.decode_vertices(32, Vert_End)

    # no layouts, just an entry point
    def decode_layout_14(self):
        L = faux_LY(self.seg2phys(self.main_header[0]))
        self.decode_entry(L)
        self.layouts = [L]
        self.decode_f3d_bin(L)
        Vert_End = L.ptr[0]
        self.decode_vertices(32, Vert_End)

    # layouts point to DL
    def decode_layout_17(self):
        self.layouts = [
            self.decode_layout(self.main_header[0] + 0x2C * i, index=i)
            for i in range(self.main_header[-1])
        ]
        starts = []
        for l in self.layouts:
            if l.ptr:
                l.dl_ptrs = [l.ptr]
                starts.extend(self.decode_f3d_bin(l))
        if starts:
            Vert_End = min(starts)
            self.decode_vertices(32, Vert_End)

    # layouts point to entry point
    def decode_layout_18(self):
        self.layouts = [
            self.decode_layout(self.main_header[0] + 0x2C * i, index=i)
            for i in range(self.main_header[-1])
        ]
        starts = []
        for l in self.layouts:
            if l.ptr:
                self.decode_entry(l)
                starts.extend(self.decode_f3d_bin(l))
        if starts:
            Vert_End = min(starts)
            self.decode_vertices(32, Vert_End)

    # layout points to pair of DLs
    def decode_layout_1B(self):
        self.layouts = [
            self.decode_layout(self.main_header[0] + 0x2C * i, index=i)
            for i in range(self.main_header[-1])
        ]
        starts = []
        for l in self.layouts:
            if l.ptr:
                self.decode_DL_pair(l)
                starts.extend(self.decode_f3d_bin(l))
        if starts:
            Vert_End = min(starts)
            self.decode_vertices(32, Vert_End)

    # layout points to entry point with pair of DL
    def decode_layout_1C(self):
        self.layouts = [
            self.decode_layout(self.main_header[0] + 0x2C * i, index=i)
            for i in range(self.main_header[-1])
        ]
        starts = []
        for l in self.layouts:
            if l.ptr:
                self.decode_entry_dbl(l)
                starts.extend(self.decode_f3d_bin(l))
        if starts:
            Vert_End = min(starts)
            self.decode_vertices(32, Vert_End)

    def decode_layout(self, start, index=None):
        start = self.seg2phys(start)
        LY = self.upt(start, ">2HL9f", 0x2C)
        v = self._Vec3._make
        return layout(*LY[0:3], v(LY[3:6]), v(LY[6:9]), v(LY[9:12]), index=index)

    # has to be after func declarations
    _render_mode_map = {
        0x13: decode_layout_13,
        0x14: decode_layout_14,
        0x17: decode_layout_17,
        0x18: decode_layout_18,
        0x1B: decode_layout_1B,
        0x1C: decode_layout_1C,
    }

    def decode_DL_pair(self, ly: layout):
        ly.dl_ptrs = []  # just a literal list of ptrs
        ptrs = self.upt(self.seg2phys(ly.ptr), ">2L", 8)
        for ptr in ptrs:
            if ptr:
                ly.dl_ptrs.append(ptr)
        ly.DL_Pair = ptrs

    def decode_entry_dbl(self, ly: layout):
        x = 0
        start = self.seg2phys(ly.ptr)
        ly.entry_dbls = []
        ly.dl_ptrs = []  # just a literal list of ptrs
        while True:
            mark, *ptrs = self.upt(start + x, ">3L", 12)
            ly.entry_dbls.append((mark, ptrs))
            if mark == 4:
                return
            else:
                for ptr in ptrs:
                    if ptr:
                        ly.dl_ptrs.append(ptr)
            x += 12
            # shouldn't execute
            if x > 120:
                print("your while loop is broken in geo_bin.decode_entry")
                break

    def decode_entry(self, ly: layout):
        x = 0
        start = self.seg2phys(ly.ptr)
        ly.entry_pts = []  # the actual entry pt raw data
        ly.dl_ptrs = []  # just a literal list of ptrs
        while True:
            mark, ptr = self.upt(start + x, ">2L", 8)
            ly.entry_pts.append((mark, ptr))
            if mark == 4:
                return
            if ptr == 0:
                continue
            else:
                ly.dl_ptrs.append(ptr)
            x += 8
            # shouldn't execute
            if x > 80:
                print("your while loop is broken in geo_bin.decode_entry")
                break

    # gonna use a module for this
    def decode_f3d_bin(self, layout):
        DLs = {}
        self.vertices = []
        starts = []
        layout.entry = layout.dl_ptrs[
            :
        ]  # create shallow copy, use this for analyzing DL, while DL ptrs will be a dict including jumped to DLs
        for dl in layout.dl_ptrs:
            start = self.seg2phys(dl)
            starts.append(start)
            f3d = self.decode_dl_bin(start, layout)
            self.DLs[dl] = f3d
            DLs[dl] = f3d
        layout.DLs = DLs
        return starts

    def DL_ptr(self, num):
        if num >> 24 == 0xE:
            return None
        else:
            return num

    def decode_dl_bin(self, start, layout):
        DL = []
        x = 0
        while True:
            cmd = self.Getf3dCmd(self.file[start + x : start + x + 8])
            x += 8
            if not cmd:
                continue
            name, args = self.split_args(cmd)
            if name == "gsSPEndDisplayList":
                break
            elif name == "gsSPDisplayList":
                ptr = self.DL_ptr(int(args[0]))
                if ptr:
                    layout.dl_ptrs.append(ptr)
                DL.append((name, args))
                continue
            # this LoD info will probably just stay destroyed for now
            elif name == "gsSPBranchLessZ":
                ptr = self.DL_ptr(int(args[0]))
                if ptr:
                    layout.dl_ptrs.append(ptr)
                DL.append((name, args))
                continue
            elif name == "gsSPBranchList":
                ptr = self.DL_ptr(int(args[0]))
                if ptr:
                    layout.dl_ptrs.append(ptr)
                DL.append((name, args))
                break
            DL.append((name, args))
        DL.append((name, args))
        return DL

    @lru_cache(maxsize=32)  # will save lots of time with repeats of tri calls
    def Getf3dCmd(self, bin):
        return f3dex2.Ex2String(bin)

    def split_args(self, cmd):
        filt = "\(.*\)"
        a = re.search(filt, cmd)
        return cmd[: a.span()[0]], cmd[a.span()[0] + 1 : a.span()[1] - 1].split(",")

    def decode_vertices(self, start, end):
        self.vertices = Vertices(self.scale)
        for i in range(start, end, 16):
            v = self.upt(i, ">6h4B", 16)
            self.vertices._make(v)


class bpy_geo:
    def __init__(self, rt, collection, scale):
        self.rt = rt
        self.collection = collection
        self.scale = scale

    def write_gfx(self, name, cls, tex_path):
        # for now, do basic import, each layout is an object
        stack = [self.rt]
        self.LastMat = None
        # create dict of models so I can reuse model dat as needed (usually for blocks)
        Models = dict()
        for i, layout in enumerate(cls.layouts):
            if (layout.depth & 0xFF) == 0x12:
                break
            # mesh object
            if layout.ptr:
                prev = Models.get(layout.ptr)
                # model was already imported, reuse data block with new obj
                if prev:
                    mesh, self.LastMat = prev
                else:
                    ModelDat = F3d(lastmat=self.LastMat)
                    (layout.vertices, layout.Triangles) = ModelDat.GetDataFromDL(
                        cls, layout
                    )
                    self.LastMat = ModelDat.LastMat
                    mesh = bpy.data.meshes.new(f"{name} {layout.depth&0xFF} {i}")
                    mesh.from_pydata(layout.vertices, [], layout.Triangles)
                    # add model to dict
                    Models[layout.ptr] = (mesh, self.LastMat)
                obj = bpy.data.objects.new(f"{name} {layout.depth&0xFF} {i}", mesh)
                self.collection.objects.link(obj)
                # set KCS props of obj
                obj.KCS_mesh.MeshType = "Graphics"
                # apply dat
                ModelDat.ApplyDat(obj, mesh, tex_path)
                # cleanup
                mesh.validate()
                mesh.update(calc_edges=True)
                if bpy.context.scene.KCS_scene.CleanUp:
                    # shade smooth
                    obj.select_set(True)
                    bpy.context.view_layer.objects.active = obj
                    bpy.ops.object.shade_smooth()
                    bpy.ops.object.mode_set(mode="EDIT")
                    bpy.ops.mesh.remove_doubles()
                    bpy.ops.object.mode_set(mode="OBJECT")
            # empty transform
            else:
                obj = MakeEmpty(
                    f"{name} {layout.depth} {i}", "PLAIN_AXES", self.collection
                )
                # set KCS props of obj
                obj.KCS_mesh.MeshType = "Graphics"
            # now that obj is created, parent and add transforms to it
            if (layout.depth & 0xFF) < len(stack) - 1:
                stack = stack[: (layout.depth & 0xFF) + 1]
            Parent(stack[-1], obj, 0)
            if (layout.depth & 0xFF) + 1 > len(stack) - 1:
                stack.append(obj)
            loc = layout.translation
            obj.location += Vector(loc) / self.scale
            obj.scale = Vector([1 / a for a in layout.scale])
            rot = layout.rotation
            if (layout.depth & 0xFF) == 0:
                rot = (
                    rot[0] - math.radians(-90),
                    rot[1],
                    rot[2],
                )  # only rotate root since tree will inherit transform
            obj.rotation_euler.rotate(Euler(rot))

    @staticmethod
    def Vec3Trans(vec3):
        return (vec3.x, -vec3.z, vec3.y)


# this will hold tile properties
class Tile:
    def __init__(self):
        self.Fmt = "RGBA"
        self.Siz = "16"
        self.Slow = 32
        self.Tlow = 32
        self.Shigh = 32
        self.Thigh = 32
        self.SMask = 5
        self.TMask = 5

        self.Sflags = None
        self.Tflags = None


# this will hold texture properties, dataclass props
# are created in order for me to make comparisons in a set
@dataclass(init=True, eq=True, unsafe_hash=True)
class Texture:
    Timg: tuple
    Fmt: str
    Siz: int
    Width: int = 0
    Height: int = 0
    Pal: tuple = None


# This is simply a data storage class
class Mat:
    def __init__(self):
        self.TwoCycle = False
        self.GeoSet = []
        self.GeoClear = []
        self.tiles = [Tile() for a in range(8)]
        self.tex0 = None
        self.tex1 = None
        self.tx_scr = None

    # calc the hash for an f3d mat and see if its equal to this mats hash
    def MatHashF3d(self, f3d):
        # texture,1 cycle combiner, render mode, geo modes, some other blender settings, tile size (very important in kirby64)
        rdp = f3d.rdp_settings
        if f3d.tex0.tex:
            T = f3d.tex0.tex_reference
        else:
            T = ""
        F3Dprops = (
            T,
            f3d.combiner1.A,
            f3d.combiner1.B,
            f3d.combiner1.C,
            f3d.combiner1.D,
            f3d.combiner1.A_alpha,
            f3d.combiner1.B_alpha,
            f3d.combiner1.C_alpha,
            f3d.combiner1.D_alpha,
            f3d.rdp_settings.rendermode_preset_cycle_1,
            f3d.rdp_settings.rendermode_preset_cycle_2,
            f3d.rdp_settings.g_lighting,
            f3d.rdp_settings.g_shade,
            f3d.rdp_settings.g_shade_smooth,
            f3d.rdp_settings.g_zbuffer,
            f3d.rdp_settings.g_mdsft_alpha_compare,
            f3d.rdp_settings.g_mdsft_zsrcsel,
            f3d.rdp_settings.g_mdsft_alpha_dither,
            f3d.tex0.S.high,
            f3d.tex0.T.high,
            f3d.tex0.S.low,
            f3d.tex0.T.low,
        )
        if hasattr(self, "Combiner"):
            MyT = ""
            if hasattr(self.tex0, "Timg"):
                MyT = str(self.tex0.Timg)
            else:
                pass

            def EvalGeo(self, mode):
                for a in self.GeoSet:
                    if mode in a.lower():
                        return True
                for a in self.GeoClear:
                    if mode in a.lower():
                        return False
                else:
                    return True

            chkT = lambda x, y, d: x.__dict__.get(y, d)
            rendermode = getattr(
                self, "RenderMode", ["G_RM_AA_ZB_OPA_SURF", "G_RM_AA_ZB_OPA_SURF2"]
            )
            MyProps = (
                MyT,
                *self.Combiner[0:8],
                *rendermode,
                EvalGeo(self, "g_lighting"),
                EvalGeo(self, "g_shade"),
                EvalGeo(self, "g_shade_smooth"),
                EvalGeo(self, "g_zbuffer"),
                chkT(self, "g_mdsft_alpha_compare", "G_AC_NONE"),
                chkT(self, "g_mdsft_zsrcsel", "G_ZS_PIXEL"),
                chkT(self, "g_mdsft_alpha_dither", "G_AD_NOISE"),
                self.tiles[0].Shigh,
                self.tiles[0].Thigh,
                self.tiles[0].Slow,
                self.tiles[0].Tlow,
            )
            dupe = hash(MyProps) == hash(F3Dprops)
            return dupe
        return False

    def MatHash(self, mat):
        return False

    def ConvertColor(self, color):
        return [int(a) / 255 for a in color]

    def LoadTexture(self, ForceNewTex, path, tex):
        png = path / f"bank_{tex.Timg[0]}" / f"{tex.Timg[1]}"
        png = (*png.glob("*.png"),)
        if png:
            i = bpy.data.images.get(str(png[0]))
            if not i or ForceNewTex:
                return bpy.data.images.load(filepath=str(png[0]))
            else:
                return i

    def ApplyPBSDFMat(self, mat):
        nt = mat.node_tree
        nodes = nt.nodes
        links = nt.links
        pbsdf = nodes.get("Principled BSDF")
        tex = nodes.new("ShaderNodeTexImage")
        links.new(pbsdf.inputs[0], tex.outputs[0])
        links.new(pbsdf.inputs[19], tex.outputs[1])
        i = self.LoadTexture(0, path)
        if i:
            tex.image = i

    def ApplyMatSettings(self, mat, tex_path):
        #        if bpy.context.scene.LevelImp.AsObj:
        #            return self.ApplyPBSDFMat(mat, textures, path, layer)

        f3d = mat.f3d_mat  # This is kure's custom property class for materials

        # set color registers if they exist
        if hasattr(self, "fog_position"):
            f3d.set_fog = True
            f3d.use_global_fog = False
            f3d.fog_position[0] = eval(self.fog_pos[0])
            f3d.fog_position[1] = eval(self.fog_pos[1])
        if hasattr(self, "fog_color"):
            f3d.set_fog = True
            f3d.use_global_fog = False
            f3d.fog_color = self.ConvertColor(self.fog_color)
        if hasattr(self, "light_col"):
            # this is a dict but I'll only use the first color for now
            f3d.set_lights = True
            if self.light_col.get(1):
                f3d.default_light_color = self.ConvertColor(
                    eval(self.light_col[1]).to_bytes(4, "big")
                )
        if hasattr(self, "env"):
            f3d.set_env = True
            f3d.env_color = self.ConvertColor(self.env[-4:])
        if hasattr(self, "prim"):
            prim = self.prim
            f3d.set_prim = True
            f3d.prim_lod_min = int(prim[0])
            f3d.prim_lod_frac = int(prim[1])
            f3d.prim_color = self.ConvertColor(prim[-4:])

        # I set these but they aren't properly stored because they're reset by fast64 or something
        # its better to have defaults than random 2 cycles
        self.SetGeoMode(f3d.rdp_settings, mat)

        if self.TwoCycle:
            f3d.rdp_settings.g_mdsft_cycletype = "G_CYC_2CYCLE"
        else:
            f3d.rdp_settings.g_mdsft_cycletype = "G_CYC_1CYCLE"

        # make combiner custom
        f3d.presetName = "Custom"
        self.SetCombiner(f3d)

        # add tex scroll objects
        if self.tx_scr:
            scr = self.tx_scr
            mat_scr = mat.KCS_tx_scroll
            if hasattr(scr, "textures"):
                [mat_scr.AddTex(t) for t in scr.textures]
            if hasattr(scr, "palettes"):
                [mat_scr.AddPal(t) for t in scr.palettes]

        # deal with custom render modes
        if hasattr(self, "RenderMode"):
            self.SetRenderMode(f3d)
        # g texture handle

        if hasattr(self, "set_tex"):
            # not exactly the same but gets the point across maybe?
            f3d.tex0.tex_set = self.set_tex
            f3d.tex1.tex_set = self.set_tex
            # tex scale gets set to 0 when textures are disabled which is automatically done
            # often to save processing power between mats or something, or just adhoc bhv
            if f3d.rdp_settings.g_tex_gen or any(
                [a < 1 and a > 0 for a in self.tex_scale]
            ):
                f3d.scale_autoprop = False
                f3d.tex_scale = self.tex_scale
                print(self.tex_scale)
            if not self.set_tex:
                # Update node values
                override = bpy.context.copy()
                override["material"] = mat
                bpy.ops.material.update_f3d_nodes(override)
                del override
                return
        # texture 0 then texture 1
        if self.tex0:
            i = self.LoadTexture(0, tex_path, self.tex0)
            tex0 = f3d.tex0
            tex0.tex_reference = str(self.tex0.Timg)  # setting prop for hash purposes
            tex0.tex_set = True
            tex0.tex = i
            tex0.tex_format = self.EvalFmt(self.tiles[0])
            tex0.autoprop = False
            Sflags = self.EvalFlags(self.tiles[0].Sflags)
            for f in Sflags:
                setattr(tex0.S, f, True)
            Tflags = self.EvalFlags(self.tiles[0].Tflags)
            for f in Sflags:
                setattr(tex0.T, f, True)
            tex0.S.low = self.tiles[0].Slow
            tex0.T.low = self.tiles[0].Tlow
            tex0.S.high = self.tiles[0].Shigh
            tex0.T.high = self.tiles[0].Thigh

            tex0.S.mask = self.tiles[0].SMask
            tex0.T.mask = self.tiles[0].TMask
        if self.tex1:
            i = self.LoadTexture(0, tex_path, self.tex1)
            tex1 = f3d.tex1
            tex1.tex_reference = str(self.tex1.Timg)  # setting prop for hash purposes
            tex1.tex_set = True
            tex1.tex = i
            tex1.tex_format = self.EvalFmt(self.tiles[1])
            Sflags = self.EvalFlags(self.tiles[1].Sflags)
            for f in Sflags:
                setattr(tex1.S, f, True)
            Tflags = self.EvalFlags(self.tiles[1].Tflags)
            for f in Sflags:
                setattr(tex1.T, f, True)
            tex1.S.low = self.tiles[1].Slow
            tex1.T.low = self.tiles[1].Tlow
            tex1.S.high = self.tiles[1].Shigh
            tex1.T.high = self.tiles[1].Thigh

            tex1.S.mask = self.tiles[0].SMask
            tex1.T.mask = self.tiles[0].TMask
        # Update node values
        override = bpy.context.copy()
        override["material"] = mat
        bpy.ops.material.update_f3d_nodes(override)
        del override

    def EvalFlags(self, flags):
        if not flags:
            return []
        GBIflags = {
            "G_TX_NOMIRROR": None,
            "G_TX_WRAP": None,
            "G_TX_MIRROR": ("mirror"),
            "G_TX_CLAMP": ("clamp"),
            "0": None,
            "1": ("mirror"),
            "2": ("clamp"),
            "3": ("clamp", "mirror"),
        }
        x = []
        fsplit = flags.split("|")
        for f in fsplit:
            z = GBIflags.get(f.strip(), 0)
            if z:
                x.append(z)
        return x

    # only work with macros I can recognize for now
    def SetRenderMode(self, f3d):
        rdp = f3d.rdp_settings
        rdp.set_rendermode = True
        # if the enum isn't there, then just print an error for now
        try:
            rdp.rendermode_preset_cycle_1 = self.RenderMode[0]
            rdp.rendermode_preset_cycle_2 = self.RenderMode[1]
            # print(f"set render modes with render mode {self.RenderMode}")
        except:
            print(f"could not set render modes with render mode {self.RenderMode}")

    def SetGeoMode(self, rdp, mat):
        # texture gen has a different name than gbi
        for a in self.GeoSet:
            setattr(rdp, a.replace("G_TEXTURE_GEN", "G_TEX_GEN").lower().strip(), True)
        for a in self.GeoClear:
            setattr(rdp, a.replace("G_TEXTURE_GEN", "G_TEX_GEN").lower().strip(), False)

    # Very lazy for now
    def SetCombiner(self, f3d):
        if not hasattr(self, "Combiner"):
            f3d.combiner1.A = "TEXEL0"
            f3d.combiner1.A_alpha = "0"
            f3d.combiner1.C = "SHADE"
            f3d.combiner1.C_alpha = "0"
            f3d.combiner1.D = "0"
            f3d.combiner1.D_alpha = "1"
        else:
            f3d.combiner1.A = self.Combiner[0]
            f3d.combiner1.B = self.Combiner[1]
            f3d.combiner1.C = self.Combiner[2]
            f3d.combiner1.D = self.Combiner[3]
            f3d.combiner1.A_alpha = self.Combiner[4]
            f3d.combiner1.B_alpha = self.Combiner[5]
            f3d.combiner1.C_alpha = self.Combiner[6]
            f3d.combiner1.D_alpha = self.Combiner[7]
            f3d.combiner2.A = self.Combiner[8]
            f3d.combiner2.B = self.Combiner[9]
            f3d.combiner2.C = self.Combiner[10]
            f3d.combiner2.D = self.Combiner[11]
            f3d.combiner2.A_alpha = self.Combiner[12]
            f3d.combiner2.B_alpha = self.Combiner[13]
            f3d.combiner2.C_alpha = self.Combiner[14]
            f3d.combiner2.D_alpha = self.Combiner[15]

    def EvalFmt(self, tex):
        GBIfmts = {
            "G_IM_FMT_RGBA": "RGBA",
            "RGBA": "RGBA",
            "G_IM_FMT_CI": "CI",
            "CI": "CI",
            "G_IM_FMT_IA": "IA",
            "IA": "IA",
            "G_IM_FMT_I": "I",
            "I": "I",
            "0": "RGBA",
            "2": "CI",
            "3": "IA",
            "4": "I",
        }
        GBIsiz = {
            "G_IM_SIZ_4b": "4",
            "G_IM_SIZ_8b": "8",
            "G_IM_SIZ_16b": "16",
            "G_IM_SIZ_32b": "32",
            "0": "4",
            "1": "8",
            "2": "16",
            "3": "32",
        }
        return GBIfmts.get(tex.Fmt, "RGBA") + GBIsiz.get(str(tex.Siz), "16")


class F3d:
    def __init__(self, lastmat=None):
        self.VB = {}
        self.Gfx = {}
        self.diff = {}
        self.amb = {}
        self.Lights = {}
        if not lastmat:
            self.LastMat = Mat()
            self.LastMat.name = 0
        else:
            self.LastMat = lastmat
        self.num = self.LastMat.name  # for debug

    # use tex scroll struct info to get the equivalent dynamic DL, and set the t_scroll flag to true in mat so when getting mats, I can return an array of mats
    def ScrollDynDL(self, Geo, layout, scr_num):
        #        try:
        Tex_Scroll = Geo.tex_scrolls[Geo.tex_header[layout.index]]
        scr = Tex_Scroll.scrolls[Tex_Scroll.scroll_ptrs[scr_num]]
        #        except:
        # if there is a key error, then I figure that the second
        # DL is just inheriting the last scroll (possibly?)
        #            return
        self.LastMat.tx_scr = scr
        flgs = scr.scroll.flags
        # do textures and palettes by taking only the first texture, the rest will have to go into the scroll object
        if flgs & 3:
            Timg = scr.textures[0]
            Fmt = scr.scroll.fmt1
            Siz = scr.scroll.siz1
            loadtex = Texture(Timg, Fmt, Siz)
            loadtex.scr_tex = scr.textures[:-1]
            self.LastMat.loadtex = loadtex
            # if both textures are present, dyn DL loads tex1 (tile 6)
            # this results in both just being the same as far as my
            # export is concerned, but I will replicate DL bhv
            if flgs & 3 == 3:
                self.LastMat.tex1 = loadtex
                self.LastMat.tex1.scr_tex = scr.textures[:-1]
        # if there is both a tex and a palette, then the load TLUT
        # is inside of the dyn DL, otherwise it isn't
        if flgs & 4:
            Timg = scr.palettes[0]
            Fmt = "G_IM_FMT_RGBA"
            Siz = "G_IM_SIZ_16b"
            pal_tex = Texture(Timg, Fmt, Siz)
            pal_tex.scr_pal = scr.palettes[:-1]
            if scr.scroll.flags & 3:
                self.LastMat.pal = pal_tex
            else:
                self.LastMat.loadtex = pal_tex
        # set some color registers
        if flgs & 0x400:
            self.LastMat.env = scr.scroll.env_col
        if flgs & 0x800:
            self.LastMat.blend = scr.scroll.blend_col
        if flgs & 0x1000:
            self.LastMat.light_col[1] = scr.scroll.light1_col
        if flgs & 0x2000:
            self.LastMat.light_col[2] = scr.scroll.light2_col
        # prim is sort of special and set with various flags
        if flgs & 0x18:
            self.LastMat.prim = (0, scr.scroll.primLODFrac, scr.scroll.prim_col)
        # texture scale
        if flgs & 0x80:
            self.LastMat.tex_scale = (scr.scroll.xScale, scr.scroll.yScale)

    # recursively parse the display list in order to return a bunch of model data
    def GetDataFromDL(self, Geo, layout):
        self.VertBuff = [
            0
        ] * 32  # If you're doing some fucky shit with a larger vert buffer it sucks to suck I guess
        self.Tris = []
        self.UVs = []
        self.VCs = []
        self.Verts = []
        self.Mats = []
        self.NewMat = 0
        if hasattr(layout, "DLs"):
            for k in layout.entry:
                DL = layout.DLs[k]
                self.ParseDL(DL, Geo, layout)
        return (self.Verts, self.Tris)

    def ParseDL(self, DL, Geo, layout):
        # This will be the equivalent of a giant switch case
        x = -1
        while x < len(DL):
            # manaual iteration so I can skip certain children efficiently
            x += 1
            (cmd, args) = DL[x]  # each member is a tuple of (cmd, arguments)
            LsW = cmd.startswith
            # Deal with control flow first
            if LsW("gsSPEndDisplayList"):
                return
            # branch and jump dealt with in pre parse
            if LsW("gsSPBranchList"):
                if self.DL_ptr(args[0]):
                    self.ParseDL(layout.DLs[self.DL_ptr(args[0])], Geo, layout)
                else:
                    scr_num = (eval(args[0]) & 0xFFFF) // 8
                    self.ScrollDynDL(Geo, layout, scr_num)
                break
            if LsW("gsSPDisplayList"):
                if self.DL_ptr(args[0]):
                    self.ParseDL(layout.DLs[self.DL_ptr(args[0])], Geo, layout)
                else:
                    scr_num = (eval(args[0]) & 0xFFFF) // 8
                    self.ScrollDynDL(Geo, layout, scr_num)
                continue
            # Vertices are one big list for kirby64, all buffers are combined in pre process
            if LsW("gsSPVertex"):
                # fill virtual buffer
                args = [int(a) for a in args]
                addr = Geo.seg2phys(args[0]) - 32
                start = int(addr // 16)
                for i in range(args[2], args[2] + args[1], 1):
                    self.VertBuff[i] = len(self.Verts) + i - args[2]
                # verts are pre processed
                self.Verts.extend(Geo.vertices.Pos[start : start + args[1]])
                self.UVs.extend(Geo.vertices.UVs[start : start + args[1]])
                self.VCs.extend(Geo.vertices.VCs[start : start + args[1]])
                continue
            # Triangles
            if LsW("gsSP2Triangles"):
                self.MakeNewMat()
                args = [int(a) for a in args]
                Tri1 = self.ParseTri(args[:3])
                Tri2 = self.ParseTri(args[4:7])
                self.Tris.append(Tri1)
                self.Tris.append(Tri2)
                continue
            if LsW("gsSP1Triangle"):
                self.MakeNewMat()
                args = [int(a) for a in args]
                Tri = self.ParseTri(args[:3])
                self.Tris.append(Tri)
                continue
            # materials
            # Mats will be placed sequentially. The first item of the list is the triangle number
            # The second is the material class
            if LsW("gsDPSetRenderMode"):
                self.NewMat = 1
                self.LastMat.RenderMode = [a.strip() for a in args]
                continue
            if LsW("gsDPSetFogColor"):
                self.NewMat = 1
                if not hasattr(self.LastMat, "fog_color"):
                    self.LastMat.fog_color = []
                self.LastMat.fog_color = args
                continue
            if LsW("gsSPFogPosition"):
                self.NewMat = 1
                if not hasattr(self.LastMat, "fog_pos"):
                    self.LastMat.fog_pos = []
                self.LastMat.fog_pos = args
                continue
            if LsW("gsSPLightColor"):
                self.NewMat = 1
                if not hasattr(self.LastMat, "light_col"):
                    self.LastMat.light_col = {}
                num = re.search("_\d", args[0]).group()[1]
                self.LastMat.light_col[num] = args[-1]
                continue
            if LsW("gsDPSetPrimColor"):
                self.NewMat = 1
                self.LastMat.prim = args
                continue
            if LsW("gsDPSetEnvColor"):
                self.NewMat = 1
                self.LastMat.env = args
                continue
            # multiple geo modes can happen in a row that contradict each other
            # this is mostly due to culling wanting diff geo modes than drawing
            # but sometimes using the same vertices
            if LsW("gsSPClearGeometryMode"):
                self.NewMat = 1
                args = [a.strip() for a in args[0].split("|")]
                for a in args:
                    if a in self.LastMat.GeoSet:
                        self.LastMat.GeoSet.remove(a)
                self.LastMat.GeoClear.extend(args)
                continue
            if LsW("gsSPSetGeometryMode"):
                self.NewMat = 1
                args = [a.strip() for a in args[0].split("|")]
                for a in args:
                    if a in self.LastMat.GeoClear:
                        self.LastMat.GeoClear.remove(a)
                self.LastMat.GeoSet.extend(args)
                continue
            if LsW("gsSPGeometryMode"):
                self.NewMat = 1
                argsC = [a.strip() for a in args[0].split("|")]
                argsS = [a.strip() for a in args[1].split("|")]
                for a in argsC:
                    if a in self.LastMat.GeoSet:
                        self.LastMat.GeoSet.remove(a)
                for a in argsS:
                    if a in self.LastMat.GeoClear:
                        self.LastMat.GeoClear.remove(a)
                self.LastMat.GeoClear.extend(argsC)
                self.LastMat.GeoSet.extend(argsS)
                continue
            if LsW("gsDPSetCycleType"):
                if "G_CYC_1CYCLE" in args[0]:
                    self.LastMat.TwoCycle = False
                if "G_CYC_2CYCLE" in args[0]:
                    self.LastMat.TwoCycle = True
                continue
            if LsW("gsDPSetCombineMode"):
                self.NewMat = 1
                self.LastMat.Combiner = self.EvalCombiner(args)
                continue
            if LsW("gsDPSetCombineLERP"):
                self.NewMat = 1
                self.LastMat.Combiner = [a.strip() for a in args]
                continue
            # root tile, scale and set tex
            if LsW("gsSPTexture"):
                self.NewMat = 1
                self.LastMat.set_tex = int(args[-1].strip()) == 2
                self.LastMat.tex_scale = [
                    ((0x10000 * (int(a) < 0)) + int(a)) / 0xFFFF for a in args[0:2]
                ]  # signed half to unsigned half
                self.LastMat.tile_root = int(
                    args[-2]
                )  # I don't think I'll actually use this
                continue
            # last tex is a palette
            if LsW("gsDPLoadTLUT"):
                try:
                    tex = self.LastMat.loadtex
                    self.LastMat.pal = tex
                except:
                    print(
                        "**--Load block before set t img, DL is partial and missing context"
                        "likely static file meant to be used as a piece of a realtime system.\n"
                        "No interpretation on file possible**--"
                    )
                    return None
                continue
            # tells us what tile the last loaded mat goes into
            if LsW("gsDPLoadBlock"):
                try:
                    tex = self.LastMat.loadtex
                    # these values can be used to calc texture size
                    tex.dxt = eval(args[4])
                    tex.texels = eval(args[3])
                    tile = self.EvalTile(args[0])
                    tex.tile = tile
                    if tile == 7:
                        self.LastMat.tex0 = tex
                    elif tile == 6:
                        self.LastMat.tex1 = tex
                except:
                    print(
                        "**--Load block before set t img, DL is partial and missing context"
                        "likely static file meant to be used as a piece of a realtime system.\n"
                        "No interpretation on file possible**--"
                    )
                    return None
                continue
            if LsW("gsDPSetTextureImage"):
                self.NewMat = 1
                Timg = (eval(args[3].strip()) >> 16, eval(args[3].strip()) & 0xFFFF)
                Fmt = args[1].strip()
                Siz = args[2].strip()
                loadtex = Texture(Timg, Fmt, Siz)
                self.LastMat.loadtex = loadtex
                continue
            # catch tile size
            if LsW("gsDPSetTileSize"):
                self.NewMat = 1
                tile = self.LastMat.tiles[self.EvalTile(args[0])]
                tile.Slow = self.EvalImFrac(args[1].strip())
                tile.Tlow = self.EvalImFrac(args[2].strip())
                tile.Shigh = self.EvalImFrac(args[3].strip())
                tile.Thigh = self.EvalImFrac(args[4].strip())
                continue
            if LsW("gsDPSetTile"):
                self.NewMat = 1
                tile = self.LastMat.tiles[self.EvalTile(args[4])]
                tile.Fmt = args[0].strip()
                tile.Siz = args[1].strip()
                tile.Tflags = args[6].strip()
                tile.TMask = int(args[7].strip())
                tile.TShift = int(args[8].strip())
                tile.Sflags = args[9].strip()
                tile.SMask = int(args[10].strip())
                tile.SShift = int(args[11].strip())

    def EvalCombiner(self, arg):
        # two args
        GBI_CC_Macros = {
            "G_CC_PRIMITIVE": ["0", "0", "0", "PRIMITIVE", "0", "0", "0", "PRIMITIVE"],
            "G_CC_SHADE": ["0", "0", "0", "SHADE", "0", "0", "0", "SHADE"],
            "G_CC_MODULATEI": ["TEXEL0", "0", "SHADE", "0", "0", "0", "0", "SHADE"],
            "G_CC_MODULATEIDECALA": [
                "TEXEL0",
                "0",
                "SHADE",
                "0",
                "0",
                "0",
                "0",
                "TEXEL0",
            ],
            "G_CC_MODULATEIFADE": [
                "TEXEL0",
                "0",
                "SHADE",
                "0",
                "0",
                "0",
                "0",
                "ENVIRONMENT",
            ],
            "G_CC_MODULATERGB": ["TEXEL0", "0", "SHADE", "0", "0", "0", "0", "SHADE"],
            "G_CC_MODULATERGBDECALA": [
                "TEXEL0",
                "0",
                "SHADE",
                "0",
                "0",
                "0",
                "0",
                "TEXEL0",
            ],
            "G_CC_MODULATERGBFADE": [
                "TEXEL0",
                "0",
                "SHADE",
                "0",
                "0",
                "0",
                "0",
                "ENVIRONMENT",
            ],
            "G_CC_MODULATEIA": [
                "TEXEL0",
                "0",
                "SHADE",
                "0",
                "TEXEL0",
                "0",
                "SHADE",
                "0",
            ],
            "G_CC_MODULATEIFADEA": [
                "TEXEL0",
                "0",
                "SHADE",
                "0",
                "TEXEL0",
                "0",
                "ENVIRONMENT",
                "0",
            ],
            "G_CC_MODULATEFADE": [
                "TEXEL0",
                "0",
                "SHADE",
                "0",
                "ENVIRONMENT",
                "0",
                "TEXEL0",
                "0",
            ],
            "G_CC_MODULATERGBA": [
                "TEXEL0",
                "0",
                "SHADE",
                "0",
                "TEXEL0",
                "0",
                "SHADE",
                "0",
            ],
            "G_CC_MODULATERGBFADEA": [
                "TEXEL0",
                "0",
                "SHADE",
                "0",
                "ENVIRONMENT",
                "0",
                "TEXEL0",
                "0",
            ],
            "G_CC_MODULATEI_PRIM": [
                "TEXEL0",
                "0",
                "PRIMITIVE",
                "0",
                "0",
                "0",
                "0",
                "PRIMITIVE",
            ],
            "G_CC_MODULATEIA_PRIM": [
                "TEXEL0",
                "0",
                "PRIMITIVE",
                "0",
                "TEXEL0",
                "0",
                "PRIMITIVE",
                "0",
            ],
            "G_CC_MODULATEIDECALA_PRIM": [
                "TEXEL0",
                "0",
                "PRIMITIVE",
                "0",
                "0",
                "0",
                "0",
                "TEXEL0",
            ],
            "G_CC_MODULATERGB_PRIM": [
                "TEXEL0",
                "0",
                "PRIMITIVE",
                "0",
                "TEXEL0",
                "0",
                "PRIMITIVE",
                "0",
            ],
            "G_CC_MODULATERGBA_PRIM": [
                "TEXEL0",
                "0",
                "PRIMITIVE",
                "0",
                "TEXEL0",
                "0",
                "PRIMITIVE",
                "0",
            ],
            "G_CC_MODULATERGBDECALA_PRIM": [
                "TEXEL0",
                "0",
                "PRIMITIVE",
                "0",
                "0",
                "0",
                "0",
                "TEXEL0",
            ],
            "G_CC_FADE": [
                "SHADE",
                "0",
                "ENVIRONMENT",
                "0",
                "SHADE",
                "0",
                "ENVIRONMENT",
                "0",
            ],
            "G_CC_FADEA": [
                "TEXEL0",
                "0",
                "ENVIRONMENT",
                "0",
                "TEXEL0",
                "0",
                "ENVIRONMENT",
                "0",
            ],
            "G_CC_DECALRGB": ["0", "0", "0", "TEXEL0", "0", "0", "0", "SHADE"],
            "G_CC_DECALRGBA": ["0", "0", "0", "TEXEL0", "0", "0", "0", "TEXEL0"],
            "G_CC_DECALFADE": ["0", "0", "0", "TEXEL0", "0", "0", "0", "ENVIRONMENT"],
            "G_CC_DECALFADEA": [
                "0",
                "0",
                "0",
                "TEXEL0",
                "TEXEL0",
                "0",
                "ENVIRONMENT",
                "0",
            ],
            "G_CC_BLENDI": [
                "ENVIRONMENT",
                "SHADE",
                "TEXEL0",
                "SHADE",
                "0",
                "0",
                "0",
                "SHADE",
            ],
            "G_CC_BLENDIA": [
                "ENVIRONMENT",
                "SHADE",
                "TEXEL0",
                "SHADE",
                "TEXEL0",
                "0",
                "SHADE",
                "0",
            ],
            "G_CC_BLENDIDECALA": [
                "ENVIRONMENT",
                "SHADE",
                "TEXEL0",
                "SHADE",
                "0",
                "0",
                "0",
                "TEXEL0",
            ],
            "G_CC_BLENDRGBA": [
                "TEXEL0",
                "SHADE",
                "TEXEL0_ALPHA",
                "SHADE",
                "0",
                "0",
                "0",
                "SHADE",
            ],
            "G_CC_BLENDRGBDECALA": [
                "TEXEL0",
                "SHADE",
                "TEXEL0_ALPHA",
                "SHADE",
                "0",
                "0",
                "0",
                "TEXEL0",
            ],
            "G_CC_BLENDRGBFADEA": [
                "TEXEL0",
                "SHADE",
                "TEXEL0_ALPHA",
                "SHADE",
                "0",
                "0",
                "0",
                "ENVIRONMENT",
            ],
            "G_CC_ADDRGB": ["TEXEL0", "0", "TEXEL0", "SHADE", "0", "0", "0", "SHADE"],
            "G_CC_ADDRGBDECALA": [
                "TEXEL0",
                "0",
                "TEXEL0",
                "SHADE",
                "0",
                "0",
                "0",
                "TEXEL0",
            ],
            "G_CC_ADDRGBFADE": [
                "TEXEL0",
                "0",
                "TEXEL0",
                "SHADE",
                "0",
                "0",
                "0",
                "ENVIRONMENT",
            ],
            "G_CC_REFLECTRGB": [
                "ENVIRONMENT",
                "0",
                "TEXEL0",
                "SHADE",
                "0",
                "0",
                "0",
                "SHADE",
            ],
            "G_CC_REFLECTRGBDECALA": [
                "ENVIRONMENT",
                "0",
                "TEXEL0",
                "SHADE",
                "0",
                "0",
                "0",
                "TEXEL0",
            ],
            "G_CC_HILITERGB": [
                "PRIMITIVE",
                "SHADE",
                "TEXEL0",
                "SHADE",
                "0",
                "0",
                "0",
                "SHADE",
            ],
            "G_CC_HILITERGBA": [
                "PRIMITIVE",
                "SHADE",
                "TEXEL0",
                "SHADE",
                "PRIMITIVE",
                "SHADE",
                "TEXEL0",
                "SHADE",
            ],
            "G_CC_HILITERGBDECALA": [
                "PRIMITIVE",
                "SHADE",
                "TEXEL0",
                "SHADE",
                "0",
                "0",
                "0",
                "TEXEL0",
            ],
            "G_CC_SHADEDECALA": ["0", "0", "0", "SHADE", "0", "0", "0", "TEXEL0"],
            "G_CC_SHADEFADEA": ["0", "0", "0", "SHADE", "0", "0", "0", "ENVIRONMENT"],
            "G_CC_BLENDPE": [
                "PRIMITIVE",
                "ENVIRONMENT",
                "TEXEL0",
                "ENVIRONMENT",
                "TEXEL0",
                "0",
                "SHADE",
                "0",
            ],
            "G_CC_BLENDPEDECALA": [
                "PRIMITIVE",
                "ENVIRONMENT",
                "TEXEL0",
                "ENVIRONMENT",
                "0",
                "0",
                "0",
                "TEXEL0",
            ],
            "_G_CC_BLENDPE": [
                "ENVIRONMENT",
                "PRIMITIVE",
                "TEXEL0",
                "PRIMITIVE",
                "TEXEL0",
                "0",
                "SHADE",
                "0",
            ],
            "_G_CC_BLENDPEDECALA": [
                "ENVIRONMENT",
                "PRIMITIVE",
                "TEXEL0",
                "PRIMITIVE",
                "0",
                "0",
                "0",
                "TEXEL0",
            ],
            "_G_CC_TWOCOLORTEX": [
                "PRIMITIVE",
                "SHADE",
                "TEXEL0",
                "SHADE",
                "0",
                "0",
                "0",
                "SHADE",
            ],
            "_G_CC_SPARSEST": [
                "PRIMITIVE",
                "TEXEL0",
                "LOD_FRACTION",
                "TEXEL0",
                "PRIMITIVE",
                "TEXEL0",
                "LOD_FRACTION",
                "TEXEL0",
            ],
            "G_CC_TEMPLERP": [
                "TEXEL1",
                "TEXEL0",
                "PRIM_LOD_FRAC",
                "TEXEL0",
                "TEXEL1",
                "TEXEL0",
                "PRIM_LOD_FRAC",
                "TEXEL0",
            ],
            "G_CC_TRILERP": [
                "TEXEL1",
                "TEXEL0",
                "LOD_FRACTION",
                "TEXEL0",
                "TEXEL1",
                "TEXEL0",
                "LOD_FRACTION",
                "TEXEL0",
            ],
            "G_CC_INTERFERENCE": [
                "TEXEL0",
                "0",
                "TEXEL1",
                "0",
                "TEXEL0",
                "0",
                "TEXEL1",
                "0",
            ],
            "G_CC_1CYUV2RGB": ["TEXEL0", "K4", "K5", "TEXEL0", "0", "0", "0", "SHADE"],
            "G_CC_YUV2RGB": ["TEXEL1", "K4", "K5", "TEXEL1", "0", "0", "0", "0"],
            "G_CC_PASS2": ["0", "0", "0", "COMBINED", "0", "0", "0", "COMBINED"],
            "G_CC_MODULATEI2": ["COMBINED", "0", "SHADE", "0", "0", "0", "0", "SHADE"],
            "G_CC_MODULATEIA2": [
                "COMBINED",
                "0",
                "SHADE",
                "0",
                "COMBINED",
                "0",
                "SHADE",
                "0",
            ],
            "G_CC_MODULATERGB2": [
                "COMBINED",
                "0",
                "SHADE",
                "0",
                "0",
                "0",
                "0",
                "SHADE",
            ],
            "G_CC_MODULATERGBA2": [
                "COMBINED",
                "0",
                "SHADE",
                "0",
                "COMBINED",
                "0",
                "SHADE",
                "0",
            ],
            "G_CC_MODULATEI_PRIM2": [
                "COMBINED",
                "0",
                "PRIMITIVE",
                "0",
                "0",
                "0",
                "0",
                "PRIMITIVE",
            ],
            "G_CC_MODULATEIA_PRIM2": [
                "COMBINED",
                "0",
                "PRIMITIVE",
                "0",
                "COMBINED",
                "0",
                "PRIMITIVE",
                "0",
            ],
            "G_CC_MODULATERGB_PRIM2": [
                "COMBINED",
                "0",
                "PRIMITIVE",
                "0",
                "0",
                "0",
                "0",
                "PRIMITIVE",
            ],
            "G_CC_MODULATERGBA_PRIM2": [
                "COMBINED",
                "0",
                "PRIMITIVE",
                "0",
                "COMBINED",
                "0",
                "PRIMITIVE",
                "0",
            ],
            "G_CC_DECALRGB2": ["0", "0", "0", "COMBINED", "0", "0", "0", "SHADE"],
            "G_CC_BLENDI2": [
                "ENVIRONMENT",
                "SHADE",
                "COMBINED",
                "SHADE",
                "0",
                "0",
                "0",
                "SHADE",
            ],
            "G_CC_BLENDIA2": [
                "ENVIRONMENT",
                "SHADE",
                "COMBINED",
                "SHADE",
                "COMBINED",
                "0",
                "SHADE",
                "0",
            ],
            "G_CC_CHROMA_KEY2": ["TEXEL0", "CENTER", "SCALE", "0", "0", "0", "0", "0"],
            "G_CC_HILITERGB2": [
                "ENVIRONMENT",
                "COMBINED",
                "TEXEL0",
                "COMBINED",
                "0",
                "0",
                "0",
                "SHADE",
            ],
            "G_CC_HILITERGBA2": [
                "ENVIRONMENT",
                "COMBINED",
                "TEXEL0",
                "COMBINED",
                "ENVIRONMENT",
                "COMBINED",
                "TEXEL0",
                "COMBINED",
            ],
            "G_CC_HILITERGBDECALA2": [
                "ENVIRONMENT",
                "COMBINED",
                "TEXEL0",
                "COMBINED",
                "0",
                "0",
                "0",
                "TEXEL0",
            ],
            "G_CC_HILITERGBPASSA2": [
                "ENVIRONMENT",
                "COMBINED",
                "TEXEL0",
                "COMBINED",
                "0",
                "0",
                "0",
                "COMBINED",
            ],
        }
        return GBI_CC_Macros.get(
            arg[0].strip(), ["TEXEL0", "0", "SHADE", "0", "TEXEL0", "0", "SHADE", "0"]
        ) + GBI_CC_Macros.get(
            arg[1].strip(), ["TEXEL0", "0", "SHADE", "0", "TEXEL0", "0", "SHADE", "0"]
        )

    def DL_ptr(self, num):
        num = int(num)
        if num >> 24 == 0xE:
            return None
        else:
            return num

    def EvalImFrac(self, arg):
        if type(arg) == int:
            return arg
        arg2 = arg.replace("G_TEXTURE_IMAGE_FRAC", "2")
        return eval(arg2)

    def EvalTile(self, arg):
        # are ther more enums??
        Tiles = {
            "G_TX_LOADTILE": 7,
            "G_TX_RENDERTILE": 0,
        }
        t = Tiles.get(arg)
        if t == None:
            t = int(arg)
        return t

    def MakeNewMat(self):
        if self.NewMat:
            self.NewMat = 0
            self.Mats.append([len(self.Tris) - 1, self.LastMat])
            self.LastMat = deepcopy(self.LastMat)  # for safety
            self.LastMat.name = self.num + 1
            self.num += 1

    def ParseTri(self, Tri):
        return [self.VertBuff[a] for a in Tri]

    def StripArgs(self, cmd):
        a = cmd.find("(")
        return cmd[a + 1 : -2].split(",")

    def ApplyDat(self, obj, mesh, tex_path):
        tris = mesh.polygons
        bpy.context.view_layer.objects.active = obj
        ind = -1
        new = -1
        UVmap = obj.data.uv_layers.new(name="UVMap")
        # I can get the available enums for color attrs with this func
        vcol_enums = GetEnums(bpy.types.FloatColorAttribute, "data_type")
        # enums were changed in a blender version, this should future proof it a little
        if "FLOAT_COLOR" in vcol_enums:
            e = "FLOAT_COLOR"
        else:
            e = "COLOR"
        Vcol = obj.data.color_attributes.get("Col")
        if not Vcol:
            Vcol = obj.data.color_attributes.new(name="Col", type=e, domain="CORNER")
        Valph = obj.data.color_attributes.get("Alpha")
        if not Valph:
            Valph = obj.data.color_attributes.new(name="Alpha", type=e, domain="CORNER")
        self.Mats.append([len(tris), 0])
        for i, t in enumerate(tris):
            if i > self.Mats[ind + 1][0]:
                new = self.Create_new_f3d_mat(self.Mats[ind + 1][1], mesh)
                ind += 1
                if not new:
                    new = len(mesh.materials) - 1
                    mat = mesh.materials[new]
                    mat.name = "KCS F3D Mat {} {}".format(obj.name, new)
                    self.Mats[new][1].ApplyMatSettings(mat, tex_path)
                else:
                    # I tried to re use mat slots but it is much slower, and not as accurate
                    # idk if I was just doing it wrong or the search is that much slower, but this is easier
                    mesh.materials.append(new)
                    new = len(mesh.materials) - 1
            # if somehow ther is no material assigned to the triangle or something is lost
            if new != -1:
                t.material_index = new
                # Get texture size or assume 32, 32 otherwise
                i = mesh.materials[new].f3d_mat.tex0.tex
                if not i:
                    WH = (32, 32)
                else:
                    WH = i.size
                # Set UV data and Vertex Color Data
                for v, l in zip(t.vertices, t.loop_indices):
                    uv = self.UVs[v]
                    vcol = self.VCs[v]
                    # scale verts. I just copy/pasted this from kirby tbh Idk
                    UVmap.data[l].uv = [
                        a * (1 / (32 * b)) if b > 0 else a * 0.001 * 32
                        for a, b in zip(uv, WH)
                    ]
                    # idk why this is necessary. N64 thing or something?
                    UVmap.data[l].uv[1] = UVmap.data[l].uv[1] * -1 + 1
                    Vcol.data[l].color = [a / 255 for a in vcol]

    def Create_new_f3d_mat(self, mat, mesh):
        # check if this mat was used already in another mesh (or this mat if DL is garbage or something)
        # even looping n^2 is probably faster than duping 3 mats with blender speed
        for j, F3Dmat in enumerate(bpy.data.materials):
            if F3Dmat.is_f3d:
                dupe = mat.MatHashF3d(F3Dmat.f3d_mat)
                if dupe:
                    return F3Dmat
        if mesh.materials:
            mat = mesh.materials[-1]
            new = mat.id_data.copy()  # make a copy of the data block
            # add a mat slot and add mat to it
            mesh.materials.append(new)
        else:
            bpy.ops.object.create_f3d_mat()  # the newest mat should be in slot[-1] for the mesh materials
        return None


# ------------------------------------------------------------------------
#    Exorter Functions
# ------------------------------------------------------------------------


# ------------------------------------------------------------------------
#    Importer
# ------------------------------------------------------------------------


def ImportColBin(bin_file, context, name):
    LS = bin_file
    LS = open(LS, "rb")
    collection = context.scene.collection
    rt = MakeEmpty(name, "PLAIN_AXES", collection)
    rt.KCS_obj.KCS_obj_type = "Collision"
    LS_Block = misc_bin(LS.read())
    write = bpy_collision(rt, collection)
    write.write_bpy_col(LS_Block, context.scene, context.scene.KCS_scene.Scale)


@time_func
def ImportGeoBin(bin_file, context, name, path):
    Geo = bin_file
    Geo = open(Geo, "rb")
    collection = context.scene.collection
    rt = MakeEmpty(name, "PLAIN_AXES", collection)
    rt.KCS_obj.KCS_obj_type = "Graphics"
    Geo_Block = geo_bin(Geo.read(), context.scene.KCS_scene.Scale)
    write = bpy_geo(rt, collection, context.scene.KCS_scene.Scale)
    write.write_gfx("geo", Geo_Block, path)


def ParseStageTable(world, level, area, path):
    f = open(path, "r")
    lines = f.readlines()
    StageArea = PreProcessC(
        lines, "StageArea", ["{", "}"]
    )  # data type StageArea, delimiter {}

    # There should a ptr to stages, and then a list of stages in the same file
    levels = None
    for k, v in StageArea.items():
        if "*" in k:
            levels = v
            StageArea.pop(k)
            break
    else:
        raise Exception("Could not find level stage table")
    for a, w in enumerate(levels):
        levels = w.split(",")
        if a != world - 1:
            continue
        cnt = 0  # use a counter becuase I don't trust that each line is actually a ptr
        for l in levels:
            start = l.find("&")  # these are ptrs
            end = l.find("]")  # if it returns -1 array slicing still work
            ptr = l[start : end + 1]
            if ptr:
                cnt += 1
                if cnt == level:
                    break
        else:
            raise Exception("could not find level selected")
        break
    # ptr is now going to tell me what var I need
    index = int(ptr[ptr.find("[") + 1 : ptr.find("]")]) + area - 1
    ptr = ptr[1 : ptr.find("[")]

    # I don't check for STAGE_TERMINATOR, so insert that now
    cnt = 0  # when I insert, I increase length by 1
    for i, s in enumerate(StageArea[ptr].copy()):
        if "STAGE_TERMINATOR" in s:
            StageArea[ptr].insert(
                i + cnt, "{{0}, {0), 0, 0, 0, {0}, 0, 0, {0}, {0}, 0}"
            )
            cnt += 1
    try:
        area = StageArea[ptr][int(index)]
    except:
        raise Exception("Could not find area within levels")

    # process the area
    macros = {"LIST_INDEX": BANK_INDEX, "BANK_INDEX": BANK_INDEX}
    stage_dict = {
        "geo": tuple,
        "geo2": tuple,
        "skybox": int,
        "color": int,
        "music": int,
        "level_block": tuple,
        "cutscene": int,
        "level_type": str,
        "dust_block": tuple,
        "dust_image": tuple,
        "name": None,
    }
    area = ProcessStruct(stage_dict, macros, area[area.find("{") + 1 : area.rfind("}")])
    return area


# ------------------------------------------------------------------------
#    Macro Unrollers + common regex
# ------------------------------------------------------------------------


def CurlyBraceRegX():
    return "\{[0-9,a-fx ]+\}"


def ParenRegX():
    return "\([0-9,a-fx ]+\)"


def BANK_INDEX(args):
    return f"{{ {args} }}"


# ------------------------------------------------------------------------
#    Helper Functions
# ------------------------------------------------------------------------


def RotateObj(deg, obj, world=0):
    deg = Euler((math.radians(-deg), 0, 0))
    deg = deg.to_quaternion().to_matrix().to_4x4()
    if world:
        obj.matrix_world = obj.matrix_world @ deg
        obj.select_set(True)
        bpy.context.view_layer.objects.active = obj
        bpy.ops.object.transform_apply(rotation=True)
    else:
        obj.matrix_basis = obj.matrix_basis @ deg


def MakeEmpty(name, type, collection):
    Obj = bpy.data.objects.new(name, None)
    Obj.empty_display_type = type
    collection.objects.link(Obj)
    return Obj


def MakeMeshObj(name, data, collection):
    Obj = bpy.data.objects.new(name, data)
    collection.objects.link(Obj)
    return Obj


def MakeMeshData(name, data):
    dat = bpy.data.meshes.new(name)
    dat.from_pydata(*data)
    dat.validate()
    return dat


# if keep, then it doesn't inherit parent trasnform
def Parent(parent, child, keep=0):
    if not keep:
        child.parent = parent
        child.matrix_local = child.matrix_parent_inverse
    else:
        # idk this is fucked
        child.parent = parent
        child.matrix_world = parent.matrix_world.inverted() * 0.5


# this will take a blender property, its enumprop name, and then return a list of the allowed enums
def GetEnums(prop, enum):
    enumProp = prop.bl_rna.properties.get(enum)
    if enumProp:
        return [item.identifier for item in enumProp.enum_items]


# a struct may have various data types inside of it, so this is here to split it when provided a struct dict
# also has support for macros, so it will unroll them given a list of macro defs, otherwise will return as is

# this will only do one value, use list comp for an array of structs

# struct dict will contain names of the args, and values tell me the delims via regex, recursion using dicts
def ProcessStruct(struct_dict, macros, value):
    # first unroll macros as they have internal commas
    for k, v in macros.items():
        if k in value:
            value = ProcessMacro(value, k, v)
    res = {}
    for k, v in struct_dict.items():
        # if None take the rest of the string as the end
        if not v:
            res[k] = value
            break
        # get the appropriate regex using the type
        if v == dict:
            # not supported for now, but a sub strcut will be found by getting the string between equal number of curly braces
            continue
        if v == tuple:
            regX = f"{CurlyBraceRegX()}\s*,"
        if v == int or v == float or v == str:
            regX = ","
        # search through the line until I hit the delim
        m = re.search(regX, value, flags=re.IGNORECASE)
        if m:
            a = value[: m.span()[1]].strip()
            if v == tuple:
                res[k] = tuple(a[a.find("{") + 1 : a.rfind("}")].split(","))
            if v == str:
                res[k] = a[: a.find(",")]
            if v == int or v == float:
                res[k] = eval(a[: a.find(",")])
            value = value[m.span()[1] :].strip()
        else:
            raise Exception(f"struct parsing failed {value}, attempt {res}")
    return res


# processes a macro and returns its value, not recursive
# line is line, macro is str of macro, process is a function equiv of macro
def ProcessMacro(line, macro, process):
    regX = f"{macro}{ParenRegX()}"
    args = ParenRegX()
    while True:
        m = re.search(regX, line, flags=re.IGNORECASE)
        if not m:
            break
        line = line.replace(
            m.group(),
            process(re.search(args, m.group(), flags=re.IGNORECASE).group()[1:-1]),
        )
    return line


# if there are macros, look for scene defs on macros, currently none, so skip them all
def EvalMacro(line):
    scene = bpy.context.scene
    #    if scene.LevelImp.Version in line:
    #        return False
    #    if scene.LevelImp.Target in line:
    #        return False
    return False


# Given a file of lines, returns a dict of all vars of type 'data_type', and the values are arrays
# chars are the container for types value, () for macros, {} for structs, None for int arrays

# splits data into array using chars, does not recursively do it, only the top level is split
def PreProcessC(lines, data_type, chars):
    # Get a dictionary made up with keys=level script names
    # and values as an array of all the cmds inside.
    Vars = {}
    InlineReg = "/\*((?!\*/).)*\*/"  # remove comments
    dat_name = 0
    skip = 0
    for l in lines:
        comment = l.rfind("//")
        # double slash terminates line basically
        if comment:
            l = l[:comment]
        # check for macro
        if "#ifdef" in l:
            skip = EvalMacro(l)
        if "#elif" in l:
            skip = EvalMacro(l)
        if "#else" in l:
            skip = 0
            continue
        # Now Check for var starts
        regX = "\[[0-9a-fx]*\]"
        match = re.search(regX, l, flags=re.IGNORECASE)
        if data_type in l and re.search(regX, l.lower()) and not skip:
            b = match.span()[0]
            a = l.find(data_type)
            var = l[a + len(data_type) : b].strip()
            Vars[var] = ""
            dat_name = var
            continue
        if dat_name and not skip:
            # remove inline comments from line
            while True:
                m = re.search(InlineReg, l)
                if not m:
                    break
                m = m.span()
                l = l[: m[0]] + l[m[1] :]
            # Check for end of Level Script array
            if "};" in l:
                dat_name = 0
            # Add line to dict
            else:
                Vars[dat_name] += l
    return ProcessLine(Vars, chars)


# given a dict of lines, turns it into a dict with arrays for value of isolated data members
def ProcessLine(Vars, chars):
    for k, v in Vars.items():
        v = v.replace("\n", "")
        arr = []
        x = 0
        stack = 0
        buf = ""
        app = 0
        while x < len(v):
            char = v[x]
            if char == chars[0]:
                stack += 1
                app = 1
            if char == chars[1]:
                stack -= 1
            if app == 1 and stack == 0:
                app = 0
                buf += v[x : x + 2]  # get the last parenthesis and comma
                arr.append(buf.strip())
                x += 2
                buf = ""
                continue
            buf += char
            x += 1
        # for when the control flow characters are nothing
        if buf:
            arr.append(buf)
        Vars[k] = arr
    return Vars


# ------------------------------------------------------------------------
#    UI Callbacks
# ------------------------------------------------------------------------


def UpdateEnt(objprop, context):
    id = objprop.Entity.split(",")
    objprop.BankNum = int(id[0])


# ------------------------------------------------------------------------
#    Scene Properties
# ------------------------------------------------------------------------


class KCS_Scene_Props(PropertyGroup):
    Scale: FloatProperty(
        name="Scale",
        description="Level Scale",
        default=100,
        min=0.0001,
    )
    Decomp_path: StringProperty(
        name="Decomp Folder",
        description="Choose a directory:",
        default="",
        maxlen=1024,
        subtype="DIR_PATH",
    )
    ImpWorld: IntProperty(
        name="World", description="World to place level in", default=1, min=1, max=7
    )
    ImpLevel: IntProperty(
        name="Level",
        description="Which level in selected world to overwrite",
        default=1,
        min=1,
        max=5,
    )
    ImpArea: IntProperty(name="Area", description="Area", default=1, min=1, max=10)
    ImpScale: FloatProperty(
        name="Scale", description="Level Scale", default=100, min=0.0001, max=5000
    )
    ImpBank: IntProperty(
        name="Bank", description="Bank for Non Level Data", default=0, min=0, max=7
    )
    ImpID: IntProperty(
        name="ID", description="ID for Non Level Data", default=1, min=1, max=1200
    )
    Format: EnumProperty(
        name="Format",
        description="The file format for data",
        items=[("binary", "binary", ""), ("C", "C", "")],
    )
    CleanUp: BoolProperty(
        name="Clean Up", description="Post process imports to be cleaner", default=True
    )
    IgnoreAdHoc: BoolProperty(
        name="Ignore Ad Hoc Bhv",
        description="Ignores certain properties that add bloat and are for optimzation, but aren't needed in fast64",
        default=True,
    )


class NodeProp(PropertyGroup):
    EntranceLocation: EnumProperty(
        name="Entrance Location",
        description="Where you start on node after warp",
        items=[
            ("walk start", "walk start", ""),
            ("walk end", "walk end", ""),
            ("appear start", "appear start", ""),
            ("appear end", "appear end", ""),
        ],
    )
    EntranceAction: EnumProperty(
        name="Entrance Action",
        description="Action when entering node after warp",
        items=[
            ("walk", "walk", ""),
            ("stand", "stand", ""),
            ("jump up", "jump up", ""),
            ("jump forward", "jump forward", ""),
            ("climb wall up", "climb wall up", ""),
            ("climb wall down", "climb wall down", ""),
            ("climb rope up", "climb rope up", ""),
            ("climb rope down", "climb rope down", ""),
            ("walking (unk)", "walking (unk)", ""),
            ("jumping (unk)", "jumping (unk)", ""),
            ("fall from air", "fall from air", ""),
        ],
    )
    LockForward: BoolProperty(name="Lock Forward", description="Stop Going Forward")
    LockBackward: BoolProperty(name="Lock Backward", description="Stop Going Backwards")
    EnWarp: BoolProperty(
        name="Enable Warp",
        description="Warps kirby if you walk past warp col type in this node",
    )
    Looping: BoolProperty(name="Looping", description="Used in Boss Stages")
    NodeNum: IntProperty(
        name="Node Number", description="Number of Curr Node", default=1, min=1
    )
    PrevNode: IntProperty(
        name="Prev Node", description="Dest Node When Walking Back", default=1, min=1
    )
    NextNode: IntProperty(
        name="Next Node", description="Dest Node When Walking Forward", default=2, min=1
    )
    Warp: IntVectorProperty(
        name="Warp Dest", description="Area Warp Dest", default=(1, 1, 1), min=1, max=9
    )
    WarpNode: IntProperty(
        name="Dest Node", description="Node of Warp Dest", default=1, min=1
    )


class CamProp(PropertyGroup):
    AxisLocks: BoolVectorProperty(
        name="Locks",
        default=(1, 1, 1),
        description="Stops Cam From Moving",
        subtype="XYZ",
    )
    PanH: BoolProperty(
        name="Pan Horizontal", description="Pans Camera in X/Z plane ahead of kirby"
    )
    PanUpDown: BoolProperty(
        name="Pan Up/Down", description="Pans Camera in Y axis to follow kirby"
    )
    PanDown: BoolProperty(
        name="Pan Down",
        description="Pans Camera in Y axis to follow kirby while falling only",
    )
    ProfileView: BoolProperty(name="ProfileView", description="View Kirby From Side")
    NodeNum: IntProperty(
        name="Node Number", description="Number of Curr Node", default=1, min=1
    )
    CamXBound: FloatVectorProperty(
        name="X",
        description="Follows Kirby in this range, Locks at Bound otherwise",
        max=9999.0,
        min=-9999.0,
        default=(-9999.0, 9999.0),
        size=2,
    )
    CamYBound: FloatVectorProperty(
        name="Y",
        size=2,
        description="Follows Kirby in this range, Locks at Bound otherwise",
        max=9999.0,
        min=-9999.0,
        default=(-9999.0, 9999.0),
    )
    Yaw: FloatVectorProperty(
        name="Yaw",
        description="The Theta Rotation from kirby to cam while following",
        size=2,
        default=(90.0, 90.0),
    )
    Pitch: FloatVectorProperty(
        name="Pitch",
        description="The Phi Rotation from kirby to cam while following",
        size=2,
        default=(120.0, 120.0),
    )
    Radius: FloatVectorProperty(
        name="Radius",
        description="How far the camera is from kirby while following",
        size=2,
        default=(600.0, 600.0),
    )
    Clips: FloatVectorProperty(
        name="Near/Far Clip",
        description="Camera Clip Planes",
        size=2,
        default=(128.0, 12800.0),
    )
    Foc: FloatVectorProperty(
        name="Focus position",
        description="9999 to not use",
        size=3,
        default=(9999.0, 9999.0, 9999.0),
        max=9999.0,
    )
    CamZBound: FloatVectorProperty(
        name="Z",
        description="Follows Kirby in this range, Locks at Bound otherwise",
        size=2,
        default=(-9999.0, 9999.0),
        max=9999.0,
        min=-9999.0,
    )
    CamPitchBound: FloatVectorProperty(
        name="Cam Pitch Bound",
        description="Follows Kirby in this range, Locks at Bound otherwise",
        size=2,
        default=(0.0, 359.0),
        max=359.0,
        min=0.0,
    )
    CamYawBound: FloatVectorProperty(
        name="Cam Yaw Bound",
        description="Follows Kirby in this range, Locks at Bound otherwise",
        size=2,
        default=(10, 170),
        max=359.0,
        min=0.0,
    )


class ColProp(PropertyGroup):
    NormType: IntProperty(
        name="Norm Type", description="Bitwise Normal Type", default=1, min=0
    )
    ColType: IntProperty(
        name="Col Type", description="Collision Type", default=0, min=0
    )
    ColParam: IntProperty(
        name="Col Param/Break Condition",
        description="Collision Param for certain ColTypes",
        default=0,
    )
    WarpNum: IntProperty(name="WarpNum", description="Number of Node warp is on", min=1)


class MeshProp(PropertyGroup):
    MeshType: EnumProperty(
        name="Mesh Type",
        items=[
            ("None", "None", ""),
            ("Collision", "Collision", ""),
            ("Graphics", "Graphics", ""),
        ],
    )
    ColMeshType: EnumProperty(
        name="Type of Mesh",
        description="Type of Col Mesh",
        items=[
            ("Default", "Default", ""),
            ("Water", "Water", ""),
            ("Breakable", "Breakable", ""),
        ],
        default="Default",
    )


class ObjProp(PropertyGroup):
    KCS_obj_type: EnumProperty(
        name="Object Type",
        items=[
            ("None", "None", ""),
            ("Level", "Level", ""),
            ("Graphics", "Graphics", ""),
            ("Collision", "Collision", ""),
            ("Entity", "Entity", ""),
            ("Camera Volume", "Camera Volume", ""),
        ],
    )


class LvlProp(PropertyGroup):
    World: IntProperty(
        name="World", description="World to place level in", default=1, min=1, max=7
    )

    Level: IntProperty(
        name="Level",
        description="Which level in selected world to overwrite",
        default=1,
        min=1,
        max=5,
    )
    Area: IntProperty(name="Area", description="Area", default=1, min=1, max=10)
    Skybox_ID: IntProperty(
        name="Skybox_ID", description="ID of level's skybox", default=13, min=0, max=72
    )

    Music_ID: IntProperty(
        name="Music_ID", description="ID of level's music", default=0, min=0, max=64
    )


# a minimalist texture prop. Only requires a source
class TextureProp(PropertyGroup):
    Bank: IntProperty(
        name="Bank", description="Bank of texture", default=0, min=0, max=7
    )
    Index: IntProperty(
        name="Index", description="Index of texture", default=1, min=1, max=1500
    )

    def draw(self, layout):
        box = layout.box()
        row = box.row()
        row.prop(self, "Bank")
        row.prop(self, "Index")


class TexScrollProp(PropertyGroup):
    Textures: CollectionProperty(name="Textures", type=TextureProp)
    Palettes: CollectionProperty(name="Palettes", type=TextureProp)

    def AddTex(self, tex):
        self.Textures.add()
        tex = self.Textures[-1]
        tex.Source = tex

    def AddPal(self, tex):
        self.Palettes.add()
        tex = self.Palettes[-1]
        tex.Source = tex


class EntProp(PropertyGroup):
    Entity: EnumProperty(
        name="Entity ID",
        description="Name of Entity",
        items=[
            ("0,0", "N-Z", ""),
            ("0,1", "Rocky", ""),
            ("0,2", "Bronto Burt", ""),
            ("0,3", "Skud", ""),
            ("0,4", "Gordo", ""),
            ("0,5", "Shotzo", ""),
            ("0,6", "Spark-i", ""),
            ("0,7", "Bouncy", ""),
            ("0,8", "Glunk", ""),
            ("0,9", "?? explodes]", ""),
            ("0,10", "Chilly", ""),
            ("0,11", "Propeller", ""),
            ("0,12", "Glom", ""),
            ("0,13", "Mahall", ""),
            ("0,14", "Poppy Bros. Jr.", ""),
            ("0,19", "Bivolt", ""),
            ("0,16", "Splinter", ""),
            ("0,17", "Gobblin", ""),
            ("0,18", "Kany", ""),
            ("0,19", "Bivolt again?", ""),
            ("0,20", "Sirkibble", ""),
            ("0,21", "Gabon", ""),
            ("0,22", "Mariel", ""),
            ("0,23", "Large I3", ""),
            ("0,24", "Snipper", ""),
            ("0,25", "?? explodes again?]", ""),
            ("0,26", "Bonehead", ""),
            ("0,27", "Squibbly", ""),
            ("0,28", "Bobo", ""),
            ("0,29", "Bo", ""),
            ("0,30", "Punc", ""),
            ("0,31", "Mite", ""),
            ("0,32", "Sandman", ""),
            ("0,33", "Flopper", ""),
            ("0,34", "Kapar", ""),
            ("0,35", "Maw", ""),
            ("0,36", "Drop", ""),
            ("0,37", "Pedo", ""),
            ("0,38", "Noo", ""),
            ("0,39", "Tick", ""),
            ("0,40", "Cairn", ""),
            ("0,41", "?? invisible]", ""),
            ("0,42", "Pompey", ""),
            ("0,43", "Hack", ""),
            ("0,44", "Burnis", ""),
            ("0,45", "Fishbone", ""),
            ("0,46", "Frigis", ""),
            ("0,47", "Sawyer", ""),
            ("0,48", "Turbite", ""),
            ("0,49", "Plugg", ""),
            ("0,50", "Ghost knight", ""),
            ("0,51", "Zoos", ""),
            ("0,52", "Kakti", ""),
            ("0,53", "Rockn", ""),
            ("0,54", "Chacha", ""),
            ("0,55", "Galbo", ""),
            ("0,56", "Bumber", ""),
            ("0,57", "Scarfy", ""),
            ("0,58", "Nruff", ""),
            ("0,59", "Emp", ""),
            ("0,60", "Magoo", ""),
            ("0,61", "Yariko", ""),
            ("0,62", "invisible?", ""),
            ("0,63", "Wall Shotzo", ""),
            ("0,64", "Keke", ""),
            ("0,65", "Sparky", ""),
            ("0,66", "Ignus", ""),
            ("0,67", "Flora", ""),
            ("0,68", "Putt", ""),
            ("0,69", "Pteran", ""),
            ("0,70", "Mumbies", ""),
            ("0,71", "Pupa", ""),
            ("0,72", "Mopoo", ""),
            ("0,73", "Zebon", ""),
            ("0,74", "invisible?]", ""),
            ("0,75", "falling rocks sometimes blue]", ""),
            ("0,76", "falling rocks sometimes blue bigger?]", ""),
            ("1,0", "Waddle Dee Boss", ""),
            ("1,1", "Ado Boss", ""),
            ("1,2", "DeeDeeDee Boss", ""),
            ("2,0", "Whispy Woods", ""),
            ("2,1", "Waddle Dee Boss)", ""),
            ("3,0", "Maxim Tomato", ""),
            ("3,1", "Sandwich", ""),
            ("3,2", "Cake", ""),
            ("3,3", "Steak", ""),
            ("3,4", "Ice Cream Bar", ""),
            ("3,5", "Invinsible Candy", ""),
            ("3,6", "Yellow Star", ""),
            ("3,7", "Blue Star", ""),
            ("3,10", "crashes]", ""),
            ("3,9", "1up", ""),
            ("3,11", "Flower", ""),
            ("3,12", "School of fish", ""),
            ("3,13", "Butterfly", ""),
            ("5,0", "warps", ""),
            ("5,31", "Door", ""),
            ("5,32", "Door 2", ""),
            ("7,1", "Ado (Gives maxim tomato)", ""),
            ("8,0", "N-Z Boss", ""),
            ("8,1", "Bouncy Boss", ""),
            ("8,2", "Kakti Boss", ""),
            ("8,3", "?", ""),
            ("8,4", "Spark-i Boss", ""),
            ("8,5", "Tick Boss", ""),
            ("8,6", "Kany Boss", ""),
            ("8,7", "Kapar Boss", ""),
            ("8,8", "Blowfish boss", ""),
            ("8,9", "Galbo boss", ""),
            ("8,10", "drop monster room", ""),
            ("8,15", "Sawyer Boss", ""),
        ],
        update=UpdateEnt,
    )
    NodeNum: IntProperty(
        name="NodeNum",
        description="The node that this entity spawns on",
        default=1,
        min=1,
    )
    BankNum: IntProperty(
        name="BankNum", description="The bank the entity is from", default=0, min=0
    )
    IndexNum: IntProperty(
        name="IndexNum", description="The index the entity is from", default=0, min=0
    )
    Action: IntProperty(
        name="Action",
        description="The action of this specific entity",
        default=0,
        min=0,
    )
    Flags: IntProperty(
        name="Flags",
        description="Flags for spawning or other conditions",
        default=0,
        min=0,
        max=255,
    )
    Respawn: IntProperty(name="Respawn", description="Respawn after killing", default=0)
    Eep: IntProperty(
        name="Eep", description="An eep flag to check, if true spawn", default=0, min=0
    )


# ------------------------------------------------------------------------
#    IO Operators
# ------------------------------------------------------------------------


class KCS_OT_Import_Col(Operator):
    bl_label = "Import Col Data"
    bl_idname = "kcs.import_col"

    def execute(self, context):
        scene = context.scene.KCS_scene
        file = (
            Path(scene.Decomp_path)
            / "assets"
            / "misc"
            / ("bank_%d" % scene.ImpBank)
            / ("%d" % scene.ImpID)
        )
        if scene.Format == "binary":
            name = file / "level.bin"
            if name.exists():
                ImportColBin(name, context, f"KCS Col {scene.ImpBank}-{scene.ImpID}")
            else:
                name = file / "misc.bin"
                if name.exists():
                    ImportColBin(
                        name, context, f"KCS Col {scene.ImpBank}-{scene.ImpID}"
                    )
                else:
                    raise Exception(
                        f"Could not find file {name}, misc Bank/ID selected is not a level"
                    )
        else:
            raise Exception("C importing is not supported yet")
        return {"FINISHED"}


class KCS_OT_Export(Operator):
    bl_label = "Export Area"
    bl_idname = "kcs.export_area"

    def execute(self, context):
        return {"FINISHED"}


class KCS_OT_Export_Gfx(Operator):
    bl_label = "Export Gfx"
    bl_idname = "kcs.export_gfx"

    def execute(self, context):
        return {"FINISHED"}


class KCS_OT_Import_Stage(Operator):
    bl_label = "Import Stage"
    bl_idname = "kcs.import_stage"

    def execute(self, context):
        scene = context.scene.KCS_scene
        stage_table = (
            Path(scene.Decomp_path) / "data" / "misc" / "kirby.066630.2.c"
        )  # this will probably change later
        stage = ParseStageTable(
            scene.ImpWorld, scene.ImpLevel, scene.ImpArea, stage_table
        )

        gfx_bank, gfx_ID = [eval(a) for a in stage["geo"]]
        col_bank, col_ID = [eval(a) for a in stage["level_block"]]

        file_gfx = (
            Path(scene.Decomp_path)
            / "assets"
            / "geo"
            / ("bank_%d" % gfx_bank)
            / ("%d" % gfx_ID)
        )
        file_col = (
            Path(scene.Decomp_path)
            / "assets"
            / "misc"
            / ("bank_%d" % col_bank)
            / ("%d" % col_ID)
        )
        if scene.Format == "binary":
            # import gfx
            name = file_gfx / "geo.bin"
            if name.exists():
                ImportGeoBin(
                    name,
                    context,
                    f"KCS Level {scene.ImpWorld}-{scene.ImpLevel}-{scene.ImpArea}",
                    Path(scene.Decomp_path) / "assets" / "image",
                )
            else:
                raise Exception(
                    f"Could not find file {name}, geo Bank/ID does not exist"
                )
            # import collision
            name = file_col / "level.bin"
            if name.exists():
                ImportColBin(name, context, f"KCS Col {scene.ImpBank}-{scene.ImpID}")
            else:
                name = file_col / "misc.bin"
                if name.exists():
                    ImportColBin(
                        name, context, f"KCS Col {scene.ImpBank}-{scene.ImpID}"
                    )
                else:
                    raise Exception(
                        f"Could not find file {name}, misc Bank/ID selected is not a level"
                    )
        else:
            raise Exception("C importing is not supported yet")
        return {"FINISHED"}


class KCS_OT_Import_NLD_Gfx(Operator):
    bl_label = "Import Gfx Data"
    bl_idname = "kcs.import_nld_gfx"

    def execute(self, context):
        scene = context.scene.KCS_scene
        file = (
            Path(scene.Decomp_path)
            / "assets"
            / "geo"
            / ("bank_%d" % scene.ImpBank)
            / ("%d" % scene.ImpID)
        )
        if scene.Format == "binary":
            name = file / "geo.bin"
            if name.exists():
                ImportGeoBin(
                    name,
                    context,
                    f"KCS Gfx {scene.ImpBank}-{scene.ImpID}",
                    Path(scene.Decomp_path) / "assets" / "image",
                )
            else:
                name = file / "block.bin"
                if name.exists():
                    ImportGeoBin(
                        name,
                        context,
                        f"KCS Gfx {scene.ImpBank}-{scene.ImpID}",
                        Path(scene.Decomp_path) / "assets" / "image",
                    )
                else:
                    raise Exception(
                        f"Could not find file {name}, geo Bank/ID does not exist"
                    )
        else:
            raise Exception("C importing is not supported yet")
        return {"FINISHED"}


# ------------------------------------------------------------------------
#    Helper Operators
# ------------------------------------------------------------------------

# allow calling of raw functions for scripting to go faster
# operators are for the UI


def AddNode(Rt, collection):
    # Make Node
    PathData = bpy.data.curves.new("KCS Path Node", "CURVE")
    PathData.splines.new("POLY")
    PathData.splines[0].points.add(4)
    for i, s in enumerate(PathData.splines[0].points):
        s.co = (i - 2, 0, 0, 0)
    Node = bpy.data.objects.new("KCS Node", PathData)
    collection.objects.link(Node)
    Parent(Rt, Node, 0)
    # make camera
    CamDat = bpy.data.cameras.new("KCS Node Cam")
    CamObj = bpy.data.objects.new("KCS Node Cam", CamDat)
    collection.objects.link(CamObj)
    Parent(Node, CamObj, 0)
    # Make Camera Volume
    Vol = MakeEmpty("KCS Cam Volume", "CUBE", collection)
    Vol.KCS_obj.KCS_obj_type = "Camera Volume"
    Parent(CamObj, Vol, 0)
    Rt.select_set(True)


# cube has side length of 40
class KCS_OT_Add_Block(Operator):
    bl_label = "Add Breakable Block"
    bl_idname = "kcs.add_kcsblock"

    def execute(self, context):
        # context vars
        scale = bpy.context.scene.KCS_scene.Scale
        Rt = context.object
        collection = context.object.users_collection[0]
        # get singleton instance of block data
        Block = BreakableBlockDat()
        BlockGfxDat = Block.Instance_Gfx(Block)
        BlockColDat = Block.Instance_Col(Block)
        BlockGfxObj = bpy.data.objects.new("KCS Block Gfx", BlockGfxDat)
        BlockColObj = bpy.data.objects.new("KCS Block Col", BlockColDat)

        # link parent and transform
        collection.objects.link(BlockGfxObj)
        Parent(Rt, BlockGfxObj, 0)
        BlockGfxObj.matrix_world *= scale
        BlockGfxObj.KCS_mesh.MeshType = "Graphics"

        collection.objects.link(BlockColObj)
        Parent(BlockGfxObj, BlockColObj, 0)
        BlockColObj.matrix_world *= scale
        BlockColObj.KCS_mesh.MeshType = "Collision"
        BlockColObj.KCS_mesh.ColMeshType = "Breakable"

        # setup mats
        Block.Instance_Mat_Gfx(Block, BlockGfxObj)
        Block.Instance_Mat_Col(Block, BlockColObj)

        Rt.select_set(True)
        bpy.context.view_layer.objects.active = Rt
        return {"FINISHED"}


class KCS_OT_Add_Level(Operator):
    bl_label = "Add Level Empty"
    bl_idname = "kcs.add_kcslevel"

    def execute(self, context):
        collection = bpy.context.scene.collection
        Lvl = MakeEmpty("KCS Level Rt", "PLAIN_AXES", collection)
        Lvl.KCS_obj.KCS_obj_type = "Level"
        Col = MakeEmpty("KCS Level Col", "PLAIN_AXES", collection)
        Col.KCS_obj.KCS_obj_type = "Collision"
        Parent(Lvl, Col, 0)
        Gfx = MakeEmpty("KCS Level Gfx", "PLAIN_AXES", collection)
        Gfx.KCS_obj.KCS_obj_type = "Graphics"
        Parent(Lvl, Gfx, 0)
        # Make Node
        PathData = bpy.data.curves.new("KCS Path Node", "CURVE")
        PathData.splines.new("POLY")
        PathData.splines[0].points.add(4)
        for i, s in enumerate(PathData.splines[0].points):
            s.co = (i - 2, 0, 0, 0)
        Node = bpy.data.objects.new("KCS Node", PathData)
        collection.objects.link(Node)
        Parent(Col, Node, 0)
        # make camera
        CamDat = bpy.data.cameras.new("KCS Node Cam")
        CamObj = bpy.data.objects.new("KCS Node Cam", CamDat)
        collection.objects.link(CamObj)
        Parent(Node, CamObj, 0)
        # Make Camera Volume
        Vol = MakeEmpty("KCS Cam Volume", "CUBE", collection)
        Vol.KCS_obj.KCS_obj_type = "Camera Volume"
        Parent(CamObj, Vol, 0)
        Lvl.select_set(True)
        return {"FINISHED"}


class KCS_OT_Add_Node(Operator):
    bl_label = "Add Node"
    bl_idname = "kcs.add_kcsnode"

    def execute(self, context):
        Rt = context.object
        collection = context.object.users_collection[0]
        AddNode(Rt, collection)
        return {"FINISHED"}


class KCS_OT_Add_Ent(Operator):
    bl_label = "Add Entity"
    bl_idname = "kcs.add_kcsent"

    def execute(self, context):
        node = context.object.data.KCS_node
        obj = bpy.data.objects.new("Entity %d" % node.NodeNum, None)
        collection = context.object.users_collection[0]
        collection.objects.link(obj)
        obj.KCS_obj.KCS_obj_type = "Entity"
        Parent(context.object, obj, 0)
        obj.location = (
            context.object.data.splines[0].points[0].co.xyz + context.object.location
        )
        return {"FINISHED"}


class KCS_OT_Add_Tex(Operator):
    bl_label = "Add Texture"
    bl_idname = "kcs.add_tex"

    def execute(self, context):
        mat = context.material
        scr = mat.KCS_tx_scroll
        scr.Textures.add()
        return {"FINISHED"}


class KCS_OT_Add_Pal(Operator):
    bl_label = "Add Palette"
    bl_idname = "kcs.add_pal"

    def execute(self, context):
        mat = context.material
        scr = mat.KCS_tx_scroll
        scr.Palettes.add()
        return {"FINISHED"}


# ------------------------------------------------------------------------
#    Panels
# ------------------------------------------------------------------------


class KCS_PROP_PT_Panel(Panel):
    bl_label = "KCS Props"
    bl_idname = "KCS_PROP_PT_Panel"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Kirby64"
    bl_context = "objectmode"

    @classmethod
    def poll(self, context):
        return context.scene is not None

    def draw(self, context):
        layout = self.layout
        KCS_scene = context.scene.KCS_scene
        layout.prop(KCS_scene, "Scale")
        layout.prop(KCS_scene, "Decomp_path")
        layout.operator("kcs.add_kcslevel")
        layout.prop(KCS_scene, "Format")


class KCS_IO_PT_Panel(Panel):
    bl_label = "KCS I/O"
    bl_idname = "KCS_IO_PT_Panel"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Kirby64"
    bl_context = "objectmode"

    @classmethod
    def poll(self, context):
        return context.scene is not None

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        KCS_scene = scene.KCS_scene
        layout.label(text="Export Selected Level")
        layout.operator("kcs.export_area")
        layout.label(text="Export Selected Geo")
        layout.operator("kcs.export_gfx")
        layout.separator()
        layout.label(text="Import Options")
        row = layout.row()
        row.prop(KCS_scene, "CleanUp")
        #        row.prop(KCS_scene, "IgnoreAdHoc")
        layout.label(text="Import Area")
        layout.prop(KCS_scene, "ImpWorld")
        layout.prop(KCS_scene, "ImpLevel")
        layout.prop(KCS_scene, "ImpArea")
        layout.operator("kcs.import_stage")
        layout.separator()
        layout.label(text="Import Bank Data")
        layout.prop(KCS_scene, "ImpBank")
        layout.prop(KCS_scene, "ImpID")
        layout.operator("kcs.import_nld_gfx")
        layout.operator("kcs.import_col")


class SCROLL_PT_Panel(Panel):
    bl_label = "KCS Tx Scroll Settings"
    bl_idname = "SCROLL_PT_Panel"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    bl_options = {"HIDE_HEADER"}

    @classmethod
    def poll(self, context):
        if context.object.type != "MESH" or context.scene.gameEditorMode != "KCS":
            return None
        return context.material is not None

    def draw(self, context):
        KCSmesh = context.object.KCS_mesh
        if KCSmesh.MeshType == "Graphics":
            layout = self.layout
            mat = context.material
            scroll = mat.KCS_tx_scroll
            box = layout.box()
            box.label(text="KCS Texture Scroll Properties")
            box_tex = box.box()
            box_tex.label(text="textures")
            box_tex.operator("kcs.add_tex")
            [t.draw(box_tex) for t in scroll.Textures]
            box_tex = box.box()
            box_tex.label(text="palettes")
            box_tex.operator("kcs.add_pal")
            [t.draw(box_tex) for t in scroll.Palettes]


class COL_PT_Panel(Panel):
    bl_label = "KCS Col Settings"
    bl_idname = "COL_PT_Panel"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    bl_options = {"HIDE_HEADER"}

    @classmethod
    def poll(self, context):
        if context.object.type != "MESH" or context.scene.gameEditorMode != "KCS":
            return None
        return context.material is not None

    def draw(self, context):
        KCSmesh = context.object.KCS_mesh
        if KCSmesh.MeshType == "Collision":
            layout = self.layout
            mat = context.material
            col = mat.KCS_col
            box = layout.box()
            box.label(text="KCS Collision Type Info")
            box.prop(col, "NormType")
            if KCSmesh.ColMeshType == "Breakable":
                box.label(text="Collision Type must be brekaable (9)")
            box.prop(col, "ColType")
            box.prop(col, "ColParam")
            box.prop(col, "WarpNum")


class NODE_PT_Panel(Panel):
    bl_label = "KCS Node Settings"
    bl_idname = "NODE_PT_Panel"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "object"
    bl_options = {"HIDE_HEADER"}

    @classmethod
    def poll(self, context):
        if context.object.type != "CURVE" or context.scene.gameEditorMode != "KCS":
            return None
        return context.object.data is not None

    def draw(self, context):
        layout = self.layout
        obj = context.object.data
        nodeprop = obj.KCS_node
        box = layout.box()
        box.label(text="KCS Node Path/Container")

        b = box.box()
        b.label(text="Create Entity Linked to this Node")
        b.operator("kcs.add_kcsent")

        box.separator()
        row = box.row()
        row.prop(nodeprop, "NodeNum")
        row.prop(nodeprop, "EnWarp")

        row = box.row()
        row.label(text="Warp Settings")
        col = row.column()
        col.alignment = "LEFT"
        col.prop(nodeprop, "EntranceLocation")
        col.prop(nodeprop, "EntranceAction")

        row = box.row()
        row.prop(nodeprop, "Warp")
        row.prop(nodeprop, "WarpNode")

        box.separator()
        row = box.row()
        row.prop(nodeprop, "LockForward")
        row.prop(nodeprop, "LockBackward")
        row.prop(nodeprop, "Looping")

        box.separator()
        row = box.row()
        row.prop(nodeprop, "NextNode")
        row.prop(nodeprop, "PrevNode")


class CAM_PT_Panel(Panel):
    bl_label = "KCS Cam Settings"
    bl_idname = "CAM_PT_Panel"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "object"
    bl_options = {"HIDE_HEADER"}

    @classmethod
    def poll(self, context):
        if context.object.type != "CAMERA" or context.scene.gameEditorMode != "KCS":
            return None
        return context.object.data is not None

    def draw(self, context):
        layout = self.layout
        obj = context.object.data
        camprop = obj.KCS_cam
        box = layout.box()
        box.label(
            text="Locks will make camera stay inside cam bounds while following kirby."
        )
        row = box.row()
        row.prop(camprop, "ProfileView")
        row.prop(camprop, "AxisLocks")
        box.separator()
        box.label(text="Pans begin when camera hits bounds.")
        row = box.row()
        row.prop(camprop, "PanH")
        row.prop(camprop, "PanUpDown")
        row.prop(camprop, "PanDown")
        box.separator()
        box.label(text="Camera position while following kirby.")
        grid = box.column_flow()
        grid.prop(camprop, "Yaw")
        grid.prop(camprop, "Pitch")
        grid.prop(camprop, "Radius")
        grid.prop(camprop, "Clips")
        box.separator()
        box.label(text="Position to focus camera. 9999 focuses on kirby.")
        box.separator()
        row = box.row()
        row.prop(camprop, "Foc")
        box.label(text="Camera Bound Pairs Determined by Camera Volume.")
        box.label(text="Bounds only used while axis is locked.")
        box.prop(camprop, "CamPitchBound")
        box.prop(camprop, "CamYawBound")


class OBJ_PT_Panel(Panel):
    bl_label = "KCS Obj Panel"
    bl_idname = "OBJ_PT_Panel"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "object"
    bl_options = {"HIDE_HEADER"}

    @classmethod
    def poll(self, context):
        if context.object.type != "EMPTY" or context.scene.gameEditorMode != "KCS":
            return None
        return context.object is not None

    def draw(self, context):
        layout = self.layout
        obj = context.object
        objprop = obj.KCS_obj
        layout.prop(context.scene, "gameEditorMode")
        box = self.layout.box().column()
        box.box().label(text="KCS Object Handler")
        box.prop(objprop, "KCS_obj_type")
        if objprop.KCS_obj_type == "Entity":
            draw_Ent_empty(box, context)
        elif objprop.KCS_obj_type == "Level":
            draw_Lvl_empty(box, context)
        elif objprop.KCS_obj_type == "Graphics":
            draw_Gfx_empty(box, context)
        elif objprop.KCS_obj_type == "Collision":
            draw_Col_empty(box, context)
        elif objprop.KCS_obj_type == "Camera Volume":
            box = box.box()
            box.label(text="KCS Camera Bounds Container")
            box.label(text="Make this object the child of a camera node.")
            box.label(text="This object should not be rotated.")
            box.label(
                text="The volume contained by this object represents the x/y/z bounds of camera movement."
            )
        else:
            box.separator()
            box = box.box()
            box.label(text="This obj will be ignored")


def draw_Ent_empty(box, context):
    obj = context.object
    entprop = obj.KCS_ent
    box.separator()
    box = box.box()
    box.label(text="KCS Entity Properties")
    box.prop(entprop, "Entity")
    box.separator()

    row = box.row()
    row.prop(entprop, "BankNum")
    row.prop(entprop, "IndexNum")
    box.prop(entprop, "Action")
    box.separator()

    row = box.row()
    row.prop(entprop, "Flags")
    row.prop(entprop, "Respawn")
    row.prop(entprop, "Eep")


def draw_Lvl_empty(box, context):
    obj = context.object
    lvlprop = obj.KCS_lvl
    box.separator()
    box = box.box()
    box.label(text="KCS Level Properties")
    box.prop(lvlprop, "World")
    box.prop(lvlprop, "Level")
    box.prop(lvlprop, "Area")
    box.prop(lvlprop, "Skybox_ID")
    box.prop(lvlprop, "Music_ID")


def draw_Gfx_empty(box, context):
    box = box.box()
    box.label(text="KCS Level Gfx Container")
    box.label(text="Make this object the child of the level empty.")
    box.label(text="Make all gfx meshes children of this empty.")
    box.operator("kcs.add_kcsblock")


def draw_Col_empty(box, context):
    box = box.box()
    box.label(text="KCS Level Col Container")
    box.label(text="Make this object the child of the level empty.")
    box.label(text="Make all Col meshes children of this empty.")
    box.operator("kcs.add_kcsnode")


class MESH_PT_Panel(Panel):
    bl_label = "KCS Mesh Panel"
    bl_idname = "MESH_PT_Panel"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "object"
    bl_options = {"HIDE_HEADER"}

    @classmethod
    def poll(self, context):
        if context.object.type != "MESH" or context.scene.gameEditorMode != "KCS":
            return None
        return context.object is not None

    def draw(self, context):
        layout = self.layout
        obj = context.object
        meshprop = obj.KCS_mesh
        layout.prop(context.scene, "gameEditorMode")
        box = self.layout.box().column()
        box.prop(meshprop, "MeshType")
        if meshprop.MeshType == "Gfx":
            draw_Gfx(box, context)
        elif meshprop.MeshType == "Collision":
            draw_Col(box, context)
        else:
            draw_Gfx(box, context)


def draw_Col(box, context):
    obj = context.object
    colprop = obj.KCS_mesh
    box.separator()
    box = box.box()
    box.label(text="KCS Col Properties")
    row = box.row()
    row.prop(colprop, "ColMeshType", expand=True)
    if colprop.MeshType == "Default":
        box.label(text="Make mesh a child of level collision empty.")
        box.label(text="Use materials to select different collision types.")
    elif colprop.MeshType == "Water":
        box.label(text="Make mesh a child of level collision empty.")
        box.label(
            text="Water is formed by the inner surface of planes and has no collision types."
        )
    elif colprop.MeshType == "Breakable":
        box.label(text="Make mesh a child of linked graphics mesh.")
        box.label(text="Must use only one material with collision type breakable.")


def draw_Gfx(box, context):
    box = box.box()
    box.label(text="KCS Gfx Mesh")
    box.label(
        text="Make mesh a child of level graphics empty, or child of another gfx mesh."
    )


# ------------------------------------------------------------------------
#    Registration
# ------------------------------------------------------------------------

classes = (
    KCS_Scene_Props,
    NodeProp,
    CamProp,
    ObjProp,
    LvlProp,
    EntProp,
    MeshProp,
    ColProp,
    TextureProp,
    TexScrollProp,
    KCS_OT_Export,
    KCS_OT_Export_Gfx,
    KCS_OT_Import_Stage,
    KCS_OT_Add_Level,
    KCS_OT_Add_Block,
    KCS_OT_Add_Ent,
    KCS_OT_Add_Node,
    KCS_OT_Import_NLD_Gfx,
    KCS_OT_Import_Col,
    KCS_OT_Add_Tex,
    KCS_OT_Add_Pal,
    KCS_PROP_PT_Panel,
    KCS_IO_PT_Panel,
    OBJ_PT_Panel,
    MESH_PT_Panel,
    NODE_PT_Panel,
    CAM_PT_Panel,
    COL_PT_Panel,
    SCROLL_PT_Panel,
)


def register():
    from bpy.utils import register_class

    for cls in classes:
        register_class(cls)

    bpy.types.Scene.KCS_scene = PointerProperty(type=KCS_Scene_Props)
    bpy.types.Curve.KCS_node = PointerProperty(type=NodeProp)
    bpy.types.Camera.KCS_cam = PointerProperty(type=CamProp)
    bpy.types.Object.KCS_lvl = PointerProperty(type=LvlProp)
    bpy.types.Object.KCS_ent = PointerProperty(type=EntProp)
    bpy.types.Object.KCS_obj = PointerProperty(type=ObjProp)
    bpy.types.Object.KCS_mesh = PointerProperty(type=MeshProp)
    bpy.types.Material.KCS_col = PointerProperty(type=ColProp)
    bpy.types.Material.KCS_tx_scroll = PointerProperty(type=TexScrollProp)


def unregister():
    from bpy.utils import unregister_class

    for cls in reversed(classes):
        unregister_class(cls)
    del bpy.types.Scene.my_tool


if __name__ == "__main__":
    register()
