from PIL import Image
import os

for file in os.scandir(os.getcwd()):
    if not "bmp" in file.name:
        continue
    im = Image.open(file.name)

    crop_im = im.crop((6, 9, 306, 181))
    im.close()
    crop_im.save(file.name)