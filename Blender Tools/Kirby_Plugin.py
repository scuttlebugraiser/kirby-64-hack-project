# ------------------------------------------------------------------------
#    Header
# ------------------------------------------------------------------------

import bpy

from bpy.props import (
    StringProperty,
    BoolProperty,
    IntProperty,
    FloatProperty,
    FloatVectorProperty,
    EnumProperty,
    PointerProperty,
    IntVectorProperty,
    BoolVectorProperty,
)
from bpy.types import (
    Panel,
    Menu,
    Operator,
    PropertyGroup,
)
from array import array
import os
from struct import *
import sys
import math
import importlib
from shutil import copy
from pathlib import Path
from types import ModuleType
from mathutils import Vector
from mathutils import Euler

bl_info = {
    "name": "KCS Importer/Exporter",
    "description": "Import&Export levels for Kirby Crystal Shards",
    "author": "scuttlebug_raiser",
    "version": (1, 0, 0),
    "blender": (2, 81, 0),
    "location": "3D View > Tools",
    "warning": "",  # used for warning icon and text in addons panel
    "wiki_url": "",
    "tracker_url": "",
    "category": "Import-Export",
}

# ------------------------------------------------------------------------
#    Classes
# ------------------------------------------------------------------------


class LS:
    def __init__(self):
        self.Main_Header = {0x0: 0x0, 0x4: 0x0, 0x8: 0x0, 0xC: 0x0}
        self.Vertices = [(0x270F, 0x270F, 0x270F)]
        self.Triangles = [((0x0, 0x1, 0x2), 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9)]
        self.Normals = [(-1.000000, -2.000000, -3.000000, -4.000000)]
        self.Triangle_Cells = [(0x8192,)]
        self.Normal_Cells = [(0, 1, 2, 3)]
        # just doing this because I don't feel like making destructables yet
        self.Destructable_Groups = [[0, 0, 0]]
        self.Destructable_Indices = [[]]
        self.Collision_Header = {
            0x0: 0x0,
            0x4: 0x0,
            0x8: 0x0,
            0xC: 0x0,
            0x10: 0x0,
            0x14: 0x0,
            0x18: 0x0,
            0x1C: 0x0,
            0x20: 0x0,
            0x24: 0x0,
            0x28: 0x0,
            0x2C: 0x0,
            0x30: 0x0,
            0x34: 0x0,
            0x38: 0x0,
            0x3C: 0x0,
            0x40: 0x0,
        }
        self.Entity_List_End = 0x99999999
        self.Node = node()

    def MakeWater(self):
        self.Water_Normals = [(1.000000, 2.000000, 3.000000, 4.000000)]
        self.Water_Data = []

    def file(self, name):
        self.file = open(name, "w")

    def write(self):
        self.labels = [a for a in self.__dict__ if a is not "file"]
        self.labels.remove("Node")
        self.attrs = [getattr(self, a) for a in self.labels]
        self.labels2 = [a for a in self.Node.__dict__]
        self.attrs.extend([getattr(self.Node, a) for a in self.labels2])
        self.labels.extend(self.labels2)
        [
            self.file.write(a[0] + " = " + str(a[1]) + "\n")
            for a in zip(self.labels, self.attrs)
        ]
        self.file.close()

    def WriteCol(self, obj, Voff):
        # add vertices
        self.AddVerts(obj)
        # add triangles/tri cells, normals/norm cells in one big loop
        tris = obj.data.loop_triangles
        mats = obj.data.materials
        Voff = self.AddTris(tris, mats, obj, Voff)
        return Voff

    def AddVerts(self, obj):
        verts = obj.data.vertices
        for v in verts:
            self.Vertices.append(
                tuple([Dec2Compliment(int(round(a, 0))) for a in v.co])
            )

    def AddTris(self, tris, mats, obj, Voff):
        PIs = [-1]
        for qr, t in enumerate(tris):
            mat = mats[t.material_index]
            verts = list(t.vertices)
            loops = t.loops
            normal = list(t.normal)
            center = t.center
            NormType = mat.colprop.NormType
            ColType = mat.colprop.ColType
            ColParam = mat.colprop.ColParam
            if ColType == 8:
                warp = 1
                WarpNum = mat.colprop.WarpNum - 1
            else:
                warp = 0
                WarpNum = 0
            BI = 0
            if ColType == 9:
                BI = obj.objprop.BreakIndex
                if obj.objprop.BreakIndex == 0:
                    obj.objprop.BreakIndex = len(self.Destructable_Groups)
                    BI = obj.objprop.BreakIndex
                    self.Destructable_Groups.append(
                        [0, 0, obj.parent.objprop.BreakIndex]
                    )
                    self.Destructable_Indices.append([])
                self.Destructable_Groups[BI][0] += 1
                self.Destructable_Indices[BI].append(len(self.Triangles))
            # If new normal, add to normals, else get index of norm
            dot = lambda x, y: sum([a * b for a, b in zip(x, y)])
            norm = self.CalcNorm(normal, center, dot)
            Six = 0
            if norm in self.Normals:
                Six = self.Normals.index(norm)
            if Six == 0:
                PIs.append(norm[:3])
                self.Normals.append(norm)
                Six = len(self.Normals) - 1
            verts = [a + 1 + Voff for a in verts]
            if BI:
                WarpNum = BI
            self.Triangles.append(
                (tuple(verts), Six, NormType, WarpNum, 0, warp, ColParam, ColType)
            )
        Voff += len(obj.data.vertices)
        return Voff

    def CalcNorm(self, normal, center, dot):
        norm = [round(a, 4) if abs(a) > 0.0001 else 0 for a in normal]
        if 1.0 in norm or -1.0 in norm:
            norm = [0 if abs(a) != 1.0 else a for a in norm]
        negdot = round(-center.dot(normal), 5)
        # check if this norm is similar to any other normal
        for n in self.Normals:
            if dot(norm, n) > 0.995 and abs(-dot(center, n) - n[3]) < 1:
                return n
        norm.append(negdot)
        return tuple(norm)

    def BuildTriCells(self):
        tris = self.Triangles[1:].copy()
        Norms = self.Normals[1:].copy()
        Normal_Cells = []
        [p, index] = [0, 0]
        # find norm with no right children and make that root
        for i, n in enumerate(Norms):
            test = [a for a in tris if a[1] != i + 1]
            p = n
            [Left, Right] = self.FindChildren(p, test)
            if not Right:
                index = i
                break
        tricell = [a for a in tris if a[1] == index + 1]
        tricell = [self.Triangles.index(a) for a in tricell]
        tris = [a for a in tris if a[1] != i + 1]
        [Left, Right] = self.FindChildren(p, tris)
        Normal_Cells = self.MakeNormCells(Normal_Cells, Left, Right, p, Norms, tricell)
        [self.Normal_Cells.append(tuple(a)) for a in Normal_Cells]

    def MakeNormCells(self, Normal_Cells, Left, Right, parent, Norms, tricell):
        Tind = self.FindTriCell(tricell)
        Normal_Cells.append([self.Normals.index(parent), 0, 0, Tind])
        ParentNode = len(Normal_Cells)
        if Left:
            [ind, parent] = self.FindOptimalNext(Left, parent)
            # remove tris of ind in left
            tricell = [
                self.Triangles.index(a) for i, a in enumerate(Left) if a[1] == ind
            ]
            tris = [a for a in Left if a[1] != ind]
            [LLeft, LRight] = self.FindChildren(parent, tris)
            Normal_Cells[ParentNode - 1][1] = len(Normal_Cells) + 1
            Normal_Cells = self.MakeNormCells(
                Normal_Cells, LLeft, LRight, parent, Norms, tricell
            )
        if Right:
            [ind, parent] = self.FindOptimalNext(Right, parent)
            # remove tris of ind in right
            tricell = [
                self.Triangles.index(a) for i, a in enumerate(Right) if a[1] == ind
            ]
            tris = [a for a in Right if a[1] != ind]
            [RLeft, RRight] = self.FindChildren(parent, tris)
            Normal_Cells[ParentNode - 1][2] = len(Normal_Cells) + 1
            Normal_Cells = self.MakeNormCells(
                Normal_Cells, RLeft, RRight, parent, Norms, tricell
            )
        return Normal_Cells

    def FindOptimalNext(self, pool, parent):
        # save some time on trivial case
        if len(pool) == 1:
            return [pool[-1][1], self.Normals[pool[-1][1]]]
        # only one normal left
        n = [a[1] for a in pool]
        if all([b == pool[0][1] for b in n]):
            return [pool[-1][1], self.Normals[pool[-1][1]]]
        # return [pool[-1][1],self.Normals[pool[-1][1]]]
        dot = lambda x, y: sum([a * b for a, b in zip(x, y)])
        # find a child that doesn't split any triangles
        LLeft, LRight = 0, 0
        for p in pool:
            ind = p[1]
            Tp = [a for a in pool if a[1] != ind]
            Tparent = self.Normals[ind]
            [LLeft, LRight] = self.FindChildren(Tparent, Tp)
            # check if any member of LLeft shares a norm with LRight
            if not LLeft or not LRight:
                return [ind, self.Normals[ind]]
            for l in LLeft:
                for r in LRight:
                    if l == r:
                        break
                else:
                    # found return
                    return [ind, self.Normals[ind]]
        # if none found somehow, pick a non sloped surface
        print(len(pool), "fail", LLeft, LRight, self.Normals[LLeft[0][1]])
        [print(self.Normals[a[1]]) for a in pool]
        for p in pool:
            w = [1, 1, 1]
            ind = p[1]
            Tparent = self.Normals[ind]
            if abs(dot(w, Tparent)) == 1.0:
                return [ind, Tparent]
        # double fail
        print(len(pool), "double fail")
        return [pool[-1][1], self.Normals[pool[-1][1]]]

    def FindTriCell(self, tricell):
        x = 1
        for t in self.Triangle_Cells[1:]:
            if type(t) == int:
                x += 1
            else:
                x += len(t)
        if tricell:
            # change order of tri cell to based on height
            order = []
            Comp2Dec = lambda x: unpack(">3h", pack(">3H", *x))
            for i, t in enumerate(tricell):
                tri = self.Triangles[t]
                verts = [self.Vertices[a] for a in tri[0]]
                verts = [Comp2Dec(a) for a in verts]
                order.append([max([a[1] for a in verts]), i])
            order.sort(key=(lambda x: x[0]))
            tcell = []
            for o in order:
                tcell.append(tricell[o[1]])
            tcell[-1] |= 0x8000
            self.Triangle_Cells.append(tuple(tcell))
        return x

    # given a list of tris, find left/right children
    def FindChildren(self, parent, tris):
        Left = []
        Right = []
        groups = []
        tpool = tris.copy()
        while tpool:
            t = tpool[0]
            groups.append([a for a in tris if a[1] == t[1]])
            tpool = [a for a in tpool if a[1] != t[1]]
        for t in groups:
            fronts = []
            for tri in t:
                fronts.append(self.RayCast(parent, tri))
                if "Both" == fronts[-1]:
                    Left.append(tri)
                    Right.append(tri)
                elif fronts[-1] == True:
                    Left.append(tri)
                elif fronts[-1] == False:
                    Right.append(tri)
            # this code keeps the groups together
        #            if "Both" in fronts or (True in fronts and False in fronts):
        #                Left.extend(t)
        #                Right.extend(t)
        #            elif all(a for a in fronts):
        #                Left.extend(t)
        #            else:
        #                Right.extend(t)
        return [Left, Right]

    def RayCast(self, parent, tri):
        v_ind = tri[0]
        verts = [self.Vertices[a] for a in v_ind]
        Comp2Dec = lambda x: unpack(">3h", pack(">3H", *x))
        dot = lambda x, y: sum([a * b for a, b in zip(x, y)])
        front = None
        for v in verts:
            v = Comp2Dec(v)
            q = dot(v, parent[0:3]) + parent[3]
            # to deal with integer rounding
            if q > -0.5 and q < 0.5:
                continue
            elif q < -0.5:
                if front == True:
                    front = "Both"
                    break
                else:
                    front = False
            else:
                if front == False:
                    front = "Both"
                    break
                else:
                    front = True
        return front

    def AddEntity(self, entity, num):
        id = entity.objprop.Entity.split(",")
        loc = (
            entity.location[0] * bpy.context.scene.my_tool.Scale,
            entity.location[2] * bpy.context.scene.my_tool.Scale,
            -entity.location[1] * bpy.context.scene.my_tool.Scale,
        )
        scale = (entity.scale[0], entity.scale[2], entity.scale[1])
        ent = {
            0x0: (entity.objprop.NodeNum - 1,),
            0x1: (entity.objprop.BankNum,),
            0x2: (entity.objprop.IndexNum,),
            0x3: (entity.objprop.Action,),
            0x4: (int(entity.objprop.Respawn) + (int(entity.objprop.Flags) << 8),),
            0x6: (entity.objprop.Eep,),
            0x8: loc,
            0x14: (0.0, 0.0, 0.0),
            0x20: scale,
        }
        setattr(self, "Entity_ID_%d" % num, ent)

    def ColHeader(self):
        # End lists
        # force alignment
        if ((len(self.Vertices) * 6 + 16) % 4) != 0:
            self.Vertices.append(0x9999)
        # tri cells is one longer
        if ((self.FindTriCell(0) * 2) % 4) != 0:
            self.Triangle_Cells.append(0x9999)
        CH = self.Collision_Header
        # verts
        CH[8] = 0x10
        CH[0xC] = len(self.Vertices)
        # tris
        CH[4] = len(self.Triangles)
        CH[0] = CH[0xC] * 6 + 16
        # deal with pad case
        if self.Vertices[-1] == 0x9999:
            CH[0xC] -= 1
            CH[0x0] -= 4
        # normals
        CH[0x10] = CH[0] + CH[4] * 20
        CH[0x14] = len(self.Normals)
        # tri groups
        CH[0x18] = CH[0x10] + CH[0x14] * 16
        CH[0x1C] = self.FindTriCell(0)
        # normal cells
        CH[0x20] = CH[0x18] + CH[0x1C] * 2
        CH[0x24] = len(self.Normal_Cells)
        # root of normal cells
        CH[0x28] = 1
        # no water data for now
        CH[0x40] = 0
        if CH[0x40]:
            CH[0x3C] = CH[0x20] + CH[0x1C] * 4
        else:
            CH[0x3C] = 0
        CH[0x38] = 0
        if CH[0x38]:
            CH[0x34] = CH[0x3C] + CH[0x40] * 0x10
        else:
            CH[0x34] = 0
        # no destructables support for now so I'll leave them at zero
        CH[0x2C] = (
            CH[0x20] + CH[0x38] * 0x18 + CH[0x40] * 0x10 + len(self.Normal_Cells) * 8
        )
        paddg = 0
        if (len(self.Destructable_Groups) * 6) % 4 != 0:
            self.Destructable_Groups.append([0x9999])
            paddg = -4
        CH[0x30] = CH[0x2C] + len(self.Destructable_Groups) * 6 + paddg
        # Now MH
        L = 0
        # fix destructable groups middle index
        for i, a in enumerate(self.Destructable_Indices):
            self.Destructable_Groups[i][1] = L // 2
            L += len(a) * 2
        if (L % 4) != 0:
            self.Destructable_Indices.append([0x9999])
            L += 2
        self.Main_Header[0x0] = CH[0x30] + L


class node:
    def __init__(self):
        self.Node_Header = {0x0: 0, 0x4: 0, 0x8: 0, 0xC: 0}
        # these need to be renamed eventually
        self.Unk_Bytes = []
        self.Unk_Floats = [9999.0, 0.0]

    # sets all the attributes needed
    def SetUpNode(self, num):
        fields = [
            "Path_Header_",
            "Path_Node_Matrix_",
            "Path_Node_Footer_",
            "Kirby_Node_Data_",
            "Path_Node_Bounds_",
            "Node_Connector_",
        ]
        # This is horrendous but I will keep it this way
        types = [
            {0x0: 0x49C, 0x4: 0x3F4, 0x8: 0x64C, 0xC: 0x2, 0xE: 0x0},
            tuple(),
            {
                0x0: 0x0,
                0x2: 0x3,
                0x4: 0.000000,
                0x8: 0x454,
                0xC: 10.000000,
                0x10: 0x478,
                0x14: 0x0,
            },
            {
                0x0: (0x0),
                0x2: (0x0),
                0x4: (0x0, 0x0, 0x0, 0x0),
                0x8: (0x0),
                0x9: (0xB, 0xB, 0xB),
                0xC: (0x0),
                0xE: (0x00),
                0x10: (0x0),
                0x12: (0x0),
                0x14: (0.000000),
                0x18: (0.000000),
                0x1C: (0.000000),
            },
            tuple(),
            tuple(),
        ]
        for s, a in zip(fields, types):
            setattr(self, "%s%d" % (s, num), a)

    # after data is written to nodes, this fills
    # all the offsets to data
    def WriteOffsets(self, MH):
        start = MH[0x0] + 0x44
        num = self.Node_Header[0x0]
        ph = num * 0x10
        cn = num * 0x90
        for i in range(num):
            NodeFooter = getattr(self, "Path_Node_Footer_%d" % i)
            NodeNum = NodeFooter[0x2]
            NodeFooter[0x8] = start
            start = 12 * NodeNum + start
            NodeFooter[0x10] = start
            start = start + 4 * NodeNum
            PathHeader = getattr(self, "Path_Header_%d" % i)
            PathHeader[0x4] = start
            start = start + 0x18
        # I loop twice so start can iterate
        # as structs are variable length
        NodeCOff = 0
        for i in range(num):
            PathHeader = getattr(self, "Path_Header_%d" % i)
            PathHeader[0x0] = start + i * 0x90
            PathHeader[0x8] = start + cn + NodeCOff
            NodeCOff = PathHeader[0xC] * 4 + NodeCOff + PathHeader[0xE] * 8
        self.Node_Header[0x8] = start + cn + NodeCOff
        self.Node_Header[0xC] = self.Node_Header[0x8] + len(self.Unk_Bytes)
        self.Node_Header[0x4] = self.Node_Header[0xC] + len(self.Unk_Floats) * 4
        MH[0x4] = self.Node_Header[0x4] + ph
        # entities
        MH[0x8] = MH[0x4] + 0x10

    def SetNode(self, props, spline):
        num = props.NodeNum - 1
        self.SetUpNode(num)
        WarpEnF = props.EnableWarpF
        WarpEnR = props.EnableWarpR
        WarpDest = list(props.Warp)
        PNM = list(getattr(self, "Path_Node_Matrix_%d" % num))
        PF = getattr(self, "Path_Node_Footer_%d" % num)
        KND = getattr(self, "Kirby_Node_Data_%d" % num)
        PH = getattr(self, "Path_Header_%d" % num)
        PB = list(getattr(self, "Path_Node_Bounds_%d" % num))
        NC = list(getattr(self, "Node_Connector_%d" % num))
        # Kirby Stuff
        KND[0x0] = num * 0x100
        if WarpEnF or WarpEnR:
            WarpDest = [a - 1 for a in WarpDest]
            WarpDest.append(props.WarpDir - 1)
            KND[0x4] = tuple(WarpDest)
            KND[0xE] = 0x21
            if WarpEnR:
                KND[0x2] = 1
        # Path Footer Stuff
        speed = props.Speed
        TotalLen = spline.calc_length()  # total length
        PF[0x2] = len(spline.points)
        PF[0xC] = (TotalLen) / (12 * speed)
        # Path Header Stuff and Node Connector Stuff
        if props.Looping:
            PH[0xC] = 0
            PH[0xE] = 1
            NC.append((0, 0, 0, 0))
            NC.append((0, 0, 0, 0))
        else:
            PH[0xC] = 2
            NC.append((props.LockBackward * 2, 0, props.PrevNode - 1, 0))
            NC.append(((not props.LockForward) * 2, 0, props.NextNode - 1, 0))
        # Path Node Bounds
        lp = spline.points[0].co.to_3d()
        prev = 0
        for a in spline.points:
            PB.append(round(((a.co.to_3d() - lp).length + prev) / TotalLen, 5))
            prev += (a.co.to_3d() - lp).length
            lp = a.co.to_3d()
        [PNM.append(tuple(p.co.to_3d())) for p in spline.points]
        # to deal with mutable shit
        setattr(self, "Path_Node_Matrix_%d" % num, PNM)
        setattr(self, "Path_Node_Bounds_%d" % num, PB)
        setattr(self, "Node_Connector_%d" % num, NC)

    def SetCam(self, cam):
        cp = cam.data.camprop
        num = cp.NodeNum
        setattr(
            self,
            "Camera_Node_Data_%d" % (num - 1),
            {
                0: [
                    0,
                ],
                1: [
                    10,
                ],
                2: [
                    1,
                ],
                3: [
                    1,
                ],
                4: [
                    1,
                ],
                5: [
                    0,
                ],
                6: [
                    1,
                ],
                7: [
                    1,
                ],
                8: [
                    0,
                ],
                9: [
                    0,
                ],
                10: [
                    100,
                ],
                12: [
                    9999.0,
                ],
                16: [
                    9999.0,
                ],
                20: [
                    9999.0,
                ],
                24: [
                    100.0,
                ],
                28: [
                    12800.0,
                ],
                32: [85.0, 85.0],
                40: [90.0, 90.0],
                48: [600.0, 600.0],
                56: [33.0, 33.0],
                64: [100.0, 100.0],
                72: [
                    -9999.0,
                    9999.0,
                ],
                80: [
                    100.0,
                    100.0,
                ],
                88: [-9999.0, 9999.0],
                96: [
                    10.0,
                    90.0,
                ],
                104: [
                    0.0,
                    359.0,
                ],
            },
        )
        CD = getattr(self, "Camera_Node_Data_%d" % (num - 1))
        # PNM=list(getattr(self,'Path_Node_Matrix_%d'%(num-1)))
        [CD[2][0], CD[3][0], CD[4][0]] = [a & 1 for a in cp.AxisLocks]
        CD[6][0] = (not cp.PanUpDown) & 1
        CD[7][0] = (not cp.PanDown) & 1
        CD[8][0] = cp.PanH & 1
        CD[0][0] = cp.ProfileView & 1
        CD[0x30] = cp.Radius[:]
        CD[0x48] = cp.CamXBound[:]
        CD[0x50] = cp.CamYBound[:]
        CD[24] = (cam.data.clip_start * 1000,)
        CD[28] = (cam.data.clip_end * 12.8,)
        b = [(a,) for a in cp.Foc]
        [CD[0xC], CD[0x10], CD[0x14]] = b
        CD[0x58] = cp.CamZBound[:]
        CD[0x60] = cp.CamYawBound[:]
        CD[0x68] = cp.CamPitchBound[:]
        CD[0x28] = cp.Yaw[:]
        CD[0x40] = cp.Pitch[:]

    def SetNodeDistances(self, num):
        for n in range(num):
            for d in range(num):
                if n == d:
                    self.Unk_Bytes.append(0)
                    continue
                # if they're connected, set to zero which is element 1 of array.
                NC = getattr(self, "Node_Connector_%d" % n)
                for j, c in enumerate(NC):
                    if c[2] == d:
                        self.Unk_Bytes.append(((1 - j) * 0x80) | 1)
                        break
                # they are not connected
                else:
                    Fdistance = self.FindConnectedNode(NC, d, 0, 1)
                    if Fdistance == None:
                        Rdistance = self.FindConnectedNode(NC, d, 0, 0)
                        if Rdistance == None:
                            print(NC, n, d)
                            raise Exception("You have an inaccessible path")
                        if Rdistance in self.Unk_Floats:
                            self.Unk_Bytes.append(
                                0x80 | self.Unk_Floats.index(Rdistance)
                            )
                        else:
                            self.Unk_Floats.append(Rdistance)
                            self.Unk_Bytes.append(0x80 | (len(self.Unk_Floats) - 1))
                    else:
                        if Fdistance in self.Unk_Floats:
                            self.Unk_Bytes.append(self.Unk_Floats.index(Fdistance))
                        else:
                            self.Unk_Floats.append(Fdistance)
                            self.Unk_Bytes.append(len(self.Unk_Floats) - 1)
        q = len(self.Unk_Bytes)
        w = q % 4
        if w != 0:
            [self.Unk_Bytes.append(0x99 - 0x99 * (min(x, 1))) for x in range(4 - w)]

    def FindConnectedNode(self, start, target, distance, dir):
        if len(start) == 1:
            if start[1][0]:
                return None
            next = start[0][2]
        else:
            if dir == 1:
                if not start[1][0]:
                    # this direction is blocked
                    return None
                next = start[1][2]
            else:
                if start[0][0]:
                    # this direction is blocked
                    return None
                next = start[0][2]
        if next == target:
            return distance
        PF = getattr(self, "Path_Node_Footer_%d" % next)
        distance += PF[0xC]
        start = getattr(self, "Node_Connector_%d" % next)
        distance = self.FindConnectedNode(start, target, distance, dir)
        return distance


class GB:
    def __init__(self):
        self.Geometry_Block_Header = {
            0x0: 0x0,
            0x4: 0x0,
            0x8: 0x17,
            0xC: 0x0,
            0x10: 0x0,
            0x14: 0x0,
            0x18: 0x0,
            0x1C: 0x0,
        }
        # No textures for now :(
        self.Img_Refs = []
        self.Vert_Refs = []
        self.MeshCnt = 0
        self.DLoff = 0
        self.GeoVertices = []
        self.VertOff = 0

    def CreateLayouts(self, num, root):
        if root > 1:
            root = 1
        self.root = root
        for i in range(num + 1 + root):
            setattr(
                self,
                "Layout_%d" % i,
                {
                    0x0: 0x0,
                    0x2: 0x0,
                    0x4: 0x00000000,
                    0x8: (0.0, 0.0, 0.0),
                    0x14: (0.0, 0.0, 0.0),
                    0x20: (1.0, 1.0, 1.0),
                },
            )

    def file(self, name):
        self.file = open(name, "w")

    def writeTmap(self):
        labels = [a for a in self.Tmap.__dict__ if not a.startswith("__")]
        attrs = [getattr(self.Tmap, a) for a in labels]
        Tmap = open(bpy.context.scene.my_tool.ROM_path + "\\Texture_ADDR.py", "w")
        [Tmap.write(a[0] + " = " + str(a[1]) + "\n") for a in zip(labels, attrs)]
        Tmap.close

    def write(self):
        del self.MeshCnt
        del self.DLoff
        del self.VertOff
        del self.LastTexture
        del self.LastTexture2
        del self.LastPrim
        del self.LastEnv
        del self.LastMode
        del self.LastGeo
        del self.LCP
        del self.TempDLoff
        del self.root
        if hasattr(self, "Tmap"):
            self.writeTmap()
            del self.Tmap
        self.labels = [a for a in self.__dict__ if a is not "file"]
        self.attrs = [getattr(self, a) for a in self.labels]
        [
            self.file.write(a[0] + " = " + str(a[1]) + "\n")
            for a in zip(self.labels, self.attrs)
        ]
        self.file.close()

    def WriteMesh(self, obj, tex, Lnum):
        # Write layout for mesh
        Layout = self.WriteLayout(obj, Lnum)
        # link destructables
        obj.objprop.BreakIndex = self.MeshCnt + 1
        if obj.type != "MESH":
            return
        # write vertices
        try:
            uv = obj.data.uv_layers[0]  # I'm only taking the first map
        except:
            uv = None
        try:
            vc = obj.data.vertex_colors[0]
        except:
            vc = None
        # calculate loop tris
        Palette = 0
        obj.data.calc_loop_triangles()
        verts = obj.data.vertices
        tris = obj.data.loop_triangles
        # uvs and vcs are not stripped, meaning each polygon loop has duplicated verts
        # they're in the same order as polygons, so by keeping track of loop indices
        # you know the same order of the uv/vc indices, e.g. polys[i].loop_indices.
        # Its only possible to have or not have a uv/vc map per object. I only use the
        # first one of each. In order to separate options I'll have to use mats.
        mats = obj.data.materials
        DL = self.InitDL()
        Palette = self.WriteVerts(tris, uv, vc, verts, mats, DL, tex, Palette)
        # Now vertex buffers and DL tri calls should be done
        # Now to end DL, and fill up the vertex refs
        self.EndDL(DL, Layout, Palette)
        self.VertOff += len(self.GeoVertices)

    def WriteOffsets(self, num):
        Layout = getattr(self, "Layout_%d" % (num))
        Layout[2] = 0x12
        for a in range(num):
            L = getattr(self, "Layout_%d" % (a))
            if L[4]:
                L[4] = L[4] + len(self.GeoVertices) * 16
        self.Img_Refs = [
            a + len(self.GeoVertices) * 16 + 67108896 for a in self.Img_Refs
        ]
        self.Img_Refs.append(0)
        GB = self.Geometry_Block_Header
        GB[0] = (
            len(self.GeoVertices) * 16 + self.DLoff + len(self.Img_Refs) * 4 + 67108896
        )
        GB[0xC] = len(self.GeoVertices) * 16 + self.DLoff + 67108896
        GB[0x10] = (
            len(self.GeoVertices) * 16 + len(self.Img_Refs) * 4 + self.DLoff + 67108896
        )
        GB[0x1C] = num + 1

    def WriteVerts(self, tris, uv, vc, verts, mats, DL, tex, Palette):
        CurrEnv = []
        VB = []
        overflow = []
        M = len(tris) - 1
        prev = len(self.GeoVertices)
        LastMat = (
            0  # Gonna try not to dupe mats by just keeping track of last mat added
        )
        self.LastTexture = 0
        self.LastTexture2 = 0
        self.LastPrim = 0
        self.LastEnv = 0
        self.LastMode = 0
        self.LastGeo = 0
        self.LCP = [0, 1, 0, 0]
        MV = len(uv.data)  # number of verts
        buffer = []
        for i, t in enumerate(tris):
            # blender tesselation
            loops = t.loops
            vert = t.vertices
            mat = mats[t.material_index]
            if not mat == LastMat:
                if buffer:
                    DL.append(
                        (
                            "G_TRI1",
                            (
                                VB.index(buffer[0]),
                                VB.index(buffer[1]),
                                VB.index(buffer[2]),
                            ),
                        )
                    )
                    buffer = []
                [LastMat, Palette] = self.AddMatDL(mat, LastMat, DL, tex, Palette)
            if not VB:
                VB.extend(overflow)
                if not MV - len(self.GeoVertices) + prev + len(overflow) < 0:
                    DL.append(
                        (
                            "G_VTX",
                            (
                                min(
                                    32,
                                    MV - len(self.GeoVertices) + prev + len(overflow),
                                ),
                                0,
                                67108896
                                + len(self.GeoVertices) * 16
                                - len(overflow) * 16,
                            ),
                        )
                    )
            # vert refers to the vert index inside the vert list, loop
            # refers to the loop index, which matches vc and uv

            # So if I wanted to get the uv coord of a face, I would take
            # the uv data from loop indices, and apply it to the corresponding
            # index in vertices
            # add verts to list
            for l, v in zip(loops, vert):
                if l in VB:
                    continue
                # vertex positions
                co = list(verts[v].co * bpy.context.scene.my_tool.Scale)
                co = [Dec2Compliment(int(round(a, 0))) for a in co]
                co = tuple(co)
                pad = (0,)
                # uv coordinates for vertex
                if uv:
                    if self.LastTexture:
                        coords = self.LastTexture.size
                    else:
                        coords = (0, 0)
                    ST = uv.data[l].uv.copy()
                    ST = list(ST)
                    # flip y coord
                    ST[1] = -ST[1]
                    # scale by texture size and increase to N64 u nit size
                    ST = [
                        Dec2Compliment(int(round((b * a * 32), 0)))
                        for a, b in zip(ST, coords)
                    ]
                    ST = tuple(ST)
                else:
                    ST = (0, 0)
                # vertex colors
                if vc:
                    color = list(vc.data[l].color)
                    color = [int(round(a * 255, 0)) for a in color]
                    color = tuple(color)
                else:
                    # lazy solution instead of getting normals
                    color = (255, 255, 255, 255)
                # append vertex to list
                self.GeoVertices.append((co, pad, ST, color))
                VB.append(l)
            if len(VB) >= 32:
                if buffer:
                    DL.append(
                        (
                            "G_TRI1",
                            (
                                VB.index(buffer[0]),
                                VB.index(buffer[1]),
                                VB.index(buffer[2]),
                            ),
                        )
                    )
                low = min(VB.index(loops[0]), VB.index(loops[1]), VB.index(loops[2]))
                high = max(VB.index(loops[0]), VB.index(loops[1]), VB.index(loops[2]))
                ran = high - low
                Off = min(loops[0], loops[1], loops[2])
                DL.append(
                    (
                        "G_VTX",
                        (
                            ran + 1,
                            0,
                            67108896
                            + len(self.GeoVertices) * 16
                            - (len(VB) - low) * 16,
                        ),
                    )
                )
                NVB = list(range(low, high + 1, 1))
                DL.append(("G_TRI1", (loops[0] - Off, loops[1] - Off, loops[2] - Off)))
                buffer = []
                overflow = VB[32:]
                VB = []
                continue
            if i == M and not buffer:
                DL.append(
                    (
                        "G_TRI1",
                        (VB.index(loops[0]), VB.index(loops[1]), VB.index(loops[2])),
                    )
                )
            elif i == M and buffer:
                DL.append(
                    (
                        "G_TRI2",
                        (
                            VB.index(loops[0]),
                            VB.index(loops[1]),
                            VB.index(loops[2]),
                            VB.index(buffer[0]),
                            VB.index(buffer[1]),
                            VB.index(buffer[2]),
                        ),
                    )
                )
                buffer = []
            elif not buffer:
                buffer = [loops[0], loops[1], loops[2]]
            else:
                DL.append(
                    (
                        "G_TRI2",
                        (
                            VB.index(loops[0]),
                            VB.index(loops[1]),
                            VB.index(loops[2]),
                            VB.index(buffer[0]),
                            VB.index(buffer[1]),
                            VB.index(buffer[2]),
                        ),
                    )
                )
                buffer = []
        return Palette

    def WriteCombinerDL(self, mat, DL):
        nodes = mat.node_tree.nodes
        modes = []
        for i in range(2):
            if not mat.colprop.TwoCycle:
                i = 0
            addc = nodes.get("ADDC%d" % (i + 1))
            subc = nodes.get("SUBC%d" % (i + 1))
            multc = nodes.get("MULTC%d" % (i + 1))
            adda = nodes.get("ADDA%d" % (i + 1))
            suba = nodes.get("SUBA%d" % (i + 1))
            multa = nodes.get("MULTA%d" % (i + 1))
            A = subc.inputs[1].links
            B = subc.inputs[2].links
            C = multc.inputs[1].links
            D = addc.inputs[1].links
            E = suba.inputs[0].links
            F = suba.inputs[1].links
            G = multa.inputs[0].links
            H = adda.inputs[0].links
            modes.extend([A, B, C, D, E, F, G, H])
        NameCC = {
            "Image Texture": [("Color", "Texel 0"), ("Alpha", "Texel 0 Alpha")],
            "Image Texture.001": [("Color", "Texel 1"), ("Alpha", "Texel 1 Alpha")],
            "ADDC1": [("Color", "Combined")],
            "ADDA1": [("Value", "Combined Alpha")],
            "ENV": [("Color", "Environment")],
            "ENVA": [("Value", "Environment Alpha")],
            "PRIM": [("Color", "Primitive")],
            "PRIMA": [("Value", "Primitive Alpha")],
            "ONE": [("Value", "1.0")],
            "ZERO": [("Value", "Zero")],
            "Shade Color": [("Color", "Shade"), ("Alpha", "Shade Alpha")],
            "Vertex Color": [("Alpha", "Shade Alpha")],
        }
        res = []
        for m in modes:
            L = NameCC.get(m[0].from_socket.node.name)
            for l in L:
                if l[0] == m[0].from_socket.name:
                    res.append(l[1])
        SetCmb = ("G_SETCOMBINE", tuple(res))
        DL.append(SetCmb)
        return ["Texel 0" in res, "Texel 1" in res]

    def AddMatDL(self, mat, LastMat, DL, tex, Palette):
        if not mat.colprop.KCSmat:
            return [mat, Palette]
        if LastMat != 0:
            DL.append(("G_RDPPIPESYNC", ()))
        # first get combiner
        Textured = self.WriteCombinerDL(mat, DL)
        # deal with textures for now
        if Textured[0]:
            TexNode = mat.node_tree.nodes.get("Image Texture").image
        else:
            TexNode = 0
        if Textured[1]:
            TexNode2 = mat.node_tree.nodes.get("Image Texture.001").image
        else:
            TexNode2 = 0
        if TexNode and (Textured[0] or Textured[1]):
            if TexNode.name != "Missing_Texture.png":
                Palette = self.LoadTextureDL(TexNode, TexNode2, DL, tex, Palette, mat)
        # deal with env/prim colors
        PRIM = mat.node_tree.nodes.get("PRIM")
        ENV = mat.node_tree.nodes.get("ENV")
        c = list(PRIM.outputs[0].default_value)
        c.insert(0, 0.0)
        c.insert(0, 0.0)
        d = list(ENV.outputs[0].default_value)
        c = [int(round(a * 255, 0)) if i > 1 else a for i, a in enumerate(c)]
        d = [int(round(a * 255, 0)) for a in d]
        if not d == self.LastEnv:
            self.LastEnv = d
            DL.append(("G_SETENVCOLOR", tuple(d)))
        if not c == self.LastPrim:
            self.LastPrim = c
            DL.append(("G_SETPRIMCOLOR", tuple(c)))
        # add othermode
        mc = mat.colprop
        if not self.LastMode == mat.colprop.OtherMode_L:
            mode = ("G_SETOTHERMODE_L", ("Render_Mode", mc.OtherMode_L))
            DL.append(mode)
            self.LastMode = mat.colprop.OtherMode_L
        # the min amount of othermodes I'm supporting right now
        self.OtherHModes(
            self.LCP[3], mc.TwoCycle, "G_MDSFT_CYCLETYPE", 1048576, DL, "H"
        )
        self.OtherHModes(
            self.LCP[0], mc.AlphaCompare, "G_MDSFT_ALPHACOMPARE", "G_AC_DITHER", DL, "L"
        )
        self.OtherHModes(self.LCP[1], mc.TextFilt, "G_MDSFT_TEXTFILT", 8192, DL, "H")
        self.OtherHModes(self.LCP[2], mc.TextLOD, "G_MDSFT_TEXTLOD", 0x10000, DL, "H")
        Geomodes = [
            "G_ZBUFFER",
            "G_SHADE",
            "G_CULL_FRONT",
            "G_CULL_BACK",
            "G_FOG",
            "G_LIGHTING",
            "G_TEXTURE_GEN",
            "G_TEXTURE_GEN_LINEAR",
            "G_SHADING_SMOOTH",
            "G_CLIPPING",
        ]
        Sets = [a for a in Geomodes if mc[a]]
        if self.LastGeo != Sets:
            set = ""
            for s in Sets:
                set += s + "|"
            if not set:
                set = "Clear|"
            mode = ("G_GEOMETRYMODE", ("All", set[:-1]))
            DL.append(mode)
            self.LastGeo = Sets
        self.LCP = [mc.AlphaCompare, mc.TextFilt, mc.TextLOD, mc.TwoCycle]
        return [mat, Palette]

    def OtherHModes(self, LastMat, mc, prop, set, DL, B):
        if mc and not LastMat:
            DL.append(("G_SETOTHERMODE_%s" % B, (prop, set)))
        elif not mc and LastMat:
            DL.append(("G_SETOTHERMODE_%s" % B, (prop, 0)))
        return

    def SetupCustomTextures(self, i, tex, mat):
        # copy file to custom textures path
        self.Tmap = tex
        mytool = bpy.context.scene.my_tool
        cp = mat.colprop
        CTP = Path(mytool.ROM_path) / "Textures\\5"
        Ipath = Path(bpy.path.abspath(i.filepath))
        ip = 0
        # check if the texture is coming from the textures folder
        if not str(Ipath).split("\\")[-3] == "Textures":
            ip = Path(copy(Ipath, CTP))
        else:
            # This means its a file that already is in the file system but I don't have its data
            name = i.name
            nameS = name.split("_")
            bank = nameS[1]
            index = nameS[3]
            Dat = (bank, index, cp.Fmt, int(cp.Depth), i.size[0], i.size[1])
            return Dat
        T5 = self.Tmap.Textures5
        M = 313
        for j, t in enumerate(T5):
            if i.name == t[-1]:
                if cp.Fmt == "CI" and t[2] == "CI":
                    Dat = (
                        str(5),
                        str(t[1]),
                        cp.Fmt,
                        int(cp.Depth),
                        i.size[0],
                        i.size[1],
                        str(5),
                        str(t[7]),
                        i.name,
                    )
                elif cp.Fmt == "CI" and t[2] != "CI":
                    continue
                else:
                    Dat = (
                        str(5),
                        str(t[1]),
                        cp.Fmt,
                        int(cp.Depth),
                        i.size[0],
                        i.size[1],
                        i.name,
                    )
                T5[j] = Dat
                if ip:
                    name = "Bank_%d_Index_%s_Tex.png" % (5, t[1])
                    if os.path.isfile(CTP / name):
                        os.remove(CTP / name)
                    os.rename(ip, CTP / name)
                edit = (5, int(t[1]), "Tex")
                AddEdit(mytool, edit)
                return Dat
            if int(t[1]) >= M:
                if t[2] == "CI":
                    M = int(t[1]) + 2
                else:
                    M = int(t[1]) + 1
        edit = (5, M, "Tex")
        if cp.Fmt == "CI":
            Dat = (
                str(5),
                str(M),
                cp.Fmt,
                int(cp.Depth),
                i.size[0],
                i.size[1],
                str(5),
                str(M + 1),
                i.name,
            )
        else:
            Dat = (str(5), str(M), cp.Fmt, int(cp.Depth), i.size[0], i.size[1], i.name)
        T5.append(Dat)
        AddEdit(mytool, edit)
        if ip:
            name = "Bank_%d_Index_%d_Tex.png" % (5, M)
            if os.path.isfile(CTP / name):
                os.remove(CTP / name)
            os.rename(ip, CTP / name)
        return Dat

    def LoadTextureDL(self, i, i2, DL, tex, Palette, mat):
        if i == self.LastTexture and (i2 == self.LastTexture2 or not i2):
            return Palette
        Num = 1
        if i2:
            Num = 2
        Dat = []
        for j in range(Num):
            if j == 0:
                name = i.name
            else:
                name = i2.name
            nameS = name.split("_")
            if len(nameS) > 3:
                bank = nameS[1]
                index = nameS[3]
                try:
                    T = getattr(tex, "Textures%s" % bank)
                    Exported = (bank, index)
                    for t in T:
                        if Exported == (t[0], t[1]):
                            Dat.append(t)
                        elif name == t[-1]:
                            Dat.append(t)
                except:
                    Dat.append(())
            else:
                Dat.append(())
        # A custom texture
        for j in range(Num):
            if not Dat[j]:
                if j == 0:
                    Dat[j] = self.SetupCustomTextures(i, tex, mat)
                else:
                    Dat[j] = self.SetupCustomTextures(i2, tex, mat)
        if Dat[0] or Dat[-1]:
            if Num == 1:
                Dat = Dat[0]
                Tname = "0x{0:04X}{1:04X}".format(int(Dat[0]), int(Dat[1]))
                if Dat[2] == "CI":
                    if not Palette:
                        DL.append(("G_SETOTHERMODE_H", ("G_MDSFT_TEXTLUT", 32768)))
                        Palette = 1
                    Pname = "0x{0:04X}{1:04X}".format(int(Dat[6]), int(Dat[7]))
                    # type,BankID,width,height,Sflags,Tflags,PaletteBankID
                    DL.append(
                        (
                            "G_LoadTextureBlock4BCI",
                            (
                                Dat[2],
                                int(Tname, 16),
                                Dat[4],
                                Dat[5],
                                mat.colprop.Tex1TFlags,
                                mat.colprop.Tex1SFlags,
                                int(Pname, 16),
                            ),
                        )
                    )
                    self.Img_Refs.append(self.TempDLoff + 16 + len(DL) * 8)
                    self.Img_Refs.append(self.TempDLoff + 48 + len(DL) * 8)
                    self.TempDLoff += 96
                else:
                    if Palette:
                        DL.append(("G_SETOTHERMODE_H", ("G_MDSFT_TEXTLUT", 0)))
                        Palette = 0
                    if Dat[3] == 4:
                        self.Img_Refs.append(self.TempDLoff + len(DL) * 8)
                        # type,BankID,height,width,Sflags,Tflags
                        DL.append(
                            (
                                "G_LoadTextureBlock4B",
                                (
                                    Dat[2],
                                    int(Tname, 16),
                                    Dat[4],
                                    Dat[5],
                                    mat.colprop.Tex1TFlags,
                                    mat.colprop.Tex1SFlags,
                                ),
                            )
                        )
                        self.TempDLoff += 48
                    elif Dat[3] == 8:
                        self.Img_Refs.append(self.TempDLoff + len(DL) * 8)
                        # type,BankID,height,width,Sflags,Tflags
                        DL.append(
                            (
                                "G_LoadTextureBlock8B",
                                (
                                    Dat[2],
                                    int(Tname, 16),
                                    Dat[4],
                                    Dat[5],
                                    mat.colprop.Tex1TFlags,
                                    mat.colprop.Tex1SFlags,
                                ),
                            )
                        )
                        self.TempDLoff += 48
                    else:
                        self.Img_Refs.append(self.TempDLoff + len(DL) * 8)
                        # type,depth,BankID,height,width,Sflags,Tflags
                        DL.append(
                            (
                                "G_LoadTextureBlock",
                                (
                                    Dat[2],
                                    Dat[3],
                                    int(Tname, 16),
                                    Dat[4],
                                    Dat[5],
                                    mat.colprop.Tex1TFlags,
                                    mat.colprop.Tex1SFlags,
                                ),
                            )
                        )
                        self.TempDLoff += 48
            else:
                for j, d in enumerate(Dat):
                    Tname = "0x{0:04X}{1:04X}".format(int(d[0]), int(d[1]))
                    if Palette:
                        DL.append(("G_SETOTHERMODE_H", ("G_MDSFT_TEXTLUT", 0)))
                        Palette = 0
                    if d[3] == 4:
                        self.Img_Refs.append(self.TempDLoff + len(DL) * 8)
                        # type,BankID,height,width,Sflags,Tflags
                        DL.append(
                            (
                                "G_LoadTextureBlock4B_Tile",
                                (
                                    d[2],
                                    int(Tname, 16),
                                    d[4],
                                    d[5],
                                    mat.colprop.Tex1TFlags,
                                    mat.colprop.Tex1SFlags,
                                    j,
                                    j * 256,
                                ),
                            )
                        )
                        self.TempDLoff += 48
                    elif d[3] == 8:
                        self.Img_Refs.append(self.TempDLoff + len(DL) * 8)
                        # type,BankID,height,width,Sflags,Tflags
                        DL.append(
                            (
                                "G_LoadTextureBlock8B_Tile",
                                (
                                    d[2],
                                    int(Tname, 16),
                                    d[4],
                                    d[5],
                                    mat.colprop.Tex1TFlags,
                                    mat.colprop.Tex1SFlags,
                                    j,
                                    j * 256,
                                ),
                            )
                        )
                        self.TempDLoff += 48
                    else:
                        self.Img_Refs.append(self.TempDLoff + len(DL) * 8)
                        # type,depth,BankID,height,width,Sflags,Tflags
                        DL.append(
                            (
                                "G_LoadTextureBlock_Tile",
                                (
                                    d[2],
                                    d[3],
                                    int(Tname, 16),
                                    d[4],
                                    d[5],
                                    mat.colprop.Tex2TFlags,
                                    mat.colprop.Tex2SFlags,
                                    j,
                                    j * 256,
                                ),
                            )
                        )
                        self.TempDLoff += 48
        self.LastTexture = i
        self.LastTexture2 = i2
        return Palette

    def InitDL(self):
        setattr(self, "Display_List_%d" % self.MeshCnt, [])
        DL = getattr(self, "Display_List_%d" % self.MeshCnt)
        self.MeshCnt += 1
        DL.append(("G_RDPPIPESYNC", ()))
        DL.append(("G_TEXTURE", (0, 0, 2, -1, -1)))
        self.TempDLoff = self.DLoff
        return DL

    def EndDL(self, DL, Layout, Palette):
        if Palette:
            DL.append(("G_SETOTHERMODE_H", ("G_MDSFT_TEXTLUT", 0)))
        if self.LCP[0]:
            DL.append(("G_SETOTHERMODE_L", ("G_MDSFT_ALPHACOMPARE", "G_AC_NONE")))
        if not self.LCP[1]:
            DL.append(("G_SETOTHERMODE_H", ("G_MDSFT_TEXTFILT", 0x2000)))
        if self.LCP[2]:
            DL.append(("G_SETOTHERMODE_H", ("G_MDSFT_TEXTLOD", 0)))
        if self.LCP[3]:
            DL.append(("G_SETOTHERMODE_H", ("G_MDSFT_CYCLETYPE", 0)))
        DL.append(("G_RDPPIPESYNC", ()))
        DL.append(("G_TEXTURE", (0, 0, 0, 0, 0)))
        DL.append(("G_GEOMETRYMODE", ("None", "G_ZBUFFER|G_LIGHTING")))
        DL.append(("G_SETOTHERMODE_L", ("Render_Mode", "G_RM_AA_ZB_OPA_SURF")))
        DL.append(("G_ENDDL", ()))
        Layout[4] = 67108896 + self.DLoff
        self.DLoff += self.TempDLoff - self.DLoff
        self.DLoff += len(DL) * 8
        self.TempDLoff = self.DLoff

    def WriteLayout(self, obj, Lnum):
        # Write layout properties of mesh
        # This is mostly just world offset of stuff
        Layout = getattr(self, "Layout_%d" % (Lnum))
        depth = self.root
        q = obj
        while True:
            if q.parent:
                q = q.parent
                depth += 1
            else:
                break
        Layout[2] = depth
        loc = obj.location
        loc = [round(a, 4) for a in loc]
        s = bpy.context.scene.my_tool.Scale
        Layout[8] = (loc[0] * s, loc[2] * s, -loc[1] * s)
        rot = [obj.rotation_euler[0], obj.rotation_euler[2], obj.rotation_euler[1]]
        rot[0] += math.radians(-90)
        rot = [round(a, 5) for a in rot]
        Layout[0x14] = tuple(rot)
        scale = obj.scale
        scale = [round(a, 4) for a in scale]
        Layout[0x20] = tuple(scale)
        return Layout


# ------------------------------------------------------------------------
#    Exorter Functions
# ------------------------------------------------------------------------


def Dec2Compliment(num):
    if num < -32768:
        num = num % -32768
    if num > 32767:
        num = num % 32767
    return unpack(">H", pack(">h", num))[0]


def RotateObj(deg, obj):
    angle = obj.matrix_world.copy()
    angle.identity()
    angle = angle.to_euler()
    angle[0] = math.radians(-deg)
    r = obj.matrix_world.to_3x3()
    r.rotate(angle)
    t = obj.matrix_world.to_translation()
    r = r.to_4x4()
    I = r.copy()
    I.identity()
    # translation function removes other transformations
    obj.matrix_world = r + r.Translation(t) - I


def Export_LS(my_tool, name):
    block = LS()
    block.file(name)
    objects = bpy.context.scene.objects
    Meshes = []
    Curves = []
    Cameras = []
    Ents = []
    # Setup object types
    for obj in objects:
        if obj.type == "MESH":
            if obj.objprop.Collision:
                Meshes.append(obj)  # get the mesh from object
        if obj.type == "CAMERA":
            Cameras.append(obj)
        if obj.type == "CURVE":
            Curves.append(obj)
        if obj.type == "EMPTY":
            if obj.objprop.Export_Ent:
                Ents.append(obj)
    if not len(Meshes) or not len(Curves) or not len(Cameras):
        raise Exception("You need a camera, mesh and curve to export data")
    if len(Curves) != len(Cameras):
        raise Exception("You need an equal number of curves and cameras")
    Voff = 0
    for obj in Meshes:
        # apply world transformation
        obj.data.transform(obj.matrix_world)
        obj.matrix_world.identity()
        obj.objprop.BreakIndex = 0
        # Rotate model by 90 degrees
        RotateObj(90, obj)
        # apply scale
        scale = my_tool.Scale
        obj.matrix_world *= scale
        # apply world transformation
        obj.data.transform(obj.matrix_world)
        obj.matrix_world.identity()
        # tesselate
        obj.data.calc_loop_triangles()
        # make vertex list
        Voff = block.WriteCol(obj, Voff)
        # reset rotation
        RotateObj(-90, obj)
        # unapply world transformation
        obj.matrix_world *= 1 / scale
        obj.data.transform(obj.matrix_world)
        obj.matrix_world.identity()
    block.BuildTriCells()
    # write col header
    block.ColHeader()
    # setupnode
    block.Node.Node_Header[0x0] = len(Curves)
    for obj in Curves:
        # apply world transformation
        obj.data.transform(obj.matrix_world)
        obj.matrix_world.identity()
        # Rotate model by 90 degrees
        RotateObj(90, obj)
        # apply scale
        obj.matrix_world *= scale
        # apply world transformation
        obj.data.transform(obj.matrix_world)
        obj.matrix_world.identity()
        # only take first spline, multiple splines is messy
        spl = obj.data.splines[0]
        block.Node.SetNode(obj.data.nodeprop, spl)
        # reset rotation
        RotateObj(-90, obj)
        # unapply world transformation
        obj.matrix_world *= 1 / scale
        obj.data.transform(obj.matrix_world)
        obj.matrix_world.identity()
    block.Node.SetNodeDistances(len(Curves))
    for cam in Cameras:
        block.Node.SetCam(cam)
    for i, e in enumerate(Ents):
        block.AddEntity(e, i)
    block.Node.WriteOffsets(block.Main_Header)
    return block


def Export_PGB(my_tool, name):
    block = GB()
    block.file(name)
    objects = bpy.context.scene.objects
    Meshes = []
    for obj in objects:
        if obj.type == "EMPTY":
            if not obj.objprop.Export_Ent:
                Meshes.append(obj)
        elif obj.type == "MESH":
            if obj.objprop.Collision == False:
                Meshes.append(obj)
    if not len(Meshes):
        raise Exception("You need a gfx mesh to export")
    root = -1
    for m in Meshes:
        if not m.parent:
            root += 1
    if root == -1:
        raise "There must be at least one graphics object with no parent"
    if root >= 1:
        empty = 1
    else:
        empty = 0
    Meshes.sort(key=(lambda x: x.objprop.Layer))
    block.CreateLayouts(len(Meshes), root)
    tex = ImportMod("LS", my_tool, "\\Texture_ADDR")
    for i, obj in enumerate(Meshes):
        # do data stuff
        block.WriteMesh(obj, tex, i + empty)
    block.WriteOffsets(len(Meshes) + empty)
    block.write()
    # now write new Texture_ADDR
    file = open(my_tool.ROM_path + "\\Texture_ADDR.py", "w")
    labels = [a for a in tex.__dict__ if a.startswith("Texture")]
    attrs = [getattr(tex, a) for a in labels]
    [file.write(a[0] + " = " + str(a[1]) + "\n") for a in zip(labels, attrs)]
    file.close()
    del file


def AddEdit(my_tool, edit):
    E = ImportMod("LS", my_tool, "\\Edited_Files")
    EE = E.Edited
    if edit not in EE:
        EE.append(edit)
    f = open(my_tool.ROM_path + "\\Edited_Files.py", "w")
    f.write("Edited =")
    f.write(str(EE))
    f.close()
    del E


def UpdateMusicSky():
    music = bpy.context.scene.my_tool.Music_ID
    sky = bpy.context.scene.my_tool.Skybox_ID
    sys.path.insert(1, bpy.context.scene.my_tool.ROM_path)
    tables = importlib.import_module("KCS_Main_Stage_Table")
    MS = getattr(
        tables,
        "World_%d_Level_%d_Area_%d"
        % (
            bpy.context.scene.my_tool.World,
            bpy.context.scene.my_tool.Level,
            bpy.context.scene.my_tool.Area,
        ),
    )
    MS[8] = (sky,)
    MS[12] = (music,)
    labels = [a for a in dir(tables) if not a.startswith("__")]
    attrs = [getattr(tables, a) for a in labels]
    file = open(bpy.context.scene.my_tool.ROM_path + "\\KCS_Main_Stage_Table.py", "w")
    file.write(
        """# MainStageStruct={
    # 0x00:['geo_block_a','>2H',4],
    # 0x04:['geo_block_b','>2H',4],
    # 0x08:['skybox_id','>H',2],
    # 0x0A:['color','>H',2],
    # 0x0C:['music_id','>L',4],
    # 0x10:['level_block','>2H',4],
    # 0x14:['Death Cutscene','>H',2],
    # 0x16:['Cutsecene related','>H',2],
    # 0x18:['dust_block','>2H',4],
    # 0x1C:['dust_img','>2H',4],
    # 0x20:['area_name','>L',4],
    # }
    """
        + "\n"
    )
    [file.write(a[0] + " = " + str(a[1]) + "\n") for a in zip(labels, attrs)]
    file.close()


# ------------------------------------------------------------------------
#    Importer
# ------------------------------------------------------------------------


def MakeColOjbect(my_tool, j, v, k, F):
    # make empty mesh
    mesh = bpy.data.meshes.new(
        "KCS_%d_%d_%d_Col_%d" % (my_tool.ImpWorld, my_tool.ImpLevel, my_tool.ImpArea, j)
    )
    mesh.from_pydata(v, [], k)
    # Not going to deal with mats until I figure that out
    mesh.update(calc_edges=True)
    # make data
    obj = bpy.data.objects.new(
        "KCS_%d_%d_%d_LS_%d" % (my_tool.ImpWorld, my_tool.ImpLevel, my_tool.ImpArea, j),
        mesh,
    )
    s = bpy.context.scene
    s.collection.objects.link(obj)
    RotateObj(-90, obj)
    # making it collision
    obj.objprop.Collision = True
    # assigning material types
    polys = obj.data.polygons
    # dict with type settings tuple as key, value of [mat,id]
    mats = {}
    i = 0
    for p, f in zip(polys, F):
        NormType = f[2]
        ColType = f[7]
        ColParam = f[6]
        if ColType == 8:
            WarpNum = f[3] + 1
        else:
            WarpNum = 0
        if (NormType, ColType, ColParam, WarpNum) in mats.keys():
            [M, id] = mats[(NormType, ColType, ColParam, WarpNum)]
        else:
            M = bpy.data.materials.new(name="KCS mat %d" % i)
            obj.data.materials.append(M)
            col = M.colprop
            col.NormType = NormType
            col.ColType = ColType
            col.ColParam = ColParam
            col.WarpNum = WarpNum
            M.diffuse_color = (
                min((0.3 + NormType / 5) / 1.55, 1),
                min((0.2 + ColType * 0.6) / 2, 1),
                min(0.3 + ColParam / 9, 0.9),
                1,
            )
            mats[(NormType, ColType, ColParam, WarpNum)] = [M, i]
            id = i
            i += 1
        p.material_index = id


def ImportMod(type, my_tool, name):
    if (my_tool.ROM_path + "\\" + name[: name.rfind("\\")]) not in sys.path:
        sys.path.insert(1, my_tool.ROM_path + "\\" + name[: name.rfind("\\")])
    q = name.split("\\")
    a = importlib.import_module(q[-1])
    importlib.reload(a)
    return a


def GetLevelBankID(World, Level, Area, my_tool):
    name = "World_%d_Level_%d_Area_%d" % (World, Level, Area)
    if my_tool.ROM_path not in sys.path:
        sys.path.insert(1, my_tool.ROM_path)
    MST = importlib.import_module("KCS_Main_Stage_Table")
    st = getattr(MST, name)
    return [st[0], st[4], st[16]]


def ImportLS(my_tool, name):
    LS = ImportMod("LS", my_tool, name)
    verts = LS.Vertices[1:]
    if 0x9999 in verts:
        verts.remove(0x9999)
    # scale verts
    FV = []
    for a in verts:
        V = pack(">3H", *a)
        V = list(unpack(">3h", V))
        V = [a * my_tool.ImpScale for a in V]
        FV.append(tuple(V))
    faces = LS.Triangles[1:]
    DGs = {}
    DGT = {}
    tris = []
    triT = []
    for i, t in enumerate(faces):
        if t[7] == 9:
            if t[3] not in DGs:
                DGs[t[3]] = []
                DGT[t[3]] = []
            DGs[t[3]].append([a - 1 for a in t[0]])
            DGT[t[3]].append(t)
        else:
            tris.append([a - 1 for a in t[0]])
            triT.append(t)
    Vs = {}
    for k, v in DGs.items():
        Vs[k] = []
        for i, t in enumerate(v):
            Vs[k].extend([FV[a] for a in t])
            DGs[k][i] = list(range(i * 3, i * 3 + 3, 1))
    TVs = []
    for i, t in enumerate(tris):
        TVs.extend([FV[a] for a in t])
        tris[i] = list(range(i * 3, i * 3 + 3, 1))
    for i, [k, v, F] in enumerate(zip(DGs.values(), Vs.values(), DGT.values())):
        MakeColOjbect(my_tool, i + 1, v, k, F)
    MakeColOjbect(my_tool, 0, TVs, tris, triT)
    s = bpy.context.scene
    # Now Splines
    num = LS.Node_Header[0]
    for i in range(num):
        # Make Curve
        CurveData = bpy.data.curves.new("curve %d" % (i + 1), type="CURVE")
        CurveData.dimensions = "3D"
        P = CurveData.splines.new("POLY")
        points = getattr(LS, "Path_Node_Matrix_%d" % i)
        # Assign points of curve
        footer = getattr(LS, "Path_Node_Footer_%d" % i)
        P.points.add(footer[2] - 1)
        for k in range(footer[2]):
            p = points[k]
            co = list(p)
            co = [a * my_tool.ImpScale for a in co]
            P.points[k].co = (co[0], co[1], co[2], 1)
        Curve = bpy.data.objects.new("KCS Node %d" % (i + 1), CurveData)
        # if an unk node exists make that a curve
        if hasattr(LS, "Path_Node_Unk_%d" % i):
            PU = getattr(LS, "Path_Node_Unk_%d" % i)
            CUnk = bpy.data.curves.new("Unk curve %d" % (i + 1), type="CURVE")
            CUnk.dimensions = "3D"
            Po = CUnk.splines.new("POLY")
            Po.points.add(len(PU))
            for q, [p, k] in enumerate(zip(PU, points[1:])):
                co = [a * my_tool.ImpScale for a in p]
                cop = [a * my_tool.ImpScale for a in k]
                Po.points[q].co = (co[0] + cop[0], co[1] + cop[1], co[2] + cop[2], 1)
            CU = bpy.data.objects.new("UNK KCS Node %d" % (i + 1), CUnk)
            s.collection.objects.link(CU)
            RotateObj(-90, CU)
        # Setup Properties of curved
        NP = Curve.data.nodeprop
        KN = getattr(LS, "Kirby_Node_Data_%d" % i)
        NP.NodeNum = i + 1
        if KN[0xE] & 1:
            NP.EnableWarp = True
            W = list(KN[4][0:3])
            W = [a + 1 for a in W]
            NP.Warp = tuple(W)
            NP.WarpDir = KN[4][3]
        NC = getattr(LS, "Node_Connector_%d" % i)
        if len(NC) == 1:
            if NC[0][0]:
                NP.LockBackward = True
            NP.NextNode = NC[0][2] + 1
            NP.PrevNode = NC[0][2] + 1
            if NC[0][3]:
                NP.LockForward = True
        else:
            if not NC[1][0]:
                NP.LockForward = True
            if NC[0][0]:
                NP.LockBackward = True
            NP.NextNode = NC[1][2] + 1
            NP.PrevNode = NC[0][2] + 1
        # make object show up in scene
        s.collection.objects.link(Curve)
        RotateObj(-90, Curve)

        # Make camera and give properties
        CamD = bpy.data.cameras.new("camera %d" % (i + 1))
        CamO = bpy.data.objects.new("KCS camera %d" % (i + 1), CamD)
        Cdata = getattr(LS, "Camera_Node_Data_%d" % i)
        Camlens_unit = "FOV"
        CamD.clip_start = (Cdata[0x18][0]) / 1000
        CamD.clip_end = (Cdata[0x1C][0]) / 12.8
        cp = CamD.camprop
        cp.AxisLocks = [1 == Cdata[2][0], 1 == Cdata[3][0], 1 == Cdata[4][0]]
        cp.Foc = (Cdata[12][0], Cdata[16][0], Cdata[20][0])
        cp.Pitch = Cdata[0x40]
        cp.Yaw = Cdata[0x28]
        cp.CamXBound = Cdata[0x48]
        cp.CamYBound = Cdata[0x50]
        cp.CamZBound = Cdata[0x58]
        cp.CamYFocBound = Cdata[0x60]
        cp.CamHFocBound = Cdata[0x68]
        cp.PanH = Cdata[8] == 1
        cp.PanUpDown = Cdata[6] == 0
        cp.PanDown = Cdata[7] == 0
        cp.ProfileView = Cdata[0] == 1
        cp.NodeNum = i + 1
        s.collection.objects.link(CamO)
        RotateObj(-90, CamO)
        CamO.location = P.points[0].co[:-1]
        CamO.location[1] += -3
    # Now add entities
    x = 0
    while True:
        try:
            ent = getattr(LS, "Entity_ID_%d" % x)
            type = str(ent[1][0]) + "," + str(ent[2][0])
            # print(type)
            loc = (
                ent[8][0] * my_tool.ImpScale,
                -ent[8][2] * my_tool.ImpScale,
                ent[8][1] * my_tool.ImpScale,
            )
            scale = (ent[0x20][0], -ent[0x20][2], ent[0x20][1])
            E = bpy.data.objects.new("Empty", None)
            s.collection.objects.link(E)
            E.location = loc
            E.scale = scale
            E.objprop.Entity = type
            E.objprop.NodeNum = ent[0][0] - 1
            E.objprop.Action = ent[3][0]
            E.objprop.Respawn = ent[4][0] == 1
            E.objprop.Eep = ent[6][0]
            # print(E.objprop.Entity)
            x += 1
        except:
            break
    del LS


def ImportGB(my_tool, type, name):
    GB = ImportMod(type, my_tool, name)
    RM = GB.Geometry_Block_Header[8]
    x = 0
    # deal with layout offsets, display lists are sorted by hex position in the rom.
    Layout = []
    # mats will be a list of tuples with index one being the mat, and index 2 being the triangle start.
    # upon having any propery set, make mat.colprop.GfxUser True
    # use this function which makes a fast3d mat using fast64 then sets it up for my use
    mats = [[CreateMat(name + " Gfx 0"), 0]]
    Objects = []
    Tn = 0
    # for each mesh, I'll keep a list of the UVs because the from pydata method does not support UVs for some reason
    # same for vertex colors
    UVs = []
    VCs = []
    if RM != 0x13 or RM != 0x14:
        while True:
            try:
                L = getattr(GB, "Layout_%d" % x)
                Layout.append([L, x])
                x += 1
            except:
                break
    EP = []
    if RM == 0x18 or RM == 0x14 or RM == 0x1C:
        x = 0
        while True:
            try:
                EP.append(getattr(GB, "Entry_Point_%d" % x))
                x += 1
            except:
                break
    if RM == 0x1B:
        x = 0
        while True:
            try:
                EP.append(getattr(GB, "Display_List_Pair_%d" % x))
                x += 1
            except:
                break
    x = 0
    try:
        map = getattr(GB, "DLmap")
    except:
        map = list(range(len(Layout)))
    s = bpy.context.scene
    depths = list(range(12))
    while True:
        try:
            if x > len(Layout):
                break
            [DLL, L, EP] = GetDLFromLayout(Layout, EP, x, RM, GB, map)
            if not DLL:
                depth = L[2] & 0xFF
                if depth == 0x12:
                    break
                E = bpy.data.objects.new("Empty", None)
                s.collection.objects.link(E)
                E.objprop.Export_Ent = False
                depths[depth] = E
                Objects.append([E, 0])
                UVs.append([])
                VCs.append([])
                # deal with parenting
                if depth != 0:
                    p = depths[depth - 1]
                    p.select_set(True)
                    E.select_set(True)
                    bpy.context.view_layer.objects.active = p
                    bpy.ops.object.parent_set()
                    p.select_set(False)
                    E.select_set(False)
                    E.matrix_world = p.matrix_world
                loc = Vector([L[8][0], -L[8][2], L[8][1]]) * my_tool.ImpScale
                E.location += loc
                for i, scale in enumerate(L[0x20]):
                    E.scale[i] *= scale
                E.name = name + "_Mesh_%d" % x
                rot = list(L[0x14])
                if depth == 0:
                    rot[0] = rot[0] - math.radians(-90)
                rot = Euler(rot)
                E.rotation_euler.rotate(rot)
                x += 1
                continue
            for DL in DLL:
                FV = []
                prev = 0
                prevprev = 0
                offset = 0
                Tris = []
                [
                    UVs,
                    VCs,
                    Objects,
                    mats,
                    FV,
                    Tris,
                    prev,
                    prevprev,
                    offset,
                    Tn,
                    depths,
                ] = MakeObjectFromDL(
                    UVs,
                    VCs,
                    Objects,
                    mats,
                    DL,
                    L,
                    RM,
                    x,
                    FV,
                    Tris,
                    prev,
                    prevprev,
                    offset,
                    1,
                    GB,
                    map,
                    my_tool,
                    Tn,
                    name,
                    1,
                    depths,
                )
            x += 1
        except:
            break
    # assign materials to triangles
    ind = 0
    MeshOff = 0
    if len(Objects) == 0:
        raise Exception("No Display lists or Verts in Model")
    for o, u, vc in zip(Objects, UVs, VCs):
        if not o[1]:
            continue
        o = o[0]
        tris = o.data.polygons
        Oind = 0
        map = o.data.uv_layers.new()
        Vcol = o.data.vertex_colors.new()
        V = o.data.vertices
        for i, t in enumerate(tris):
            if ind == len(mats):
                # all mats are applied
                break
            if i >= mats[ind][1] - MeshOff:
                ind += 1
                Oind += 1
                o.data.materials.append(mats[ind - 1][0])
            t.material_index = Oind - 1
            im = mats[ind - 1][0].node_tree.nodes.get("Image Texture").image
            if not im:
                WH = (32, 32)
            else:
                WH = im.size
            # UV data
            for v, l in zip(t.vertices, t.loop_indices):
                uv = u[v]
                uv = pack(">2H", *uv)
                uv = list(unpack(">2h", uv))
                # scale verts
                uv = [
                    a * 0.001 * (32 / b) if b > 0 else a * 0.001 * 32
                    for a, b in zip(uv, WH)
                ]
                map.data[l].uv = tuple(uv)
                Col = list(vc[v])
                Col = [a / 255 for a in Col]
                Vcol.data[l].color = Col
        MeshOff += len(tris)
    del GB


def MakeObjectFromDL(
    UVs,
    VCs,
    Objects,
    mats,
    DL,
    L,
    RM,
    x,
    FV,
    Tris,
    prev,
    prevprev,
    offset,
    new,
    GB,
    map,
    my_tool,
    Tn,
    name,
    ret,
    depths,
):
    if new:
        UVs.append([])
        VCs.append([])
    # R is a list mapping the vert buffer, to positions in the vert list.
    R = [0 for a in range(32)]
    for cmd in DL:
        # print(cmd)
        M = mats[-1][0]
        # set material properties
        if cmd[0] == "G_GEOMETRYMODE":
            M.colprop.GfxUser = True
            clr = cmd[1][0]
            set = cmd[1][1]
            try:
                clr = clr.split("|")
                set = set.split("|")
                mc = M.colprop
                if clr[0] != "All":
                    for c in clr:
                        mc[c] = False
                else:
                    mc["G_ZBUFFER"] = False
                    mc["G_SHADE"] = False
                    mc["G_CULL_FRONT"] = False
                    mc["G_CULL_BACK"] = False
                    mc["G_FOG"] = False
                    mc["G_LIGHTING"] = False
                    mc["G_TEXTURE_GEN"] = False
                    mc["G_TEXTURE_GEN_LINEAR"] = False
                    mc["G_SHADING_SMOOTH"] = False
                    mc["G_CLIPPING"] = False
                if set[0] != "Clear":
                    for s in set:
                        mc[s] = True
                M.node_tree.nodes["LIGHTING"].outputs[0].default_value = (
                    M.colprop.G_LIGHTING & 1
                )
            except:
                pass
        if cmd[0] == "G_SETCOMBINE":
            mc = M.colprop
            mc.GfxUser = True
            # my shader don't support this yet
            col = cmd[1]
            a = col
            # AAAAAAAAAAAAAAAAAAAAAAAA
            mc.ACmode1 = a[0]
            mc.BCmode1 = a[1]
            mc.CCmode1 = a[2]
            mc.DCmode1 = a[3]
            mc.AAmode1 = a[4]
            mc.BAmode1 = a[5]
            mc.CAmode1 = a[6]
            mc.DAmode1 = a[7]
            mc.ACmode2 = a[8]
            mc.BCmode2 = a[9]
            mc.CCmode2 = a[10]
            mc.DCmode2 = a[11]
            mc.AAmode2 = a[12]
            mc.BAmode2 = a[13]
            mc.CAmode2 = a[14]
            mc.DAmode2 = a[15]
            SetupCombiner(M.node_tree, a, M)

        if cmd[0] == "G_TEXTURE":
            M.colprop.GfxUser = True
            # my shader won't support this

        if cmd[0] == "G_SETTIMG":
            M.colprop.GfxUser = True
            tex = GetTexture(cmd[1][2], my_tool)
            TexNode = M.node_tree.nodes.get("Image Texture")
            TexNode.image = tex
            M.colprop.Tex = tex
        if cmd[0] == "G_SETOTHERMODE_L":
            M.colprop.GfxUser = True
            try:
                if "G_MDSFT_ALPHACOMPARE" in cmd[1][0]:
                    if "G_AC_DITHER" in cmd[1][1]:
                        M.colprop.AlphaCompare = True
                    elif "G_AC_NONE" in cmd[1][1]:
                        M.colprop.AlphaCompare = False
                if "Render_Mode" in cmd[1][0]:
                    OL = cmd[1][1]
                    M.colprop.OtherMode_L = OL
                    if "OPA" in OL:
                        M.blend_method = "OPAQUE"
                    if "XLU" in OL:
                        M.blend_method = "BLEND"
                    if "TEX_EDGE" in OL:
                        M.blend_method = "CLIP"
            except:
                pass
        if cmd[0] == "G_SETOTHERMODE_H":
            M.colprop.GfxUser = True
            if type(cmd[1][0]) == str:
                if "G_MDSFT_CYCLETYPE" in cmd[1][0]:
                    if cmd[1][1] & 1048576 != 0:
                        M.colprop.TwoCycle = True
                        colprop = M.colprop
                        INS = [
                            colprop.ACmode1,
                            colprop.BCmode1,
                            colprop.CCmode1,
                            colprop.DCmode1,
                            colprop.AAmode1,
                            colprop.BAmode1,
                            colprop.CAmode1,
                            colprop.DAmode1,
                            colprop.ACmode2,
                            colprop.BCmode2,
                            colprop.CCmode2,
                            colprop.DCmode2,
                            colprop.AAmode2,
                            colprop.BAmode2,
                            colprop.CAmode2,
                            colprop.DAmode2,
                        ]
                        SetupCombiner(M.node_tree, INS, M)
                    else:
                        M.colprop.TwoCycle = False
                if "G_MDSFT_TEXTFILT" in cmd[1][0]:
                    if cmd[1][1] & 8192 != 0:
                        M.colprop.TextFilt = True
                    else:
                        M.colprop.TextFilt = False
                if "G_MDSFT_TEXTLOD" in cmd[1][0]:
                    if cmd[1][1] & 10000 != 0:
                        M.colprop.TextLOD = True
                    else:
                        M.colprop.TextLOD = False
        if cmd[0] == "G_SETTILE":
            M.colprop.GfxUser = True
            # my shader don't support this yet
            TexNode = M.node_tree.nodes.get("Image Texture")
            # don't know how to do mirror :\
            # if 'mirror' in cmd[1][6]:
            #    TexNode.extension = 'CHECKER'
            if "clamp" in cmd[1][6]:
                TexNode.extension = "EXTEND"
            elif "wrap" in cmd[1][6]:
                TexNode.extension = "REPEAT"

        if cmd[0] == "G_SETPRIMCOLOR":
            M.colprop.GfxUser = True
            # I don't distinguish between prim and env, so whatever happens to be
            # set last is what you'll see
            node = M.node_tree.nodes.get("PRIM")
            color = list(cmd[1][2:6])
            color = [a / 255 for a in color]
            M.node_tree.nodes.get("PRIM").outputs[0].default_value = tuple(color)
            M.node_tree.nodes.get("PRIMA").outputs[0].default_value = color[3]
            M.colprop.Prim = color[:3]
            M.colprop.PrimA = color[3]
        if cmd[0] == "G_SETENVCOLOR":
            M.colprop.GfxUser = True
            # I don't distinguish between prim and env, so whatever happens to be
            # set last is what you'll see
            color = list(cmd[1])
            color = [a / 255 for a in color]
            M.node_tree.nodes.get("ENV").outputs[0].default_value = tuple(color)
            M.node_tree.nodes.get("ENVA").outputs[0].default_value = color[3]
            M.colprop.Env = color[:3]
            M.colprop.EnvA = color[3]
        # VERT BUFFER
        if cmd[0] == "G_VTX":
            start = (cmd[1][2] - 67108896) // 16
            Verts = GB.GeoVertices[start : start + cmd[1][0]]
            V = [a[0] for a in Verts]
            UVs[-1].extend([a[2] for a in Verts])
            VCs[-1].extend([a[3] for a in Verts])
            # convert to signed decimal
            for v in V:
                v = pack(">3H", *v)
                v = list(unpack(">3h", v))
                # scale verts
                v = [a * my_tool.ImpScale for a in v]
                FV.append(tuple(v))
                del v
            offset += prev
            prev = cmd[1][0]  # to offset triangle indices
            # fix range
            for i, r in enumerate(R):
                # update range
                if i >= cmd[1][1] and i <= cmd[1][1] + cmd[1][0] - 1:
                    R[i] = offset - cmd[1][1]
        if cmd[0] == "G_CULLDL":
            FV = FV[:-8]
            UVs[-1] = UVs[-1][:-8]
            VCs[-1] = VCs[-1][:-8]
            offset -= 8
        if cmd[0] == "G_DL":
            ret = cmd[1][0]
            DL = cmd[1][1]
            try:
                DL = getattr(GB, "Display_List_%d" % (map.index(DL)))
                [
                    UVs,
                    VCs,
                    Objects,
                    mats,
                    FV,
                    Tris,
                    prev,
                    prevprev,
                    offset,
                    Tn,
                    depths,
                ] = MakeObjectFromDL(
                    UVs,
                    VCs,
                    Objects,
                    mats,
                    DL,
                    L,
                    RM,
                    x,
                    FV,
                    Tris,
                    prev,
                    prevprev,
                    offset,
                    0,
                    GB,
                    map,
                    my_tool,
                    Tn,
                    name,
                    1 - ret,
                    depths,
                )
            except:
                ret = 1
        if cmd[0] == "G_MOVEWORD":
            if cmd[1][0] == "G_MV_LIGHTCOL":
                color = (cmd[1][2]).to_bytes(4, "big")
                if cmd[1][1] == 0:
                    M.colprop.Ambient = color[:3]
                elif cmd[1][1] == 24:
                    M.colprop.Diffuse = color[:3]
                M.node_tree.nodes["AMBIENT"].outputs[0].default_value = color
                M.node_tree.nodes["DIFFUSE"].inputs[0].default_value = color
        # TRIANGLES
        if FV:
            if cmd[0] == "G_TRI2":
                mats = resolvemats(mats, Tn, my_tool, name)
                indices = list(cmd[1])
                indices = [a + R[a] for a in indices]
                Tris.append(tuple(indices[0:3]))
                Tris.append(tuple(indices[3:6]))
                Tn += 2
            if cmd[0] == "G_TRI1":
                mats = resolvemats(mats, Tn, my_tool, name)
                indices = list(cmd[1])
                indices = [a + R[a] for a in indices]
                Tris.append(tuple(indices))
                Tn += 1
    if Tris and ret:
        # make empty mesh
        mesh = bpy.data.meshes.new("Display_List_%d_Data" % x)
        mesh.from_pydata(FV, [], Tris)
        mesh.validate()
        # Not going to deal with mats until I figure that out
        mesh.update(calc_edges=True)
        # make data
        obj = bpy.data.objects.new(name + "_Mesh_%d" % x, mesh)
        s = bpy.context.scene
        s.collection.objects.link(obj)
        depth = L[2] & 0xFF
        depths[depth] = obj
        # deal with parenting
        if depth != 0:
            p = depths[depth - 1]
            p.select_set(True)
            obj.select_set(True)
            bpy.context.view_layer.objects.active = p
            bpy.ops.object.parent_set()
            p.select_set(False)
            obj.select_set(False)
            obj.matrix_world = p.matrix_world
        loc = Vector([L[8][0], -L[8][2], L[8][1]]) * my_tool.ImpScale
        obj.location += loc
        for i, scale in enumerate(L[0x20]):
            obj.scale[i] *= scale
        rot = list(L[0x14])
        if depth == 0:
            rot[0] = rot[0] - math.radians(-90)
        rot = Euler(rot)
        obj.rotation_euler.rotate(rot)
        Objects.append([obj, 1])
    elif ret:
        UVs.pop(-1)
        VCs.pop(-1)
    return [UVs, VCs, Objects, mats, FV, Tris, prev, prevprev, offset, Tn, depths]


def GetDLFromLayout(Layout, EP, x, RM, GB, map):
    # switch case in python :)
    if RM == 0x17:
        L = Layout[x][0]
        if L[4]:
            DL = getattr(GB, "Display_List_%d" % map.index(L[4]))
        else:
            return [0, L, EP]
        return [[DL], L, EP]
    elif RM == 0x18:
        L = Layout[x][0]
        if L[4]:
            q = []
            for p in EP[0]:
                if p[1]:
                    DL = getattr(GB, "Display_List_%d" % map.index(p[1]))
                    q.append(DL)
            EP.pop(0)
        else:
            return [0, L, EP]
        return [q, L, EP]
    elif RM == 0x1B:
        L = Layout[x][0]
        if L[4]:
            q = []
            for p in EP:
                if p[0]:
                    DL = getattr(GB, "Display_List_%d" % map.index(p[0]))
                    q.append(DL)
                if p[1]:
                    DL = getattr(GB, "Display_List_%d" % map.index(p[1]))
                    q.append(DL)
        else:
            return [0, L, EP]
        return [q, L, EP]
    elif RM == 0x1C:
        L = Layout[x][0]
        if L[4]:
            q = []
            for p in EP[0]:
                if p[1]:
                    DL = getattr(GB, "Display_List_%d" % map.index(p[1]))
                    q.append(DL)
                if p[2]:
                    DL = getattr(GB, "Display_List_%d" % map.index(p[2]))
                    q.append(DL)
            EP.pop(0)
        else:
            return [0, L, EP]
        return [q, L, EP]
    elif RM == 0x13:
        DL = getattr(GB, "Display_List_%d" % 0)
        return [[DL], GetGenericLayout(), EP]
    elif RM == 0x14:
        q = []
        for p in EP[0]:
            if p[1]:
                DL = getattr(GB, "Display_List_%d" % map.index(p[1]))
                q.append(DL)
        return [q, GetGenericLayout(), EP, EP]


def GetGenericLayout():
    Layout = {
        0x0: 0x0,
        0x2: 0x0,
        0x4: 0x0,
        0x8: (0.0, 0.0, 0.0),
        0x14: (0.0, 0.0, 0.0),
        0x20: (1.0, 1.0, 1.0),
    }
    return Layout


def GetTexture(num, my_tool):
    index = num & 0xFFFF
    bank = num >> 16
    name = "Textures\\%d\\Bank_%d_Index_%d_Tex" % (bank, bank, index) + ".png"
    filestart = my_tool.ROM_path
    path = Path(filestart) / Path(name)
    name = repr(name)
    name = name.replace("'", "")
    if os.path.isfile(path):
        i = bpy.data.images.get(name.split("\\")[-1])
        if not i:
            i = bpy.data.images.load(filepath=str(path))
    else:
        i = bpy.data.images.get("Missing_Texture.png")
        if not i:
            missing = Path(filestart) / Path("Textures\\Missing_Texture.png")
            i = bpy.data.images.load(filepath=str(missing))
    return i


def UpdateEnt(objprop, context):
    id = objprop.Entity.split(",")
    objprop.BankNum = int(id[0])
    objprop.IndexNum = int(id[1])


def UpdateColor(colprop, context):
    if hasattr(context, "material"):
        mat = context.material
        mat.node_tree.nodes["PRIM"].outputs[0].default_value = [*colprop.Prim, 1.0]
        mat.node_tree.nodes["PRIMA"].outputs[0].default_value = colprop.PrimA
        mat.node_tree.nodes["ENV"].outputs[0].default_value = [*colprop.Env, 1.0]
        mat.node_tree.nodes["ENVA"].outputs[0].default_value = colprop.EnvA
        mat.node_tree.nodes["AMBIENT"].outputs[0].default_value = [
            *colprop.Ambient,
            1.0,
        ]
        mat.node_tree.nodes["DIFFUSE"].inputs[0].default_value = [*colprop.Diffuse, 1.0]
        mat.node_tree.nodes["Image Texture"].image = colprop.Tex
        mat.node_tree.nodes["Image Texture.001"].image = colprop.Tex2


def UpdateShade(colprop, context):
    if hasattr(context, "material"):
        mat = context.material
        mat.node_tree.nodes["LIGHTING"].outputs[0].default_value = (
            colprop.G_LIGHTING & 1
        )


def UpdateCC(colprop, context):
    if hasattr(context, "material"):
        mat = context.material
        INS = [
            colprop.ACmode1,
            colprop.BCmode1,
            colprop.CCmode1,
            colprop.DCmode1,
            colprop.AAmode1,
            colprop.BAmode1,
            colprop.CAmode1,
            colprop.DAmode1,
            colprop.ACmode2,
            colprop.BCmode2,
            colprop.CCmode2,
            colprop.DCmode2,
            colprop.AAmode2,
            colprop.BAmode2,
            colprop.CAmode2,
            colprop.DAmode2,
        ]
        SetupCombiner(mat.node_tree, INS, mat)


def UpdateOtherL(colprop, context):
    if hasattr(context, "material"):
        mat = context.material
        OL = colprop.OtherMode_L
        if "OPA" in OL:
            mat.blend_method = "OPAQUE"
        if "XLU" in OL:
            mat.blend_method = "BLEND"
        if "TEX_EDGE" in OL:
            mat.blend_method = "CLIP"


def ValidFmt(colprop, context):
    rgba = ["16", "32"]
    I = ["4", "8", "16"]
    CI = ["4", "8"]
    if colprop.Fmt == "RGBA":
        if not colprop.Depth in rgba:
            colprop.Depth = "16"
    if colprop.Fmt == "IA":
        if not colprop.Depth in I:
            colprop.Depth = "8"
    if colprop.Fmt == "I":
        if not colprop.Depth in I:
            colprop.Depth = "8"
    if colprop.Fmt == "CI":
        if not colprop.Depth in CI:
            colprop.Depth = "16"

    if colprop.Fmt2 == "RGBA":
        if not colprop.Depth2 in rgba:
            colprop.Depth2 = "16"
    if colprop.Fmt2 == "IA":
        if not colprop.Depth2 in I:
            colprop.Depth2 = "8"
    if colprop.Fmt2 == "I":
        if not colprop.Depth2 in I:
            colprop.Depth2 = "8"
    if colprop.Fmt2 == "CI":
        if not colprop.Depth2 in CI:
            colprop.Depth2 = "16"


def SetupCombiner(node_tree, ins, mat):
    # combined gets mapped to image 0, same with image 1. K registers are mapped as zero, same with LOD
    NodeGetsCC = {
        "Combined": ["ADDC1", 0],
        "Texel 0": ["Image Texture", 0],
        "Texel 1": ["Image Texture.001", 0],
        "Primitive": ["PRIM", 0],
        "Shade": ["Shade Color", 0],
        "Environment": ["ENV", 0],
        "Key: Scale": ["ZERO", 0],
        "Combined Alpha": ["Image Texture", 1],
        "Texel 0 Alpha": ["Image Texture", 1],
        "Texel 1 Alpha": ["Image Texture.001", 1],
        "Primitive Alpha": ["PRIMA", 0],
        "Shade Alpha": ["Vertex Color", 1],
        "Environment Alpha": ["ENVA", 0],
        "LOD fraction": ["ONE", 0],
        "Primitive LOD fraction": ["ONE", 0],
        "Convert K5": ["ZERO", 0],
    }
    NodeGetsAC = {
        "Combined": ["ADDC1", 0],
        "Texel 0": ["Image Texture", 0],
        "Texel 1": ["Image Texture.001", 0],
        "Primitive": ["PRIM", 0],
        "Shade": ["Shade Color", 0],
        "Environment": ["ENV", 0],
        "1.0": ["ONE", 0],
        "Zero": ["ZERO", 0],
    }
    NodeGetsAA = {
        "Combined Alpha": ["ADDA1", 0],
        "Texel 0 Alpha": ["Image Texture", 1],
        "Texel 1 Alpha": ["Image Texture.001", 1],
        "Primitive Alpha": ["PRIMA", 0],
        "Shade Alpha": ["Vertex Color", 1],
        "Environment Alpha": ["ENVA", 0],
        "1.0": ["ONE", 0],
    }
    mapping = [
        NodeGetsAC,
        NodeGetsAC,
        NodeGetsCC,
        NodeGetsAC,
        NodeGetsAA,
        NodeGetsAA,
        NodeGetsAA,
        NodeGetsAA,
        NodeGetsAC,
        NodeGetsAC,
        NodeGetsCC,
        NodeGetsAC,
        NodeGetsAA,
        NodeGetsAA,
        NodeGetsAA,
        NodeGetsAA,
    ]
    links = node_tree.links
    nodes = node_tree.nodes
    addc1 = nodes.get("ADDC1")
    subc1 = nodes.get("SUBC1")
    multc1 = nodes.get("MULTC1")
    adda1 = nodes.get("ADDA1")
    suba1 = nodes.get("SUBA1")
    multa1 = nodes.get("MULTA1")
    addc2 = nodes.get("ADDC2")
    subc2 = nodes.get("SUBC2")
    multc2 = nodes.get("MULTC2")
    adda2 = nodes.get("ADDA2")
    suba2 = nodes.get("SUBA2")
    multa2 = nodes.get("MULTA2")
    # Now I have input nodes for a,b,c,d
    Ins = [nodes.get(mapping[i].get(a, ["ZERO", 0])[0]) for i, a in enumerate(ins)]
    # remove input links
    Ls = []
    for l in links:
        if l.from_node in Ins:
            Ls.append(l)
    [links.remove(l) for l in Ls]
    links.new(nodes.get("Invert").inputs[1], adda1.outputs[0])
    # now I map outputs
    Ins = [
        a.outputs[mapping[i].get(b, ["ZERO", 0])[1]]
        for i, [a, b] in enumerate(zip(Ins, ins))
    ]
    # re add this because it gets destroyed by above thing
    links.new(
        node_tree.nodes["Shade Color"].inputs[1],
        node_tree.nodes["Vertex Color"].outputs[0],
    )
    # make new inputs
    links.new(Ins[0], subc1.inputs[1])
    links.new(Ins[1], subc1.inputs[2])
    links.new(Ins[2], multc1.inputs[1])
    links.new(Ins[3], addc1.inputs[1])
    links.new(Ins[4], suba1.inputs[0])
    links.new(Ins[5], suba1.inputs[1])
    links.new(Ins[6], multa1.inputs[0])
    links.new(Ins[7], adda1.inputs[0])
    # setoutput
    mix = node_tree.nodes["Mix Shader"]
    links.new(addc1.outputs[0], mix.inputs[1])
    links.new(node_tree.nodes["Transparent BSDF"].outputs[0], mix.inputs[2])

    if mat.colprop.TwoCycle:
        links.new(Ins[8], subc2.inputs[1])
        links.new(Ins[9], subc2.inputs[2])
        links.new(Ins[10], multc2.inputs[1])
        links.new(Ins[11], addc2.inputs[1])
        links.new(Ins[12], suba2.inputs[0])
        links.new(Ins[13], suba2.inputs[1])
        links.new(Ins[14], multa2.inputs[0])
        links.new(Ins[15], adda2.inputs[0])
        links.new(addc2.outputs[0], mix.inputs[1])
        links.new(node_tree.nodes["Transparent BSDF.001"].outputs[0], mix.inputs[2])


def CreateMat(name):
    # make mat
    mat = bpy.data.materials.new(name)
    # enable nodes
    mat.use_nodes = True
    # remove default shader
    node_tree = mat.node_tree
    nodes = node_tree.nodes
    links = node_tree.links
    nodes.remove(nodes.get("Principled BSDF"))
    Mat_Out = nodes.get("Material Output")
    Mat_Out.location = (1000, 800)
    # add in mixing channels for color and alpha
    for j in range(2):
        for i in range(2):
            if i == 0:
                char = "C"
                type = "MixRGB"
            else:
                char = "A"
                type = "Math"
            # mixing nodes. These are what the inputs go to
            add = nodes.new("ShaderNode%s" % type)
            add.location = (000 + i * 200, 800 - i * 200 - j * 600)
            add.name = "ADD%s%d" % (char, j + 1)
            if i == 0:
                add.inputs[0].default_value = 1.0
            sub = nodes.new("ShaderNode%s" % type)
            sub.location = (-400 + i * 200, 400 - i * 200 - j * 600)
            sub.name = "SUB%s%d" % (char, j + 1)
            if i == 0:
                sub.inputs[0].default_value = 1.0
            mult = nodes.new("ShaderNode%s" % type)
            mult.location = (-200 + i * 200, 600 - i * 200 - j * 600)
            mult.name = "MULT%s%d" % (char, j + 1)
            if i == 0:
                mult.inputs[0].default_value = 1.0

            # change modes
            if i == 0:
                add.blend_type = "ADD"
                mult.blend_type = "MULTIPLY"
                sub.blend_type = "SUBTRACT"
            else:
                add.operation = "ADD"
                mult.operation = "MULTIPLY"
                sub.operation = "SUBTRACT"
            # mix/math nodes
            links.new(mult.inputs[2 - i], sub.outputs[0])
            links.new(add.inputs[2 - i], mult.outputs[0])

    # add in LIGHTING color inputs
    amb = nodes.new("ShaderNodeRGB")
    amb.location = (-1100, 0)
    amb.name = "AMBIENT"
    amb.outputs[0].default_value = (1, 0.127142, 0.4, 1)  # pink
    mat.colprop.Ambient = (1, 0.127142, 0.4)  # pink
    mat.colprop.Diffuse = (0.8, 0.8, 0.8)  # grey
    vec = nodes.new("ShaderNodeVectorMath")
    vec.location = (-900, 0)
    dif = nodes.new("ShaderNodeBsdfDiffuse")
    dif.location = (-1300, -200)
    dif.name = "DIFFUSE"
    conv = nodes.new("ShaderNodeShaderToRGB")
    conv.location = (-1100, -200)
    gl = nodes.new("ShaderNodeValue")
    gl.location = (-1100, -300)
    gl.name = "LIGHTING"
    gl.outputs[0].default_value = 1.0
    vc = nodes.new("ShaderNodeVertexColor")
    vc.location = (-1100, 200)
    Shade = nodes.new("ShaderNodeMixRGB")
    Shade.location = (-700, 200)
    Shade.name = "Shade Color"

    # mix alpha and color outputs
    TP = nodes.new("ShaderNodeBsdfTransparent")
    TP.location = (600, 700)
    TP2 = nodes.new("ShaderNodeBsdfTransparent")
    TP2.location = (600, 100)
    mix = nodes.new("ShaderNodeMixShader")
    mix.location = (800, 800)

    # invert alpha
    Inv = nodes.new("ShaderNodeInvert")
    Inv.location = (400, 700)
    Inv2 = nodes.new("ShaderNodeInvert")
    Inv2.location = (400, 100)

    # links
    links.new(conv.inputs[0], dif.outputs[0])
    links.new(vec.inputs[0], conv.outputs[0])
    links.new(vec.inputs[1], amb.outputs[0])
    links.new(Shade.inputs[2], vec.outputs[0])
    links.new(Shade.inputs[1], vc.outputs[0])
    links.new(Shade.inputs[0], gl.outputs[0])
    links.new(Inv.inputs[1], nodes.get("ADDA1").outputs[0])
    links.new(Inv.outputs[0], TP.inputs[0])
    links.new(Inv.outputs[0], mix.inputs[0])
    links.new(TP.outputs[0], mix.inputs[2])
    links.new(Inv2.inputs[1], nodes.get("ADDA2").outputs[0])
    links.new(Inv2.outputs[0], TP2.inputs[0])
    links.new(nodes.get("ADDC1").outputs[0], mix.inputs[1])
    links.new(Mat_Out.inputs[0], mix.outputs[0])

    # input nodes
    tex = nodes.new("ShaderNodeTexImage")
    tex.location = (-700, 1300)
    tex2 = nodes.new("ShaderNodeTexImage")
    tex2.location = (-700, -300)
    env = nodes.new("ShaderNodeRGB")
    env.location = (-700, 1000)
    env.name = "ENV"
    enva = nodes.new("ShaderNodeValue")
    enva.location = (-700, 800)
    enva.name = "ENVA"

    prim = nodes.new("ShaderNodeRGB")
    prim.location = (-700, 700)
    prim.name = "PRIM"
    prima = nodes.new("ShaderNodeValue")
    prima.location = (-700, 500)
    prima.name = "PRIMA"

    zero = nodes.new("ShaderNodeValue")
    zero.location = (-700, 400)
    zero.outputs[0].default_value = 0
    zero.name = "ZERO"

    one = nodes.new("ShaderNodeValue")
    one.location = (-700, 300)
    one.outputs[0].default_value = 1
    one.name = "ONE"
    # turn to kcs material
    mc = mat.colprop
    mc.KCSmat = True
    # set all geo modes to false so they show up in dict because blender is dumb
    mc.G_ZBUFFER = False
    mc.G_SHADE = False
    mc.G_CULL_FRONT = False
    mc.G_CULL_BACK = False
    mc.G_FOG = False
    mc.G_LIGHTING = False
    mc.G_TEXTURE_GEN = False
    mc.G_TEXTURE_GEN_LINEAR = False
    mc.G_SHADING_SMOOTH = False
    mc.G_CLIPPING = False
    return mat


def CheckDuplicateMat(mats):
    last = mats[-1][0]
    im = last.node_tree.nodes.get("Image Texture").image
    node = last.node_tree.nodes.get("PRIM")
    PrimC = node.outputs[0].default_value
    node = last.node_tree.nodes.get("ENV")
    EnvC = node.outputs[0].default_value
    replace = False
    for m in mats[:-1]:
        props = dict(m[0].colprop)
        lp = dict(last.colprop)
        stop = False
        for a, b in zip(lp.items(), props.items()):
            if not a == b:
                stop = True
                break
        if stop:
            continue
        # check node values
        Mim = m[0].node_tree.nodes.get("Image Texture").image
        if not im == Mim:
            continue
        Mpc = m[0].node_tree.nodes.get("PRIM").outputs[0].default_value
        if not PrimC[:] == Mpc[:]:
            continue
        Mec = m[0].node_tree.nodes.get("ENV").outputs[0].default_value
        if not EnvC[:] == Mec[:]:
            continue
        # material is a dupe
        last = m[0]
        replace = True
        break
    if replace:
        q = mats[-1][0]
        mats[-1][0] = last
        bpy.data.materials.remove(q)
    return last


def resolvemats(mats, Tn, my_tool, name):
    if not mats[-1][0].colprop.GfxUser:
        return mats
    else:
        lastmat = CheckDuplicateMat(mats)
        name = name + " Gfx %d" % len(mats)
        mats.append([lastmat.copy(), Tn])
        # copy old mat to new one
        mats[-1][0].name = name
        # this will be set to true because its copied from prev mat
        mats[-1][0].colprop.GfxUser = False
        return mats


def GetLayout17(GB, num, Layouts):
    map = GB.DLmap
    addr = map[num]
    for l in Layouts:
        if l[0][4] == addr:
            return l[0]
    # if none, it must be a jumped to DL
    BranchNum = DetectJump(GB, addr, num)
    addr = map[BranchNum]
    for l in Layouts:
        if l[0][4] == addr:
            return l[0]
    # Now you're fucked
    return None


def GetLayout18(GB, num, Layouts, EP, RM):
    map = GB.DLmap
    addr = map[num]
    for i, e in enumerate(EP):
        for p in e:
            if addr == p[1]:
                return Layouts[i][0]
            if RM == 0x1C:
                if addr == p[2]:
                    return Layouts[i][0]
    # if none, it must be a jumped to DL
    BranchNum = DetectJump(GB, addr, num)
    addr = map[BranchNum]
    for i, e in enumerate(EP):
        for p in e:
            if addr == p[1]:
                return Layouts[i][0]
    # haha oinks
    return None


def DetectJump(GB, addr, num):
    x = 0
    while True:
        if x == num:
            x += 1
        try:
            DL = getattr(GB, "Display_List_%d" % x)
            for cmd in DL:
                if cmd[0] == "G_DL":
                    if cmd[1][1] == addr:
                        return x
            x += 1
        except:
            x = None
            break
    return x


# ------------------------------------------------------------------------
#    UI begin
# ------------------------------------------------------------------------


# ------------------------------------------------------------------------
#    Scene Properties
# ------------------------------------------------------------------------


class MyProperties(PropertyGroup):
    World: IntProperty(
        name="World", description="World to place level in", default=1, min=1, max=7
    )

    Level: IntProperty(
        name="Level",
        description="Which level in selected world to overwrite",
        default=1,
        min=1,
        max=5,
    )
    Area: IntProperty(name="Area", description="Area", default=1, min=1, max=10)
    Scale: FloatProperty(
        name="Scale",
        description="Level Scale",
        default=25.0,
        min=0.1,
    )
    ImpWorld: IntProperty(
        name="World", description="World to place level in", default=1, min=1, max=7
    )

    ImpLevel: IntProperty(
        name="Level",
        description="Which level in selected world to overwrite",
        default=1,
        min=1,
        max=5,
    )
    ImpArea: IntProperty(name="Area", description="Area", default=1, min=1, max=10)
    ImpScale: FloatProperty(
        name="Scale", description="Level Scale", default=0.01, min=0.0001, max=5
    )
    Skybox_ID: IntProperty(
        name="Skybox_ID", description="ID of level's skybox", default=13, min=0, max=72
    )

    Music_ID: IntProperty(
        name="Music_ID", description="ID of level's music", default=0, min=0, max=64
    )
    ROM_path: StringProperty(
        name="Path",
        description="Choose a directory:",
        default="",
        maxlen=1024,
        subtype="DIR_PATH",
    )
    ImpBank: IntProperty(
        name="Bank", description="Bank for Non Level Data", default=0, min=0, max=7
    )
    ImpID: IntProperty(
        name="ID", description="ID for Non Level Data", default=1, min=1, max=1200
    )
    ExpBank: IntProperty(
        name="Bank", description="Bank for Non Level Data", default=0, min=0, max=7
    )
    ExpID: IntProperty(
        name="ID", description="ID for Non Level Data", default=1, min=1, max=1200
    )


class TextureProp(PropertyGroup):
    IntendedImage: StringProperty(name="Inteded path", default="", maxlen=1024)


class NodeProp(PropertyGroup):
    EnableWarpF: BoolProperty(name="Enable Forwards", description="Warp at node")
    EnableWarpR: BoolProperty(name="Enable Back", description="Warp at node")
    LockForward: BoolProperty(name="Lock Forward", description="Stop Going Forward")
    LockBackward: BoolProperty(name="Lock Backward", description="Stop Going Backwards")
    Looping: BoolProperty(name="Looping", description="Used in Boss Stages")
    NodeNum: IntProperty(
        name="Node Number", description="Number of Curr Node", default=1, min=1
    )
    PrevNode: IntProperty(
        name="Prev Node", description="Dest Node When Walking Back", default=1, min=1
    )
    NextNode: IntProperty(
        name="Next Node", description="Dest Node When Walking Forward", default=2, min=1
    )
    Warp: IntVectorProperty(
        name="Warp Dest", description="Area Warp Dest", default=(1, 1, 1), min=1, max=9
    )
    WarpDir: IntProperty(
        name="Dest Node", description="Node of Warp Dest", default=1, min=1
    )
    Speed: FloatProperty(
        name="NodeSpeed", description="Speed", default=0.85, min=0.0, max=200.0
    )


class CamProp(PropertyGroup):
    AxisLocks: BoolVectorProperty(
        name="Locks", description="Stops Cam From Moving", subtype="XYZ"
    )
    PanH: BoolProperty(
        name="Pan Horizontal", description="Pans Camera in X/Z plane ahead of kirby"
    )
    PanUpDown: BoolProperty(
        name="Pan Up/Down", description="Pans Camera in Y axis to follow kirby"
    )
    PanDown: BoolProperty(
        name="Pan Down",
        description="Pans Camera in Y axis to follow kirby while falling only",
    )
    ProfileView: BoolProperty(name="ProfileView", description="View Kirby From Side")
    NodeNum: IntProperty(
        name="Node Number", description="Number of Curr Node", default=1, min=1
    )
    CamXBound: FloatVectorProperty(
        name="X",
        description="Follows Kirby in this range, Locks at Bound otherwise",
        max=9999.0,
        min=-9999.0,
        default=(-9999.0, 9999.0),
        size=2,
    )
    CamYBound: FloatVectorProperty(
        name="Y",
        size=2,
        description="Follows Kirby in this range, Locks at Bound otherwise",
        max=9999.0,
        min=-9999.0,
        default=(-9999.0, 9999.0),
    )
    Yaw: FloatVectorProperty(
        name="Yaw",
        description="The Theta Rotation from kirby to cam while following",
        size=2,
        default=(90.0, 90.0),
    )
    Pitch: FloatVectorProperty(
        name="Pitch",
        description="The Phi Rotation from kirby to cam while following",
        size=2,
        default=(120.0, 120.0),
    )
    Radius: FloatVectorProperty(
        name="Radius",
        description="How far the camera is from kirby while following",
        size=2,
        default=(600.0, 600.0),
    )
    Foc: FloatVectorProperty(
        name="Focus position",
        description="9999 to not use",
        size=3,
        default=(9999.0, 9999.0, 9999.0),
        max=9999.0,
    )
    CamZBound: FloatVectorProperty(
        name="Z",
        description="Follows Kirby in this range, Locks at Bound otherwise",
        size=2,
        default=(-9999.0, 9999.0),
        max=9999.0,
        min=-9999.0,
    )
    CamPitchBound: FloatVectorProperty(
        name="Cam Pitch Bound",
        description="Follows Kirby in this range, Locks at Bound otherwise",
        size=2,
        default=(0.0, 359.0),
        max=359.0,
        min=0.0,
    )
    CamYawBound: FloatVectorProperty(
        name="Cam Yaw Bound",
        description="Follows Kirby in this range, Locks at Bound otherwise",
        size=2,
        default=(10, 170),
        max=359.0,
        min=0.0,
    )


class ColProp(PropertyGroup):
    NormType: IntProperty(
        name="Norm Type", description="Bitwise Normal Type", default=3, min=0
    )
    ColType: IntProperty(
        name="Col Type", description="Collision Type", default=0, min=0
    )
    ColParam: IntProperty(
        name="Col Param/Break Condition",
        description="Collision Param for certain ColTypes",
        default=0,
    )
    BreakIndex: IntProperty(
        name="Break Index",
        description="The index of this breakable geometry. All shared indices break simultaneously",
        min=1,
    )
    GfxUser: BoolProperty(name="Gfx User")
    KCSmat: BoolProperty(name="KCS mat")
    WarpNum: IntProperty(name="WarpNum", description="Number of Node warp is on", min=1)
    OtherMode_L: EnumProperty(
        name="OtherMode_L",
        description="Changes the blender and other settings",
        items=[
            ("G_RM_AA_ZB_OPA_SURF", "G_RM_AA_ZB_OPA_SURF", ""),
            ("G_RM_AA_ZB_OPA_SURF2", "G_RM_AA_ZB_OPA_SURF2", ""),
            ("G_RM_AA_ZT_OPA_SURF", "G_RM_AA_ZT_OPA_SURF", ""),
            ("G_RM_AA_ZT_OPA_SURF", "G_RM_AA_ZT_OPA_SURF", ""),
            ("G_RM_RA_OPA_SURF", "G_RM_RA_OPA_SURF", ""),
            ("G_RM_RA_OPA_SURF2", "G_RM_RA_OPA_SURF2", ""),
            ("G_RM_ID_OPA_SURF", "G_RM_ID_OPA_SURF", ""),
            ("G_RM_ID_OPA_SURF2", "G_RM_ID_OPA_SURF2", ""),
            ("G_RM_RA_ZT_OPA_SURF", "G_RM_RA_ZT_OPA_SURF", ""),
            ("G_RM_RA_ZT_OPA_SURF2", "G_RM_RA_ZT_OPA_SURF2", ""),
            ("G_RM_AA_ZB_XLU_SURF", "G_RM_AA_ZB_XLU_SURF", ""),
            ("G_RM_AA_ZB_XLU_SURF2", "G_RM_AA_ZB_XLU_SURF2", ""),
            ("G_RM_AA_XLU_SURF", "G_RM_AA_XLU_SURF", ""),
            ("G_RM_AA_XLU_SURF2", "G_RM_AA_XLU_SURF2", ""),
            ("G_RM_AA_ZB_XLU_SURF_REVERSE", "G_RM_AA_ZB_XLU_SURF_REVERSE", ""),
            ("G_RM_RA_TEX_EDGE", "G_RM_RA_TEX_EDGE", ""),
            ("G_RM_RA_TEX_EDGE2", "G_RM_RA_TEX_EDGE2", ""),
            ("G_RM_RA_ZB_OPA_SURF", "G_RM_RA_ZB_OPA_SURF", ""),
            ("G_RM_RA_ZB_OPA_SURF2", "G_RM_RA_ZB_OPA_SURF2", ""),
            ("G_RM_RA_ZB_TEX_EDGE", "G_RM_RA_ZB_TEX_EDGE", ""),
            ("G_RM_RA_ZB_TEX_EDGE2", "G_RM_RA_ZB_TEX_EDGE2", ""),
            ("G_RM_AA_TEX_EDGE", "G_RM_AA_TEX_EDGE", ""),
            ("G_RM_AA_TEX_EDGE2", "G_RM_AA_TEX_EDGE2", ""),
            ("G_RM_AA_ZB_OPA_DECAL", "G_RM_AA_ZB_OPA_DECAL", ""),
            ("G_RM_AA_ZB_OPA_DECAL2", "G_RM_AA_ZB_OPA_DECAL2", ""),
            ("G_RM_AA_ZB_TEX_EDGE_DECAL", "G_RM_AA_ZB_TEX_EDGE_DECAL", ""),
            ("G_RM_AA_ZB_TEX_EDGE_DECAL2", "G_RM_AA_ZB_TEX_EDGE_DECAL2", ""),
            ("G_RM_AA_OPA_SURF", "G_RM_AA_OPA_SURF", ""),
            ("G_RM_AA_OPA_SURF2", "G_RM_AA_OPA_SURF2", ""),
            ("G_RM_AA_ZB_TEX_EDGE", "G_RM_AA_ZB_TEX_EDGE", ""),
            ("G_RM_AA_ZB_TEX_EDGE2", "G_RM_AA_ZB_TEX_EDGE2", ""),
            ("G_RM_AA_ZB_OPA_INVERT", "G_RM_AA_ZB_OPA_INVERT", ""),
            ("G_RM_AA_ZB_TEX_EDGE_INVERT", "G_RM_AA_ZB_TEX_EDGE_INVERT", ""),
            ("G_RM_RA_ZB_TEX_EDGE_INVERT", "G_RM_RA_ZB_TEX_EDGE_INVERT", ""),
            ("G_RM_AA_TEX_EDGE_INVERT", "G_RM_AA_TEX_EDGE_INVERT", ""),
            ("G_RM_RA_ZB_OPA_INVERT", "G_RM_RA_ZB_OPA_INVERT", ""),
            ("G_RM_RA_ZB_TEX_DECAL_INVERT", "G_RM_RA_ZB_TEX_DECAL_INVERT", ""),
            ("G_RM_AA_OPA_INVERT", "G_RM_AA_OPA_INVERT", ""),
            ("G_RM_RA_OPA_INVERT", "G_RM_RA_OPA_INVERT", ""),
            ("G_RM_AA_ZB_XLU_DECAL", "G_RM_AA_ZB_XLU_DECAL", ""),
            ("G_RM_AA_ZB_XLU_DECAL_INVERT", "G_RM_AA_ZB_XLU_DECAL_INVERT", ""),
            ("G_RM_AA_ZB_XLU_DECAL_REVERSE", "G_RM_AA_ZB_XLU_DECAL_REVERSE", ""),
            ("G_RM_AA_ZB_XLU_SURF_REVERSE", "G_RM_AA_ZB_XLU_SURF_REVERSE", ""),
            ("G_RM_RA_ZB_XLU_SURF_REVERSE", "G_RM_RA_ZB_XLU_SURF_REVERSE", ""),
            ("G_RM_RA_XLU_SURF_REVERSE", "G_RM_RA_XLU_SURF_REVERSE", ""),
            ("G_RM_AA_ZB_OPA_SURF_FOG_ALPHA", "G_RM_AA_ZB_OPA_SURF_FOG_ALPHA", ""),
            ("G_RM_NOOP", "G_RM_NOOP", ""),
        ],
        update=UpdateOtherL,
    )
    ShowGeo: BoolProperty(name="Expand GeoModes", default=False)
    ShowColor: BoolProperty(name="Expand Color Selectors", default=True)
    ShowOther: BoolProperty(name="Expand OtherModes", default=False)
    AlphaCompare: BoolProperty(name="Alpha Noise", default=False)
    TextFilt: BoolProperty(name="Bilinear Texture Filter", default=True)
    TextLOD: BoolProperty(name="Texture LOD", default=False)
    TwoCycle: BoolProperty(name="Enable Two Cycle", default=False)
    G_ZBUFFER: BoolProperty(name="G_ZBUFFER")
    G_SHADE: BoolProperty(name="G_SHADE")
    G_CULL_FRONT: BoolProperty(name="G_CULL_FRONT")
    G_CULL_BACK: BoolProperty(name="G_CULL_BACK")
    G_FOG: BoolProperty(name="G_FOG")
    G_LIGHTING: BoolProperty(name="G_LIGHTING", update=UpdateShade)
    G_TEXTURE_GEN: BoolProperty(name="G_TEXTURE_GEN")
    G_TEXTURE_GEN_LINEAR: BoolProperty(name="G_TEXTURE_GEN_LINEAR")
    G_SHADING_SMOOTH: BoolProperty(name="G_SHADING_SMOOTH")
    G_CLIPPING: BoolProperty(name="G_CLIPPING")
    Prim: FloatVectorProperty(name="Primary Color", min=0, max=1, update=UpdateColor)
    PrimA: FloatProperty(name="Prim Alpha", min=0, max=1, update=UpdateColor)
    Env: FloatVectorProperty(name="Environment Color", min=0, max=1, update=UpdateColor)
    EnvA: FloatProperty(name="Env Alpha", min=0, max=1, update=UpdateColor)
    Ambient: FloatVectorProperty(name="Ambient Color", min=0, max=1, update=UpdateColor)
    Diffuse: FloatVectorProperty(name="Diffuse Color", min=0, max=1, update=UpdateColor)
    Tex: PointerProperty(name="Texture", type=bpy.types.Image, update=UpdateColor)
    Tex2: PointerProperty(name="Texture", type=bpy.types.Image, update=UpdateColor)
    Fmt: EnumProperty(
        name="Format",
        description="Format of Texture",
        items=[
            ("RGBA", "RGBA", ""),
            ("IA", "IA", ""),
            ("I", "I", ""),
            ("CI", "CI", ""),
        ],
        update=ValidFmt,
    )
    Depth: EnumProperty(
        name="Bit Depth",
        items=[("4", "4", ""), ("8", "8", ""), ("16", "16", ""), ("32", "32", "")],
        update=ValidFmt,
        default="16",
    )
    Fmt2: EnumProperty(
        name="Format",
        description="Format of Texture",
        items=[
            ("RGBA", "RGBA", ""),
            ("IA", "IA", ""),
            ("I", "I", ""),
            ("CI", "CI", ""),
        ],
        update=ValidFmt,
    )
    Depth2: EnumProperty(
        name="Bit Depth",
        items=[("4", "4", ""), ("8", "8", ""), ("16", "16", ""), ("32", "32", "")],
        update=ValidFmt,
        default="16",
    )
    Tex1TFlags: EnumProperty(
        name="T Flags",
        items=[("wrap", "wrap", ""), ("mirror", "mirror", ""), ("clamp", "clamp", "")],
        default="wrap",
    )
    Tex2TFlags: EnumProperty(
        name="T Flags",
        items=[("wrap", "wrap", ""), ("mirror", "mirror", ""), ("clamp", "clamp", "")],
        default="wrap",
    )
    Tex1SFlags: EnumProperty(
        name="S Flags",
        items=[("wrap", "wrap", ""), ("mirror", "mirror", ""), ("clamp", "clamp", "")],
        default="wrap",
    )
    Tex2SFlags: EnumProperty(
        name="S Flags",
        items=[("wrap", "wrap", ""), ("mirror", "mirror", ""), ("clamp", "clamp", "")],
        default="wrap",
    )
    ACmode1: EnumProperty(
        name="A",
        description="A Color mode of Color Combiner",
        items=[
            ("Texel 0", "Texel 0", ""),
            ("Texel 1", "Texel 1", ""),
            ("Primitive", "Primitive", ""),
            ("Shade", "Shade", ""),
            ("Environment", "Environment", ""),
            ("Noise", "Noise", ""),
            ("1.0", "1.0", ""),
            ("Zero", "Zero", ""),
        ],
        update=UpdateCC,
        default="Zero",
    )
    BCmode1: EnumProperty(
        name="B",
        description="B Color mode of Color Combiner",
        items=[
            ("Texel 0", "Texel 0", ""),
            ("Texel 1", "Texel 1", ""),
            ("Primitive", "Primitive", ""),
            ("Shade", "Shade", ""),
            ("Environment", "Environment", ""),
            ("Key: Center", "Key: Center", ""),
            ("Key: 4", "Key: 4", ""),
            ("Zero", "Zero", ""),
        ],
        update=UpdateCC,
        default="Zero",
    )
    CCmode1: EnumProperty(
        name="C",
        description="C Color mode of Color Combiner",
        items=[
            ("Texel 0", "Texel 0", ""),
            ("Texel 1", "Texel 1", ""),
            ("Primitive", "Primitive", ""),
            ("Shade", "Shade", ""),
            ("Environment", "Environment", ""),
            ("Key: Scale", "Key: Scale", ""),
            ("Combined Alpha", "Combined Alpha", ""),
            ("Texel 0 Alpha", "Texel 0 Alpha", ""),
            ("Texel 1 Alpha", "Texel 1 Alpha", ""),
            ("Primitive Alpha", "Primitive Alpha", ""),
            ("Shade Alpha", "Shade Alpha", ""),
            ("Environment Alpha", "Environment Alpha", ""),
            ("LOD fraction", "LOD fraction", ""),
            ("Primitive LOD fraction", "Primitive LOD fraction", ""),
            ("Convert K5", "Convert K5", ""),
            ("Zero", "Zero", ""),
        ],
        update=UpdateCC,
        default="Zero",
    )
    DCmode1: EnumProperty(
        name="D",
        description="D Color mode of Color Combiner",
        items=[
            ("Texel 0", "Texel 0", ""),
            ("Texel 1", "Texel 1", ""),
            ("Primitive", "Primitive", ""),
            ("Shade", "Shade", ""),
            ("Environment", "Environment", ""),
            ("1.0", "1.0", ""),
            ("Zero", "Zero", ""),
        ],
        update=UpdateCC,
        default="Texel 0",
    )
    AAmode1: EnumProperty(
        name="A",
        description="A Alpha mode of Color Combiner",
        items=[
            ("Texel 0 Alpha", "Texel 0 Alpha", ""),
            ("Texel 1 Alpha", "Texel 1 Alpha", ""),
            ("Primitive Alpha", "Primitive Alpha", ""),
            ("Shade Alpha", "Shade Alpha", ""),
            ("Environment Alpha", "Environment Alpha", ""),
            ("1.0", "1.0", ""),
            ("Zero", "Zero", ""),
        ],
        update=UpdateCC,
        default="Zero",
    )
    BAmode1: EnumProperty(
        name="B",
        description="B Alpha mode of Color Combiner",
        items=[
            ("Texel 0 Alpha", "Texel 0 Alpha", ""),
            ("Texel 1 Alpha", "Texel 1 Alpha", ""),
            ("Primitive Alpha", "Primitive Alpha", ""),
            ("Shade Alpha", "Shade Alpha", ""),
            ("Environment Alpha", "Environment Alpha", ""),
            ("1.0", "1.0", ""),
            ("Zero", "Zero", ""),
        ],
        update=UpdateCC,
        default="Zero",
    )
    CAmode1: EnumProperty(
        name="C",
        description="C Alpha mode of Color Combiner",
        items=[
            ("Texel 0 Alpha", "Texel 0 Alpha", ""),
            ("Texel 1 Alpha", "Texel 1 Alpha", ""),
            ("Primitive Alpha", "Primitive Alpha", ""),
            ("Shade Alpha", "Shade Alpha", ""),
            ("Environment Alpha", "Environment Alpha", ""),
            ("LoD Fraction", "LoD Fraction", ""),
            ("Zero", "Zero", ""),
        ],
        update=UpdateCC,
        default="Zero",
    )
    DAmode1: EnumProperty(
        name="D",
        description="D Alpha mode of Color Combiner",
        items=[
            ("Texel 0 Alpha", "Texel 0 Alpha", ""),
            ("Texel 1 Alpha", "Texel 1 Alpha", ""),
            ("Primitive Alpha", "Primitive Alpha", ""),
            ("Shade Alpha", "Shade Alpha", ""),
            ("Environment Alpha", "Environment Alpha", ""),
            ("1.0", "1.0", ""),
            ("Zero", "Zero", ""),
        ],
        update=UpdateCC,
        default="Texel 0 Alpha",
    )
    ACmode2: EnumProperty(
        name="A",
        description="A Color mode of Color Combiner",
        items=[
            ("Texel 0", "Texel 0", ""),
            ("Texel 1", "Texel 1", ""),
            ("Primitive", "Primitive", ""),
            ("Shade", "Shade", ""),
            ("Environment", "Environment", ""),
            ("Noise", "Noise", ""),
            ("Combined", "Combined", ""),
            ("1.0", "1.0", ""),
            ("Zero", "Zero", ""),
        ],
        update=UpdateCC,
        default="Zero",
    )
    BCmode2: EnumProperty(
        name="B",
        description="B Color mode of Color Combiner",
        items=[
            ("Texel 0", "Texel 0", ""),
            ("Texel 1", "Texel 1", ""),
            ("Primitive", "Primitive", ""),
            ("Shade", "Shade", ""),
            ("Environment", "Environment", ""),
            ("Key: Center", "Key: Center", ""),
            ("Key: 4", "Key: 4", ""),
            ("Combined", "Combined", ""),
            ("Zero", "Zero", ""),
        ],
        update=UpdateCC,
        default="Zero",
    )
    CCmode2: EnumProperty(
        name="C",
        description="C Color mode of Color Combiner",
        items=[
            ("Texel 0", "Texel 0", ""),
            ("Texel 1", "Texel 1", ""),
            ("Primitive", "Primitive", ""),
            ("Shade", "Shade", ""),
            ("Environment", "Environment", ""),
            ("Key: Scale", "Key: Scale", ""),
            ("Combined Alpha", "Combined Alpha", ""),
            ("Texel 0 Alpha", "Texel 0 Alpha", ""),
            ("Texel 1 Alpha", "Texel 1 Alpha", ""),
            ("Primitive Alpha", "Primitive Alpha", ""),
            ("Shade Alpha", "Shade Alpha", ""),
            ("Environment Alpha", "Environment Alpha", ""),
            ("LOD fraction", "LOD fraction", ""),
            ("Primitive LOD fraction", "Primitive LOD fraction", ""),
            ("Convert K5", "Convert K5", ""),
            ("Combined", "Combined", ""),
            ("Zero", "Zero", ""),
        ],
        update=UpdateCC,
        default="Zero",
    )
    DCmode2: EnumProperty(
        name="D",
        description="D Color mode of Color Combiner",
        items=[
            ("Texel 0", "Texel 0", ""),
            ("Texel 1", "Texel 1", ""),
            ("Primitive", "Primitive", ""),
            ("Shade", "Shade", ""),
            ("Environment", "Environment", ""),
            ("1.0", "1.0", ""),
            ("Combined", "Combined", ""),
            ("Zero", "Zero", ""),
        ],
        update=UpdateCC,
        default="Texel 0",
    )
    AAmode2: EnumProperty(
        name="A",
        description="A Alpha mode of Color Combiner",
        items=[
            ("Texel 0 Alpha", "Texel 0 Alpha", ""),
            ("Texel 1 Alpha", "Texel 1 Alpha", ""),
            ("Primitive Alpha", "Primitive Alpha", ""),
            ("Shade Alpha", "Shade Alpha", ""),
            ("Environment Alpha", "Environment Alpha", ""),
            ("Combined Alpha", "Combined Alpha", ""),
            ("1.0", "1.0", ""),
            ("Zero", "Zero", ""),
        ],
        update=UpdateCC,
        default="Zero",
    )
    BAmode2: EnumProperty(
        name="B",
        description="B Alpha mode of Color Combiner",
        items=[
            ("Texel 0 Alpha", "Texel 0 Alpha", ""),
            ("Texel 1 Alpha", "Texel 1 Alpha", ""),
            ("Primitive Alpha", "Primitive Alpha", ""),
            ("Shade Alpha", "Shade Alpha", ""),
            ("Environment Alpha", "Environment Alpha", ""),
            ("1.0", "1.0", ""),
            ("Combined Alpha", "Combined Alpha", ""),
            ("Zero", "Zero", ""),
        ],
        update=UpdateCC,
        default="Zero",
    )
    CAmode2: EnumProperty(
        name="C",
        description="C Alpha mode of Color Combiner",
        items=[
            ("Texel 0 Alpha", "Texel 0 Alpha", ""),
            ("Texel 1 Alpha", "Texel 1 Alpha", ""),
            ("Primitive Alpha", "Primitive Alpha", ""),
            ("Shade Alpha", "Shade Alpha", ""),
            ("Environment Alpha", "Environment Alpha", ""),
            ("LoD Fraction", "LoD Fraction", ""),
            ("Zero", "Zero", ""),
        ],
        update=UpdateCC,
        default="Zero",
    )
    DAmode2: EnumProperty(
        name="D",
        description="D Alpha mode of Color Combiner",
        items=[
            ("Texel 0 Alpha", "Texel 0 Alpha", ""),
            ("Texel 1 Alpha", "Texel 1 Alpha", ""),
            ("Primitive Alpha", "Primitive Alpha", ""),
            ("Shade Alpha", "Shade Alpha", ""),
            ("Environment Alpha", "Environment Alpha", ""),
            ("1.0", "1.0", ""),
            ("Combined Alpha", "Combined Alpha", ""),
            ("Zero", "Zero", ""),
        ],
        update=UpdateCC,
        default="Texel 0 Alpha",
    )


class ObjProp(PropertyGroup):
    Collision: BoolProperty(
        name="Export As Collision",
        description="Export as Collision instead of Gfx",
    )
    Layer: IntProperty(
        name="Layer",
        description="The layer corresponds to the order layouts are placed",
        min=0,
    )
    BreakIndex: IntProperty(
        name="Breakable Index",
        description="The index of the model associated with the index of the breakable level geometry",
        min=0,
    )
    Entity: EnumProperty(
        name="Entity ID",
        description="Name of Entity",
        items=[
            ("0,0", "N-Z", ""),
            ("0,1", "Rocky", ""),
            ("0,2", "Bronto Burt", ""),
            ("0,3", "Skud", ""),
            ("0,4", "Gordo", ""),
            ("0,5", "Shotzo", ""),
            ("0,6", "Spark-i", ""),
            ("0,7", "Bouncy", ""),
            ("0,8", "Glunk", ""),
            ("0,9", "?? explodes]", ""),
            ("0,10", "Chilly", ""),
            ("0,11", "Propeller", ""),
            ("0,12", "Glom", ""),
            ("0,13", "Mahall", ""),
            ("0,14", "Poppy Bros. Jr.", ""),
            ("0,19", "Bivolt", ""),
            ("0,16", "Splinter", ""),
            ("0,17", "Gobblin", ""),
            ("0,18", "Kany", ""),
            ("0,19", "Bivolt again?", ""),
            ("0,20", "Sirkibble", ""),
            ("0,21", "Gabon", ""),
            ("0,22", "Mariel", ""),
            ("0,23", "Large I3", ""),
            ("0,24", "Snipper", ""),
            ("0,25", "?? explodes again?]", ""),
            ("0,26", "Bonehead", ""),
            ("0,27", "Squibbly", ""),
            ("0,28", "Bobo", ""),
            ("0,29", "Bo", ""),
            ("0,30", "Punc", ""),
            ("0,31", "Mite", ""),
            ("0,32", "Sandman", ""),
            ("0,33", "Flopper", ""),
            ("0,34", "Kapar", ""),
            ("0,35", "Maw", ""),
            ("0,36", "Drop", ""),
            ("0,37", "Pedo", ""),
            ("0,38", "Noo", ""),
            ("0,39", "Tick", ""),
            ("0,40", "Cairn", ""),
            ("0,41", "?? invisible]", ""),
            ("0,42", "Pompey", ""),
            ("0,43", "Hack", ""),
            ("0,44", "Burnis", ""),
            ("0,45", "Fishbone", ""),
            ("0,46", "Frigis", ""),
            ("0,47", "Sawyer", ""),
            ("0,48", "Turbite", ""),
            ("0,49", "Plugg", ""),
            ("0,50", "Ghost knight", ""),
            ("0,51", "Zoos", ""),
            ("0,52", "Kakti", ""),
            ("0,53", "Rockn", ""),
            ("0,54", "Chacha", ""),
            ("0,55", "Galbo", ""),
            ("0,56", "Bumber", ""),
            ("0,57", "Scarfy", ""),
            ("0,58", "Nruff", ""),
            ("0,59", "Emp", ""),
            ("0,60", "Magoo", ""),
            ("0,61", "Yariko", ""),
            ("0,62", "invisible?", ""),
            ("0,63", "Wall Shotzo", ""),
            ("0,64", "Keke", ""),
            ("0,65", "Sparky", ""),
            ("0,66", "Ignus", ""),
            ("0,67", "Flora", ""),
            ("0,68", "Putt", ""),
            ("0,69", "Pteran", ""),
            ("0,70", "Mumbies", ""),
            ("0,71", "Pupa", ""),
            ("0,72", "Mopoo", ""),
            ("0,73", "Zebon", ""),
            ("0,74", "invisible?]", ""),
            ("0,75", "falling rocks sometimes blue]", ""),
            ("0,76", "falling rocks sometimes blue bigger?]", ""),
            ("1,0", "Waddle Dee Boss", ""),
            ("1,1", "Ado Boss", ""),
            ("1,2", "DeeDeeDee Boss", ""),
            ("2,0", "Whispy Woods", ""),
            ("2,1", "Waddle Dee Boss)", ""),
            ("3,0", "Maxim Tomato", ""),
            ("3,1", "Sandwich", ""),
            ("3,2", "Cake", ""),
            ("3,3", "Steak", ""),
            ("3,4", "Ice Cream Bar", ""),
            ("3,5", "Invinsible Candy", ""),
            ("3,6", "Yellow Star", ""),
            ("3,7", "Blue Star", ""),
            ("3,10", "crashes]", ""),
            ("3,9", "1up", ""),
            ("3,11", "Flower", ""),
            ("3,12", "School of fish", ""),
            ("3,13", "Butterfly", ""),
            ("5,0", "warps", ""),
            ("5,31", "Door", ""),
            ("5,32", "Door 2", ""),
            ("7,1", "Ado (Gives maxim tomato)", ""),
            ("8,0", "N-Z Boss", ""),
            ("8,1", "Bouncy Boss", ""),
            ("8,2", "Kakti Boss", ""),
            ("8,3", "?", ""),
            ("8,4", "Spark-i Boss", ""),
            ("8,5", "Tick Boss", ""),
            ("8,6", "Kany Boss", ""),
            ("8,7", "Kapar Boss", ""),
            ("8,8", "Blowfish boss", ""),
            ("8,9", "Galbo boss", ""),
            ("8,10", "drop monster room", ""),
            ("8,15", "Sawyer Boss", ""),
        ],
        update=UpdateEnt,
    )
    Export_Ent: BoolProperty(
        name="Export Entity", description="Export object as entity", default=True
    )
    NodeNum: IntProperty(
        name="NodeNum",
        description="The node that this entity spawns on",
        default=1,
        min=1,
    )
    BankNum: IntProperty(
        name="BankNum", description="The bank the entity is from", default=0, min=0
    )
    IndexNum: IntProperty(
        name="IndexNum", description="The index the entity is from", default=0, min=0
    )
    Action: IntProperty(
        name="Action",
        description="The action of this specific entity",
        default=0,
        min=0,
    )
    Flags: IntProperty(
        name="Flags",
        description="Flags for spawning or other conditions",
        default=0,
        min=0,
        max=255,
    )
    Respawn: BoolProperty(
        name="Respawn", description="Respawn after killing", default=0
    )
    Eep: IntProperty(
        name="Eep", description="An eep flag to check, if true spawn", default=0, min=0
    )


# ------------------------------------------------------------------------
#    Operators
# ------------------------------------------------------------------------


class KCS_OT_Export(Operator):
    bl_label = "Export Area"
    bl_idname = "wm.export_area"

    def execute(self, context):
        scene = context.scene
        mytool = scene.my_tool

        UpdateMusicSky()
        [PGB, SGB, LSS] = GetLevelBankID(
            mytool.World, mytool.Level, mytool.Area, mytool
        )
        name = mytool.ROM_path + "\\Geo Blocks\\7\\Bank_%d_Index_%d_Geo.py" % (PGB)
        Export_PGB(mytool, name)
        name = mytool.ROM_path + "\\Misc Banks\\7\\Bank_%d_Index_%d_Misc.py" % (LSS)
        LS = Export_LS(mytool, name)
        LS.write()
        edit = (PGB[0], PGB[1], "Geo")
        AddEdit(mytool, edit)
        edit = (LSS[0], LSS[1], "Misc")
        AddEdit(mytool, edit)
        return {"FINISHED"}


class KCS_OT_Export_Gfx(Operator):
    bl_label = "Export Gfx"
    bl_idname = "wm.export_gfx"

    def execute(self, context):
        scene = context.scene
        mytool = scene.my_tool
        name = mytool.ROM_path + "\\Geo Blocks\\%d\\Bank_%d_Index_%d_Geo.py" % (
            mytool.ExpBank,
            mytool.ExpBank,
            mytool.ExpID,
        )
        Export_PGB(mytool, name)
        edit = (mytool.ExpBank, mytool.ExpID, "Geo")
        AddEdit(mytool, edit)
        return {"FINISHED"}


class KCS_OT_Import_Gfx(Operator):
    bl_label = "Import Stage"
    bl_idname = "wm.import_gfx"

    def execute(self, context):
        scene = context.scene
        mytool = scene.my_tool
        [PGB, SGB, LS] = GetLevelBankID(
            mytool.ImpWorld, mytool.ImpLevel, mytool.ImpArea, mytool
        )
        name = "Geo Blocks\\7\\Bank_%d_Index_%d_Geo" % (PGB)
        ImportGB(mytool, "PGB", name)
        if SGB != (0, 0):
            name = "Geo Blocks\\7\\Bank_%d_Index_%d_Geo" % (SGB)
            ImportGB(mytool, "SGB", name)
        name = "Misc Banks\\7\\Bank_%d_Index_%d_Misc" % (LS)
        ImportLS(mytool, name)
        return {"FINISHED"}


class KCS_OT_Import_NLD_Gfx(Operator):
    bl_label = "Import Gfx Data"
    bl_idname = "wm.import_nld_gfx"

    def execute(self, context):
        scene = context.scene
        mytool = scene.my_tool
        name = "Geo Blocks\\%d\\Bank_%d_Index_%d_Geo" % (
            mytool.ImpBank,
            mytool.ImpBank,
            mytool.ImpID,
        )
        ImportGB(mytool, "NLD", name)
        return {"FINISHED"}


class KCS_OT_Add_Mat(Operator):
    bl_label = "Add KCSmat"
    bl_idname = "wm.add_kcsmat"

    def execute(self, context):
        obj = context.object
        mat = CreateMat("KCS material")
        cp = mat.colprop
        a = (
            "Zero",
            "Zero",
            "Shade",
            "Texel 0",
            "Zero",
            "Zero",
            "Shade Alpha",
            "Texel 0 Alpha",
        )
        SetupCombiner(mat.node_tree, a, mat)
        obj.data.materials.append(mat)
        tex = GetTexture(0x70468, bpy.context.scene.my_tool)
        TexNode = mat.node_tree.nodes.get("Image Texture")
        if tex:
            TexNode.image = tex
        cp.Tex = tex
        cp.G_ZBUFFER = True
        cp.ACmode = a[0]
        cp.BCmode = a[1]
        cp.CCmode = a[2]
        cp.DCmode = a[3]
        cp.AAmode = a[4]
        cp.BAmode = a[5]
        cp.CAmode = a[6]
        cp.DAmode = a[7]
        return {"FINISHED"}


# ------------------------------------------------------------------------
#    Panels
# ------------------------------------------------------------------------


class KCSExport_PT_Panel(Panel):
    bl_label = "KCS Exporter"
    bl_idname = "KCSExport_PT_Panel"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Tools"
    bl_context = "objectmode"

    @classmethod
    def poll(self, context):
        return context.scene is not None

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        mytool = scene.my_tool
        layout.prop(mytool, "World")
        layout.prop(mytool, "Level")
        layout.prop(mytool, "Area")
        layout.prop(mytool, "Skybox_ID")
        layout.prop(mytool, "Music_ID")
        layout.prop(mytool, "Scale")
        layout.prop(mytool, "ROM_path")
        layout.operator("wm.export_area")
        layout.label(text="Export Gfx Data")
        layout.prop(mytool, "ExpBank")
        layout.prop(mytool, "ExpID")
        layout.operator("wm.export_gfx")
        layout.separator()


class KCSImport_PT_Panel(Panel):
    bl_label = "KCS Importer"
    bl_idname = "KCSImport_PT_Panel"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Tools"
    bl_context = "objectmode"

    @classmethod
    def poll(self, context):
        return context.scene is not None

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        mytool = scene.my_tool
        layout.prop(mytool, "ImpWorld")
        layout.prop(mytool, "ImpLevel")
        layout.prop(mytool, "ImpArea")
        layout.prop(mytool, "ImpScale")
        layout.prop(mytool, "ROM_path")
        layout.operator("wm.import_gfx")
        layout.separator()
        layout.label(text="Import Gfx Data")
        layout.prop(mytool, "ImpBank")
        layout.prop(mytool, "ImpID")
        layout.operator("wm.import_nld_gfx")


class Collision_PT_Panel(Panel):
    bl_label = "KCS Col Settings"
    bl_idname = "Collision_PT_Panel"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"

    @classmethod
    def poll(self, context):
        if not context.object.objprop.Collision:
            return None
        return context.material is not None

    def draw(self, context):
        layout = self.layout
        mat = context.material
        col = mat.colprop
        layout.prop(col, "NormType")
        layout.prop(col, "ColType")
        layout.prop(col, "ColParam")
        layout.prop(col, "WarpNum")


class GfxExport_PT_Panel(Panel):
    bl_label = "KCS Gfx Settings"
    bl_idname = "GfxExport_PT_Panel"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"
    bl_options = {"HIDE_HEADER"}

    @classmethod
    def poll(self, context):
        if not context.object:
            return None
        if context.object.objprop.Collision:
            return None
        return context.object is not None

    def draw(self, context):
        layout = self.layout
        layout.operator("wm.add_kcsmat")
        if not context.material:
            return
        mat = context.material
        gfx = mat.colprop
        layout.label(text="ColorCombiner Mode 1")
        self.ColorCombiner(context, layout, gfx, 1)
        if gfx.TwoCycle:
            layout.separator()
            layout.label(text="ColorCombiner Mode 2")
            self.ColorCombiner(context, layout, gfx, 2)
        layout.separator()
        layout.prop(gfx, "OtherMode_L")
        layout.separator()
        box = layout.box()
        box.prop(gfx, "ShowGeo", icon="TRIA_RIGHT")
        self.GeoMode(context, layout, gfx, box, gfx.ShowGeo)
        Cbox = layout.box()
        Cbox.prop(gfx, "ShowColor", icon="TRIA_RIGHT")
        self.ColorSel(context, layout, gfx, Cbox, mat, gfx.ShowColor)
        Obox = layout.box()
        Obox.prop(gfx, "ShowOther", icon="TRIA_RIGHT")
        self.OtherSel(context, layout, gfx, Obox, mat, gfx.ShowOther)

    def ColorCombiner(self, context, layout, gfx, num):
        row = layout.row()
        row.label(text="Mode %d Color" % num)
        row.prop(gfx, "ACmode%d" % num)
        row.prop(gfx, "BCmode%d" % num)
        row.prop(gfx, "CCmode%d" % num)
        row.prop(gfx, "DCmode%d" % num)
        row2 = layout.row()
        row2.label(text="Mode %d Alpha" % num)
        row2.prop(gfx, "AAmode%d" % num)
        row2.prop(gfx, "BAmode%d" % num)
        row2.prop(gfx, "CAmode%d" % num)
        row2.prop(gfx, "DAmode%d" % num)

    def GeoMode(self, context, layout, gfx, box, show):
        if show:
            box.label(text="Geometry Modes")
            row = box.row()
            col1 = row.column()
            col2 = row.column()
            col2.prop(gfx, "G_ZBUFFER")
            col1.prop(gfx, "G_SHADE")
            col1.prop(gfx, "G_CULL_FRONT")
            col2.prop(gfx, "G_CULL_BACK")
            col2.prop(gfx, "G_FOG")
            col1.prop(gfx, "G_LIGHTING")
            col1.prop(gfx, "G_TEXTURE_GEN")
            col2.prop(gfx, "G_TEXTURE_GEN_LINEAR")
            col1.prop(gfx, "G_SHADING_SMOOTH")
            col2.prop(gfx, "G_CLIPPING")

    def ColorSel(self, context, layout, gfx, Cbox, mat, show):
        if show:
            grid = Cbox.split()
            grid.label(text="Primary Color")
            grid.template_color_picker(gfx, "Prim", value_slider=True)
            grid.prop(gfx, "PrimA", slider=True)
            grid = Cbox.split()
            grid.label(text="Environment Color")
            grid.template_color_picker(gfx, "Env", value_slider=True)
            grid.prop(gfx, "EnvA", slider=True)
            grid = Cbox.split()
            grid.label(text="Ambient Shade Color")
            grid.template_color_picker(gfx, "Ambient", value_slider=True)
            grid.label(text="Diffuse Shade Color")
            grid.template_color_picker(gfx, "Diffuse", value_slider=True)
            Cbox.separator()
            Cbox.label(
                text="If using a texture from the original game, ignore settings below"
            )
            Cbox.template_image(
                gfx,
                "Tex",
                mat.node_tree.nodes.get("Image Texture").image_user,
                compact=False,
            )
            Cbox.prop(gfx, "Fmt")
            Cbox.prop(gfx, "Depth")
            row = Cbox.row()
            row.prop(gfx, "Tex1SFlags")
            row.prop(gfx, "Tex1TFlags")
            if gfx.TwoCycle:
                Cbox.template_image(
                    gfx,
                    "Tex2",
                    mat.node_tree.nodes.get("Image Texture.001").image_user,
                    compact=False,
                )
                Cbox.prop(gfx, "Fmt2")
                Cbox.prop(gfx, "Depth2")
                row = Cbox.row()
                row.prop(gfx, "Tex2SFlags")
                row.prop(gfx, "Tex2TFlags")

    def OtherSel(self, context, layout, gfx, Obox, mat, show):
        if show:
            Obox.prop(gfx, "AlphaCompare")
            Obox.prop(gfx, "TextFilt")
            Obox.prop(gfx, "TextLOD")
            Obox.prop(gfx, "TwoCycle")


class Nodes_PT_Panel(Panel):
    bl_label = "KCS Node Settings"
    bl_idname = "Nodes_PT_Panel"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "data"

    @classmethod
    def poll(self, context):
        if context.object.type != "CURVE":
            return None
        return context.object.data is not None

    def draw(self, context):
        layout = self.layout
        obj = context.object.data
        nodeprop = obj.nodeprop

        layout.prop(nodeprop, "NodeNum")
        layout.prop(nodeprop, "Speed")
        row = layout.row()
        row.label(text="Warp Settings")
        row.prop(nodeprop, "EnableWarpF")
        row.prop(nodeprop, "EnableWarpR")
        row = layout.row()
        row.prop(nodeprop, "Warp")
        row.prop(nodeprop, "WarpDir")
        layout.separator()
        layout.prop(nodeprop, "LockForward")
        layout.prop(nodeprop, "LockBackward")
        layout.prop(nodeprop, "Looping")
        layout.separator()
        row = layout.row()
        row.prop(nodeprop, "NextNode")
        row.prop(nodeprop, "PrevNode")


class Cam_PT_Panel(Panel):
    bl_label = "KCS Cam Settings"
    bl_idname = "Cam_PT_Panel"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "data"

    @classmethod
    def poll(self, context):
        if context.object.type != "CAMERA":
            return None
        return context.object.data is not None

    def draw(self, context):
        layout = self.layout
        obj = context.object.data
        camprop = obj.camprop
        row = layout.row()
        row.prop(camprop, "NodeNum")
        row.prop(camprop, "ProfileView")
        layout.label(text="Cam Locks")
        layout.prop(camprop, "AxisLocks")
        row = layout.row()
        row.prop(camprop, "PanH")
        row.prop(camprop, "PanUpDown")
        row.prop(camprop, "PanDown")
        row = layout.row()
        row.prop(camprop, "Yaw")
        row.prop(camprop, "Pitch")
        row.prop(camprop, "Radius")
        layout.prop(camprop, "Foc")
        layout.label(text="Camera Bound Pairs Below. Only use while axis is locked")
        row = layout.row()
        row.prop(camprop, "CamXBound")
        row.prop(camprop, "CamYBound")
        row.prop(camprop, "CamZBound")
        layout.prop(camprop, "CamPitchBound")
        layout.prop(camprop, "CamYawBound")


class Gfx_PT_Panel(Panel):
    bl_label = "KCS Gfx Settings"
    bl_idname = "Gfx_PT_Panel"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "object"

    @classmethod
    def poll(self, context):
        if context.object.type != "MESH":
            return None
        return context.object is not None

    def draw(self, context):
        layout = self.layout
        obj = context.object
        objprop = obj.objprop
        layout.prop(objprop, "Collision")
        layout.prop(objprop, "Layer")


class Ent_PT_Panel(Panel):
    bl_label = "KCS Entity ID"
    bl_idname = "Ent_PT_Panel"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "object"

    @classmethod
    def poll(self, context):
        if context.object.type != "EMPTY":
            return None
        return context.object is not None

    def draw(self, context):
        layout = self.layout
        obj = context.object
        objprop = obj.objprop
        layout.prop(objprop, "Entity")
        layout.prop(objprop, "BankNum")
        layout.prop(objprop, "IndexNum")
        layout.prop(objprop, "NodeNum")
        layout.prop(objprop, "Action")
        layout.prop(objprop, "Flags")
        layout.prop(objprop, "Respawn")
        layout.prop(objprop, "Eep")
        layout.prop(objprop, "Export_Ent")


# ------------------------------------------------------------------------
#    Registration
# ------------------------------------------------------------------------

classes = (
    MyProperties,
    KCS_OT_Export,
    KCS_OT_Export_Gfx,
    KCS_OT_Import_Gfx,
    KCS_OT_Add_Mat,
    KCS_OT_Import_NLD_Gfx,
    KCSExport_PT_Panel,
    KCSImport_PT_Panel,
    Nodes_PT_Panel,
    Cam_PT_Panel,
    NodeProp,
    CamProp,
    ObjProp,
    Gfx_PT_Panel,
    ColProp,
    Ent_PT_Panel,
    GfxExport_PT_Panel,
    Collision_PT_Panel,
    TextureProp,
)


def register():
    from bpy.utils import register_class

    for cls in classes:
        register_class(cls)

    bpy.types.Scene.my_tool = PointerProperty(type=MyProperties)
    bpy.types.Curve.nodeprop = PointerProperty(type=NodeProp)
    bpy.types.Camera.camprop = PointerProperty(type=CamProp)
    bpy.types.Object.objprop = PointerProperty(type=ObjProp)
    bpy.types.Material.colprop = PointerProperty(type=ColProp)
    bpy.types.Texture.texprop = PointerProperty(type=TextureProp)


def unregister():
    from bpy.utils import unregister_class

    for cls in reversed(classes):
        unregister_class(cls)
    del bpy.types.Scene.my_tool


if __name__ == "__main__":
    register()
