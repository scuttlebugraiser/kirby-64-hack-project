/*
 This file should be assembled onto a built ROM of kirby64
 as kirby code is non shiftable, edits are required to be done in place
 and cannot extend past the existing size of code.
 
 This file will be assembled using armips,
 see https://github.com/Kingcom/armips for more info
 
 Kirby actions are separated into init and run functions, and may have
 sub-action functions that run as well on a case by case basis.
 Which functions are run are decided by a v_table. See `Kirby Ent Vtable Map`
 in my notes (should be this same repo). In decomp the locations are 
 vtbl_80196990, D_80196A10, D_80196AE8, D_80196B88.
*/

; some globals that may help
gKirbyController equ 0x800d6fe8
set_kirby_action_1 equ 0x80122f6c


.headersize 0x800f61a0 - 0x7ec10 ; ovl2, lvl processing, 0x80077590
; Walking
; init 0x8016D3A8, run 0x8016D81C, but physics logic is done in 0x8011ED68
; the speeds present are the base speeds, and they are changed by modifiers
; these are things like terrain type or maybe carried ent
@walk_spd equ 5.0
@walk_accel equ 0.625

; Running
; init is 0X8016DA14, run is 0x8016DD0C, processing is done in 0x8011ED68
@run_spd equ 6.0
@run_accel equ 0.8

@water_spd_multiplier equ 0.5

@ability_walk_spd equ 2.0
@ability_walk_accel equ 0.5 ; accelerate only not decel
@ability_walk_spd_water equ 1.0

@rock_walk_spd equ ability_walk_spd
@double_rock_walk_spd equ 2.5

@spark_walk_spd equ ability_walk_spd
@double_spark_walk_spd equ ability_walk_spd


; these are loaded from data
.org 0x80128338
.float @walk_accel
.float @walk_spd

.float @run_accel
.float @run_spd

.org 0x8011F448
li at, @water_spd_multiplier

; rock and spark share the same speed
.org 0x8011EE24
li at, @ability_walk_accel

.org 0x8011EE40
li at, @ability_walk_spd_water
li at, @ability_walk_spd

.org 0x8011F0D4
li at, @double_rock_walk_spd



; don't write to any code before 0x80151100 after this headersize
.headersize 0x80151100 - 0xB1B40 ; ovl3, kirby, 0x8009F5C0

; Jump
; init is 0x8016DDE8, run is 0x8016E15C, processing is done in 0x8011ED68
; (really 0x8011F58C and forward though).
@jump_spd_init equ 17.0
@jump_gravity equ -0.980665 ; kirby on earth confirmed
@jump_height_var_unk equ 8 ; idk what this does, but it controls whether you fast fall or not

.org 0x8016dF08
li at, @jump_height_var_unk

.org 0x8016dF10
li at, @jump_spd_init

.org 0x80197360
.float @jump_gravity

; Falling
; init is 0x8016EF5C, run is 0x8016F240


; Float
; init is 0x8016FD88, run is 0x80170D88


; Swim
; init is 0x80176860, run is 0x80176DE0
@swim_accel equ 0.24
@swim_speed equ 3.4

.org 0x801974D4
.float @swim_accel
.float @swim_speed

; Crouch Slide
; init is 0x801736BC, run is 0x80173AF4
; speed is set to 9, and then it decels until it hits 0
@crouch_slide_init_speed equ 9.0
@crouch_slide_decel equ 0.5

.org 0x80173748
li at, @crouch_slide_init_speed

.org 0x80173784
li at, @crouch_slide_init_speed

; decel seems to be common with other things and it breaks a bunch of stuff
; as a ref decel is set by func_80120AF8 called at 0x80173884 
/*
this is replacement code for the func loading
.org 0x80173884
lui at, @crouch_slide_decel
addiu a0, sp, 0x54
lw v0, 0x0 (s1)
mtc1 at, f8

*/

; jump out of crouch slide, does not work, hangs game
/*
.org 0x80173B7C
j 0x800C0540 ; this is dev string space
NOP

.org 0x800C0540
lui t3, 0x8013
lw t3, 0xE7F0 (t3) ; basically checking if ability is ended
lui t4, 0x8013
bnez t3, @@return_slide_end
lui v1, 0x8005

; add a second end condition, which is jumping
lw t3, gKirbyController
andi t3, t3, 0x8000 ; A btn
beqz t3, @@return_cont
li a0, 0x3
jal set_kirby_action_1
li a1, 0x5


j 0x80173CA8 ; func end
NOP

@@return_cont
j 0x80173B94
NOP
@@return_slide_end
j 0x80173BEC
*/

; Single Fire
; init is 0x80179370, run is 0x8017982C
; fire has six different speeds, based on held direction and underwater state
; that said it doesn't seem like half of them are used, so there is potential
; to make more changes

@fire_dir_agree equ 10.5
@fire_dir_agree_water equ 10.5

@fire_dir_differ equ 6.5
@fire_dir_differ_water equ 6.5

@fire_dir_neutral equ 8.5
@fire_dir_neutral_water equ 8.5

; agree spds
.org 0x801799A0
li at, @fire_dir_agree

.org 0x801799BC
li at, @fire_dir_agree_water

; differ spds
.org 0x80179A34
li at, @fire_dir_differ

.org 0x80179A50
li at, @fire_dir_differ_water

; neutral spds
.org 0x80179AD8
li at, @fire_dir_neutral

.org 0x80179AF4
li at, @fire_dir_neutral_water

; Rock & Double Rock
; rock: init is 0x80179C28, run is 0x8017A390
; rock/rock: init is 0x8017F1C0, run is 0x8017F988
; speeds are set in the walking function 0x8011ED68


; Ice
; init is 0x8017B068, run is 0x8017B3C4
; idk what you'd change


; Spark
; init is 0x8017BF34, run is 0x8017C1FC

; this gets overwritten instantly in the walk func so it is kind of useless
.org 0x8017c160
li at, @spark_walk_spd

; Fire/Ice
; init is 0x8017E074, run is 0x8017E1EC


; Ice/Needle
; init is 0x80181F64, run is 0x80181CFC


; Cutter/Rock
; init is 0x80189914, run is 0x8018B228