# -------------------------------------------------------------------------------
# Animation Enums
# -------------------------------------------------------------------------------

HS = 0x80057DB0

anim_data_includes = """
#include <ultra64.h>
#include "anim_types.h"
#include "anim.h"
"""


anim_header = {
    0x00: ("struct AnimBone", "bone_ptr", "ptr"),
    0x04: ("u32", "anim_mode"),
    0x08: ("u32", "num_virtual_converts"),
}

cmd_enums = {
    0: "Stop",
    1: "Goto",
    2: "Block",
    3: "Lerp_Cusp_B",
    4: "Lerp_Cusp_NB",
    5: "Lerp_Vel_B",
    6: "Lerp_Vel_NB",
    7: "Set_Lerp_Velocity",
    8: "Lerp_Ease_B",
    9: "Lerp_Ease_NB",
    10: "Set_B",
    11: "Set_NB",
    12: "Extend",
    13: "UnkD",
    14: "Loop",
    15: "SetProcessFlag",
    16: "Unk10",
    17: "Unk11",
    18: "Set_Color_B",
    19: "Set_Color_NB",
    20: "Lerp_Color_B",
    21: "Lerp_Color_NB",
    22: "Set_Int_Field_B",
    23: "Unk17"
}
