from __future__ import annotations

# file to exporting animation bins, and animations in an annotated
# file structure made up of macros. Anims are packed bytecode, so there
# is no struct formatting for them

import struct, sys, os
from pathlib import Path
from collections import namedtuple
from dataclasses import dataclass, field
from copy import copy

# if the path is from the same directory, these imports will work
# otherwise it will use an absolute import as if it was from the parent
try:
    from Anim_Types import *
except:
    from anims.Anim_Types import *

# to import utility.py, we need to add parent dir to path
file = Path(os.path.dirname(__file__))
sys.path.append(str(file.parent))
from Utility import *

# -------------------------------------------------------------------------------
# Anim Processing
# -------------------------------------------------------------------------------

# this animation class holds all the anim data in the file
class AnimBin(BinProcess, BinWrite):
    debug = False
    def __init__(self, file, ptr=None, debug: Bool = False):
        self.debug = debug
        self.file = file
        self.ptrs = ptr
        self.symbols = dict()
        self.header = []
        self.child_symbols = []
        self.anim_header = self.upt(0, ">3L", 12)
        num_parts = self.anim_header[2]
        self.mode = self.anim_header[1]
        self.anim_scripts = []
        self.anim_offsets = []
        self.anim_starts = []
        self.virtual_offsets = [self.upt(12 + 4 * i, ">L", 4) for i in range(num_parts)]
        # mode 2 just starts
        if self.mode == 2:
            return self.decode_mode_2(file)
        data_start = min(self.upt(offset, ">L", 4) for offset in self.virtual_offsets)
        head = self.anim_header[0]
        self.symbols[head] = "anim_offsets"
        while head < data_start:
            start = self.upt(head, ">L", 4)
            self.anim_offsets.append(start)
            head += 4
        if self.mode == 0:
            return self.decode_mode_0(file)
        elif self.mode == 1:
            return self.decode_mode_1(file)

    def decode_mode_0(self, file):
        for bone in self.anim_offsets:
            if bone == 0:
                continue
            self.anim_scripts.append(AnimScript(file, bone, debug = self.debug))
            self.anim_starts.append(bone)
            self.symbols[bone] = f"anim_script_{bone:X}"
        self.virtual_offsets = [
            self.get_symbol_from_locations(
                {
                    "anim_script_{}{}": self.anim_starts,
                    "anim_offsets{1}": [self.anim_header[0]],
                },
                offset,
                size=4,
            )
            for offset in self.virtual_offsets
        ]
        self.anim_offsets = [
            self.get_symbol_from_locations(
                {
                    "anim_offsets{1}": [self.anim_header[0]],
                    "anim_script_{}{}": self.anim_starts
                }, offset, size=4
            )
            for offset in self.anim_offsets
        ]
    
    def decode_mode_1(self, file):
        self.texture_scroll_anims = []
        ranges = [offset for offset in self.anim_offsets if offset]
        ranges = [*sorted(ranges), len(file)]
        for start, end in zip(ranges[0::1], ranges[1::1]):
            head = start
            scrolls = []
            while head < end:
                start = self.upt(head, ">L", 4)
                self.texture_scroll_anims.append(start)
                if start:
                    self.anim_scripts.append(AnimScript(file, start, debug = self.debug))
                    self.anim_starts.append(start)
                    self.symbols[start] = f"anim_script_{start:X}"
                head += 4
        self.virtual_offsets = [
            self.get_symbol_from_locations(
                {
                    "anim_offsets{1}": [self.anim_header[0]],
                    "anim_script_{}{}": self.anim_starts,
                    "scroll_offsets{1}": [
                        min([off for off in self.anim_offsets if off])
                    ],
                },
                offset,
                size=4,
            )
            for offset in self.virtual_offsets
        ]
        self.anim_offsets = [
            self.get_symbol_from_locations(
                {"scroll_offsets{1}": [ranges[0]]}, offset, size=4
            )
            for offset in self.anim_offsets
        ]
    
    def decode_mode_2(self, file):
        self.anim_scripts.append(AnimScript(file, self.anim_header[0], debug = self.debug))

    def unique_cmds(self):
        cmds = set()
        for bone in self.anim_scripts:
            cmds.update(bone.unique_cmds())
        return cmds

    def Write(self, file):
        # write comment with file location
        if self.ptrs:
            file.write(
                f"//start 0x{self.ptrs[0]:X} - end 0x{self.ptrs[1]:X}\n{anim_data_includes}\n"
            )
        # write global include statements

        self.WriteDictStruct(
            self.anim_header,
            anim_header,
            file,
            "",
            "Animheader Animation_Header",
            symbols=self.symbols,
            static=True,
        )

        if self.mode != 2:
            self.write_arr(
                file,
                "void *virtual_offsets",
                self.virtual_offsets,
                self.format_arr_ptr,
                length=self.anim_header[2],
                static=True,
                prefix="&"
            )

        if self.mode != 2:
            self.write_arr(
                file,
                "void *anim_offsets",
                self.anim_offsets,
                self.format_arr_ptr,
                length=len(self.anim_offsets),
                static=True,
                prefix="&"
            )

        for anims in self.anim_scripts:
            anims.write(file)
            self.add_header_child(anims)

        if self.mode == 1:
            self.write_arr(
                file,
                "void *scroll_offsets",
                self.texture_scroll_anims,
                self.format_arr_symbols,
                length=len(self.texture_scroll_anims),
                static=True,
                symbols=self.symbols,
            )


class AnimScript(BinProcess, BinWrite):
    debug = False
    def __init__(self, file, start, debug: Bool = False):
        self.file = file
        self.debug = debug
        x = 0
        self.cmds = []
        self.header = []
        self.start = start
        while True:
            cmd = self.upt(start + x, ">L", 4)
            if self.debug:
                print(f"cmd start: 0x{start + x:X}")
            anim_cmd = self.get_cmd(cmd)
            x += anim_cmd.unpack_transform(file, start + x)
            if self.debug:
                print(anim_cmd)
            self.cmds.append(anim_cmd)
            if anim_cmd.cmd in [0x0, 0xE] or (start + x) >= len(file):
                break

    def unique_cmds(self):
        return set(cmd.cmd for cmd in self.cmds)

    def get_cmd(self, cmd):
        case = cmd >> 25
        if self.debug:
            print(f"cmd MSB: 0x{case:X}")
        assert case < 24
        scale = cmd & 0x7FFF
        transform = ((cmd << 7) >> 0x16) & 0x3FF
        # cmds over 0x10 use integer arguments, they seem to affect some
        # sort of texture scroll channel
        if case in [0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17]:
            # I won't know which channel is what, at least for now
            num_chans = bin(transform).count("1")
            chan_transform = [ChannelTransform(transform, case, "Field") for i in range(num_chans)]
            anim_cmd = AnimCmd(case, scale, chan_transform, transform_bitfield = transform)
        # aforementioned cmds just have single transform/value
        elif case not in [0x0, 0x1, 0x2, 0xC, 0xD, 0xE]:
            rotate_transform = FieldTransform(transform & 0x7, "Rotate", has_vel = (case in [0x5, 0x6]))
            translate_transform = FieldTransform((transform & 0x70) >> 4, "Translate", has_vel = (case in [0x5, 0x6]))
            scale_transform = FieldTransform((transform & 0x380) >> 7, "Scale", has_vel = (case in [0x5, 0x6]))
            base_transform = ChannelTransform(transform & 0x8, case, "Base", has_vel = (case in [0x5, 0x6]))
            transforms = [base_transform, rotate_transform, translate_transform, scale_transform]
            anim_cmd = AnimCmd(case, scale, transforms, transform_bitfield = transform)
        else:
            anim_cmd = AnimCmd(case, scale, None, transform_bitfield = transform)
        return anim_cmd

    def write(self, file):
        length = sum([cmd.length() for cmd in self.cmds])
        self.add_header_data(f"AnimScript anim_script_{self.start:X}[{length}]", static=True)
        file.write(f"static AnimScript anim_script_{self.start:X}[{length}] = {{\n")
        for cmd in self.cmds:
            cmd.write(file)
        file.write("};\n\n")


class ChannelTransform(BinProcess):
    def __init__(self, field, cmd, name: str = "", has_vel: bool = False):
        self.field = field or None
        self.cmd = cmd
        self.name = name
        # a vel cmd has two floats per field
        self.has_vel = has_vel

    def __bool__(self):
        return self.has_transform

    def __str__(self):
        return str(self.field)

    def __format__(self, format_spec):
        return format(self.field, format_spec)

    def length(self):
        if self.has_vel:
            return 8
        else:
            return 4 if self.has_transform else 0

    @property
    def has_transform(self):
        return self.field != None

    def write(self):
        if is_arr(self.field):
            return f"Color{str(self.field)}"
        else:
            return f"{self.name}({self.field})"

    def unpack_dat(self, dat: bytes):
        if self.cmd == 0x11:
            return self.unpack_int(dat)
        if self.cmd > 0x11 and self.cmd < 0x17:
            return self.unpack_scroll(dat)
        else:
            return self.unpack_float(dat)

    def unpack_int(self, dat: bytes):
        self.file = dat
        self.field = self.upt(0, ">L", 4)
        return 4

    def unpack_float(self, dat: bytes):
        self.file = dat
        if self.has_vel:
            self.field = self.upt(0, ">2f", 8)
            return 8
        else:
            self.field = self.upt(0, ">f", 4)
            return 4

    def unpack_scroll(self, dat: bytes):
        self.file = dat
        self.field = self.upt(0, ">4B", 4)
        return 4


class FieldTransform(BinProcess):
    def __init__(self, bit_field: int, name: str, has_vel: bool = False):
        self.x = bit_field & 4 != 0
        self.y = bit_field & 2 != 0
        self.z = bit_field & 1 != 0
        # a vel cmd has two floats per field
        self.has_vel = has_vel
        self._len = sum(self.fields) * 4 * (1 + has_vel)
        self.name = name

    def __bool__(self):
        return self.has_transform

    def __str__(self):
        return f"x: {self.x}, y: {self.y}, z: {self.z}"

    @property
    def has_transform(self):
        return any([self.exists(field) for field in self.fields])

    @property
    def fields(self):
        return self.x, self.y, self.z
        
    @property
    def write_fields(self):
        fields = []
        for val in self.fields:
            if hasattr(val, "__iter__"):
                fields.extend(val)
            else:
                fields.append(val)
        return fields

    def exists(self, val):
        return val is not False

    def length(self):
        return self._len

    def write(self):
        def str_out(val):
            return f"{val}f"

        return f"{self.name}_{'X' if self.exists(self.x) else ''}{'Y' if self.exists(self.y) else ''}{'Z' if self.exists(self.z) else ''}({', '.join([str_out(val) for val in self.write_fields if val is not False])})"

    def unpack_dat(self, dat: bytes):
        self.file = dat
        head = 0
        
        def up_float(has_vel, head):
            if has_vel:
                return self.upt(head, ">2f", 8), head + 8
            else:
                return self.upt(head, ">f", 4), head + 4
        
        if self.exists(self.x):
            self.x, head = up_float(self.has_vel, head)
        if self.exists(self.y):
            self.y, head = up_float(self.has_vel, head)
        if self.exists(self.z):
            self.z, head = up_float(self.has_vel, head)
        return head


@dataclass
class AnimCmd(BinProcess):
    cmd: int
    scale: int
    transforms: Union[float, tuple[FieldTransform]]
    transform_bitfield: int = 0

    def length(self):
        val = 4
        if self.transforms:
            if is_arr(self.transforms):
                val += sum([cmd.length() for cmd in self.transforms])
            else:
                val += self.transforms.length()
        return val // 4

    def __str__(self):
        if hasattr(self.transforms, "__iter__"):
            transforms = ", ".join(str(transform) for transform in self.transforms)
        else:
            transforms = self.transforms
        return f"cmd: {hex(self.cmd)}, scale: {self.scale}, transform: {transforms}"

    def unpack_transform(self, file, start):
        # these cmds have no args
        if self.cmd in [0x0, 0x1, 0x2, 0xC, 0xF, 0x10]:
            return 4

        # these cmds have a fixed number of args
        if not self.transforms:
            self.file = file
            if self.cmd == 0xD:
                self.transforms = ChannelTransform(self.upt(start + 4, ">2L", 8), self.cmd)
                return 12
            if self.cmd == 0xE:
                self.transforms = ChannelTransform(self.upt(start + 4, ">L", 4), self.cmd)
                return 8
            if self.cmd == 0x17:
                self.transforms = ChannelTransform(self.upt(start + 4, ">2f", 8), self.cmd)
                return 12
            raise Exception(f"cmd {self} has no unpacking instructions")

        x = 4
        for transform in self.transforms:
            if not transform or not transform.has_transform:
                continue
            dat = file[start + x : start + x + transform.length()]
            x += transform.unpack_dat(dat)
        return x

    def transform_str(self):
        if not hasattr(self.transforms, "__iter__"):
            return str(self.transforms)
        return ", ".join([transform.write() for transform in self.transforms if transform])

    def write_cmd_16(self, file):
        file.write(f"\t{cmd_enums.get(self.cmd, self.cmd)}(0x{self.transform_bitfield:X}, {self.scale}),\n")
    
    def write_cmd_15(self, file):
        file.write(f"\t{cmd_enums.get(self.cmd, self.cmd)}(0x{self.transform_bitfield:X}, {self.scale}),\n")

    def write_cmd_14(self, file):
        file.write(f"\t{cmd_enums.get(self.cmd, self.cmd)}(0x{self.transforms:X}),\n")
        
    def write_cmd_12(self, file):
        file.write(f"\t{cmd_enums.get(self.cmd, self.cmd)}(0x{self.transform_bitfield:X}, {self.scale}),\n")

    def write_cmd_2(self, file):
        file.write(f"\t{cmd_enums.get(self.cmd, self.cmd)}({self.scale}),\n")
    
    def write_cmd_0(self, file):
        file.write(f"\t{cmd_enums.get(self.cmd, self.cmd)}(),\n")

    def write(self, file):
        write_func = getattr(self, f"write_cmd_{self.cmd}", None)
        if write_func:
            write_func(file)
            return
        else:
            file.write(
                f"\t{cmd_enums.get(self.cmd, self.cmd)}(0x{self.transform_bitfield:X}, {self.scale}, {self.transform_str()}),\n"
            )


# -------------------------------------------------------------------------------
# Main
# -------------------------------------------------------------------------------


def GetSingleAnim(bank, index, Kirb: "binary ROM file", path=None, debug: bool = False):
    ptrs = GetPointers(bank, index, "Anim", Kirb)
    if path:
        name = path
    else:
        name = f"assets/anim/bank_{bank}/{index}"
    Path(name).mkdir(exist_ok=True, parents=True)
    WriteBin(Kirb, *ptrs, str(name) + "/anim")
    with open(Path(name) / "anim.bin", "rb") as f:
        block = AnimBin(f.read(), ptr=ptrs, debug = debug)
    with open(Path(name) / "anim.c", "w", newline="") as f:
        block.Write(f)
    with open(Path(name) / "anim.h", "w", newline='') as f:
        block.write_header(f)
    print(f"Animation {bank} index {index} file wrote")
    return block.unique_cmds()


# exports all geo block data from an entire bank
def ExportAnimBank(bank, Kirb: "binary ROM file", debug: bool = False, start = 1):
    cmds = set()
    for index in LoopOverBank(bank, Kirb, block_type="Anim", start = start):
        cmds.update(GetSingleAnim(bank, index, Kirb, debug = debug))


def main(Kirb: "binary ROM file"):
    for i in range(0, 8):
        ExportAnimBank(i, Kirb)
    # ExportAnimBank(7, Kirb, debug = True, start = 19)
    # GetSingleAnim(0, 1, Kirb, debug = True)


# this file only works if called from the folder Split_Tools
# otherwise the pathing will not be correct
if __name__ == "__main__":
    Kirby = open(sys.argv[1], "rb")
    Kirb = Kirby.read()
    main(Kirb)
    print("Anim Exports finished")
