# Splits up geo blocks into c files
# the basis classes here are mostly the same ones used in the kirby decomp plugin

# imports
import os, sys, struct, shutil, math, re
from functools import lru_cache
from collections import namedtuple
from dataclasses import dataclass, field
from pathlib import Path
from copy import deepcopy

# if the path is from the same directory, these imports will work
# otherwise it will use an absolute import as if it was from the parent
try:
    import f3d.F3DEX2_gbi as f3dex2
    from GfxDataTypes import *
    import AnalyzeGeo
except:
    from gfx.f3d import F3DEX2_gbi as f3dex2
    from gfx.GfxDataTypes import *
    import gfx.AnalyzeGeo as AnalyzeGeo

# to import utility.py, we need to add parent dir to path
file = Path(os.path.dirname(__file__))
sys.path.append(str(file.parent))
from Utility import *

# -------------------------------------------------------------------------------
# Geo Blocks Decoding
# -------------------------------------------------------------------------------

# geo data

# these aren't actually all ints
@dataclass
class layout(BinProcess, BinWrite):
    flag: int
    depth: int
    ptr: int
    translation: int
    rotation: int
    scale: int
    index: int = 0
    # a manual DL stop
    stop = None

    @property
    def is_vfx(self):
        return (self.ptr >> 24) == 0x80

    def Write(self, file, num=0):
        dat = [
            self.flag,
            self.depth,
            self.ptr,
            self.translation,
            self.rotation,
            self.scale,
        ]
        self.WriteDictStruct(
            dat, Layout, file, "", f"Layout Geo_LY_{num}", self.symbols, static=True
        )


# fake storage class that mirrors methods of layout
@dataclass
class faux_LY:
    ptr: int
    index: int = 0
    header = []
    # a manual DL stop
    stop = None

    def Write(self, file, num=0):
        pass


@dataclass
class GfxList:
    commands: list = field(default_factory=list)
    length: int = 0
    
    def append(self, item):
        self.commands.append(item)
    
    def extend(self, items):
        self.commands.extend(items)


class Vertices(BinWrite):
    _Vec3 = namedtuple("Vec3", "x y z")
    _UV = namedtuple("UV", "s t")
    _color = namedtuple("rgba", "r g b a")

    def __init__(self):
        self.UVs = []
        self.VCs = []
        self.Pos = []

    def _make(self, v):
        self.Pos.append(self._Vec3._make(v[0:3]))
        self.UVs.append(self._UV._make(v[4:6]))
        self.VCs.append(self._color._make(v[6:10]))

    # has a little bit of special formatting so it uses its own method
    def Write(self, file):
        self.add_header_data(f"Vtx Geo_Vertices[{len(self.Pos)}]", static = True)
        file.write(f"static Vtx Geo_Vertices[{len(self.Pos)}] = {{\n")

        def spc(x):
            return "{" + ", ".join([hex(a) for a in x]) + "}"

        for position, uv, colorOrNormal in zip(self.Pos, self.UVs, self.VCs):
            line = (
                "{{ "
                + ", ".join([spc(position), "0", spc(uv), spc(colorOrNormal)])
                + " }}"
            )
            file.write(f"\t{line},\n")
        file.write("};\n\n")


# this is the class that holds the actual individual scroll struct and textures
class tx_scroll:
    _scroll = namedtuple(
        "texture_scroll", " ".join([x[1] for x in TextureScrollStruct.values()])
    )

    def __init__(self, *args):
        self.scroll = self._scroll._make(*args)

    # a debug print
    def Print(self):
        print(f"num tex {len(self.textures)} field0 0x{self.scroll.field_0x4c:X}")


# each texture scroll will start from an array of ptrs, and each ptr will reference
# tex scroll data
class Tex_Scroll(BinProcess, BinWrite):
    def __init__(self, scroll_ptrs, file, ptr):
        self.scroll_ptrs = scroll_ptrs
        self.scrolls = {}
        self.file = file
        self.ptr = ptr
        self.textures = dict()
        self.palettes = dict()
        self.symbols = dict()
        for p in scroll_ptrs:
            if p != 0x99999999 and p:
                # get struct
                scr = tx_scroll(
                    self.extract_dict(self.seg2phys(p), TextureScrollStruct)
                )
                self.scrolls[p] = scr
                # search for palletes
                if scr.scroll.palettes:
                    start = self.seg2phys(scr.scroll.palettes)
                    self.palettes[scr.scroll.palettes] = self.get_BI_pairs(
                        start, stop=(0x9999, 0x9999)
                    )
                    self.symbols[scr.scroll.palettes] = (
                        "(void *)",
                        f"tx_scroll_palettes_{scr.scroll.palettes:X}",
                    )
                # search for textures
                if scr.scroll.textures:
                    start = self.seg2phys(scr.scroll.textures)
                    self.textures[scr.scroll.textures] = self.get_BI_pairs(
                        start, stop=(0x9999, 0x9999)
                    )
                    self.symbols[scr.scroll.textures] = (
                        "(void *)",
                        f"tx_scroll_textures_{scr.scroll.textures:X}",
                    )

    @property
    def data(self):
        return {**self.scrolls, **self.textures, **self.palettes}

    # in order to make this sort in write order, I will have to sort the dict containing
    # all Tex_Scroll objects at once, since they are not consistent with order based on usage

    # only call this after accumulating all Tex_Scroll.data into one Tex_Scroll object
    def Write(self, file):
        # sort structs in case they aren't in order
        write_vals = self.SortDict(self.data)
        for val_start, val_list in write_vals.items():
            # textures
            if val_start in self.textures:
                self.write_arr(
                    file,
                    f"void *tx_scroll_textures_{val_start:X}",
                    val_list,
                    self.format_arr_ptr,
                    BI=1,
                    length=len(val_list),
                    static=True
                )
            # palletes
            elif val_start in self.palettes:
                self.write_arr(
                    file,
                    f"void *tx_scroll_palettes_{val_start:X}",
                    val_list,
                    self.format_arr_ptr,
                    BI=1,
                    length=len(val_list),
                    static=True
                )
            else:
                # struct
                self.WriteDictStruct(
                    val_list.scroll,
                    TextureScrollW,
                    file,
                    "",
                    f"TextureScroll scroll_{val_start:X}",
                    symbols=self.symbols,
                    align="ALIGNED8",
                    static=True
                )

    def WriteHeader(self, file):
        # write header, separate func because these are at the end of the file
        self.write_arr(
            file,
            f"struct TextureScroll *tx_scroll_hdr_{self.ptr:X}",
            self.scroll_ptrs,
            self.format_arr_ptr,
            ptr_format="&scroll_",
            length=len(self.scroll_ptrs),
            static=True
        )

    def Print(self, scroll):
        print(f"flags {scroll.scroll.flags:X}")


class GeoBin(BinProcess, BinWrite):
    _Vec3 = namedtuple("Vec3", "x y z")
    _texture = namedtuple("texture", "fmt siz bank_index")

    def __init__(self, file, ptr=None):
        self.file = file
        self.ptrs = ptr
        self.header = []
        self.child_symbols = []
        self.max_vert = 0
        self.symbols = dict()
        self.main_header = self.upt(0, ">8L", 32)
        self.get_tex_scrolls()
        self.DLs = (
            dict()
        )  # this is also in layouts, but I want it raw here to print in RAM order, and in layouts to analyze in the processed order
        self.render_mode = self.main_header[2]
        self._render_mode_map[self.render_mode](self)
        # get vtx and img refs null terminated arrays
        self.get_refs()
        self.get_anims()

    def get_tex_scrolls(self):
        if self.main_header[1]:
            start = self.main_header[1]
            # get header of POINTERS
            self.tex_header = self.get_referece_list(start, stop=0x99999999)
            self.tex_scrolls = {}
            for p in self.tex_header:
                if p and p != 0x99999999:
                    self.tex_scrolls[p] = Tex_Scroll(
                        self.get_referece_list(p, stop=0x99999999), self.file, p
                    )
                    self.tex_scrolls[p].header = self.header
            # sort scrolls
            self.tex_scrolls = self.SortDict(self.tex_scrolls)
            # add symbols
            self.symbols[self.main_header[1]] = (
                "(struct TextureScroll ***)",
                "tx_scroll_header",
            )

    # anims are bank indices
    def get_anims(self):
        num = self.main_header[5]
        start = self.seg2phys(self.main_header[6])
        if start:
            self.anims = self.get_BI_pairs(start, num=num)
            self.symbols[self.main_header[6]] = ("(void *)", "anims")

    # both types of refs are null terminated lists
    def get_refs(self):
        self.img_refs = [
            self.get_symbol_from_locations({"&DL_{}{}": self.DLs.keys()}, img_ref)
            for img_ref in self.get_referece_list(self.main_header[3])
        ]
        self.vtx_refs = [
            self.get_symbol_from_locations({"&DL_{}{}": self.DLs.keys()}, vtx_ref)
            for vtx_ref in self.get_referece_list(self.main_header[4])
        ]
        # add symbols
        self.symbols.update(
            {
                self.main_header[3]: ("(Gfx **)", "img_refs"),
                self.main_header[4]: ("(Gfx **)", "vtx_refs"),
            }
        )

    # no layout, just a single DL
    def decode_layout_13(self):
        L = faux_LY(self.main_header[0])
        L.dl_ptrs = [L.ptr]
        self.layouts = [L]
        starts = self.decode_f3d_bin(L)
        Vert_End = self.seg2phys(L.ptr)
        self.decode_vertices(starts)
        self.symbols[self.main_header[0]] = ("(Gfx *)", f"DL_{self.main_header[0]:X}")

    # no layouts, just an entry point
    def decode_layout_14(self):
        L = faux_LY(self.main_header[0])
        self.decode_entry(L)
        self.layouts = [L]
        starts = self.decode_f3d_bin(L)
        self.decode_vertices(starts)
        self.symbols[self.main_header[0]] = (
            "(struct EntryPoint  *)",
            f"Entry_Point_{self.main_header[0]:X}",
        )

    # layouts point to DL
    def decode_layout_17(self):
        self.get_layouts()
        starts = []
        for l in self.layouts:
            if l.ptr and (l.ptr >> 24) == 0x04:
                l.dl_ptrs = [l.ptr]
                starts.extend(self.decode_f3d_bin(l))
        if starts:
            self.decode_vertices(starts)
        else:
            starts = [self.seg2phys(self.main_header[3])]
            self.decode_stray(starts)
        self.symbols[self.main_header[0]] = ("(struct Layout  *)", f"Geo_LY_0")

    # layouts point to entry point
    def decode_layout_18(self):
        self.get_layouts()
        starts = []
        for l in self.layouts:
            if l.ptr and (l.ptr >> 24) == 0x04:
                self.decode_entry(l)
                starts.extend(self.decode_f3d_bin(l))
        if starts:
            self.decode_vertices(starts)
        else:
            starts = [self.seg2phys(self.main_header[3])]
            self.decode_stray(starts)
        self.symbols[self.main_header[0]] = ("(struct Layout  *)", f"Geo_LY_0")

    # layout points to pair of DLs
    def decode_layout_1B(self):
        self.get_layouts()
        starts = []
        for l in self.layouts:
            if l.ptr and (l.ptr >> 24) == 0x04:
                self.decode_DL_pair(l)
                starts.extend(self.decode_f3d_bin(l))
        if starts:
            self.decode_vertices(starts)
        else:
            starts = [self.seg2phys(self.main_header[3])]
            self.decode_stray(starts)
        self.symbols[self.main_header[0]] = ("(struct Layout  *)", f"Geo_LY_0")

    # layout points to entry point with pair of DL
    def decode_layout_1C(self):
        self.get_layouts()
        starts = []
        for l in self.layouts:
            if l.ptr and (l.ptr >> 24) == 0x04:
                self.decode_entry_dbl(l)
                starts.extend(self.decode_f3d_bin(l))
        if starts:
            self.decode_vertices(starts)
        else:
            starts = [self.seg2phys(self.main_header[3])]
            self.decode_stray(starts)
        self.symbols[self.main_header[0]] = ("(struct Layout  *)", f"Geo_LY_0")

    def get_layouts(self):
        self.layouts = [
            self.decode_layout(self.main_header[0] + 0x2C * i, index=i)
            for i in range(self.main_header[-1] + 1)
        ]
        # env vfx exists
        if self.layouts[-1].is_vfx:
            start = len(self.layouts)
            x = 0
            while True:
                new_ly = self.decode_layout(
                    self.main_header[0] + 0x2C * (start + x), index=(start + x)
                )
                self.layouts.append(new_ly)
                x += 1
                if new_ly.flag & 0x8000:
                    break

    def decode_layout(self, start, index=None):
        start = self.seg2phys(start)
        LY = self.upt(start, ">2HL9f", 0x2C)
        v = self._Vec3._make
        LY = layout(*LY[0:3], v(LY[3:6]), v(LY[6:9]), v(LY[9:12]), index=index)
        LY.header = self.header
        self.Layout_Symbols(LY)
        return LY

    # has to be after func declarations
    _render_mode_map = {
        0x13: decode_layout_13,
        0x14: decode_layout_14,
        0x17: decode_layout_17,
        0x18: decode_layout_18,
        0x1B: decode_layout_1B,
        0x1C: decode_layout_1C,
    }

    def Layout_Symbols(self, LY):
        # names for each ptr type
        render_mode_symbols = {
            0x17: "DL_{:X}",
            0x18: "Entry_Point_{:X}",
            0x1B: "DL_Pair_{:X}",
            0x1C: "Entry_Pair_{:X}",
        }
        if LY.ptr and not LY.is_vfx:
            st = render_mode_symbols.get(self.render_mode).format(LY.ptr)
            LY.symbols = {LY.ptr: ("(struct EntryPoint *)", st)}
        elif LY.is_vfx:
            LY.symbols = None
        else:
            LY.symbols = {LY.ptr: "NULL"}

    def decode_DL_pair(self, ly: layout):
        ly.dl_ptrs = []  # just a literal list of ptrs
        ptrs = self.upt(self.seg2phys(ly.ptr), ">2L", 8)
        for ptr in ptrs:
            if ptr:
                ly.dl_ptrs.append(ptr)
        ly.DL_Pair = ptrs

    def decode_entry_dbl(self, ly: layout):
        x = 0
        start = self.seg2phys(ly.ptr)
        ly.entry_dbls = []
        ly.dl_ptrs = []  # just a literal list of ptrs
        while True:
            mark, *ptrs = self.upt(start + x, ">3L", 12)
            ly.entry_dbls.append((mark, ptrs))
            if mark == 4:
                return
            else:
                for ptr in ptrs:
                    if ptr:
                        ly.dl_ptrs.append(ptr)
            x += 12
            # shouldn't execute
            if x > 120:
                print("your while loop is broken in GeoBin.decode_entry")
                break

    def decode_entry(self, ly: layout):
        x = 0
        start = self.seg2phys(ly.ptr)
        ly.entry_pts = []  # the actual entry pt raw data
        ly.dl_ptrs = []  # just a literal list of ptrs
        while True:
            mark, ptr = self.upt(start + x, ">2L", 8)
            ly.entry_pts.append((mark, ptr))
            if mark == 4:
                return
            if ptr == 0:
                continue
            else:
                ly.dl_ptrs.append(ptr)
            x += 8
            # shouldn't execute
            if x > 80:
                print("your while loop is broken in GeoBin.decode_entry")
                break

    # use a module for f3d decoding
    def decode_f3d_bin(self, layout):
        DLs = {}
        starts = []
        # create shallow copy, use this for analyzing DL, while DL ptrs will be a dict including jumped to DLs
        layout.entry = layout.dl_ptrs[:]
        for dl in layout.dl_ptrs:
            start = self.seg2phys(dl)
            starts.append(start)
            f3d = self.decode_dl_bin(start, layout)
            self.DLs[dl] = f3d
            DLs[dl] = f3d
        layout.DLs = DLs
        return starts

    def DL_ptr(self, num):
        if num >> 24 == 0xE:
            # these pointers are dynamically created, but need symbols
            # so I will add them now, instead of when writing like for existing ones

            # for now, since these symbols don't exist in the file, I will cast them as ints
            # self.add_header_data(f"Gfx DL_{num:X}[]")
            # print(f"Gfx DL_{num:X}")
            return None
        else:
            return num

    def decode_dl_bin(self, start, layout):
        DL = GfxList()
        x = 0
        while True:
            cmd = self.Getf3dCmd(self.file[start + x : start + x + 8])
            x += 8
            if not cmd:
                continue
            name, args = self.split_args(cmd)
            # check for multi length cmd (branchZ and tex rect generally)
            extra = f3dex2.check_double_cmd(name)
            if extra:
                name, args = f3dex2.fix_multi_cmd(
                    self.file[start + x : start + x + extra], name, args
                )
                x += extra
            # adjust vertex pointer so it is an index into vert arr
            if name == "gsSPVertex":
                args[0] = self.seg2phys(int(args[0]) - 0x20) // 0x10
                if (args[0] + int(args[1])) > self.max_vert:
                    self.max_vert = args[0] + int(args[1])
            # check for flow control
            if name == "gsSPEndDisplayList":
                break
            elif name == "gsSPDisplayList":
                ptr = self.DL_ptr(int(args[0]))
                if ptr:
                    layout.dl_ptrs.append(ptr)
                DL.append((name, args))
                continue
            elif name == "gsSPBranchLessZraw":
                ptr = self.DL_ptr(int(args[0]))
                if ptr:
                    layout.dl_ptrs.append(ptr)
                DL.append((name, args))
                continue
            elif name == "gsSPBranchList":
                ptr = self.DL_ptr(int(args[0]))
                if ptr:
                    layout.dl_ptrs.append(ptr)
                DL.append((name, args))
                break
            # check for manual stop
            if layout.stop and start + x + 8 > layout.stop:
                break
            DL.append((name, args))
        DL.append((name, args))
        DL.length = x // 8
        return DL

    @lru_cache(maxsize=32)  # will save lots of time with repeats of tri calls
    def Getf3dCmd(self, bin):
        return f3dex2.Ex2String(bin)

    def split_args(self, cmd):
        filt = "\(.*\)"
        a = re.search(filt, cmd)
        return cmd[: a.span()[0]], cmd[a.span()[0] + 1 : a.span()[1] - 1].split(",")

    def decode_stray(self, starts):
        # double check end of vert loads against start of DLs for stray DL
        first_DL = min(starts)
        last_vert = self.max_vert * 16 + 0x20
        if last_vert < first_DL:
            self.stray_DL = faux_LY(last_vert + (0x04 << 24))
            # set manual stop to first DL
            self.stray_DL.stop = first_DL
            self.stray_DL.dl_ptrs = [self.stray_DL.ptr]
            self.decode_f3d_bin(self.stray_DL)
            return last_vert
        else:
            return first_DL

    def decode_vertices(self, starts):
        end = self.decode_stray(starts)
        self.vertices = Vertices()
        self.vertices.header = self.header
        for i in range(0x20, end, 16):
            v = self.upt(i, ">6h4B", 16)
            self.vertices._make(v)

    def Write(self, file):
        # write comment with file location
        if self.ptrs:
            file.write(
                f"//start 0x{self.ptrs[0]:X} - end 0x{self.ptrs[1]:X}\n{geo_block_includes}\n"
            )
        # write global include statements

        self.WriteDictStruct(
            self.main_header,
            geo_block_header,
            file,
            "",
            "GeoBlockHeader Geo_Header",
            symbols=self.symbols,
            static=True
        )
        # some geos are just layouts
        if hasattr(self, "vertices"):
            self.vertices.Write(file)
        self.write_DLs(file)
        self.write_arr(file, "Gfx *img_refs", self.img_refs, self.format_arr, length=len(self.img_refs), static=True)
        self.write_arr(file, "Gfx *vtx_refs", self.vtx_refs, self.format_arr, length=len(self.vtx_refs), static=True)
        self.write_entry(file)
        [l.Write(file, i) for i, l in enumerate(self.layouts)]
        if hasattr(self, "tex_header"):
            self.write_arr(
                file,
                "struct TextureScroll **tx_scroll_header",
                self.tex_header,
                self.format_arr_ptr,
                ptr_format="(struct TextureScroll **) &tx_scroll_hdr_",
                length=len(self.tex_header),
                static=True
            )
            # make new object with all scrolls, then write
            all_scrolls = Tex_Scroll([], 0, 0)
            all_scrolls.header = self.header
            for k, tex in self.tex_scrolls.items():
                all_scrolls.textures.update(tex.textures)
                all_scrolls.palettes.update(tex.palettes)
                all_scrolls.scrolls.update(tex.scrolls)
            all_scrolls.Write(file)
            # then iterate again for the scroll headers, this is to match file order
            for k, tex in self.tex_scrolls.items():
                tex.WriteHeader(file)
        else:
            file.write("//No texture scrolls\n")
        if hasattr(self, "anims"):
            self.write_arr(
                file, "void *anims", self.anims, self.format_arr_ptr, BI=True, length=len(self.anims), static=True
            )

    def write_entry(self, file):
        # only need to write in render modes 0x18 and 0x1C
        # render mode 0x1B is a DL pair
        # render mode 0x14 has one entry point, but I have a faux layout in place
        # so using it in the same way as 0x18 should work
        for l in self.layouts:
            # DL_Pair
            if self.render_mode == 0x1B:
                if not hasattr(l, "DL_Pair"):
                    continue
                self.write_arr(
                    file,
                    f"Gfx *DL_Pair_{l.ptr:X}",
                    l.DL_Pair,
                    self.format_arr_ptr,
                    ptr_format="&DL_",
                    static=True,
                    length=len(l.DL_Pair)
                )
            # entry_pts
            elif self.render_mode == 0x18 or self.render_mode == 0x14:
                if not hasattr(l, "entry_pts"):
                    continue
                self.add_header_data(f"struct EntryPoint Entry_Point_{l.ptr:X}[{len(l.entry_pts)}]", static=True)
                file.write(f"static struct EntryPoint Entry_Point_{l.ptr:X}[{len(l.entry_pts)}] = {{\n")
                buf = []
                for e in l.entry_pts:
                    if e[1]:
                        buf.append("\t{{0x{:X}, (Gfx *) &DL_{:X}}}".format(*e))
                    else:
                        buf.append("\t{{0x{:X}, NULL}}".format(*e))
                file.write(",\n".join(buf) + "\n};\n\n")
            # entry_dbls
            elif self.render_mode == 0x1C:
                if not hasattr(l, "entry_dbls"):
                    continue
                self.add_header_data(f"struct EntryPair Entry_Pair_{l.ptr:X}[{len(l.entry_dbls)}]", static=True)
                file.write(f"static struct EntryPair Entry_Pair_{l.ptr:X}[{len(l.entry_dbls)}] = {{\n")
                buf = []
                for mark, (dl_1, dl_2) in l.entry_dbls:

                    def sym_transmute(sym):
                        return "NULL" if not sym else f"(Gfx *) &DL_{sym:X}"

                    buf.append(
                        f"\t{{0x{mark:X}, {sym_transmute(dl_1)}, {sym_transmute(dl_2)}}}"
                    )
                file.write(",\n".join(buf) + "\n};\n\n")

    def write_DLs(self, file):
        for k in sorted(self.DLs.keys()):
            v = self.DLs[k]
            file.write(f"static Gfx DL_{k:X}[{v.length}] = {{\n")
            self.add_header_data(f"Gfx DL_{k:X}[{v.length}]", static=True)
            file.write(
                ",\n".join(["\t{}({})".format(*self.DLSymbols(a, b)) for a, b in v.commands])
            )
            file.write("\n};\n\n")

    def dl_ptr_sym(self, name):
        if int(name) >> 24 == 0xE:
            return f"0x{int(name):X}"
        else:
            return f"&DL_{int(name):X}"

    # give appropriate symbols to DLs
    def DLSymbols(self, name, args):
        if name == "gsSPBranchLessZraw":
            return name, f"{self.dl_ptr_sym(args[0])}, {','.join(args[1:])}"
        if name == "gsSPDisplayList":
            return name, f"{self.dl_ptr_sym(args[0])}"
        if name == "gsSPVertex":
            return name, ",".join([f"&Geo_Vertices[0x{args[0]:X}]", *args[1:]])
        else:
            return name, ",".join(args)


# -------------------------------------------------------------------------------
# Analyze Textures
# -------------------------------------------------------------------------------

# for a given geo block, get list of all tex data blocks used and their types
# separates palettes and removes NULL/terminator textures
def AnalyzeTex(geo: GeoBin):
    LastMat = None
    tex_dict = {"tex": [], "palettes": []}
    # by parsing DLs given by the layouts in order, find how textures
    # are used and determine their dimensions, and format
    for l in geo.layouts:
        f3d = AnalyzeGeo.F3d(lastmat=LastMat)
        f3d.GetDataFromDL(geo, l)
        new_dict = f3d.GetTexBlocks()
        tex_dict["tex"].extend(new_dict["tex"])
        tex_dict["palettes"].extend(new_dict["palettes"])
        LastMat = f3d.LastMat
    return tex_dict


# for a given geo block, find all the textures used
def AnalyzeBlock(geo: GeoBin, tex_set=None):
    LastMat = None
    if not tex_set:
        tex_set = set()
    if not GeoBin:
        return tex_set
    # by parsing DLs given by the layouts in order, find how textures
    # are used and determine their dimensions, and format
    for l in geo.layouts:
        f3d = AnalyzeGeo.F3d(lastmat=LastMat)
        f3d.GetDataFromDL(geo, l)
        tex_set.update(f3d.GetTexturesFromMats())
        LastMat = f3d.LastMat
    return tex_set


# -------------------------------------------------------------------------------
# Main
# -------------------------------------------------------------------------------


def GetSingleGeo(bank, index, Kirb: "binary ROM file", path=None):
    ptrs = GetPointers(bank, index, "Geo_Block", Kirb)
    if path:
        name = path
    else:
        name = f"assets/geo/bank_{bank}/{index}"
    Path(name).mkdir(exist_ok=True, parents=True)
    WriteBin(Kirb, *ptrs, str(name) + "/geo")
    with open(Path(name) / "geo.bin", "rb") as f:
        block = GeoBin(f.read(), ptr=ptrs)
    with open(Path(name) / "geo.c", "w", newline='') as f:
        block.Write(f)
    with open(Path(name) / "geo.h", "w", newline='') as f:
        block.write_header(f)
    print(f"Geo bank {bank} index {index} file wrote")
    return block


# exports all geo block data from an entire bank
def ExportGeoBank(bank, Kirb: "binary ROM file"):
    for index in LoopOverBank(bank, Kirb):
        block = GetSingleGeo(bank, index, Kirb)


# exports all geo block data
def ExportAllGeoData(Kirb: "binary ROM file"):
    for bank in range(8):
        for index in LoopOverBank(bank, Kirb):
            block = GetSingleGeo(bank, index, Kirb)


# later write this to grab all geo files
def main(Kirb: "binary ROM file"):
    block = GetSingleGeo(0, 1, Kirb)
    # ExportGeoBank(2, Kirb)
    # ExportAllGeoData(Kirb)
    # print(AnalyzeBlock(block))


# this file only works if called from the folder Split_Tools
# otherwise the pathing will not be correct
if __name__ == "__main__":
    Kirby = open(sys.argv[1], "rb")
    Kirb = Kirby.read()
    main(Kirb)
    print("Geo Bank Exports finished")
