import re

G_ZS_PIXEL = 0
G_ZS_PRIM = 1 << 2

G_AC_NONE = 0
G_AC_THRESHOLD = 1
G_AC_DITHER = 3

defs = """#define	AA_EN		0x8
#define	Z_CMP		0x10
#define	Z_UPD		0x20
#define	IM_RD		0x40
#define	CLR_ON_CVG	0x80
#define	CVG_DST_CLAMP	0
#define	CVG_DST_WRAP	0x100
#define	CVG_DST_FULL	0x200
#define	CVG_DST_SAVE	0x300
#define	ZMODE_OPA	0
#define	ZMODE_INTER	0x400
#define	ZMODE_XLU	0x800
#define	ZMODE_DEC	0xc00
#define	CVG_X_ALPHA	0x1000
#define	ALPHA_CVG_SEL	0x2000
#define	FORCE_BL	0x4000
#define	TEX_EDGE	0x0000

#define	G_BL_CLR_IN	0
#define	G_BL_CLR_MEM	1
#define	G_BL_CLR_BL	2
#define	G_BL_CLR_FOG	3
#define	G_BL_1MA	0
#define	G_BL_A_MEM	1
#define	G_BL_A_IN	0
#define	G_BL_A_FOG	1
#define	G_BL_A_SHADE	2
#define	G_BL_1		2
#define	G_BL_0		3"""

filt_1 = "\s.+\s"
filt_2 = "\s{1}\S+$"

defines = defs.split("\n")

for line in defines:
    a = re.search(filt_1, line)
    b = re.search(filt_2, line)
    if a and b:
        globals()[a.group().strip()] = eval(b.group().strip())


def GBL_c1(m1a, m1b, m2a, m2b):
    return (m1a) << 30 | (m1b) << 26 | (m2a) << 22 | (m2b) << 18


def GBL_c2(m1a, m1b, m2a, m2b):
    return (m1a) << 28 | (m1b) << 24 | (m2a) << 20 | (m2b) << 16


def GBL_c(clk):
    if clk == 1:
        return GBL_c1
    else:
        return GBL_c2


def RM_AA_ZB_OPA_SURF(clk):
    return (
        AA_EN
        | Z_CMP
        | Z_UPD
        | IM_RD
        | CVG_DST_CLAMP
        | ZMODE_OPA
        | ALPHA_CVG_SEL
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_A_MEM)
    )


def RM_RA_ZB_OPA_SURF(clk):
    return (
        AA_EN
        | Z_CMP
        | Z_UPD
        | CVG_DST_CLAMP
        | ZMODE_OPA
        | ALPHA_CVG_SEL
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_A_MEM)
    )


def RM_AA_ZB_XLU_SURF(clk):
    return (
        AA_EN
        | Z_CMP
        | IM_RD
        | CVG_DST_WRAP
        | CLR_ON_CVG
        | FORCE_BL
        | ZMODE_XLU
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_AA_ZB_OPA_DECAL(clk):
    return (
        AA_EN
        | Z_CMP
        | IM_RD
        | CVG_DST_WRAP
        | ALPHA_CVG_SEL
        | ZMODE_DEC
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_A_MEM)
    )


def RM_RA_ZB_OPA_DECAL(clk):
    return (
        AA_EN
        | Z_CMP
        | CVG_DST_WRAP
        | ALPHA_CVG_SEL
        | ZMODE_DEC
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_A_MEM)
    )


def RM_AA_ZB_XLU_DECAL(clk):
    return (
        AA_EN
        | Z_CMP
        | IM_RD
        | CVG_DST_WRAP
        | CLR_ON_CVG
        | FORCE_BL
        | ZMODE_DEC
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_AA_ZB_OPA_INTER(clk):
    return (
        AA_EN
        | Z_CMP
        | Z_UPD
        | IM_RD
        | CVG_DST_CLAMP
        | ALPHA_CVG_SEL
        | ZMODE_INTER
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_A_MEM)
    )


def RM_RA_ZB_OPA_INTER(clk):
    return (
        AA_EN
        | Z_CMP
        | Z_UPD
        | CVG_DST_CLAMP
        | ALPHA_CVG_SEL
        | ZMODE_INTER
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_A_MEM)
    )


def RM_AA_ZB_XLU_INTER(clk):
    return (
        AA_EN
        | Z_CMP
        | IM_RD
        | CVG_DST_WRAP
        | CLR_ON_CVG
        | FORCE_BL
        | ZMODE_INTER
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_AA_ZB_XLU_LINE(clk):
    return (
        AA_EN
        | Z_CMP
        | IM_RD
        | CVG_DST_CLAMP
        | CVG_X_ALPHA
        | ALPHA_CVG_SEL
        | FORCE_BL
        | ZMODE_XLU
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_AA_ZB_DEC_LINE(clk):
    return (
        AA_EN
        | Z_CMP
        | IM_RD
        | CVG_DST_SAVE
        | CVG_X_ALPHA
        | ALPHA_CVG_SEL
        | FORCE_BL
        | ZMODE_DEC
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_AA_ZB_TEX_EDGE(clk):
    return (
        AA_EN
        | Z_CMP
        | Z_UPD
        | IM_RD
        | CVG_DST_CLAMP
        | CVG_X_ALPHA
        | ALPHA_CVG_SEL
        | ZMODE_OPA
        | TEX_EDGE
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_A_MEM)
    )


def RM_AA_ZB_TEX_INTER(clk):
    return (
        AA_EN
        | Z_CMP
        | Z_UPD
        | IM_RD
        | CVG_DST_CLAMP
        | CVG_X_ALPHA
        | ALPHA_CVG_SEL
        | ZMODE_INTER
        | TEX_EDGE
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_A_MEM)
    )


def RM_AA_ZB_SUB_SURF(clk):
    return (
        AA_EN
        | Z_CMP
        | Z_UPD
        | IM_RD
        | CVG_DST_FULL
        | ZMODE_OPA
        | ALPHA_CVG_SEL
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_A_MEM)
    )


def RM_AA_ZB_PCL_SURF(clk):
    return (
        AA_EN
        | Z_CMP
        | Z_UPD
        | IM_RD
        | CVG_DST_CLAMP
        | ZMODE_OPA
        | G_AC_DITHER
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_AA_ZB_OPA_TERR(clk):
    return (
        AA_EN
        | Z_CMP
        | Z_UPD
        | IM_RD
        | CVG_DST_CLAMP
        | ZMODE_OPA
        | ALPHA_CVG_SEL
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_AA_ZB_TEX_TERR(clk):
    return (
        AA_EN
        | Z_CMP
        | Z_UPD
        | IM_RD
        | CVG_DST_CLAMP
        | CVG_X_ALPHA
        | ALPHA_CVG_SEL
        | ZMODE_OPA
        | TEX_EDGE
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_AA_ZB_SUB_TERR(clk):
    return (
        AA_EN
        | Z_CMP
        | Z_UPD
        | IM_RD
        | CVG_DST_FULL
        | ZMODE_OPA
        | ALPHA_CVG_SEL
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_AA_OPA_SURF(clk):
    return (
        AA_EN
        | IM_RD
        | CVG_DST_CLAMP
        | ZMODE_OPA
        | ALPHA_CVG_SEL
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_A_MEM)
    )


def RM_RA_OPA_SURF(clk):
    return (
        AA_EN
        | CVG_DST_CLAMP
        | ZMODE_OPA
        | ALPHA_CVG_SEL
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_A_MEM)
    )


def RM_AA_XLU_SURF(clk):
    return (
        AA_EN
        | IM_RD
        | CVG_DST_WRAP
        | CLR_ON_CVG
        | FORCE_BL
        | ZMODE_OPA
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_AA_XLU_LINE(clk):
    return (
        AA_EN
        | IM_RD
        | CVG_DST_CLAMP
        | CVG_X_ALPHA
        | ALPHA_CVG_SEL
        | FORCE_BL
        | ZMODE_OPA
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_AA_DEC_LINE(clk):
    return (
        AA_EN
        | IM_RD
        | CVG_DST_FULL
        | CVG_X_ALPHA
        | ALPHA_CVG_SEL
        | FORCE_BL
        | ZMODE_OPA
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_AA_TEX_EDGE(clk):
    return (
        AA_EN
        | IM_RD
        | CVG_DST_CLAMP
        | CVG_X_ALPHA
        | ALPHA_CVG_SEL
        | ZMODE_OPA
        | TEX_EDGE
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_A_MEM)
    )


def RM_AA_SUB_SURF(clk):
    return (
        AA_EN
        | IM_RD
        | CVG_DST_FULL
        | ZMODE_OPA
        | ALPHA_CVG_SEL
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_A_MEM)
    )


def RM_AA_PCL_SURF(clk):
    return (
        AA_EN
        | IM_RD
        | CVG_DST_CLAMP
        | ZMODE_OPA
        | G_AC_DITHER
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_AA_OPA_TERR(clk):
    return (
        AA_EN
        | IM_RD
        | CVG_DST_CLAMP
        | ZMODE_OPA
        | ALPHA_CVG_SEL
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_AA_TEX_TERR(clk):
    return (
        AA_EN
        | IM_RD
        | CVG_DST_CLAMP
        | CVG_X_ALPHA
        | ALPHA_CVG_SEL
        | ZMODE_OPA
        | TEX_EDGE
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_AA_SUB_TERR(clk):
    return (
        AA_EN
        | IM_RD
        | CVG_DST_FULL
        | ZMODE_OPA
        | ALPHA_CVG_SEL
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_ZB_OPA_SURF(clk):
    return (
        Z_CMP
        | Z_UPD
        | CVG_DST_FULL
        | ALPHA_CVG_SEL
        | ZMODE_OPA
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_A_MEM)
    )


def RM_ZB_XLU_SURF(clk):
    return (
        Z_CMP
        | IM_RD
        | CVG_DST_FULL
        | FORCE_BL
        | ZMODE_XLU
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_ZB_OPA_DECAL(clk):
    return (
        Z_CMP
        | CVG_DST_FULL
        | ALPHA_CVG_SEL
        | ZMODE_DEC
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_A_MEM)
    )


def RM_ZB_XLU_DECAL(clk):
    return (
        Z_CMP
        | IM_RD
        | CVG_DST_FULL
        | FORCE_BL
        | ZMODE_DEC
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_ZB_CLD_SURF(clk):
    return (
        Z_CMP
        | IM_RD
        | CVG_DST_SAVE
        | FORCE_BL
        | ZMODE_XLU
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_ZB_OVL_SURF(clk):
    return (
        Z_CMP
        | IM_RD
        | CVG_DST_SAVE
        | FORCE_BL
        | ZMODE_DEC
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_ZB_PCL_SURF(clk):
    return (
        Z_CMP
        | Z_UPD
        | CVG_DST_FULL
        | ZMODE_OPA
        | G_AC_DITHER
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_0, G_BL_CLR_IN, G_BL_1)
    )


def RM_OPA_SURF(clk):
    return (
        CVG_DST_CLAMP
        | FORCE_BL
        | ZMODE_OPA
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_0, G_BL_CLR_IN, G_BL_1)
    )


def RM_XLU_SURF(clk):
    return (
        IM_RD
        | CVG_DST_FULL
        | FORCE_BL
        | ZMODE_OPA
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_TEX_EDGE(clk):
    return (
        CVG_DST_CLAMP
        | CVG_X_ALPHA
        | ALPHA_CVG_SEL
        | FORCE_BL
        | ZMODE_OPA
        | TEX_EDGE
        | AA_EN
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_0, G_BL_CLR_IN, G_BL_1)
    )


def RM_CLD_SURF(clk):
    return (
        IM_RD
        | CVG_DST_SAVE
        | FORCE_BL
        | ZMODE_OPA
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_IN, G_BL_CLR_MEM, G_BL_1MA)
    )


def RM_PCL_SURF(clk):
    return (
        CVG_DST_FULL
        | FORCE_BL
        | ZMODE_OPA
        | G_AC_DITHER
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_0, G_BL_CLR_IN, G_BL_1)
    )


def RM_ADD(clk):
    return (
        IM_RD
        | CVG_DST_SAVE
        | FORCE_BL
        | ZMODE_OPA
        | GBL_c(clk)(G_BL_CLR_IN, G_BL_A_FOG, G_BL_CLR_MEM, G_BL_1)
    )


def RM_NOOP(clk):
    return GBL_c(clk)(0, 0, 0, 0)


def RM_VISCVG(clk):
    return IM_RD | FORCE_BL | GBL_c(clk)(G_BL_CLR_IN, G_BL_0, G_BL_CLR_BL, G_BL_A_MEM)


# /* for rendering to an 8-bit framebuffer */
def RM_OPA_CI(clk):
    return (
        CVG_DST_CLAMP | ZMODE_OPA | GBL_c(clk)(G_BL_CLR_IN, G_BL_0, G_BL_CLR_IN, G_BL_1)
    )


# /* Custom version of RM_AA_ZB_XLU_SURF with Z_UPD */
def RM_CUSTOM_AA_ZB_XLU_SURF(clk):
    return RM_AA_ZB_XLU_SURF(clk) | Z_UPD


modes = """
#define	G_RM_AA_ZB_OPA_SURF	RM_AA_ZB_OPA_SURF(1)
#define	G_RM_AA_ZB_OPA_SURF2	RM_AA_ZB_OPA_SURF(2)
#define	G_RM_AA_ZB_XLU_SURF	RM_AA_ZB_XLU_SURF(1)
#define	G_RM_AA_ZB_XLU_SURF2	RM_AA_ZB_XLU_SURF(2)
#define	G_RM_AA_ZB_OPA_DECAL	RM_AA_ZB_OPA_DECAL(1)
#define	G_RM_AA_ZB_OPA_DECAL2	RM_AA_ZB_OPA_DECAL(2)
#define	G_RM_AA_ZB_XLU_DECAL	RM_AA_ZB_XLU_DECAL(1)
#define	G_RM_AA_ZB_XLU_DECAL2	RM_AA_ZB_XLU_DECAL(2)
#define	G_RM_AA_ZB_OPA_INTER	RM_AA_ZB_OPA_INTER(1)
#define	G_RM_AA_ZB_OPA_INTER2	RM_AA_ZB_OPA_INTER(2)
#define	G_RM_AA_ZB_XLU_INTER	RM_AA_ZB_XLU_INTER(1)
#define	G_RM_AA_ZB_XLU_INTER2	RM_AA_ZB_XLU_INTER(2)
#define	G_RM_AA_ZB_XLU_LINE	RM_AA_ZB_XLU_LINE(1)
#define	G_RM_AA_ZB_XLU_LINE2	RM_AA_ZB_XLU_LINE(2)
#define	G_RM_AA_ZB_DEC_LINE	RM_AA_ZB_DEC_LINE(1)
#define	G_RM_AA_ZB_DEC_LINE2	RM_AA_ZB_DEC_LINE(2)
#define	G_RM_AA_ZB_TEX_EDGE	RM_AA_ZB_TEX_EDGE(1)
#define	G_RM_AA_ZB_TEX_EDGE2	RM_AA_ZB_TEX_EDGE(2)
#define	G_RM_AA_ZB_TEX_INTER	RM_AA_ZB_TEX_INTER(1)
#define	G_RM_AA_ZB_TEX_INTER2	RM_AA_ZB_TEX_INTER(2)
#define	G_RM_AA_ZB_SUB_SURF	RM_AA_ZB_SUB_SURF(1)
#define	G_RM_AA_ZB_SUB_SURF2	RM_AA_ZB_SUB_SURF(2)
#define	G_RM_AA_ZB_PCL_SURF	RM_AA_ZB_PCL_SURF(1)
#define	G_RM_AA_ZB_PCL_SURF2	RM_AA_ZB_PCL_SURF(2)
#define	G_RM_AA_ZB_OPA_TERR	RM_AA_ZB_OPA_TERR(1)
#define	G_RM_AA_ZB_OPA_TERR2	RM_AA_ZB_OPA_TERR(2)
#define	G_RM_AA_ZB_TEX_TERR	RM_AA_ZB_TEX_TERR(1)
#define	G_RM_AA_ZB_TEX_TERR2	RM_AA_ZB_TEX_TERR(2)
#define	G_RM_AA_ZB_SUB_TERR	RM_AA_ZB_SUB_TERR(1)
#define	G_RM_AA_ZB_SUB_TERR2	RM_AA_ZB_SUB_TERR(2)

#define	G_RM_RA_ZB_OPA_SURF	RM_RA_ZB_OPA_SURF(1)
#define	G_RM_RA_ZB_OPA_SURF2	RM_RA_ZB_OPA_SURF(2)
#define	G_RM_RA_ZB_OPA_DECAL	RM_RA_ZB_OPA_DECAL(1)
#define	G_RM_RA_ZB_OPA_DECAL2	RM_RA_ZB_OPA_DECAL(2)
#define	G_RM_RA_ZB_OPA_INTER	RM_RA_ZB_OPA_INTER(1)
#define	G_RM_RA_ZB_OPA_INTER2	RM_RA_ZB_OPA_INTER(2)

#define	G_RM_AA_OPA_SURF	RM_AA_OPA_SURF(1)
#define	G_RM_AA_OPA_SURF2	RM_AA_OPA_SURF(2)
#define	G_RM_AA_XLU_SURF	RM_AA_XLU_SURF(1)
#define	G_RM_AA_XLU_SURF2	RM_AA_XLU_SURF(2)
#define	G_RM_AA_XLU_LINE	RM_AA_XLU_LINE(1)
#define	G_RM_AA_XLU_LINE2	RM_AA_XLU_LINE(2)
#define	G_RM_AA_DEC_LINE	RM_AA_DEC_LINE(1)
#define	G_RM_AA_DEC_LINE2	RM_AA_DEC_LINE(2)
#define	G_RM_AA_TEX_EDGE	RM_AA_TEX_EDGE(1)
#define	G_RM_AA_TEX_EDGE2	RM_AA_TEX_EDGE(2)
#define	G_RM_AA_SUB_SURF	RM_AA_SUB_SURF(1)
#define	G_RM_AA_SUB_SURF2	RM_AA_SUB_SURF(2)
#define	G_RM_AA_PCL_SURF	RM_AA_PCL_SURF(1)
#define	G_RM_AA_PCL_SURF2	RM_AA_PCL_SURF(2)
#define	G_RM_AA_OPA_TERR	RM_AA_OPA_TERR(1)
#define	G_RM_AA_OPA_TERR2	RM_AA_OPA_TERR(2)
#define	G_RM_AA_TEX_TERR	RM_AA_TEX_TERR(1)
#define	G_RM_AA_TEX_TERR2	RM_AA_TEX_TERR(2)
#define	G_RM_AA_SUB_TERR	RM_AA_SUB_TERR(1)
#define	G_RM_AA_SUB_TERR2	RM_AA_SUB_TERR(2)

#define	G_RM_RA_OPA_SURF	RM_RA_OPA_SURF(1)
#define	G_RM_RA_OPA_SURF2	RM_RA_OPA_SURF(2)

#define	G_RM_ZB_OPA_SURF	RM_ZB_OPA_SURF(1)
#define	G_RM_ZB_OPA_SURF2	RM_ZB_OPA_SURF(2)
#define	G_RM_ZB_XLU_SURF	RM_ZB_XLU_SURF(1)
#define	G_RM_ZB_XLU_SURF2	RM_ZB_XLU_SURF(2)
#define	G_RM_ZB_OPA_DECAL	RM_ZB_OPA_DECAL(1)
#define	G_RM_ZB_OPA_DECAL2	RM_ZB_OPA_DECAL(2)
#define	G_RM_ZB_XLU_DECAL	RM_ZB_XLU_DECAL(1)
#define	G_RM_ZB_XLU_DECAL2	RM_ZB_XLU_DECAL(2)
#define	G_RM_ZB_CLD_SURF	RM_ZB_CLD_SURF(1)
#define	G_RM_ZB_CLD_SURF2	RM_ZB_CLD_SURF(2)
#define	G_RM_ZB_OVL_SURF	RM_ZB_OVL_SURF(1)
#define	G_RM_ZB_OVL_SURF2	RM_ZB_OVL_SURF(2)
#define	G_RM_ZB_PCL_SURF	RM_ZB_PCL_SURF(1)
#define	G_RM_ZB_PCL_SURF2	RM_ZB_PCL_SURF(2)

#define	G_RM_OPA_SURF		RM_OPA_SURF(1)
#define	G_RM_OPA_SURF2		RM_OPA_SURF(2)
#define	G_RM_XLU_SURF		RM_XLU_SURF(1)
#define	G_RM_XLU_SURF2		RM_XLU_SURF(2)
#define	G_RM_CLD_SURF		RM_CLD_SURF(1)
#define	G_RM_CLD_SURF2		RM_CLD_SURF(2)
#define	G_RM_TEX_EDGE		RM_TEX_EDGE(1)
#define	G_RM_TEX_EDGE2		RM_TEX_EDGE(2)
#define	G_RM_PCL_SURF		RM_PCL_SURF(1)
#define	G_RM_PCL_SURF2		RM_PCL_SURF(2)
#define G_RM_ADD       		RM_ADD(1)
#define G_RM_ADD2      		RM_ADD(2)
#define G_RM_NOOP       	RM_NOOP(1)
#define G_RM_NOOP2      	RM_NOOP(2)
#define G_RM_VISCVG    		RM_VISCVG(1)
#define G_RM_VISCVG2    	RM_VISCVG(2)
#define G_RM_OPA_CI         RM_OPA_CI(1)
#define G_RM_OPA_CI2        RM_OPA_CI(2)

#define G_RM_CUSTOM_AA_ZB_XLU_SURF	RM_CUSTOM_AA_ZB_XLU_SURF(1)
#define G_RM_CUSTOM_AA_ZB_XLU_SURF2	RM_CUSTOM_AA_ZB_XLU_SURF(2)
"""

extra = """
#define	G_RM_FOG_SHADE_A	GBL_c1(G_BL_CLR_FOG, G_BL_A_SHADE, G_BL_CLR_IN, G_BL_1MA)
#define	G_RM_FOG_PRIM_A		GBL_c1(G_BL_CLR_FOG, G_BL_A_FOG, G_BL_CLR_IN, G_BL_1MA)
#define	G_RM_PASS		GBL_c1(G_BL_CLR_IN, G_BL_0, G_BL_CLR_IN, G_BL_1)
"""

modes = modes.split("\n")
render = {}

filt_3 = ".{6}\(.+\)"
filt_4 = "\s\S+\s"
extras = extra.split("\n")

for line in extras:
    a = re.search(filt_4, line)
    b = re.search(filt_3, line)
    if a and b:
        render[eval(b.group().strip())] = a.group().strip()

for line in modes:
    a = re.search(filt_1, line)
    b = re.search(filt_2, line)
    if a and b:
        if not (eval(b.group().strip())):
            print(b.group().strip())
        render[eval(b.group().strip())] = a.group().strip()

for a, b in render.items():
    print(f"0x{a:08X} : {repr(b)},")
