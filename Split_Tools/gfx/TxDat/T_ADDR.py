# This file contains methods to print textures given bank, indices and a texture
# description, by using the BinPNG module

# this file will also contain methods for writing a JSON and reading a JSON
# with all the texture data. This file is not meant to be used by itself

import json, math, shutil, struct, os, sys
from pathlib import Path

# since this uses relative import it cannot be run as main
from . import BinPNG

# to import utility.py, we need to add parent dir to path, this needs two parents
file = Path(os.path.dirname(__file__))
sys.path.append(str(file.parent.parent))
from Utility import *

# -------------------------------------------------------------------------------
# JSON get/write and pathing
# -------------------------------------------------------------------------------

# return directory of this file, not the call file
def GetFileLoc():
    return os.path.dirname(os.path.realpath(__file__))


# load a texture JSON
def GetJSON(name="Kirby_Textures.json", jsPath=None):
    if not jsPath:
        floc = GetFileLoc()
        jsPath = floc / Path(name)
    if not jsPath.exists():
        # create file it doesn't exist
        with open(jsPath, "w") as jsF:
            jsF.write("{}")
    with open(jsPath, "r") as jsF:
        return json.load(jsF)


# write a texture JSON
def WriteJSON(js, name="Kirby_Textures.json"):
    floc = GetFileLoc()
    jsPath = floc / Path(name)
    jsF = open(jsPath, "w")
    jsF.write(json.dumps(js, sort_keys=True, indent=4))
    jsF.close()


def ClearGuesses():
    Guesses = GetFileLoc() / Path(f"Tests")
    if Guesses.exists():
        shutil.rmtree(Guesses)
    Guesses.mkdir(exist_ok=True)


# -------------------------------------------------------------------------------
# Sprites
# -------------------------------------------------------------------------------

# Spites are files that contain large images meant to be displayed directly to the screen
# they are generally dispalyed using S2DEX microcode, and the files themselves contain
# the struct descriptors that the game uses to create the microcode used to display them.

# by decoding this header struct, we can get all the info about the sprite texture
# necessary to correctly export it

# decode from sprite header
def ExportSprite(bank, index, Kirb: "binary ROM file", JustDat=False, **kwargs):
    file = GetBinFile(bank, index, Kirb=Kirb)
    if len(file) < 0x10:
        return None
    res = struct.unpack(">BBHHHLL", file[0:0x10])
    if res[-2] != 0x10:
        return None
    Siz = 4 * (2 ** res[1])
    Fmt = {0: "RGBA", 1: "YUV", 2: "CI", 3: "IA", 4: "I"}
    sprite = {}
    sprite["Sprite_Header"] = 0
    sprite["T_Offset"] = 16
    sprite["Fmt"] = Fmt.get(res[0])
    sprite["Siz"] = Siz

    if Fmt.get(res[0]) == "CI":
        sprite["Pal_Offset"] = res[-1]
        sprite["Pal"] = (bank, index)
        end = res[-1]
    else:
        pal = []
        end = len(file)

    width = res[3]
    IdealW = ((end - 16) / (Siz / 8)) / res[4]
    if IdealW > 4:
        width = int(IdealW)

    sprite["Width"] = width
    sprite["Height"] = res[4]

    if JustDat:
        return sprite
    ExportSingle(bank, index, sprite, **kwargs)
    return sprite


# quick check if texture is a sprite
def SpriteCheck(bank, index, Kirb: "binary ROM file" = None):
    file = GetBinFile(bank, index, Kirb=Kirb)
    if len(file) < 0x10:
        return None
    res = struct.unpack(">BBHHHLL", file[0:0x10])
    if res[-2] != 0x10:
        return None
    return True


# -------------------------------------------------------------------------------
# File I/O
# -------------------------------------------------------------------------------

# get the bin from the cwd (which should be "Split_Tools" folder)
# if tex is not present, use the rom file provided to create it
def GetBinFile(bank, index, Kirb: "binary ROM file" = None, bin_name = "block"):
    name = Path.cwd() / Path((f"assets/image/bank_{bank}/{index}/{bin_name}.bin"))
    if name.exists():
        f = open(name, "rb")
        return f.read()
    else:
        name.parent.mkdir(exist_ok=True, parents=True)
        if Kirb:
            # I clear the suffix so the file doesn't get .bin.bin
            WriteBin(
                Kirb,
                *GetPointers(bank, index, "Image", Kirb),
                str(name.with_suffix("")),
            )
            return GetBinFile(bank, index)
        else:
            raise Exception(
                f"Binary file {name} does not exist and no rom provided for extraction"
            )


def ExportFromJS(js, bank, index, Kirb: "binary ROM file" = None):
    tex = js["Bank_%d" % Bank][str(ID)]
    ExportSingle(bank, index, tex, Kirb=Kirb)


# exports textures given a bank and id
def ExportSingle(
    bank,
    index,
    tex,
    Kirb: "binary ROM file" = None,
    path=None,
    tx_name="image",
    bin_name = "block"
):
    if not tex or index == 0:
        return
    if not path:
        name = GetFileLoc() / Path("Tests")
        name.mkdir(exist_ok=True, parents=True)
        png = BinPNG.MakeImage(str(name / ("bank_{}_index_{}_tex".format(bank, index) + tx_name)))
    else:
        name = Path(str(path).format(index))
        name.mkdir(exist_ok=True, parents=True)
        png = BinPNG.MakeImage(str(name / tx_name))
    ImgTypes = {"RGBA": BinPNG.RGBA, "CI": BinPNG.CI, "IA": BinPNG.IA, "I": BinPNG.I}
    b = GetBinFile(bank, index, Kirb=Kirb)
    if tex.get("T_Offset"):
        b = b[tex["T_Offset"] :]
    func = ImgTypes.get(tex["Fmt"])
    if tex["Fmt"] == "CI":
        # I mixed up the format because I didn't like old one.
        # old format was tex[Pal_Bank], tex[Pal_Index]
        # new is just tex[Pal]
        try:
            pal = tex["Pal"]
        except:
            pal = (int(tex["Pal_Bank"]), int(tex["Pal_Index"]))
        pal = GetBinFile(*pal, Kirb=Kirb)
        if tex.get("Pal_Offset"):
            pal = pal[tex["Pal_Offset"] :]
        # palette is a tuple in this module for some reason?
        func(tex["Width"], tex["Height"], tex["Siz"], (pal, 0), b, png)
    else:
        func(tex["Width"], tex["Height"], tex["Siz"], b, png)
    png.close()


# just for some inspection, prob won't ever be used
def ExportPalette(
    bank,
    index,
    tex,
    Kirb: "binary ROM file" = None,
    path=None,
    tx_name="image",
    **kwargs
):
    if not tex or index == 0:
        return
    if not path:
        name = GetFileLoc() / Path("Tests")
        name.mkdir(exist_ok=True, parents=True)
        png = BinPNG.MakeImage(str(name / ("bank_{}_index_{}_tex".format(bank, index) + tx_name)))
    else:
        name = Path(str(path).format(index))
        name.mkdir(exist_ok=True, parents=True)
        png = BinPNG.MakeImage(str(name / tx_name))
    ImgTypes = {"RGBA": BinPNG.RGBA, "CI": BinPNG.CI, "IA": BinPNG.IA, "I": BinPNG.I}
    b = GetBinFile(bank, index, Kirb=Kirb)
    if tex.get("T_Offset"):
        b = b[tex["T_Offset"] :]
    func = ImgTypes.get(tex["Fmt"])
    if tex["Fmt"] != "CI":
        png.close()
        return
    # I mixed up the format because I didn't like old one.
    # old format was tex[Pal_Bank], tex[Pal_Index]
    # new is just tex[Pal]
    try:
        pal = tex["Pal"]
    except:
        pal = (int(tex["Pal_Bank"]), int(tex["Pal_Index"]))
    pal = GetBinFile(*pal, Kirb=Kirb)
    if tex.get("Pal_Offset"):
        pal = pal[tex["Pal_Offset"] :]
    # palette is a tuple in this module for some reason?
    BinPNG.RGBA(len(pal)//2, 1, 16, pal, png)
    png.close()

# -------------------------------------------------------------------------------
# Main Funcs for interop with other files
# -------------------------------------------------------------------------------

# get all textures in one bank for comparison
def GetTexBank(bank, valid=True):
    js = GetJSON()
    if valid:
        return js["Validated"][f"Bank_{bank}"]
    else:
        return js["UnValidated"][f"Bank_{bank}"]


# export all textures in one bank for visual comparison
def ExportTexBank(bank):
    js = GetJSON()
    Guesses = Path(f"Test/bank_{bank}")
    if Guesses.exists():
        shutil.rmtree(Guesses)
    Guesses.mkdir(exist_ok=True)
    TestBank(js, bank, valid=True, rm=False)
    TestBank(js, bank, valid=False, rm=False)
