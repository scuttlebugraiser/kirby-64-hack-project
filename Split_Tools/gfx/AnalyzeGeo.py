# the purpose of this file is to analyze the geobin class created by SplitGeo in order to get the textures within it
# this is a modification of the blender plugin importing script
import math, re, os, sys
from dataclasses import dataclass
from copy import deepcopy
from functools import partial
from pathlib import Path

# to import utility.py, we need to add parent dir to path
file = Path(os.path.dirname(__file__))
sys.path.append(str(file.parent))
from Utility import *

# this will hold tile properties
class Tile:
    def __init__(self):
        self.Fmt = "RGBA"
        self.Siz = "16"
        self.Slow = 32
        self.Tlow = 32
        self.Shigh = 32
        self.Thigh = 32
        self.SMask = 5
        self.TMask = 5
        self.Sflags = None
        self.Tflags = None


# this will hold texture properties, dataclass props
# are created in order for me to make comparisons in a set
@dataclass(init=True, eq=True, unsafe_hash=True)
class Texture:
    Timg: tuple
    Fmt: str
    Siz: int
    Width: int = 0
    Height: int = 0
    Pal: tuple = None

    def dictify(self):
        timg = self.Timg.exp()
        try:
            pal = getattr(self, "Pal").exp()
            pal = pal.exp()
        except:
            pal = None
        return (
            timg,
            {
                "Fmt": self.Fmt,
                "Siz": self.Siz,
                "Width": self.Width,
                "Height": self.Height,
                "Pal": pal,
            },
        )


# This is simply a data storage class
class Mat:
    def __init__(self):
        self.TwoCycle = False
        self.GeoSet = []
        self.GeoClear = []
        self.tiles = [Tile() for a in range(8)]
        self.tex0 = None
        self.tex1 = None
        self.pal = None
        self.tx_scr = None

    # I only care about the source, size, and fmt here
    def FormatTex(self, tex, tile):
        tex.Fmt, tex.Siz = self.EvalFmt(tile)
        self.TextureSize(tex)

    def ExtractTextures(self):
        if self.tex0:
            # fmt from tile 0
            self.FormatTex(self.tex0, self.tiles[0])
            if self.pal and self.tex0.Fmt == "CI":
                self.tex0.Pal = self.pal.Timg
        if self.tex1:
            # fmt from tile 1
            self.FormatTex(self.tex1, self.tiles[1])
            if self.pal and self.tex1.Fmt == "CI":
                self.tex1.Pal = self.pal.Timg
        # expand tex0 to have copies for every tex in the scroll
        if not self.tx_scr:
            return (self.tex0, self.tex1)
        else:
            t0 = []
            if self.tx_scr.scroll.flags & 1:
                for tx in self.tex0.scr_tex:
                    if tx == (0x9999, 0x9999):
                        continue
                    new_tex = deepcopy(self.tex0)
                    new_tex.Timg = tx
                    t0.append(new_tex)
            else:
                t0.append(self.tex0)

            t1 = []
            if self.tx_scr.scroll.flags & 2:
                for tx in self.tex1.scr_tex:
                    if tx == (0x9999, 0x9999):
                        continue
                    new_tex = deepcopy(self.tex0)
                    new_tex.Timg = tx
                    t1.append(new_tex)
            else:
                t1.append(self.tex1)
            return (*(t0), *(t1))

    def TextureSize(self, tex):
        # dxt is a ratio between words and lines of a texture
        # we can use it to get the true texture width for textures
        # loaded via loadblock

        # that said sometimes dxt is used in funny ways for special effects
        # in these cases, I will default to another measurement because
        # dxt is no longer reliable
        if tex.dxt == 0:
            # this just allows export but in no way is this a normal texture
            # nor will it properly show up in blender as an import
            texels = tex.texels
            tex.Width = tex.texels
            tex.Height = 1
            return
        bitSiz = math.log2(tex.Siz // 4)
        if not bitSiz:
            bitSiz = 0.5
        # 32b is normally 3, but it should be 4 for math to work
        # gbi uses a different define than the normal bitsize for this
        if bitSiz == 3:
            bitSiz = 4
        # basically 0x800 represents the fixed notation of dxt (u1.11)
        # 8/bitSiz is the number of 64 bit chunks per texel
        # dxt is chunks / texel in fixed point, so width = chunks / dxt * 0x800
        width = int((0x7FF * 8 / bitSiz) / (tex.dxt - 1))
        # width * bitsize can't be below one
        if width * bitSiz < 1:
            bitSiz = 1 / width
            width = int((8 * 0x7FF / bitSiz) / (tex.dxt - 1))
        # in 4bit loading, texels are faked as if they're 16 bit
        # so each texel here is actually 4 texels
        if bitSiz == 0.5:
            height = int(4 * (tex.texels + 1) / width)
        else:
            height = int((tex.texels + 1) / width)
        tex.Width = width
        tex.Height = height

    def EvalFmt(self, tex):
        GBIfmts = {
            "G_IM_FMT_RGBA": "RGBA",
            "RGBA": "RGBA",
            "G_IM_FMT_CI": "CI",
            "CI": "CI",
            "G_IM_FMT_IA": "IA",
            "IA": "IA",
            "G_IM_FMT_I": "I",
            "I": "I",
            "0": "RGBA",
            "2": "CI",
            "3": "IA",
            "4": "I",
            0: "RGBA",
            2: "CI",
            3: "IA",
            4: "I",
        }
        GBIsiz = {
            "G_IM_SIZ_4b": 4,
            "G_IM_SIZ_8b": 8,
            "G_IM_SIZ_16b": 16,
            "G_IM_SIZ_32b": 32,
            "0": 4,
            "1": 8,
            "2": 16,
            "3": 32,
            0: 4,
            1: 8,
            2: 16,
            3: 32,
        }
        return GBIfmts.get(tex.Fmt, "RGBA"), GBIsiz.get(str(tex.Siz), 16)


# handles DL processing, specifically built to process each cmd into the mat class
# should be inherited into a larger F3d class which wraps DL processing
# does not deal with flow control or gathering the data containers (VB, Geo cls etc.)
class DL:
    # the min needed for this class to work
    def __init__(self, lastmat=None):
        self.VB = {}
        self.Gfx = {}
        self.diff = {}
        self.amb = {}
        self.Lights = {}
        if not lastmat:
            self.LastMat = Mat()
            self.LastMat.name = 0
        else:
            self.LastMat = lastmat

    # DL cmds that control the flow of a DL cannot be handled within a independent	#class method without larger context of the total DL
    # def gsSPEndDisplayList():
    # return
    # def gsSPBranchList():
    # break
    # def gsSPDisplayList():
    # continue
    # Vertices are one big list for kirby64, all buffers are combined in pre process
    def gsSPVertex(self, args, Geo):
        # fill virtual buffer
        args = [int(a) for a in args]
        for i in range(args[2], args[2] + args[1], 1):
            self.VertBuff[i] = len(self.Verts) + i - args[2]
        # verts are pre processed
        self.Verts.extend(Geo.vertices.Pos[args[0] : args[0] + args[1]])
        self.UVs.extend(Geo.vertices.UVs[args[0] : args[0] + args[1]])
        self.VCs.extend(Geo.vertices.VCs[args[0] : args[0] + args[1]])

    # Triangles
    def gsSP2Triangles(self, args, Geo):
        self.MakeNewMat()
        args = [int(a) for a in args]
        Tri1 = self.ParseTri(args[:3])
        Tri2 = self.ParseTri(args[4:7])
        self.Tris.append(Tri1)
        self.Tris.append(Tri2)

    def gsSP1Triangle(self, args, Geo):
        self.MakeNewMat()
        args = [int(a) for a in args]
        Tri = self.ParseTri(args[:3])
        self.Tris.append(Tri)

    # materials
    # Mats will be placed sequentially. The first item of the list is the triangle number
    # The second is the material class
    def gsDPSetRenderMode(self, args, Geo):
        self.NewMat = 1
        self.LastMat.RenderMode = [a.strip() for a in args]

    def gsDPSetFogColor(self, args, Geo):
        self.NewMat = 1
        self.LastMat.fog_color = args

    def gsSPFogPosition(self, args, Geo):
        self.NewMat = 1
        self.LastMat.fog_pos = args

    def gsSPLightColor(self, args, Geo):
        self.NewMat = 1
        if not hasattr(self.LastMat, "light_col"):
            self.LastMat.light_col = {}
        num = re.search("_\d", args[0]).group()[1]
        self.LastMat.light_col[num] = args[-1]

    def gsDPSetPrimColor(self, args, Geo):
        self.NewMat = 1
        self.LastMat.prim = args

    def gsDPSetEnvColor(self, args, Geo):
        self.NewMat = 1
        self.LastMat.env = args

    # multiple geo modes can happen in a row that contradict each other
    # this is mostly due to culling wanting diff geo modes than drawing
    # but sometimes using the same vertices
    def gsSPClearGeometryMode(self, args, Geo):
        self.NewMat = 1
        args = [a.strip() for a in args[0].split("|")]
        for a in args:
            if a in self.LastMat.GeoSet:
                self.LastMat.GeoSet.remove(a)
        self.LastMat.GeoClear.extend(args)

    def gsSPSetGeometryMode(self, args, Geo):
        self.NewMat = 1
        args = [a.strip() for a in args[0].split("|")]
        for a in args:
            if a in self.LastMat.GeoClear:
                self.LastMat.GeoClear.remove(a)
        self.LastMat.GeoSet.extend(args)

    def gsSPGeometryMode(self, args, Geo):
        self.NewMat = 1
        argsC = [a.strip() for a in args[0].split("|")]
        argsS = [a.strip() for a in args[1].split("|")]
        for a in argsC:
            if a in self.LastMat.GeoSet:
                self.LastMat.GeoSet.remove(a)
        for a in argsS:
            if a in self.LastMat.GeoClear:
                self.LastMat.GeoClear.remove(a)
        self.LastMat.GeoClear.extend(argsC)
        self.LastMat.GeoSet.extend(argsS)

    def gsDPSetCycleType(self, args, Geo):
        if "G_CYC_1CYCLE" in args[0]:
            self.LastMat.TwoCycle = False
        if "G_CYC_2CYCLE" in args[0]:
            self.LastMat.TwoCycle = True

    def gsDPSetCombineMode(self, args, Geo):
        self.NewMat = 1
        self.LastMat.Combiner = args

    def gsDPSetCombineLERP(self, args, Geo):
        self.NewMat = 1
        self.LastMat.Combiner = [a.strip() for a in args]

    # root tile, scale and set tex
    def gsSPTexture(self, args, Geo):
        self.NewMat = 1
        macros = {
            "G_ON": 2,
            "G_OFF": 0,
        }
        set_tex = macros.get(args[-1].strip())
        if set_tex == None:
            set_tex = int(args[-1].strip())
        self.LastMat.set_tex = set_tex == 2
        self.LastMat.tex_scale = [
            ((0x10000 * (int(a) < 0)) + int(a)) / 0xFFFF for a in args[0:2]
        ]  # signed half to unsigned half
        self.LastMat.tile_root = self.EvalTile(
            args[-2].strip()
        )  # I don't think I'll actually use this

    # last tex is a palette
    def gsDPLoadTLUT(self, args, Geo):
        try:
            tex = self.LastMat.loadtex
            self.LastMat.pal = tex
        except:
            print(
                "**--Load block before set t img, DL is partial and missing context"
                "likely static file meant to be used as a piece of a realtime system.\n"
                "No interpretation on file possible**--"
            )
            return None

    # tells us what tile the last loaded mat goes into
    def gsDPLoadBlock(self, args, Geo):
        try:
            tex = self.LastMat.loadtex
            # these values can be used to calc texture size
            tex.dxt = eval(args[4])
            tex.texels = eval(args[3])
            tile = self.EvalTile(args[0])
            tex.tile = tile
            if tile == 7:
                self.LastMat.tex0 = tex
            elif tile == 6:
                self.LastMat.tex1 = tex
        except:
            print(
                "**--Load block before set t img, DL is partial and missing context"
                "likely static file meant to be used as a piece of a realtime system.\n"
                "No interpretation on file possible**--"
            )
            return None

    def gsDPSetTextureImage(self, args, Geo):
        self.NewMat = 1
        Timg = BankIndex(eval(args[3].strip()) >> 16, eval(args[3].strip()) & 0xFFFF)
        Fmt = args[1].strip()
        Siz = args[2].strip()
        loadtex = Texture(Timg, Fmt, Siz)
        self.LastMat.loadtex = loadtex

    # catch tile size
    def gsDPSetTileSize(self, args, Geo):
        self.NewMat = 1
        tile = self.LastMat.tiles[self.EvalTile(args[0])]
        tile.Slow = self.EvalImFrac(args[1].strip())
        tile.Tlow = self.EvalImFrac(args[2].strip())
        tile.Shigh = self.EvalImFrac(args[3].strip())
        tile.Thigh = self.EvalImFrac(args[4].strip())

    def gsDPSetTile(self, args, Geo):
        self.NewMat = 1
        tile = self.LastMat.tiles[self.EvalTile(args[4])]
        tile.Fmt = args[0].strip()
        tile.Siz = args[1].strip()
        tile.Tflags = args[6].strip()
        tile.TMask = int(args[7].strip())
        tile.TShift = int(args[8].strip())
        tile.Sflags = args[9].strip()
        tile.SMask = int(args[10].strip())
        tile.SShift = int(args[11].strip())

    def MakeNewMat(self):
        if self.NewMat:
            self.NewMat = 0
            self.Mats.append([len(self.Tris) - 1, self.LastMat])
            self.LastMat = deepcopy(self.LastMat)  # for safety
            self.LastMat.name = self.num + 1
            if self.LastMat.tx_scr:
                # I'm clearing here because I did some illegal stuff a bit before, temporary (maybe)
                self.LastMat.tx_scr = None
            self.num += 1

    def ParseTri(self, Tri):
        return [self.VertBuff[a] for a in Tri]

    def EvalImFrac(self, arg):
        if type(arg) == int:
            return arg
        arg2 = arg.replace("G_TEXTURE_IMAGE_FRAC", "2")
        return eval(arg2)

    def EvalTile(self, arg):
        # only 0 and 7 have enums, other stuff just uses int (afaik)
        Tiles = {
            "G_TX_LOADTILE": 7,
            "G_TX_RENDERTILE": 0,
            "G_TX_NOMASK": 0,
            "G_TX_NOLOD": 0,
        }
        t = Tiles.get(arg)
        if t == None:
            t = int(arg)
        return t


# covers all f3d data, DL cmds, tex scrolls, making mats etc.
# inhertis from DL class to add versatility when porting to other games/plats
class F3d(DL):
    def __init__(self, lastmat=None):
        super().__init__()
        self.num = self.LastMat.name  # for debug

    # gets all the textures in block name only and separates palettes
    # this is solely for creating a dict/data analysis
    def GetTexBlocks(self):
        tx = []
        pals = []
        # if an iterable, increase via extend, else append
        def concat(block, dat):
            if hasattr(dat, "__iter__"):
                block.extend(dat)
                return block
            else:
                block.append(dat)
                return block

        # add images in tex to lists
        def Fill(tx, pals, tex, tile, m):
            m.FormatTex(tex, tile)
            if m.pal and tex.Fmt == "CI":
                pals = concat(pals, m.pal.Timg)
            tx = concat(tx, tex.Timg)

        Fill = partial(Fill, tx, pals)
        # loop over mats and grab out texture indices
        for t, m in self.Mats:
            if m.tx_scr:
                if m.tx_scr.scroll.flags & 1:
                    m.tex0.Timg = tuple(m.tex0.scr_tex)
                if m.tx_scr.scroll.flags & 2:
                    m.tex1.Timg = tuple(m.tex1.scr_tex)
                if m.tx_scr.scroll.flags & 4:
                    m.pal.Timg = tuple(m.pal.scr_pal)
            # fill lists with image blocks found in each texture
            if m.tex0:
                Fill(m.tex0, m.tiles[0], m)
            if m.tex1:
                Fill(m.tex1, m.tiles[1], m)
        return {"tex": tx, "palettes": pals}

    # get all the textures in mats
    def GetTexturesFromMats(self):
        tx = []
        for t, m in self.Mats:
            tx.extend(m.ExtractTextures())
        # [print(t) for t in tx]
        return tx

    # use tex scroll struct info to get the equivalent dynamic DL, and set the t_scroll flag to true in mat so when getting mats, I can return an array of mats
    def ScrollDynDL(self, Geo, layout, scr_num):
        try:
            Tex_Scroll = Geo.tex_scrolls[Geo.tex_header[layout.index]]
            scr = Tex_Scroll.scrolls[Tex_Scroll.scroll_ptrs[scr_num]]
        except:
            # if there is a key error, then I figure that the second
            # DL is just inheriting the last scroll (possibly?)
            return
        # I really only care about palettes and textures
        if scr.scroll.flags & 3:
            self.LastMat.tx_scr = scr
            tex = Tex_Scroll.textures[scr.scroll.textures]
            Timg = tex[0]
            Fmt = scr.scroll.fmt1
            Siz = scr.scroll.siz1
            loadtex = Texture(Timg, Fmt, Siz)
            loadtex.scr_tex = tex[:-1]
            self.LastMat.loadtex = loadtex
            # if both textures are present, dyn DL loads tex1 (tile 6)
            # this results in both just being the same as far as my
            # export is concerned, but I will replicate DL bhv
            if scr.scroll.flags & 3 == 3:
                self.LastMat.tex1 = loadtex
                self.LastMat.tex1.scr_tex = tex[:-1]
        # if there is both a tex and a palette, then the load TLUT
        # is inside of the dyn DL, otherwise it isn't
        if scr.scroll.flags & 4:
            pals = Tex_Scroll.palettes[scr.scroll.palettes]
            Timg = pals[0]
            Fmt = "G_IM_FMT_RGBA"
            Siz = "G_IM_SIZ_16b"
            pal_tex = Texture(Timg, Fmt, Siz)
            pal_tex.scr_pal = pals[:-1]
            if scr.scroll.flags & 3:
                self.LastMat.pal = pal_tex
            else:
                self.LastMat.loadtex = pal_tex

    # recursively parse the display list in order to return a bunch of model data
    def GetDataFromDL(self, Geo, layout):
        self.VertBuff = [
            0
        ] * 32  # If you're doing some fucky shit with a larger vert buffer it sucks to suck I guess
        self.Tris = []
        self.UVs = []
        self.VCs = []
        self.Verts = []
        self.Mats = []
        self.NewMat = 0
        if hasattr(layout, "DLs"):
            for k in layout.entry:
                DL = layout.DLs[k].commands
                self.ParseDL(DL, Geo, layout)
        return (self.Verts, self.Tris)

    def ParseDL(self, DL, Geo, layout):
        # This will be the equivalent of a giant switch case
        x = -1
        while x < len(DL):
            # manaual iteration so I can skip certain children efficiently if needed
            x += 1
            (cmd, args) = DL[x]  # each member is a tuple of (cmd, arguments)
            LsW = cmd.startswith
            # Deal with control flow first, this requires total DL context
            if LsW("gsSPEndDisplayList"):
                return
            # recursively call ParseDL
            if LsW("gsSPBranchList"):
                if self.DL_ptr(args[0]):
                    self.ParseDL(layout.DLs[self.DL_ptr(args[0])].commands, Geo, layout)
                else:
                    scr_num = (eval(args[0]) & 0xFFFF) // 8
                    self.ScrollDynDL(Geo, layout, scr_num)
                break
            if LsW("gsSPDisplayList"):
                if self.DL_ptr(args[0]):
                    self.ParseDL(layout.DLs[self.DL_ptr(args[0])].commands, Geo, layout)
                else:
                    scr_num = (eval(args[0]) & 0xFFFF) // 8
                    self.ScrollDynDL(Geo, layout, scr_num)
                continue
            # tri and mat DL cmds will be called via parent class
            func = getattr(self, cmd, None)
            if func:
                func(args, Geo)
                continue

    # specific to kirby
    def DL_ptr(self, num):
        num = int(num)
        if num >> 24 == 0xE:
            return None
        else:
            return num

    def StripArgs(self, cmd):
        a = cmd.find("(")
        return cmd[a + 1 : -2].split(",")
