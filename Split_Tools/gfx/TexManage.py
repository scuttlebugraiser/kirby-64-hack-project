# this is a high level file which will serve as interop between
# SplitGeo.py and T_ADDR.py which each serve the individual purpose
# of analyzing geo binary files and texture binary files

# The main purpose of this file is to get all textures identified properly
# using data from the geo block, and then to export/verify with T_ADDR

import os, sys, shutil
from pathlib import Path
from collections import namedtuple

# if the path is from the same directory, these imports will work
# otherwise it will use an absolute import as if it was from the parent
try:
    from TxDat import T_ADDR as t_addr
    import SplitGeo as SplitGeo
except:
    from gfx.TxDat import T_ADDR as t_addr
    import gfx.SplitGeo as SplitGeo

# to import utility.py, we need to add parent dir to path
file = Path(os.path.dirname(__file__))
sys.path.append(str(file.parent))
from Utility import *

# -------------------------------------------------------------------------------
# Helper functions
# -------------------------------------------------------------------------------


def GetGeoJS():
    return t_addr.GetJSON(name="KirbTextures_FromGeo.json")


# check if value is inside iterable (only check one lvl deep)
def Missing(val, iterable):
    for i in iterable:
        try:
            if val in i:
                break
        except:
            if val == i:
                break
    else:
        return True
    return False


# takes a set of textures, and makes a dict
# dict takes indices as values only, banknum is in higher branch of dict
def SetConvert(tx_set):
    dat = dict()
    for tx in tx_set:
        if not tx:
            continue
        t = tx.dictify()
        dat[t[0][1]] = t[1]
    return dat


# -------------------------------------------------------------------------------
# Get Geo Bin Data
# -------------------------------------------------------------------------------

# gets a single geo block and returns texture set
def GetGeoBlock(bank, index, Kirb: "binary ROM file"):
    block = SplitGeo.GetSingleGeo(bank, index, Kirb)
    return SplitGeo.AnalyzeBlock(block)


# gets all geo block data from an entire bank
def GetGeoBank(bank, Kirb: "binary ROM file"):
    tex_set = set()
    for index in LoopOverBank(bank, Kirb):
        block = SplitGeo.GetSingleGeo(bank, index, Kirb)
        tex_set.update(SplitGeo.AnalyzeBlock(block))
    return tex_set


# gets all palettes and textures used in a single block
# this is a dictionary textures, categorized by type, not itemized info on textures used
def GetBlockTex(bank, index, Kirb: "binary ROM file"):
    block = SplitGeo.GetSingleGeo(bank, index, Kirb)
    tex_dict = SplitGeo.AnalyzeTex(block)
    return tex_dict


# given a dict of textures, check a bank for sprites and return dict of them
def GetSprites(bank, Kirb: "binary ROM file", tx_dict=None):
    sprites = dict()
    if not tx_dict:
        tx_dict = dict()
    for index in LoopOverBank(bank, Kirb, block_type="Image"):
        if Missing(index, tx_dict.keys()):
            if t_addr.SpriteCheck(bank, index, Kirb=Kirb):
                sprites[index] = t_addr.ExportSprite(
                    bank, index, Kirb=Kirb, JustDat=True
                )
    return sprites


# gets all textures from bank possible and returns a dict
def GetBankTex(bank, Kirb: "binary ROM file"):
    tx_dict = SetConvert(GetGeoBank(bank, Kirb))
    tx_dict.update(GetSprites(bank, Kirb, tx_dict=tx_dict))
    return tx_dict


# -------------------------------------------------------------------------------
# Compare totals, for debugging
# -------------------------------------------------------------------------------

# gets all banks and compares the image counts for each bank, can show missing cnts
def CompareBank(bank, Kirb: "binary ROM file"):
    tx_dict = {"tex": [], "palettes": [], "Missing": []}
    index = 0
    for index in LoopOverBank(bank, Kirb):
        new_dict = GetBlockTex(bank, index, Kirb)
        tx_dict["tex"].extend(new_dict["tex"])
        tx_dict["palettes"].extend(new_dict["palettes"])
    # look for sprites
    for index in LoopOverBank(bank, Kirb, block_type="Image"):
        if Missing((bank, index), tx_dict):
            if t_addr.SpriteCheck(bank, index, Kirb=Kirb):
                tx_dict["tex"].append((bank, index))
            else:
                tx_dict["Missing"].append((bank, index))
    # convert each member into a set
    tx_dict["tex"] = set(tx_dict["tex"])
    tx_dict["palettes"] = set(tx_dict["palettes"])
    tx_dict["Missing"] = set(tx_dict["Missing"])
    return tx_dict


# compares all textures in the game, creates a dict for each bank
def CompareAll(Kirb: "binary ROM file"):
    tx_dict = {}
    dat_dict = {}
    for i in range(8):
        new_dict = CompareBank(i, Kirb)
        num = BankLen(i, Kirb, block_type="Image")
        tx_dict[i] = tx_cnt(
            num - 1,
            len(new_dict["tex"]) + len(new_dict["palettes"]),
            len(new_dict["Missing"]),
        )
        dat_dict[i] = new_dict
    return dat_dict


# tuple data type for comparisons
tx_cnt = namedtuple("tex_count", "bank_cnt num_tex missing")

# -------------------------------------------------------------------------------
# Texture Creators
# -------------------------------------------------------------------------------
# all of these files have the ability to pass arbitrary **kwargs to T_ADDR as needed
# the named kwargs are ones that are required for exporting from this file correctly

# given a set of textures, exports them all
def ExportTexSet(tex_set: set, Kirb: "binary ROM file", tx_name="image", **kwargs):
    for t in tex_set:
        if t and BlockExists(*t.Timg, Kirb, block_type="Image"):
            try:
                t_dict = t.dictify()[1]
                if t_dict.get("Sprite_Header"):
                    exp_tx_name=f"{tx_name}.{t_dict['Fmt'].lower()}{t_dict['Siz']}.background"
                else:
                    exp_tx_name=f"{tx_name}.{t_dict['Fmt'].lower()}{t_dict['Siz']}"
                t_addr.ExportSingle(*t.Timg, t_dict, Kirb=Kirb, tx_name=exp_tx_name, **kwargs)
                print(f"{t.Timg} image exported")
            except:
                print(f"texture {t.Timg} export failed")


# exports textures given a dictionary of textures
def ExportTexDict(bank, t_dict: dict, Kirb: "binary ROM file", tx_name="image", **kwargs):
    for k, v in t_dict.items():
        timg = (bank, int(k))
        try:
            if v.get("Sprite_Header") is not None:
                exp_tx_name=f"{tx_name}.{v['Fmt'].lower()}{v['Siz']}.background"
            else:
                exp_tx_name=f"{tx_name}.{v['Fmt'].lower()}{v['Siz']}"
            t_addr.ExportSingle(*timg, v, Kirb=Kirb, tx_name=exp_tx_name, **kwargs)
            print(f"{timg} image exported")
        except:
            print(f"texture {timg} export failed")


# exports contents of one block from geo bins
def ExportBlockTex(bank, index, Kirb: "binary ROM file", **kwargs):
    ExportTexSet(GetGeoBlock(bank, index, Kirb), Kirb, **kwargs)


# export contents from one entire bank of geo bins
def ExportBankTex(bank, Kirb: "binary ROM file", **kwargs):
    ExportTexSet(GetGeoBank(bank, Kirb), Kirb, **kwargs)


# export contents of bank from json
def ExportBankjs(bank, Kirb: "binary ROM file", js=None, **kwargs):
    if not js:
        js = t_addr.GetTexBank(bank, valid=True, **kwargs)  # gets Kirby_Textures.json
    ExportTexDict(bank, js, Kirb, **kwargs)


def ExportSingeJS(bank, index, Kirb: "binary ROM file", js=None, **kwargs):
    if not js:
        js = t_addr.GetTexBank(bank, valid=True, **kwargs)  # gets Kirby_Textures.json
    timg = (bank, index)
    tex_dat = js.get(str(index), None)
    try:
        if tex_dat.get("Sprite_Header") is not None:
            exp_tx_name=f"image.{tex_dat['Fmt'].lower()}{tex_dat['Siz']}.background"
        else:
            exp_tx_name=f"image.{tex_dat['Fmt'].lower()}{tex_dat['Siz']}"
        t_addr.ExportSingle(*timg, tex_dat, Kirb=Kirb, tx_name=exp_tx_name, path=Path(f"assets/image/bank_{bank}/{index}"), **kwargs)
        print(f"{timg} image exported")
    except:
        print(f"texture {timg} export failed")

# look for sprites in a bank and export them
def ExportSprites(bank, Kirb: "binary ROM file", tx_name="image", **kwargs):
    for index in LoopOverBank(bank, Kirb, block_type="Image"):
        if Missing((bank, index), tx_dict):
            t_addr.ExportSprite(bank, index, Kirb=Kirb, tx_name=f"{tx_name}.background.{tx_dict['Fmt']}{tx_dict['Siz']}", **kwargs)


# -------------------------------------------------------------------------------
# Main
# -------------------------------------------------------------------------------

# reads all the geo blocks and then creates a js file
def CreateGeoJS(Kirb: "binary ROM file"):
    tx = dict()
    for i in range(8):
        tx[f"Bank_{i}"] = GetBankTex(i, Kirb)
    t_addr.WriteJSON(tx, name="KirbTextures_FromGeo.json")


# from the created JS file, export all textures
def ExportAllJS():
    js = GetGeoJS()
    for i in range(8):
        ExportBankjs(i, Kirb, js=js[f"Bank_{i}"])


# edit as needed to do work
def main(Kirb: "binary ROM file"):
    js = GetGeoJS()
    t_addr.ClearGuesses()
    # for i in range(1021, 1022):
        # ExportSingeJS(7, i, Kirb, js=js[f"Bank_7"])
    # ExportBankjs(0, Kirb, js=js)
    # CreateGeoJS(Kirb)
    # ExportAllJS()
    # ExportBlockTex(7, 2, Kirb)
    # print(GetBankTex(0, Kirb))
    # GetGeoBank(0, Kirb)
    # CompareAll(Kirb)
    pass


if __name__ == "__main__":
    Kirby = open(sys.argv[1], "rb")
    Kirb = Kirby.read()
    main(Kirb)
    print("finished")
