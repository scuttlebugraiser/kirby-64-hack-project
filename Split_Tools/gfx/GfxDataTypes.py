# -------------------------------------------------------------------------------
# Geometry Block Structs/Data
# -------------------------------------------------------------------------------

HS = 0x80057DB0

geo_block_includes = """
#include <ultra64.h>
#include "geo_block_header.h"
#include "stages.h"
#include "geo.h"
"""


# type dicts for writing to file only
# values are (data type, then name, is arr (only as needed))

geo_block_header = {
    0x00: ("struct Layout", "*layout[]", "ptr"),
    0x04: ("struct TextureScroll", "*tex_scroll[]", "ptr"),
    0x08: ("u32", "rendering_mode"),
    0x0C: ("void", "*img_refs[]", "ptr"),
    0x10: ("void", "*vtx_refs[]", "ptr"),
    0x14: ("u32", "Num_Anims"),
    0x18: ("void", "*Anims[]", "ptr"),
    0x1C: ("u32", "Num_Layouts"),
}

Layout = {
    0x00: ("u16", "Flag"),
    0x02: ("u16", "Depth"),
    0x04: ("struct Entry_Point", "Entry Points", "ptr"),
    0x08: ("f32", "Translation[3]", "arr"),
    0x14: ("f32", "Rotation[3]", "arr"),
    0x20: ("f32", "Scale[3]", "arr"),
}

# use for extraction,
TextureScrollStruct = {
    0x00: [">H", "field_0x0", 2],
    0x02: [">B", "fmt1", 1],
    0x03: [">B", "siz1", 1],
    0x04: [">L", "textures", 4],
    0x08: [">H", "stretch", 2],
    0x0A: [">H", "sharedOffset", 2],
    0x0C: [">H", "t0_w", 2],
    0x0E: [">H", "t0_h", 2],
    0x10: [">L", "halve", 4],
    0x14: [">f", "t0_xShift", 4],
    0x18: [">f", "t0_yShift", 4],
    0x1C: [">f", "xScale", 4],
    0x20: [">f", "yScale", 4],
    0x24: [">f", "field_0x24", 4],
    0x28: [">f", "field_0x28", 4],
    0x2C: [">L", "palettes", 4],
    0x30: [">H", "flags", 2],
    0x32: [">B", "fmt2", 1],
    0x33: [">B", "siz2", 1],
    0x34: [">H", "w2", 2],
    0x36: [">H", "h2", 2],
    0x38: [">H", "t1_w", 2],
    0x3A: [">H", "t1_h", 2],
    0x3C: [">f", "t1_xShift", 4],
    0x40: [">f", "t1_yShift", 4],
    0x44: [">f", "field_0x44", 4],
    0x48: [">f", "field_0x48", 4],
    0x4C: [">L", "field_0x4c", 4],
    0x50: [">4B", "prim_col", 4, "arr"],
    0x55: [">B", "field_0x54", 1],
    0x54: [">B", "primLODFrac", 1],
    0x56: [">B", "field_0x56", 1],
    0x57: [">B", "field_0x57", 1],
    0x58: [">4B", "env_col", 4, "arr"],
    0x5C: [">4B", "blend_col", 4, "arr"],
    0x60: [">4B", "light1_col", 4, "arr"],
    0x64: [">4B", "light2_col", 4, "arr"],
    0x68: [">L", "field_0x68", 4],
    0x6C: [">L", "field_0x6c", 4],
    0x70: [">L", "field_0x70", 4],
    0x74: [">L", "field_0x74", 4],
}


TextureScrollW = {
    0x00: ["u16", "field_0x0"],
    0x02: ["u8", "fmt1"],
    0x03: ["u8", "siz1"],
    0x04: ["u32", "textures", "ptr"],
    0x08: ["u16", "stretch"],
    0x0A: ["u16", "sharedOffset"],
    0x0C: ["u16", "t0_w"],
    0x0E: ["u16", "t0_h"],
    0x10: ["u32", "halve"],
    0x14: ["f32", "t0_xShift"],
    0x18: ["f32", "t0_yShift"],
    0x1C: ["f32", "xScale"],
    0x20: ["f32", "yScale"],
    0x24: ["f32", "field_0x24"],
    0x28: ["f32", "field_0x28"],
    0x2C: ["u32", "palettes", "ptr"],
    0x30: ["u16", "flags"],
    0x32: ["u8", "fmt2"],
    0x33: ["u8", "siz2"],
    0x34: ["u16", "w2"],
    0x36: ["u16", "h2"],
    0x38: ["u16", "t1_w"],
    0x3A: ["u16", "t1_h"],
    0x3C: ["f32", "t1_xShift"],
    0x40: ["f32", "t1_yShift"],
    0x44: ["f32", "field_0x44"],
    0x48: ["f32", "field_0x48"],
    0x4C: ["u32", "field_0x4c"],
    0x50: ["u8", "prim_col[]", "arr"],
    0x54: ["u8", "primLODFrac"],
    0x55: ["u8", "field_0x55"],
    0x56: ["u8", "field_0x56"],
    0x57: ["u8", "field_0x57"],
    0x58: ["u8", "env_col[]", "arr"],
    0x5C: ["u8", "blend_col[]", "arr"],
    0x60: ["u8", "light1_col[]", "arr"],
    0x64: ["u8", "light2_col[]", "arr"],
    0x68: ["u32", "field_0x68"],
    0x6C: ["u32", "field_0x6c"],
    0x70: ["u32", "field_0x70"],
    0x74: ["u32", "field_0x74"],
}
