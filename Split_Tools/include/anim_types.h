#include "ultra64.h"
#include "types.h"


typedef union{
	u32 word;
	f32 single;
} AnimScript;


struct Animheader {
	union {
		AnimScript bone; // mode 2
		AnimScript *anim_bones; // mode 0
		AnimScript **anim_scrolls; // mode 1
	};
	u32 mode; // 0, 1 or 2
	u32 num_virtual_offsets;
};


// start of anim cmd encoding

// anonymous macros for values encoded into the bytearray
#define AnimSingle(value) {.single = value}
#define AnimWord(value) {.word = value}
#define AnimColor(r, g, b, a) {.word =	\
	(_SHIFTL(r, 24, 8) | _SHIFTL(g, 16, 8) | 	\
	 _SHIFTL(b, 8, 8) | _SHIFTL(a, 0, 8))\
}


// cmd is first 0xFE of word
// transform field is 0x01 FF 80 00 of word, only exists if cmd has args
// scale is 0x00 00 7F FF of the arg
#define AnimCmd_NoArgs(cmd)	\
	AnimWord(_SHIFTL(cmd, 25, 7))
	
#define AnimCmd_PackedArg(cmd, transform, scale, value)	\
	AnimWord(_SHIFTL(cmd, 25, 7) | _SHIFTL(transform, 15, 10) | _SHIFTL(scale, 0, 15) | value)

#define AnimCmd_Ptr(cmd, scale, ptr)	\
	AnimWord(_SHIFTL(cmd, 25, 7) | _SHIFTL(scale, 0, 15)), AnimWord((uintptr_t) ptr)

#define AnimCmd_Single(cmd, transform, scale, single)	\
	AnimWord(_SHIFTL(cmd, 25, 7) | _SHIFTL(transform, 15, 10) | _SHIFTL(scale, 0, 15)), AnimSingle(single)

#define AnimCmd_Word(cmd, transform, scale, word)	\
	AnimWord(_SHIFTL(cmd, 25, 7) | _SHIFTL(transform, 15, 10) | _SHIFTL(scale, 0, 15)), AnimWord(word)

// these are the important ones
#define AnimCmd_Color(cmd, transform, scale, ...)	\
	AnimWord(_SHIFTL(cmd, 25, 7) | _SHIFTL(transform, 15, 10) | _SHIFTL(scale, 0, 15)),FOR_EACH(AnimWord, __VA_ARGS__)

#define AnimCmd_Transform(cmd, transform, scale, ...)	\
	AnimWord(_SHIFTL(cmd, 25, 7) | _SHIFTL(transform, 15, 10) | _SHIFTL(scale, 0, 15)), FOR_EACH(AnimSingle, __VA_ARGS__)


// now define actual cmds
#define Stop() AnimCmd_NoArgs(0x0)
#define Goto() AnimCmd_Ptr(0x1, 0, ptr)
#define Block(scale) AnimCmd_PackedArg(0x2, 0, scale, 0)
#define Lerp_Cusp_B(transform, scale, ...) AnimCmd_Transform(0x3, transform, scale, __VA_ARGS__)
#define Lerp_Cusp_NB(transform, scale, ...) AnimCmd_Transform(0x4, transform, scale, __VA_ARGS__)
#define Lerp_Vel_B(transform, scale, ...) AnimCmd_Transform(0x5, transform, scale, __VA_ARGS__)
#define Lerp_Vel_NB(transform, scale, ...) AnimCmd_Transform(0x6, transform, scale, __VA_ARGS__)
#define Set_Lerp_Velocity(transform, scale, ...) AnimCmd_Transform(0x7, transform, scale, __VA_ARGS__)
#define Lerp_Ease_B(transform, scale, ...) AnimCmd_Transform(0x8, transform, scale, __VA_ARGS__)
#define Lerp_Ease_NB(transform, scale, ...) AnimCmd_Transform(0x9, transform, scale, __VA_ARGS__)
#define Set_B(transform, scale, ...) AnimCmd_Transform(0xA, transform, scale, __VA_ARGS__)
#define Set_NB(transform, scale, ...) AnimCmd_Transform(0xB, transform, scale, __VA_ARGS__)
#define Extend(transform, scale) AnimCmd_PackedArg(0xC, transform, scale, 0)
#define UnkD(transform, scale, ...) AnimCmd_Color(0xD, transform, scale, __VA_ARGS__)
#define Loop(ptr) AnimCmd_Ptr(0xE, 0, ptr)
#define SetProcessFlag(transform, scale) AnimCmd_PackedArg(0xF, transform, 0, 0)
#define Unk10(transform, scale) AnimCmd_PackedArg(0x10, transform, scale, 0)
#define Unk11(transform, scale, ...) AnimCmd_Transform(0x11, transform, scale, __VA_ARGS__)
#define Set_Color_B(transform, scale, ...) AnimCmd_Color(0x12, transform, scale, __VA_ARGS__)
#define Set_Color_NB(transform, scale, ...) AnimCmd_Color(0x13, transform, scale, __VA_ARGS__)
#define Lerp_Color_B(transform, scale, ...) AnimCmd_Color(0x14, transform, scale, __VA_ARGS__)
#define Lerp_Color_NB(transform, scale, ...) AnimCmd_Color(0x15, transform, scale, __VA_ARGS__)
#define Set_Int_Field_Block(transform, scale, word) AnimCmd_Color(0x16, transform, scale, __VA_ARGS__)
#define Unk17(scale, transform, ...) AnimCmd_Transform(0x17, transform, scale, __VA_ARGS__)



// some transforms for user clarifications

// using the for each macro, values are put into comma separated lists
// use -E -o file_test.c to unroll and see test of this

#define Color(r, g, b, a) \
	(_SHIFTL(r, 24, 8) | _SHIFTL(g, 16, 8) | 	\
	 _SHIFTL(b, 8, 8) | _SHIFTL(a, 0, 8))\

#define pass(value) value

#define	Rotate_X(...) FOR_EACH(pass, ##__VA_ARGS__)
#define	Rotate_Y(...) FOR_EACH(pass, ##__VA_ARGS__)
#define	Rotate_Z(...) FOR_EACH(pass, ##__VA_ARGS__)
#define	Rotate_XY(...) FOR_EACH(pass, ##__VA_ARGS__)
#define	Rotate_XZ(...) FOR_EACH(pass, ##__VA_ARGS__)
#define	Rotate_YZ(...) FOR_EACH(pass, ##__VA_ARGS__)
#define	Rotate_XYZ(...) FOR_EACH(pass, ##__VA_ARGS__)

#define	Base(...) FOR_EACH(pass, ##__VA_ARGS__)
#define	Field(...) FOR_EACH(pass, ##__VA_ARGS__)

#define	Translate_X(...) FOR_EACH(pass, ##__VA_ARGS__)
#define	Translate_Y(...) FOR_EACH(pass, ##__VA_ARGS__)
#define	Translate_Z(...) FOR_EACH(pass, ##__VA_ARGS__)
#define	Translate_XY(...) FOR_EACH(pass, ##__VA_ARGS__)
#define	Translate_XZ(...) FOR_EACH(pass, ##__VA_ARGS__)
#define	Translate_YZ(...) FOR_EACH(pass, ##__VA_ARGS__)
#define	Translate_XYZ(...) FOR_EACH(pass, ##__VA_ARGS__)

#define	Scale_X(...) FOR_EACH(pass, ##__VA_ARGS__)
#define	Scale_Y(...) FOR_EACH(pass, ##__VA_ARGS__)
#define	Scale_Z(...) FOR_EACH(pass, ##__VA_ARGS__)
#define	Scale_XY(...) FOR_EACH(pass, ##__VA_ARGS__)
#define	Scale_XZ(...) FOR_EACH(pass, ##__VA_ARGS__)
#define	Scale_YZ(...) FOR_EACH(pass, ##__VA_ARGS__)
#define	Scale_XYZ(...) FOR_EACH(pass, ##__VA_ARGS__)