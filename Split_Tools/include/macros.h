#ifndef _MACROS_H_
#define _MACROS_H_


#if !defined(__sgi) || defined(__CTX__)
#define GLOBAL_ASM(...)
#endif

#define ARRAY_COUNT(arr) (s32)(sizeof(arr) / sizeof(arr[0]))

#define GLUE(a, b) a ## b
#define GLUE2(a, b) GLUE(a, b)

// Avoid compiler warnings for unused variables
#ifdef __GNUC__
#define UNUSED __attribute__((unused))
#else
#define UNUSED
#endif

// Avoid undefined behaviour for non-returning functions
#ifdef __GNUC__
#define NORETURN __attribute__((noreturn))
#else
#define NORETURN
#endif

// Static assertions
#ifdef __GNUC__
#define STATIC_ASSERT(cond, msg) _Static_assert(cond, msg)
#else
#define STATIC_ASSERT(cond, msg) typedef char GLUE2(static_assertion_failed, __LINE__)[(cond) ? 1 : -1]
#endif

// Align to 4-byte boundary for DMA requirements
#ifdef __GNUC__
#define ALIGNED4 __attribute__((aligned(4)))
#else
#define ALIGNED4
#endif

// Align to 8-byte boundary for DMA requirements
#ifdef __GNUC__
#define ALIGNED8 __attribute__((aligned(8)))
#else
#define ALIGNED8
#endif

// Align to 16-byte boundary for audio lib requirements
#ifdef __GNUC__
#define ALIGNED16 __attribute__((aligned(16)))
#else
#define ALIGNED16
#endif

// convert a virtual address to physical.
#define VIRTUAL_TO_PHYSICAL(addr)   ((uintptr_t)(addr) & 0x1FFFFFFF)

// convert a physical address to virtual.
#define PHYSICAL_TO_VIRTUAL(addr)   ((uintptr_t)(addr) | 0x80000000)

// another way of converting virtual to physical
#define VIRTUAL_TO_PHYSICAL2(addr)  ((u8 *)(addr) - 0x80000000U)

#define ABSF(x) ((x) < 0.0f ? -(x) : (x))

#define HW_REG(reg, type) *(volatile type *)(uintptr_t)(reg | 0xa0000000)

#define ALIGN16(x) (((x) + 0xF) & -0x10)
#define ALIGN8(x) (((x) + 7) & -8)
#define ALIGN4(x) (((x) + 3) & -4)

// makes it easier to move all the prototypes and externs to the top of the file
#define IN_FILE

// for loops of macros, support up to 20 args to match anims max
#define FOR_EACH_1(op, val, ...) op(val)

#define FOR_EACH_2(op, val, ...) \
	op(val),	\
	FOR_EACH_1(op, __VA_ARGS__)

#define FOR_EACH_3(op, val, ...) \
	op(val),	\
	FOR_EACH_2(op, __VA_ARGS__)

#define FOR_EACH_4(op, val, ...) \
	op(val),	\
	FOR_EACH_3(op, __VA_ARGS__)

#define FOR_EACH_5(op, val, ...) \
	op(val),	\
	FOR_EACH_4(op, __VA_ARGS__)

#define FOR_EACH_6(op, val, ...) \
	op(val),	\
	FOR_EACH_5(op, __VA_ARGS__)

#define FOR_EACH_7(op, val, ...) \
	op(val),	\
	FOR_EACH_6(op, __VA_ARGS__)

#define FOR_EACH_8(op, val, ...) \
	op(val),	\
	FOR_EACH_7(op, __VA_ARGS__)

#define FOR_EACH_9(op, val, ...) \
	op(val),	\
	FOR_EACH_8(op, __VA_ARGS__)

#define FOR_EACH_10(op, val, ...) \
	op(val),	\
	FOR_EACH_9(op, __VA_ARGS__)

#define FOR_EACH_11(op, val, ...) \
	op(val),	\
	FOR_EACH_10(op, __VA_ARGS__)

#define FOR_EACH_12(op, val, ...) \
	op(val),	\
	FOR_EACH_11(op, __VA_ARGS__)

#define FOR_EACH_13(op, val, ...) \
	op(val),	\
	FOR_EACH_12(op, __VA_ARGS__)

#define FOR_EACH_14(op, val, ...) \
	op(val),	\
	FOR_EACH_13(op, __VA_ARGS__)

#define FOR_EACH_15(op, val, ...) \
	op(val),	\
	FOR_EACH_14(op, __VA_ARGS__)

#define FOR_EACH_16(op, val, ...) \
	op(val),	\
	FOR_EACH_15(op, __VA_ARGS__)

#define FOR_EACH_17(op, val, ...) \
	op(val),	\
	FOR_EACH_16(op, __VA_ARGS__)

#define FOR_EACH_18(op, val, ...) \
	op(val),	\
	FOR_EACH_17(op, __VA_ARGS__)

#define FOR_EACH_19(op, val, ...) \
	op(val),	\
	FOR_EACH_18(op, __VA_ARGS__)

#define FOR_EACH_20(op, val, ...) \
	op(val),	\
	FOR_EACH_19(op, __VA_ARGS__)
	
#define CONCATENATE(arg1, arg2)   CONCATENATE1(arg1, arg2)
#define CONCATENATE1(arg1, arg2)  CONCATENATE2(arg1, arg2)
#define CONCATENATE2(arg1, arg2)  arg1##arg2

#define FOR_EACH_NARG(...) FOR_EACH_NARG_(__VA_ARGS__, FOR_EACH_RSEQ_N())
#define FOR_EACH_NARG_(...) FOR_EACH_ARG_N(__VA_ARGS__)
#define FOR_EACH_ARG_N(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, N, ...) N
#define FOR_EACH_RSEQ_N() 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0

// ## before __VA_ARGS__ allows passing of zero args, super intuitive, thank you based gcc
#define FOR_EACH_(N, op, val, ...) CONCATENATE(FOR_EACH_, N)(op, val, __VA_ARGS__)
#define FOR_EACH(op, val, ...) FOR_EACH_(FOR_EACH_NARG(val, ##__VA_ARGS__), op, val, __VA_ARGS__)


#endif
