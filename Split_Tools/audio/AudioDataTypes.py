# -------------------------------------------------------------------------------
# Audio Structs
# -------------------------------------------------------------------------------
ALBankFile = {
    0: (">H", "Revision", 2),
    2: (">H", "BankCount", 2),
    4: (">L", "BankArray Offset", 4),
}

ALBankFileW = {
    0: ("u16", "Revision"),
    2: ("u16", "BankCount"),
    4: ("u32", "BankArray Offset", "ptr"),
}

ALBankFileComment = """
/**
# ALBankFile={
# 0:(">H","Revision",2),
# 2:(">H","BankCount",2),
# 4:(">L","BankArray Offset",4)
# }
**/
"""

ALBank = {
    0: (">H", "Instcount", 2),
    2: (">B", "Flags", 1),
    3: (">B", "Pad", 1),
    4: (">l", "Sample Rate", 4),
    8: (">l", "Percussion", 4),
    12: (">{}l", "Inst Array Offsets", 4, lambda x: x[0]),
}

ALBankW = {
    0: ("u16", "Instcount"),
    2: (">B", "Flags"),
    3: (">B", "Pad"),
    4: ("s32", "Sample Rate"),
    8: ("s32", "Percussion", "ptr"),
    12: ("s32[]", "Inst Array Offsets", "ptr arr"),
}

ALBankComment = """
/**
# ALBank={
# 0:(">H","Instcount",2),
# 2:(">B","Flags",1),
# 3:(">B","Pad",1),
# 4:(">l","Sample Rate",4),
# 8:(">l","Percussion",4)
# 12:(">{}l"%Instcount,"Inst Array Offsets",4*Instcount)
# }
**/
"""

ALInstrument = {
    0: (">B", "Volume", 1),
    1: (">B", "Pan", 1),
    2: (">B", "Priority", 1),
    3: (">B", "Flags", 1),
    4: (">B", "Trem Type", 1),
    5: (">B", "Trem Rate", 1),
    6: (">B", "Trem Depth", 1),
    7: (">B", "Trem Delay", 1),
    8: (">B", "Vib Type", 1),
    9: (">B", "Vib Rate", 1),
    10: (">B", "Vib Depth", 1),
    11: (">B", "Vib Delay", 1),
    12: (">h", "Bend Range", 2),
    14: (">h", "Sound Count", 2),
    16: (">{}l", "Sound Array Offsets", 4, lambda x: x[13]),
}

ALInstrumentW = {
    0: ("u8", "Volume"),
    1: ("u8", "Pan"),
    2: ("u8", "Priority"),
    3: ("u8", "Flags"),
    4: ("u8", "Trem Type"),
    5: ("u8", "Trem Rate"),
    6: ("u8", "Trem Depth"),
    7: ("u8", "Trem Delay"),
    8: ("u8", "Vib Type"),
    9: ("u8", "Vib Rate"),
    10: ("u8", "Vib Depth"),
    11: ("u8", "Vib Delay"),
    12: ("s16", "Bend Range"),
    14: ("s16", "Sound Count"),
    16: ("s32[]", "Sound Array Offsets", "ptr arr"),
}

ALInstrumentComment = """
/**
# ALInstrument={
# 0:(">B","Volume",1),
# 1:(">B","Pan",1),
# 2:(">B","Priority",1),
# 3:(">B","Flags",1),
# 4:(">B","Trem Type",1),
# 5:(">B","Trem Rate",1),
# 6:(">B","Trem Depth",1),
# 7:(">B","Trem Delay",1),
# 8:(">B","Vib Type",1),
# 9:(">B","Vib Rate",1),
# 10:(">B","Vib Depth",1),
# 11:(">B","Vib Delay",1),
# 12:(">h","Bend Range",2),
# 14:(">h","Sound_Count",2),
# 16:(">%dl"%Sound_Count,"Sound Array Offsets",4*Sound_Count),
# }
**/
"""

ALKeyMap = {
    0: (">B", "VelocityMin", 1),
    1: (">B", "VelocityMax", 1),
    2: (">B", "Key Min", 1),
    3: (">B", "Key Max", 1),
    4: (">B", "Key Base", 1),
    5: (">B", "Detune", 1),
}


ALKeyMapW = {
    0: ("u8", "VelocityMin"),
    1: ("u8", "VelocityMax"),
    2: ("u8", "Key Min"),
    3: ("u8", "Key Max"),
    4: ("u8", "Key Base"),
    5: ("u8", "Detune"),
}


AlKeyMapComment = """
/**
# ALKeyMap={
# 0:(">B","VelocityMin",1),
# 1:(">B","VelocityMax",1),
# 2:(">B","Key Min",1),
# 3:(">B","Key Max",1),
# 4:(">B","Key Base",1),
# 5:(">B","Detune",1),
# }
**/
"""

ALSound = {
    0: (">l", "Envelope", 4),
    4: (">l", "KeyMap", 4),
    8: (">l", "WaveTable", 4),
    12: (">B", "SamplePan", 1),
    13: (">B", "SampleVolume", 1),
    14: (">B", "Flags", 1),
}

ALSoundW = {
    0: ("s32", "Envelope", "ptr"),
    4: ("s32", "KeyMap", "ptr"),
    8: ("s32", "WaveTable", "ptr"),
    12: ("u8", "SamplePan"),
    13: ("u8", "SampleVolume"),
    14: ("u8", "Flags"),
}

AlSoundComment = """
/**
# ALSound={
# 0:(">l","Envelope",4),
# 4:(">l","KeyMap",4),
# 8:(">l","WaveTable",4),
# 12:(">B","SamplePan",1),
# 13:(">B","SampleVolume",1),
# 14:(">B","Flags",1),
# }
**/
"""

ALEnvelope = {
    0: (">l", "Attack Time", 4),
    4: (">l", "Decay Time", 4),
    8: (">l", "Release Time", 4),
    12: (">h", "Attack Volume", 2),
    14: (">h", "Decay Volume", 2),
}


ALEnvelopeW = {
    0: ("s32", "Attack Time"),
    4: ("s32", "Decay Time"),
    8: ("s32", "Release Time"),
    12: ("s16", "Attack Volume"),
    14: ("s16", "Decay Volume"),
}


ALEnvelopeComment = """
/**
# ALEnvelope={
# 0:(">l","Attack Time",4),
# 4:(">l","Decay Time",4),
# 8:(">l","Release Time",4),
# 12:(">h","Attack Volume",2),
# 14:(">h","Decay Volume",2),
# }
**/
"""

ALWaveTable = {
    0: (">l", "base", 4),
    4: (">l", "len", 4),
    8: (">B", "Type", 1),
    9: (">B", "Flag", 1),
    10: (">H", "Padding", 2),
    12: (">l", "ALADPCMloop Offset", 4),
    16: (">l", "ALADPCMBook/ALRawLoop Offset", 4),
}

ALWaveTableW = {
    0: ("s32", "base"),
    4: ("s32", "len"),
    8: ("u8", "Type"),
    9: ("u8", "Flag"),
    10: ("u16", "Padding"),
    12: ("s32", "ALADPCMloop Offset", "ptr"),
    16: ("s32", "ALADPCMBook Offset", "ptr"),
}

ALWaveTableComment = """
/**
# ALWaveTable={
# 0:(">l","base",4),
# 4:(">l","len",4),
# 8:(">B","Type",1),
# 9:(">B","Flag",1),
# 10:(">H","Padding",2),
# 12:(">l","ALADPCMloop Offset",4),
# 16:(">l","ALADPCMBook/ALRawLoop Offset",4),
# }
**/
"""

ALADPCMloop = {0: (">l", "start", 4), 4: (">l", "end", 4), 8: (">L", "count", 4)}

ALADPCMloopW = {0: ("s32", "start"), 4: ("s32", "end"), 8: ("s32", "count")}

ALADPCMloopComment = """
/**
# ALADPCMloop={
# 0:(">l","start",4),
# 4:(">l","end",4),
# 8:(">L","count",4)
# }
**/
"""

ALADPCMBook = {
    0: (">l", "order", 4),
    4: (">l", "npredictors", 4),
    8: (">{}h", "book", 2, lambda x: x[1]),
}

ALADPCMBookW = {
    0: ("s32", "order"),
    4: ("s32", "npredictors"),
    8: ("s16[]", "book", "arr"),
}

ALADPCMBookComment = """
/**
# ALADPCMBook={
# 0:(">l","order",4),
# 4:(">l","npredictors",4),
# 8:(">%dh"%npredictors,"book",2*npredictors)
# }
**/
"""


# sound args
Sound_Args_Header = {0: ("u32", "Num_Args"), 4: ("u32[]", "*Args[]", "ptr arr")}
