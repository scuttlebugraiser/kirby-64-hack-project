# imports
import os, sys, struct, shutil
from pathlib import Path


# if the path is from the same directory, these imports will work
# otherwise it will use an absolute import as if it was from the parent
try:
    from AudioDataTypes import *
except:
    from audio.AudioDataTypes import *

# to import utility.py, we need to add parent dir to path
file = Path(os.path.dirname(__file__))
sys.path.append(str(file.parent))
from Utility import *

# -------------------------------------------------------------------------------
# Audio
# -------------------------------------------------------------------------------

# I get all these file locations from the main file table

# Struct File_Layout{
# /*0x00*/	u32		*UnkRam;
# /*0x04*/	u32		*Unk; //unused
# /*0x08*/	u32		*Unk2;
# /*0x0C*/	u32		*Unk3;
# /*0x10*/	u32		*UnkLen;
# /*0x14*/	u32		*Sfx_BankFile; //0x3ff64
# /*0x18*/	u32		*Sfx_SampleData;
# /*0x1C*/	u32		*Sfx_SampleData2; //repeat?
# /*0x20*/	u32		*Instrument_BankFile;
# /*0x24*/	u32		*Instrument_SampleData;
# /*0x28*/	u32		*Instrument_SampleData2; //repeat
# /*0x2C*/	u32		*Midis;
# /*0x30*/	u32		*Unk4;
# /*0x34*/	u32[7]	*Pad; //not sure if its supposed to be part of same struct tbh
# /*0x50*/	u32		*SoundMapping[2]; //0x3ffA0, affects how sfx work, not sure on exact format
# /*0x58*/	u32		*SoundArgs1[2];
# /*0x60*/	u32		*SoundArgs2; //AssetData is the end ptr, but it is re used for asset start
# /*0x64*/	u32		*AssetData; //0x3FFB4
# };

# I reference the ptr directly instead of using the struct. e.g. If I
# want to get SoundArgs0, I will use 0x3FFA0 directly.

# -------------------------------------------------------------------------------
# Bank and Sample files
# -------------------------------------------------------------------------------

# Bank files are well defined in the audio and programming manauls.
# there is a lot of different types, but basically it is formed from
# a hierarchy of types. The bank file, indidividual banks, then insts
# then sound samples, then envelopes and keymaps and wavetables in the sample

# getting the entire bank file
def GetBankFile(Kirb: "binary ROM file", StartAddr: "ptr", bank_name: "str", Aud: Path):
    BnkFile = UPW(Kirb[StartAddr : StartAddr + 4])
    SampleFile = UPW(Kirb[StartAddr + 4 : StartAddr + 8])
    name = Aud / (f"{bank_name}/KCS_BankFile_{bank_name}")
    WriteBin(Kirb, BnkFile, SampleFile, str(name))
    with open(Path(name).with_suffix(".bin"), "rb") as f:
        block = BankFile(f.read())
        # write files based on what type of file I read, which is contained within the class variable
    with open(Path(name).with_suffix(".c"), "w", newline='') as f:
        block.Write(f)
    print(f"{name} wrote")


# class for extracting and writing the bank file
class BankFile(BinProcess, BinWrite):
    def __init__(self, bank_bin: "binary bank file"):
        self.file = bank_bin
        self.ptrs = dict()
        self.ExtractBank()

    # when updating a set, accept both an iterable or single val
    @staticmethod
    def update(set, val):
        if hasattr(val, "__iter__"):
            set.update(val)
        else:
            set.add(val)

    # if a ptr is passed, then auto add it to ptr dict
    def __setattr__(self, name, value, ptr=None):
        self.__dict__[name] = value
        if ptr:
            self.AddPtr(name, ptr)
        return getattr(self, name)

    # adds a symbol to a dict of ptrs, when writing via dict, if ptr arg is passed will search for name from void *ptr; value
    def AddPtr(self, name, ptr):
        self.ptrs[ptr] = name

    # quick access for extracting dict with ptr
    def ExDict(self, name: str, dict: dict, ptr: int):
        return self.__setattr__(name, self.extract_dict(ptr, dict), ptr)

    def ExtractBank(self):
        # Bank File Header
        self.ALBankFile = self.extract_dict(0, ALBankFile)
        # Instrument Banks
        self.ExDict("ALBank", ALBank, self.ALBankFile[2])
        self.Insts = self.ALBank[-1]  # list of ptrs
        if type(self.Insts) == int:
            self.Insts = [self.Insts]
        # Instruments
        self.Sounds = set()
        for q, inst in enumerate(self.Insts):
            Instrument = self.ExDict("Inst_%d" % q, ALInstrument, inst)
            self.update(self.Sounds, Instrument[-1])  # each inst has a list of sounds
        # each sound has these three structs associated with it
        self.KeyMaps = set()
        self.Envelopes = set()
        self.WaveTables = set()
        for q, s in enumerate(self.Sounds):
            Sound = self.ExDict("Sound_%d" % q, ALSound, s)
            self.update(self.KeyMaps, Sound[1])
            self.update(self.Envelopes, Sound[0])
            self.update(self.WaveTables, Sound[2])
        # now extract all the ptrs from each collection of values
        for q, s in enumerate(self.KeyMaps):
            self.ExDict("KeyMap_%d" % q, ALKeyMap, s)
        for q, s in enumerate(self.Envelopes):
            self.ExDict("Envelope_%d" % q, ALEnvelope, s)
        # Wave table for each sound
        # wave tables contain various child structs
        self.Samples = set()
        self.Loop = set()
        self.Book = set()
        for q, s in enumerate(self.WaveTables):
            Wave = self.ExDict("WaveTable_%d" % q, ALWaveTable, s)
            # some wave tables don't have loops or books, determined by the "type" flag
            if Wave[2]:
                print(Wave)  # this never executes, but could in some other game
                # I will leave it generic so this can be reused for other games
            else:
                self.update(self.Loop, Wave[-2])
                self.update(self.Book, Wave[-1])
            self.Samples.update([Wave[0], Wave[1]])
        # Loop information for each wave
        for q, s in enumerate(self.Loop):
            self.ExDict("ALADPCMloop_%d" % q, ALADPCMloop, s)
        # book information for each wave
        for q, s in enumerate(self.Book):
            self.ExDict("ALADPCMBook_%d" % q, ALADPCMBook, s)

    # extract sample binary files from the data extracted within the bank file
    def ExtractSamples(self, SampleFile: "ptr", Kirb: "binary ROM file", bank_name):
        # extract binaries of actual samples
        for s in self.Samples:
            start = SampleFile + s[0]
            end = SampleFile + s[0] + s[1]
            name = "audio/%s/KCS_Sample_0x%X" % (bank_name, start)
            WriteBin(Kirb, start, end, name)

    def Write(self, file: "filestream write"):
        # ALBankFile
        self.WriteDictStruct(
            self.ALBankFile,
            ALBankFileW,
            file,
            ALBankFileComment,
            "ALBankFile",
            symbols=self.ptrs,
        )
        # ALBank
        self.WriteDictStruct(
            self.ALBank, ALBankW, file, ALBankComment, "ALBank", symbols=self.ptrs
        )
        # instruments, ALInstrument
        for q, i in enumerate(self.Insts):
            inst = getattr(self, "Inst_%d" % q)
            self.WriteDictStruct(
                inst, ALInstrumentW, file, "", "Inst_%d" % q, symbols=self.ptrs
            )
        # sounds, ALSound
        for q, s in enumerate(self.Sounds):
            Sound = getattr(self, "Sound_%d" % q)
            self.WriteDictStruct(
                Sound, ALSoundW, file, "", "Sound_%d" % q, symbols=self.ptrs
            )
        # keymaps, ALKeyMap
        for q, s in enumerate(self.KeyMaps):
            KeyMap = getattr(self, "KeyMap_%d" % q)
            self.WriteDictStruct(
                KeyMap, ALKeyMapW, file, "", "KeyMap_%d" % q, symbols=self.ptrs
            )
        # envelopes, ALEnvelope
        for q, s in enumerate(self.Envelopes):
            Envelope = getattr(self, "Envelope_%d" % q)
            self.WriteDictStruct(
                Envelope, ALEnvelopeW, file, "", "Envelope_%d" % q, symbols=self.ptrs
            )
        # wave tables, ALWaveTable
        for q, s in enumerate(self.WaveTables):
            WaveTable = getattr(self, "WaveTable_%d" % q)
            self.WriteDictStruct(
                WaveTable, ALWaveTableW, file, "", "WaveTable_%d" % q, symbols=self.ptrs
            )
        # loop info, ALRawLoop or ALADPCMloop
        for q, s in enumerate(self.Loop):
            Loop = getattr(self, "ALADPCMloop_%d" % q)
            self.WriteDictStruct(
                Loop, ALADPCMloopW, file, "", "ALADPCMloop_%d" % q, symbols=self.ptrs
            )
        # book info, ALADPCMBook
        for q, s in enumerate(self.Book):
            Book = getattr(self, "ALADPCMBook_%d" % q)
            self.WriteDictStruct(
                Book, ALADPCMBookW, file, "", "ALADPCMBook_%d" % q, symbols=self.ptrs
            )


# -------------------------------------------------------------------------------
# Midi files
# -------------------------------------------------------------------------------

# Midi files follow the n64 standard of the compressed midi format.
# all structs and args can be found from the n64 audio manual

# Structs for identifying midis and how I split. This info can be found in the
# programming manual

# typedef struct {
# u16 version; /* Should be 0x5331 in kirby*/
# s16 seqCount;
# ALSeqData seqArray[];
# } ALCSeqHdr; //not a name from the manual

# typedef struct {
# u8 *offset;
# s32 seqLen;
# } ALSeqData;

# get ALCSeqHdr and then unpack all the ALSeqData's, then write the contents
# to binary files
def GetMidiBins(Kirb: "binary ROM file", StartAddr: "ptr", Aud: Path):
    ALCSeqHdr = UPW(Kirb[StartAddr + 0x2C : StartAddr + 0x30])
    SeqCount = struct.unpack(">H", Kirb[ALCSeqHdr + 2 : ALCSeqHdr + 4])[0]
    q = 0
    for i in range(SeqCount):
        [offset, seqLen] = GetMidi(i, Kirb, ALCSeqHdr + 4)
        # ALSeqData is relative to the seq bank file start aka ALCSeqHdr
        offset = offset + ALCSeqHdr
        name = Aud / (f"midi/{i:03d}_KCS_Midi")
        WriteBin(Kirb, offset, offset + seqLen, str(name))


# unpack one ALSeqData from ALCSeqHdr
def GetMidi(
    index: "int", Kirb: "binary ROM file", ALCSeqHdr: "ptr"
) -> ["start: int", "length: int"]:
    offset = UPW(Kirb[index * 8 + ALCSeqHdr : index * 8 + ALCSeqHdr + 4])
    seqLen = UPW(Kirb[index * 8 + ALCSeqHdr + 4 : index * 8 + ALCSeqHdr + 8])
    return [offset, seqLen]


# -------------------------------------------------------------------------------
# Sound Args
# -------------------------------------------------------------------------------

# Sound args are byte structs that detail how to play sfx inside the game
# It starts with a ptr from the main file table at 0x3FF50 at offset 0x50.

# The "header" is really a classic N64 sound table type of argument.
# It follows a similar format as the seq data table, but without the version

# typedef struct {
# u32 SfxArgCount;
# u8 *SfxArgs[];
# } SoundArgsHeader


# Table 1 and Table 2 refer to file starts from the main file system entry
# file table format is start:end
def GetSoundArgFiles(Kirb: "binary ROM file", StartAddr: "ptr", Aud: Path):
    StartAddr += 0x58  # SoundArgs1 first offset
    SoundArgs1 = UPW(Kirb[StartAddr : StartAddr + 4])
    SoundArgs2 = UPW(Kirb[StartAddr + 4 : StartAddr + 8])  # also SoundArgs1 end
    END = UPW(Kirb[StartAddr + 12 : StartAddr + 16])
    root = Aud / "sound_effects"
    ExtractSoundArg(Kirb, SoundArgs1, SoundArgs2, root / "Sound_Args_1")
    ExtractSoundArg(Kirb, SoundArgs2, END, root / "Sound_Args_2")


# extract one sound arg file given a start and end
def ExtractSoundArg(Kirb: "binary ROM file", Start: "ptr", End: "ptr", name: Path):
    WriteBin(Kirb, Start, End, str(name))
    with open(name.with_suffix(".bin"), "rb") as f:
        block = SoundArgs(f.read())
        # write files based on what type of file I read, which is contained within the class variable
    with open(Path(name).with_suffix(".c"), "w", newline='') as f:
        block.Write(f)
    print(f"{name} wrote")


class SoundArgs(BinProcess, BinWrite):
    def __init__(self, file: "binary sound args file"):
        self.file = file
        self.ExtractArgs()

    def ExtractArgs(self):
        num = self.upt(0, ">L", 4)
        offs = self.upt(4, f">{num}L", num * 4)
        end = len(self.file)
        # since it is packed bytes, idk the length, so I will use while loop
        x = 0
        self.args = dict()
        while True:
            if x + 1 == len(offs):
                self.args[offs[x]] = self.file[offs[x] : end]
                break
            self.args[offs[x]] = self.file[offs[x] : offs[x + 1]]
            x += 1
        self.Header = (num, offs)

    def Write(self, file):
        self.WriteDictStruct(
            self.Header, Sound_Args_Header, file, "", "Sound_Args_Header"
        )
        # write data, byte array for now
        for ptr, dat in self.args.items():
            self.write_arr(file, f"u8 Sound_Args_{ptr:X}", dat, self.format_arr)


# -------------------------------------------------------------------------------
# Main
# -------------------------------------------------------------------------------

# extract all the different audio data files in the game
def main(Kirb: "binary ROM file", path=None):
    # Paths
    if path:
        Aud = Path(path)
    else:
        Aud = Path("audio")
    Midi = Aud / "midi"
    Sound_Args = Aud / "sound_args"
    Instruments = Aud / "instruments"
    Sound_Effects = Aud / "sound_effects"
    Aud.mkdir(exist_ok=True)
    Sound_Args.mkdir(exist_ok=True)
    Instruments.mkdir(exist_ok=True)
    Sound_Effects.mkdir(exist_ok=True)
    try:
        shutil.rmtree(Midi)  # clear midis
    except:
        pass
    Midi.mkdir(exist_ok=True)
    # get GetSoundArgs
    GetSoundArgFiles(Kirb, 0x3FF50, Aud)
    # get midi bins
    GetMidiBins(Kirb, 0x3FF50, Aud)
    # get bank data
    GetBankFile(Kirb, 0x3FF70, "instruments", Aud)
    GetBankFile(Kirb, 0x3FF64, "sound_effects", Aud)


if __name__ == "__main__":
    Kirby = open(sys.argv[1], "rb")
    Kirb = Kirby.read()
    main(Kirb)
    print("Audio Exports finished")
