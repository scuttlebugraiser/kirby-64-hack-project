#include "types.h"
#ifndef BANKS_H
#define BANKS_H

extern struct BankHeader bank_0_filetable;
extern struct BankHeader bank_1_filetable;
extern struct BankHeader bank_2_filetable;
extern struct BankHeader bank_3_filetable;
extern struct BankHeader bank_4_filetable;
extern struct BankHeader bank_5_filetable;
extern struct BankHeader bank_6_filetable;
extern struct BankHeader bank_7_filetable;

#endif