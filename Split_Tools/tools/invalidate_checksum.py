import sys

def defeat_checksum(rom):
    rom.seek(0x63c)
    [rom.write(b'\x00') for x in range(4)]
    rom.seek(0x648)
    [rom.write(b'\x00') for x in range(4)]
    print("checksum defeated in build/kirby64.z64")

with open(sys.argv[1],'rb') as rom:
    dat = rom.read()

with open(sys.argv[1],'wb') as rom:
    rom.write(dat)
    defeat_checksum(rom)