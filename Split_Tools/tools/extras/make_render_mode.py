import gfx.f3d.F3DEX2_gbi as f3dex2
from bitstring import *
import re

RM = {
    # ones I didn't get from GBI
    # OPA / TEX_EDGE stuff
    # same as OPA_DECAL but has Z_UPD and CVG_DST_CLAMP instead of CVG_DST_WRAP, aka a TERR mode
    # terrain modes are for large polygons, fixes Z inaccuracies at the cost of not handling aliasing in "pinwheels"
    0x00442C78: "G_RM_AA_ZB_OPA_DECAL_TERR",  # AA_EN | Z_CMP | Z_UPD | IM_RD | CVG_DST_CLAMP | ALPHA_CVG_SEL | ZMODE_DEC | (CLR, CLR, MEM, MEM)
    0x00112C78: "G_RM_AA_ZB_OPA_DECAL_TERR2",  # AA_EN | Z_CMP | Z_UPD | IM_RD | CVG_DST_CLAMP | ALPHA_CVG_SEL | ZMODE_DEC | (CLR, CLR, MEM, MEM)
    # doesn't update Z, but otherwise the same, there is no fitting name for this distinction
    0x00442058: "G_RM_AA_ZB_OPA_SURF",  # AA_EN | Z_CMP | IM_RD | CVG_DST_CLAMP | ALPHA_CVG_SEL | ZMODE_OPA | (CLR, CLR, MEM, MEM)
    0x00112058: "G_RM_AA_ZB_OPA_SURF2",  # AA_EN | Z_CMP | IM_RD | CVG_DST_CLAMP | ALPHA_CVG_SEL | ZMODE_OPA | (CLR, CLR, MEM, MEM)
    0x00442018: "G_RM_RA_ZB_OPA_SURF",  # AA_EN | Z_CMP | CVG_DST_CLAMP | ALPHA_CVG_SEL | ZMODE_OPA | (CLR, CLR, MEM, MEM)
    0x00112018: "G_RM_RA_ZB_OPA_SURF2",  # AA_EN | Z_CMP | CVG_DST_CLAMP | ALPHA_CVG_SEL | ZMODE_OPA | (CLR, CLR, MEM, MEM)
    # I guess this is an option
    0x00443C58: "G_RM_AA_ZB_TEX_DECAL_TERR",  # AA_EN | Z_CMP | IM_RD | CVG_DST_CLAMP | CVG_X_ALPHA | ALPHA_CVG_SEL | ZMODE_DEC | (CLR, CLR, MEM, MEM)
    0x00113C58: "G_RM_AA_ZB_TEX_DECAL_TERR2",  # AA_EN | Z_CMP | IM_RD | CVG_DST_CLAMP | CVG_X_ALPHA | ALPHA_CVG_SEL | ZMODE_DEC | (CLR, CLR, MEM, MEM)
    # add Z_UPD, no idea what that makes this mode called then, seems redundant
    0x00443C78: "G_RM_AA_ZB_TEX_DECAL_TERR",  # AA_EN | Z_CMP | Z_UPD | IM_RD | CVG_DST_CLAMP | CVG_X_ALPHA | ALPHA_CVG_SEL | ZMODE_DEC | (CLR, CLR, MEM, MEM)
    0x00113C78: "G_RM_AA_ZB_TEX_DECAL_TERR2",  # AA_EN | Z_CMP | Z_UPD | IM_RD | CVG_DST_CLAMP | CVG_X_ALPHA | ALPHA_CVG_SEL | ZMODE_DEC | (CLR, CLR, MEM, MEM)
    # names fit
    0x00443038: "G_RM_RA_TEX_EDGE",  # AA_EN | IM_RD | Z_CMP | Z_UPD | CVG_DST_CLAMP | ZMODE_OPA | (CLR, CLR, MEM, MEM)
    0x00113038: "G_RM_RA_TEX_EDGE2",  # AA_EN | IM_RD | Z_CMP | Z_UPD | CVG_DST_CLAMP | ZMODE_OPA | (CLR, CLR, MEM, MEM)
    0x00442030: "G_RM_ZB_OPA_TERR",  # Z_CMP | Z_UPD | CVG_DST_CLAMP | ALPHA_CVG_SEL | ZMODE_OPA | (CLR, CLR, MEM, MEM)
    0x00112030: "G_RM_ZB_OPA_TERR2",  # Z_CMP | Z_UPD | CVG_DST_CLAMP | ALPHA_CVG_SEL | ZMODE_OPA | (CLR, CLR, MEM, MEM)
    # fitting name
    0x00443008: "G_RM_RA_TEX_EDGE",  # AA_EN | CVG_DST_CLAMP | CVG_X_ALPHA | ALPHA_CVG_SEL | ZMODE_OPA | (CLR, CLR, MEM, MEM)
    0x00113008: "G_RM_RA_TEX_EDGE2",  # AA_EN | CVG_DST_CLAMP | CVG_X_ALPHA | ALPHA_CVG_SEL | ZMODE_OPA | (CLR, CLR, MEM, MEM)
    # not sure what is up here, doesn't seem to fit the logic of any particular mode
    0x00442040: "G_RM_OPA_TERR",  # IM_RD | CVG_DST_CLAMP | CVG_X_ALPHA | ZMODE_OPA | (CLR, CLR, MEM, MEM)
    0x00112040: "G_RM_OPA_TERR2",  # IM_RD | CVG_DST_CLAMP | CVG_X_ALPHA | ZMODE_OPA | (CLR, CLR, MEM, MEM)
    # XLU STUFF
    # G_RM_AA_XLU_SURF but it has no IM_RD, so I'm not sure how it is supposed to blend
    0x004049C8: "G_RM_RA_XLU_SURF",  # AA_EN | IM_RD | CLR_ON_CVG | CVG_DST_WRAP | FORCE_BL | ZMODE_XLU | (CLR, CLR, MEM, 1-A)
    0x001049C8: "G_RM_RA_XLU_SURF2",  # AA_EN | IM_RD | CLR_ON_CVG | CVG_DST_WRAP | FORCE_BL | ZMODE_XLU | (CLR, CLR, MEM, 1-A)
    # with no AA, this mode is supposed to 'ZAP' which means CVG_DST_FULL, assuming pt filter, not sure what to name this with that in mind
    0x00404950: "G_RM_ZB_XLU_SURF",  # Z_CMP | IM_RD | CVG_DST_WRAP | ZMODE_XLU | FORCE_BL | (CLR, CLR, MEM, 1-A)
    0x00104950: "G_RM_ZB_XLU_SURF2",  # Z_CMP | IM_RD | CVG_DST_WRAP | ZMODE_XLU | FORCE_BL | (CLR, CLR, MEM, 1-A)
    0x00404D50: "G_RM_ZB_XLU_DECAL",  # Z_CMP | IM_RD | CVG_DST_WRAP | ALPHA_CVG_SEL | ZMODE_DEC | FORCE_BL | (CLR, CLR, MEM, 1-A)
    0x00104D50: "G_RM_ZB_XLU_DECAL2",  # Z_CMP | IM_RD | CVG_DST_WRAP | ALPHA_CVG_SEL | ZMODE_DEC | FORCE_BL | (CLR, CLR, MEM, 1-A)
    # a XLU decal terrain polygon with z buffering. not 100% on if this even makes sense
    0x00404C78: "G_RM_AA_ZB_XLU_DECAL_TERR",  # AA_EN | Z_CMP | Z_UPD | IM_RD | CVG_DST_CLAMP | ZMODE_DEC | FORCE_BL | (CLR, CLR, MEM, 1-A)
    0x00104C78: "G_RM_AA_ZB_XLU_DECAL_TERR2",  # AA_EN | Z_CMP | Z_UPD | IM_RD | CVG_DST_CLAMP | ZMODE_DEC | FORCE_BL | (CLR, CLR, MEM, 1-A)
    # I don't get how this is supposed to work or what this really looks like
    0x00404BD0: "G_RM_ZB_CLD_SURF_CLR",  # Z_CMP | IM_RD | CLR_ON_CVG | CVG_DST_SAVE | FORCE_BL | ZMODE_XLU | (CLR, CLR, MEM, 1-A)
    0x00104BD0: "G_RM_ZB_CLD_SURF_CLR2",  # Z_CMP | IM_RD | CLR_ON_CVG | CVG_DST_SAVE | FORCE_BL | ZMODE_XLU | (CLR, CLR, MEM, 1-A)
    # should be close to accurate name, same as OVL_SURF but with CLR_ON_CVG, not sure of significance
    0x00404FD0: "G_RM_ZB_OVL_DECAL",  # Z_CMP | IM_RD | CLR_ON_CVG | CVG_DST_SAVE | FORCE_BL | ZMODE_DEC | (CLR, CLR, MEM, 1-A)
    0x00104FD0: "G_RM_ZB_OVL_DECAL2",  # Z_CMP | IM_RD | CLR_ON_CVG | CVG_DST_SAVE | FORCE_BL | ZMODE_DEC | (CLR, CLR, MEM, 1-A)
    # isn't OVL and AA+ZB illegal? I don't know, but kirby seems to think it is ok
    0x00404F78: "G_RM_AA_ZB_OVL_SURF",  # AA_EN | Z_CMP | Z_UPD | IM_RD | CVG_DST_SAVE | ZMODE_DEC | FORCE_BL | (CLR, CLR, MEM, 1-A)
    0x00104F78: "G_RM_AA_ZB_OVL_SURF2",  # AA_EN | Z_CMP | Z_UPD | IM_RD | CVG_DST_SAVE | ZMODE_DEC | FORCE_BL | (CLR, CLR, MEM, 1-A)
    # Cloud, but it is not OPA, which is the normal way according to manual
    0x00404B40: "G_RM_CLD_SURF",  # IM_RD | CVG_DST_SAVE | ALPHA_CVG_SEL | ZMODE_XLU | FORCE_BL | (CLR, CLR, MEM, 1-A)
    0x00104B40: "G_RM_OVL_SURF2",  # IM_RD | CVG_DST_SAVE | ALPHA_CVG_SEL | ZMODE_XLU | FORCE_BL | (CLR, CLR, MEM, 1-A)
    # XLU even though z mode isn't XLU, same as G_RM_AA_XLU_SURF but with Z_CMP, not sure on naming
    0x004041D8: "G_RM_AA_ZB_XLU_SURF",  # AA_EN | Z_CMP | IM_RD | CVG_DST_WRAP | ZMODE_OPA | FORCE_BL | (CLR, CLR, MEM, 1-A)
    0x001041D8: "G_RM_AA_ZB_XLU_SURF2",  # AA_EN | Z_CMP | IM_RD | CVG_DST_WRAP | ZMODE_OPA | FORCE_BL | (CLR, CLR, MEM, 1-A)
    # several 2 cycle modes
    # I actually have no clue on these
    0x00104038: "G_RM_RA_ZB_OPA_SURF2",  # AA_EN | Z_CMP | Z_UPD | CVG_DST_CLAMP | ZMODE_OPA | FORCE_BL | (CLR, CLR, MEM, 1-A)
    0x00104008: "G_RM_RA_OPA_SURF2",  # AA_EN | CVG_DST_CLAMP | ZMODE_OPA | FORCE_BL | (CLR, CLR, MEM, 1-A)
    0x00104858: "G_RM_RA_ZB_XLU_SURF2",  # AA_EN | Z_CMP | IM_RD | CVG_DST_CLAMP | ZMODE_XLU | FORCE_BL | (CLR, CLR, MEM, 1-A)
    0x01024078: "G_RM_AA_ZB_OPA_FOG2",  # AA_EN | Z_CMP | Z_UPD | IM_RD | CVG_DST_CLAMP | ZMODE_OPA | FORCE_BL | (CLR, FOG, CLR, 1) ???
    # I don't believe there is a name for this, this is prob a 1 cycle mode only
    0x08802038: "G_RM_RA_ZB_OPA_BLEND_SURF",  # AA_EN | Z_CMP | Z_UPD | CVG_DST_CLAMP | ZMODE_OPA | ALPHA_CVG_SEL | FORCE_BL | (CLR, SHD, BLND, 1-A)
    0x08802078: "G_RM_AA_ZB_OPA_BLEND_SURF",  # AA_EN | Z_CMP | Z_UPD | IM_RD | CVG_DST_CLAMP | ZMODE_OPA | ALPHA_CVG_SEL | FORCE_BL | (CLR, SHD, BLND, 1-A)
    0x08004DD8: "G_RM_AA_ZB_DECAL_FOG_ALPHA",  # AA_EN | Z_CMP | IM_RD | CVG_DST_WRAP | ZMODE_DEC | FORCE_BL | (CLR, SHD, CLR, 1-A)
}

# for raw, rm_str in RM.items():
# cmd_upper = (0x00001C).to_bytes(3, byteorder="big")
# cmd_lower = raw.to_bytes(4, byteorder="big")
# raw = BitArray(cmd_upper+cmd_lower)
# res = f3dex2.G_SETOTHERMODE_L_Decode(raw)
# print(f"#define {rm_str}(clk)               \\\n\
# {res[1]}\n")
# print(f"#define {rm_str} {rm_str}()\n")


# blends = """
# define	AA_EN		0x8
# define	Z_CMP		0x10
# define	Z_UPD		0x20
# define	IM_RD		0x40
# define	CLR_ON_CVG	0x80
# define	CVG_DST_CLAMP	0
# define	CVG_DST_WRAP	0x100
# define	CVG_DST_FULL	0x200
# define	CVG_DST_SAVE	0x300
# define	ZMODE_OPA	0
# define	ZMODE_INTER	0x400
# define	ZMODE_XLU	0x800
# define	ZMODE_DEC	0xc00
# define	CVG_X_ALPHA	0x1000
# define	ALPHA_CVG_SEL	0x2000
# define	FORCE_BL	0x4000
# define	TEX_EDGE	0x0000
# """

# d = f3dex2.define_to_dictionary(blends)

# for k, v in d.items():
# print(f"{repr(k)}: {v}")

render_modes = """
#define RM_AA_ZB_OPA_DECAL_TERR G_RM_AA_ZB_OPA_DECAL_TERR(1)
#define RM_AA_ZB_OPA_DECAL_TERR2 G_RM_AA_ZB_OPA_DECAL_TERR(2)
#define RM_AA_ZB_TEX_DECAL_TERR G_RM_AA_ZB_TEX_DECAL_TERR(1)
#define RM_AA_ZB_TEX_DECAL_TERR2 G_RM_AA_ZB_TEX_DECAL_TERR2(2)
#define RM_ZB_OPA_TERR G_RM_ZB_OPA_TERR(1)
#define RM_ZB_OPA_TERR2 G_RM_ZB_OPA_TERR2(2)
#define RM_RA_TEX_EDGE G_RM_RA_TEX_EDGE(1)
#define RM_RA_TEX_EDGE2 G_RM_RA_TEX_EDGE2(2)
#define RM_OPA_TERR G_RM_OPA_TERR(1)
#define RM_OPA_TERR2 G_RM_OPA_TERR2(2)
#define RM_RA_XLU_SURF G_RM_RA_XLU_SURF(1)
#define RM_RA_XLU_SURF2 G_RM_RA_XLU_SURF2(2)
#define RM_AA_ZB_XLU_DECAL_TERR G_RM_AA_ZB_XLU_DECAL_TERR(1)
#define RM_AA_ZB_XLU_DECAL_TERR2 G_RM_AA_ZB_XLU_DECAL_TERR2(2)
#define RM_ZB_CLD_SURF_CLR G_RM_ZB_CLD_SURF_CLR(1)
#define RM_ZB_CLD_SURF_CLR2 G_RM_ZB_CLD_SURF_CLR2(2)
#define RM_ZB_OVL_DECAL G_RM_ZB_OVL_DECAL(1)
#define RM_ZB_OVL_DECAL2 G_RM_ZB_OVL_DECAL2(2)
#define RM_AA_ZB_OVL_SURF G_RM_AA_ZB_OVL_SURF(1)
#define RM_AA_ZB_OVL_SURF2 G_RM_AA_ZB_OVL_SURF2(2)
#define RM_OVL_SURF G_RM_OVL_SURF(1)
#define RM_OVL_SURF2 G_RM_OVL_SURF2(2)
#define RM_RA_ZB_XLU_SURF G_RM_RA_ZB_XLU_SURF(1)
#define RM_RA_ZB_XLU_SURF2 G_RM_RA_ZB_XLU_SURF2(2)
#define RM_AA_ZB_OPA_FOG2 G_RM_AA_ZB_OPA_FOG2(2)
#define RM_RA_ZB_OPA_BLEND_SURF G_RM_RA_ZB_OPA_BLEND_SURF(2)
#define RM_AA_ZB_OPA_BLEND_SURF G_RM_RA_ZB_OPA_BLEND_SURF(2)
#define RM_AA_ZB_DECAL_FOG_ALPHA G_RM_AA_ZB_DECAL_FOG_ALPHA2(2)
"""

for line in render_modes.split("\n"):
    if "#define" not in line:
        continue
    words = re.split("\s+", line)
    match = re.search("\(\d\)", words[2])
    name = words[2][: match.span()[0]]
    call = words[2][match.span()[0] :]
    print(f"#define {name}\t{words[1]}{call}")
