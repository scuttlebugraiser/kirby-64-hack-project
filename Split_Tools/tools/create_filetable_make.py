import sys, struct
from pathlib import Path
from dataclasses import dataclass, field

# this file will be called and will then get a list of all asset files
# it will then take those args and make filetable_<file_type>.mk

target = Path(sys.argv[1])

aggregate_target = r"""
GEO_BUILD := $(GEO_BANK_0_FILES)\
                $(GEO_BANK_1_FILES)\
                $(GEO_BANK_2_FILES)\
                $(GEO_BANK_3_FILES)\
                $(GEO_BANK_4_FILES)\
                $(GEO_BANK_5_FILES)\
                $(GEO_BANK_6_FILES)\
                $(GEO_BANK_7_FILES)

IMAGE_BUILD := $(IMAGE_BANK_0_FILES)\
                $(IMAGE_BANK_1_FILES)\
                $(IMAGE_BANK_2_FILES)\
                $(IMAGE_BANK_3_FILES)\
                $(IMAGE_BANK_4_FILES)\
                $(IMAGE_BANK_5_FILES)\
                $(IMAGE_BANK_6_FILES)\
                $(IMAGE_BANK_7_FILES)

ANIM_BUILD := $(ANIM_BANK_0_FILES)\
                $(ANIM_BANK_1_FILES)\
                $(ANIM_BANK_2_FILES)\
                $(ANIM_BANK_3_FILES)\
                $(ANIM_BANK_4_FILES)\
                $(ANIM_BANK_5_FILES)\
                $(ANIM_BANK_6_FILES)\
                $(ANIM_BANK_7_FILES)

MISC_BUILD := $(MISC_BANK_0_FILES)\
                $(MISC_BANK_1_FILES)\
                $(MISC_BANK_2_FILES)\
                $(MISC_BANK_3_FILES)\
                $(MISC_BANK_4_FILES)\
                $(MISC_BANK_5_FILES)\
                $(MISC_BANK_6_FILES)\
                $(MISC_BANK_7_FILES)
"""

@dataclass
class IndexFile():
    filename: str
    index: int

# this will hold each of the different files for each type as attrs
@dataclass
class FileContainer():
    bank: int
    files: list[IndexFile] = field(default_factory=list)
    
    def sort(self):
        self.files = sorted(self.files, key = lambda x: x.index)
    
    def write_filetable(self, output_file, file_type):
        output_file.write(f"{file_type}_BANK_{self.bank}_FILES := \\\n")
        [output_file.write(f"\tbuild/{file.filename} \\\n") for file in self.files]
        output_file.write("\n\n")


def init_file_variables(file_list: list[str]):
    containers = [FileContainer(b) for b in range(8)]
    for file in file_list:
        parts = file.parts
        bank = int(parts[2].removeprefix("bank_"))
        index = int(parts[3])
        file_bank = containers[bank].files.append(IndexFile(str(file.with_suffix(".o")), index))
    for file_bank in containers:
        file_bank.sort()
    return containers


def write_filetable(file_containers, file, file_type):
    [container.write_filetable(file, file_type) for container in file_containers]


def is_empty(generator):
    for item in generator:
        return False
    return True


def glob_geo_files():
    path = Path("assets")
    for geo in path.glob("geo/**/geo.c"):
        yield geo.with_suffix(".bin")


def glob_misc_files():
    path = Path("assets")
    yield from path.glob("misc/*[!bank_7]/*/misc.bin")
    for misc_path in  path.glob("misc/bank_7/*"):
        if not (misc_path / "level.c").exists():
            yield misc_path / "misc.bin"
        else:
            yield (misc_path / "level.c").with_suffix(".bin")


# choose image.bin if a png exists, else do block.bin
# exclude background images for now
def glob_image_files():
    path = Path("assets")
    for img_path in path.glob("image/*/*/"):
        if is_empty(img_path.glob("*[!background].png")):
            yield img_path / "block.bin"
        else:
            yield next(img_path.glob("*.png"))


def glob_anim_files():
    path = Path("assets")
    yield from path.glob("anim/**/anim.bin")


def main():
    with open(target, "w") as file:
        write_filetable(init_file_variables(glob_geo_files()), file, "GEO")
        write_filetable(init_file_variables(glob_image_files()), file, "IMAGE")
        write_filetable(init_file_variables(glob_anim_files()), file, "ANIM")
        write_filetable(init_file_variables(glob_misc_files()), file, "MISC")
        file.write(aggregate_target)


if __name__ == "__main__":
    # profiling
    # import cProfile
    # import pstats
    
    # with cProfile.Profile() as pr:
    main()
    
    # stats = pstats.Stats(pr)
    # stats.sort_stats(pstats.SortKey.TIME)
    # stats.print_stats()