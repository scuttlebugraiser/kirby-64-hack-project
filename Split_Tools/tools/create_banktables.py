# ------------------------------------------------------------------------
#    Header
# ------------------------------------------------------------------------
from __future__ import annotations

import sys, struct
from pathlib import Path
from dataclasses import dataclass, field

# this file will be called and given all the asset bin files as sys args
# it will then take those args and make bank%.ld, bank%.h, bank%.filetable.c

kirby_rom = sys.argv[1]
matching = sys.argv[2]

# ------------------------------------------------------------------------
#    Classes
# ------------------------------------------------------------------------

@dataclass
class BankIndex():
    bank: int
    index: int
    path: str = ""
    def __str__(self):
        return f"{self.bank}, {self.index}"


# this will hold each of the different files for each type as attrs
@dataclass
class BankLinker():
    bank: int
    models: list[BankIndex] = field(default_factory=list)
    textures: list[BankIndex] = field(default_factory=list)
    animations: list[BankIndex] = field(default_factory=list)
    misc: list[BankIndex] = field(default_factory=list)
    
    def sort(self):
        self.models = sorted(self.models, key = lambda x: x.index)
        self.textures = sorted(self.textures, key = lambda x: x.index)
        self.animations = sorted(self.animations, key = lambda x: x.index)
        self.misc = sorted(self.misc, key = lambda x: x.index)
    
    def size(self):
        return (len(self.models)+1)*8 + \
            (len(self.textures)+2)*4 + \
            (len(self.animations)+2)*4 + \
            (len(self.misc)+2)*4 + \
            0x20 # bank headers
    
    @property
    def images(self):
        for img in self.textures:
            if "image" in img.path:
                yield f"TEXTURE({img}, {img.path})"
            else:
                yield f"IMAGEBIN({img})"
    
    @property
    def levels(self):
        for misc in self.misc:
            if "level" in misc.path:
                yield f"LEVEL({misc}, {misc.path})"
            else:
                yield f"MISC({misc})"
    
    @property
    def geo_symbols(self):
        for model in self.models:
            yield f"bank_{model.bank}_index_{model.index}_geo_start"
            yield f"bank_{model.bank}_index_{model.index}_geo_end"
        
    @property
    def image_symbols(self):
        for image in self.textures:
            yield f"bank_{image.bank}_index_{image.index}_image_rel_pos"
        yield f"bank_{self.bank}_image_size"
        
    @property
    def anim_symbols(self):
        for anim in self.animations:
            yield f"bank_{anim.bank}_index_{anim.index}_anim_rel_pos"
        yield f"bank_{self.bank}_anim_size"
    
    @property
    def misc_symbols(self):
        for misc in self.misc:
            yield f"bank_{misc.bank}_index_{misc.index}_misc_rel_pos"
        yield f"bank_{self.bank}_misc_size"


# ------------------------------------------------------------------------
#    Constants
# ------------------------------------------------------------------------


BANK_HEADER_GUARD = \
"""#include "types.h"
#ifndef BANK_{0}_H
#define BANK_{0}_H

"""


BANK_FILETABLE_INCLUDES = \
"""#include "types.h"
#include "segments.h"
#include "bank{0}.h"

"""


BANK_FILETABLE_STRUCT = \
"""
struct BankHeader bank_{0}_filetable = {{
    /* geo table RAM   0x0  */ &bank_{0}_geo_table,
    /* geo table ROM   0x4  */ -1,
    /* image table RAM 0x8  */ &bank_{0}_image_table,
    /* image table ROM 0xC  */ &bank_{0}_index_1_image_start,
    /* anim table ROM  0x10 */ &bank_{0}_anim_table,
    /* anim table RAM  0x14 */ &bank_{0}_index_1_anim_start,
    /* misc table RAM  0x18 */ &bank_{0}_misc_table,
    /* misc table ROM  0x1C */ &bank_{0}_index_1_misc_start,
}};
"""


FILE_MAPPING = {
    "geo": lambda x, y: x.models.append(y),
    "misc": lambda x, y: x.misc.append(y),
    "image": lambda x, y: x.textures.append(y),
    "anim": lambda x, y: x.animations.append(y),
}


# ------------------------------------------------------------------------
#    Main Functions
# ------------------------------------------------------------------------


def create_bank_header(filename: str, linker: BankLinker):
    filepath = Path("assets") / filename
    bank_num = linker.bank
    with open(filepath, 'w') as file:
        file.write(BANK_HEADER_GUARD.format(bank_num))
        # write externs
        [file.write(f"extern u32 {sym}[];\n") for sym in linker.geo_symbols]
        [file.write(f"extern u32 {sym}[];\n") for sym in linker.image_symbols]
        [file.write(f"extern u32 {sym}[];\n") for sym in linker.anim_symbols]
        [file.write(f"extern u32 {sym}[];\n") for sym in linker.misc_symbols]
        file.write(f"extern struct BankHeader bank_{linker.bank}_filetable;\n")
        # symbols not in the filetables, but used to identify starts of the tables
        file.write(f"extern u32 bank_{bank_num}_index_1_image_start[];\n")
        file.write(f"extern u32 bank_{bank_num}_index_1_anim_start[];\n")
        file.write(f"extern u32 bank_{bank_num}_index_1_misc_start[];\n")
        file.write("#endif")


def create_bank_linker(filename: str, linker: BankLinker):
    filepath = Path("assets") / filename
    with open(filepath, 'w') as file:
        file.write('#include "bank_header.ld.in"\n\n')
        bank = linker.bank
        file.write(f"GEO_INIT({bank})\n")
        [file.write(f"MODEL({model})\n") for model in linker.models]
        file.write(f"IMAGES_INIT({bank})\n")
        [file.write(f"{tex}\n") for tex in linker.images]
        file.write(f"FILLER(image, {bank})\n")
        file.write(f"ANIMS_INIT({bank})\n")
        [file.write(f"ANIMATION({anim})\n") for anim in linker.animations]
        file.write(f"FILLER(anim, {bank})\n")
        file.write(f"MISC_INIT({bank})\n")
        [file.write(f"{level}\n") for level in linker.levels]
        file.write(f"FILLER(misc, {bank})\n")


def create_bank_filetable(filename: str, linker: BankLinker):
    filepath = Path("assets") / filename
    bank_num = linker.bank
    with open(filepath, 'w') as file:
        file.write(BANK_FILETABLE_INCLUDES.format(bank_num))
        # geos
        file.write(f"u32 *bank_{bank_num}_geo_table[] = {{\n\tNULL,\n\tNULL,\n")
        [file.write(f"\t{sym},\n") for sym in linker.geo_symbols]
        file.write("};\n\n")
        # images
        file.write(f"u32 *bank_{bank_num}_image_table[] = {{\n\tNULL,\n")
        [file.write(f"\t{sym},\n") for sym in linker.image_symbols]
        file.write("};\n\n")
        # anims
        file.write(f"u32 *bank_{bank_num}_anim_table[] = {{\n\tNULL,\n")
        [file.write(f"\t{sym},\n") for sym in linker.anim_symbols]
        file.write("};\n\n")
        # images
        file.write(f"u32 *bank_{bank_num}_misc_table[] = {{\n\tNULL,\n")
        [file.write(f"\t{sym},\n") for sym in linker.misc_symbols]
        file.write("};\n\n")
        # filetable struct
        file.write(BANK_FILETABLE_STRUCT.format(bank_num))


def is_empty(generator):
    for item in generator:
        return False
    return True


def glob_files():
    path = Path("build/assets")
    yield from path.glob("geo/**/geo.o")
    yield from path.glob("misc/**/misc.o")
    yield from path.glob("misc/**/level.o")
    yield from path.glob("anim/**/anim.o")
    for img_path in path.glob("image/*/*/"):
            yield next(img_path.glob("*.o"))


def init_bank_linkers(file_list: list[str]):
    linkers = [BankLinker(b) for b in range(8)]
    for file in file_list:
        file = Path(file)
        parts = file.parts
        bank = int(parts[3].removeprefix("bank_"))
        index =  int(parts[4])
        bank_id = BankIndex(bank, index)
        linker = linkers[bank]
        # for symbols that depend on the filetype
        bank_id.path = str(file)
        FILE_MAPPING.get(parts[2])(linker, bank_id)
    for linker in linkers:
        linker.sort()
    return linkers


# in a non matching ROM, we need to expand the filetable, while keeping everything else the
# same location, so we will strategically alter the size of our extracted binaries
#in a matching file, everything will be a certain size
def extract_binaries(linkers: BankLinker, kirby_rom, matching: bool):
    with open(kirby_rom, 'rb') as rom_file:
        bin_path = Path("bin")
        bin_path.mkdir(exist_ok=True)
        binary_data = rom_file.read()
        # binary 0x7E8D0 to 0x4AA8F0, aka assets start
        f = open(bin_path / ("until_assets.bin"), "wb")
        f.write(binary_data[0x7E8D0:0x4AA8F0])
        f.close()
        # binary 0x7D8B0 to 0x7e050, aka after filetables, but before dev stage strings
        f = open(bin_path / ("0x7D8B0_to_0x7E050.bin"), "wb")
        f.write(binary_data[0x7D8B0:0x7e050])
        f.close()
        # binary 0 to 0x6C8F0, aka until start of filetable
        if matching:
            filetable_start = 0x6C8F0
        else:
            size_delta = sum(linker.size() for linker in linkers) - 0xBA24
            if size_delta > 0:
                filetable_start = 0x6C8F0 - size_delta
        f = open(bin_path / ("until_0x6C8F0.bin"), "wb")
        f.write(binary_data[0:filetable_start])
        f.close()
        


def main():
    linkers = init_bank_linkers(glob_files())
    for linker in linkers:
        create_bank_linker(f"bank{linker.bank}.ld", linker)
        create_bank_header(f"bank{linker.bank}.h", linker)
        create_bank_filetable(f"bank{linker.bank}.filetable.c", linker)
    extract_binaries(linkers, kirby_rom, matching)


if __name__ == "__main__":
    # profiling
    # import cProfile
    # import pstats
    
    # with cProfile.Profile() as pr:
    main()
    
    # stats = pstats.Stats(pr)
    # stats.sort_stats(pstats.SortKey.TIME)
    # stats.print_stats()