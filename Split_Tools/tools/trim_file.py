import sys, struct
from pathlib import Path

file = open(Path(sys.argv[1]), "rb")
file = file.read()

# look for arr terminator
end = len(file)
while True:
    dat = struct.unpack(">L", file[end - 4 : end])[0]
    if dat != 0:
        break
    end -= 4
    if end <= 0:
        break

if not end:
    sys.exit()

with open(Path(sys.argv[1]), "wb") as write_file:
    write_file.write(file[:end])
