# Makefile to rebuild specific Kirby 64 files
# modified to only make assets

VERSION = us

GRUCODE := F3DEX2_2.04H

# creates error on binary diff
MATCHING := 0

ifeq ($(shell type mips-linux-gnu-ld >/dev/null 2>/dev/null; echo $$?), 0)
  CROSS := mips-linux-gnu-
else ifeq ($(shell type mips64-linux-gnu-ld >/dev/null 2>/dev/null; echo $$?), 0)
  CROSS := mips64-linux-gnu-
else
  CROSS := mips64-elf-
endif


BUILD_DIR := build
TARGET := kirby64
LD_SCRIPT := $(TARGET).us.ld

# compilers
GCC := $(CROSS)gcc
CPP := $(CROSS)cpp -P -Wno-trigraphs
CC := tools/ido-7.1_recomp_decomp_pals_v.5/cc
IRIX_ROOT := tools/ido7.1
QEMU_IRIX := $(shell which qemu-irix 2>/dev/null)
QEMU_CC := $(QEMU_IRIX) -silent -L $(IRIX_ROOT) $(IRIX_ROOT)/usr/bin/cc


# FLAGS
OPT_FLAGS := -O2
MIPSISET := -mips2 -32
INCLUDE_CFLAGS := -I include -I . -I include/libc -I $(BUILD_DIR) -I $(BUILD_DIR)/assets
TARGET_CFLAGS := -nostdinc  -DTARGET_N64 -DF3DEX_GBI_2
CFLAGS = -Wab,-r4300_mul -non_shared -Xcpluscomm -Xfullwarn -signed $(OPT_FLAGS) $(TARGET_CFLAGS) $(INCLUDE_CFLAGS) $(MIPSISET)

LDFLAGS = --no-check-sections -mips3 --accept-unknown-input-arch -T $(BUILD_DIR)/$(LD_SCRIPT) -Map $(BUILD_DIR)/$(TARGET).map

PRELIM_OBJCOPY_FLAGS = --pad-to=0x101000 --gap-fill=0x00
OBJCOPY_FLAGS = --pad-to=0x2000000 --gap-fill=0xFF

CC_CHECK := gcc -fsyntax-only -fsigned-char -m32 $(CC_CFLAGS) $(TARGET_CFLAGS) $(INCLUDE_CFLAGS) -std=gnu90 -Wall -Wextra -Wno-format-security -Wno-main -DNON_MATCHING -DAVOID_UB $(VERSION_CFLAGS) $(GRUCODE_CFLAGS)
GCCFLAGS = -Wall $(TARGET_CFLAGS) $(INCLUDE_CFLAGS) -march=vr4300 -mtune=vr4300 -mfix4300 -mabi=32 -mno-shared -G 0 -fno-PIC -mno-abicalls -fno-zero-initialized-in-bss -fno-toplevel-reorder -Wno-missing-braces -Wno-int-conversion -Wno-unused-variable

# tools
TOOLS_DIR = tools
N64CRC = tools/n64crc
N64GRAPHICS = $(TOOLS_DIR)/n64graphics

default: all

# build list of files to make
assets/targets.mk:
	python3 tools/create_filetable_make.py $@

-include assets/targets.mk


ASSET_FILES := $(GEO_BUILD) $(ANIM_BUILD) $(IMAGE_BUILD) $(LEVEL_BUILD) $(MISC_BUILD)

# use this block to check if a file is matching


# if [ $(MATCHING) -ne 0 ]; then \
	# diff $@ $(@D)/block.bin;\
# fi


# make geo bins
assets/geo/%/geo.bin: assets/geo/%/geo.c
	$(GCC) -c $(GCCFLAGS) -r -o $(@D)/geo.elf $<
	@$(CROSS)ld -Tdata=0x04000000 -o $(@D)/geo.o $(@D)/geo.elf
	@$(CROSS)objcopy -j .data -O binary $(@D)/geo.o $@
	@echo "$@ built successfully"

# make anim bins, not matching yet, commented out to build
# assets/anim/%/anim.bin: assets/anim/%/anim.c
	# $(GCC) $(GCCFLAGS) -E $< -o $(@D)/anim_test.c
	# $(GCC) -c $(GCCFLAGS) -r -o $(@D)/anim.elf $<
	# @$(CROSS)ld -Tdata=0 -o $(@D)/anim.o $(@D)/anim.elf
	# @$(CROSS)objcopy -j .data -O binary $(@D)/anim.o $@
	# @python3 tools/trim_file.py $@
	# @echo "$@ built successfully"


# make level bins
assets/misc/%/level.bin: assets/misc/%/level.c
	@$(GCC) -c $(GCCFLAGS) -o $(@D)/level.elf $<
	@$(CROSS)ld -Tdata=0 -r -o $(@D)/level.o $(@D)/level.elf
	@$(CROSS)objcopy -j .data -O binary $(@D)/level.o $@
	@python3 tools/trim_file.py $@
	@echo "$@ built successfully"


# make backgrounds into sprite header and image, broken for now
# assets/image/%.background.o: assets/image/%.background.png
	# mkdir -p $(@D)
	# tools/rgb2c -B $(shell ../tools/img_getprop $<) -o RAW $< > $(@:.o=.bin)
	# $(CROSS)ld -r -b binary -o $@ $(@:.o=.bin)

# make png images into binary files
assets/image/%.bin: assets/image/%.png
	@mkdir -p $(@D)
	@tools/rgb2c -G $(lastword $(subst ., ,$*)) -o RAW $< > $@
	@$(CROSS)ld -r -b binary -o $(@:.bin=.o) $@
	@echo "$@ image converted successfully"

# make sure interim image files are not deleted, this should lower build times when editing assets
# doesn't seem to work :/ sadg3
.PRECIOUS: build/assets/image/%.bin

# copy all asset bins to build folder
$(BUILD_DIR)/assets/%.o: assets/%.bin
	@mkdir -p $(@D)
	@$(CROSS)ld -r -b binary -o $@ $<
	@echo "$@ copied successfully"


# construct a bank.filetable, and bank file bin
$(BUILD_DIR)/assets/assets.marker: $(ASSET_FILES)
	@python3 tools/create_banktables.py Kirby64.z64 $(MATCHING)
	@touch $@


# code files
$(BUILD_DIR)/data/%.o: data/%.c
	@mkdir -p $(@D)
	@$(CC_CHECK) -MMD -MP -MT $@ -MF $(@D)/$*.d $<
	@$(GCC) -c $(GCCFLAGS) -mno-local-sdata -o $@ $<

CODE_FILES := $(BUILD_DIR)/data/kirby_misc_data.o


# binary files, copy them over as .o files
$(BUILD_DIR)/bin/%.o: bin/%.bin
	@mkdir -p $(@D)
	@$(CROSS)ld -r -b binary -o $@ $<

BINARY_FILES := $(BUILD_DIR)/bin/until_0x6C8F0.o $(BUILD_DIR)/bin/0x7D8B0_to_0x7E050.o $(BUILD_DIR)/bin/until_assets.o 

# main targets
$(BUILD_DIR)/$(LD_SCRIPT): $(LD_SCRIPT) $(BUILD_DIR)/assets/assets.marker
	$(CPP) $(VERSION_CFLAGS) $(INCLUDE_CFLAGS) -MMD -MP -MT $@ -MF $@.d -o $@ $< \
	-DBUILD_DIR=$(BUILD_DIR) -Umips

$(BUILD_DIR)/$(TARGET).elf: $(BUILD_DIR)/$(LD_SCRIPT) $(ASSET_FILES) $(CODE_FILES) $(BINARY_FILES)
	$(CROSS)ld -L $(BUILD_DIR) $(LDFLAGS) -o $@

# final z64 updates checksum
$(BUILD_DIR)/$(TARGET).z64: $(BUILD_DIR)/$(TARGET).elf
	$(CROSS)objcopy $< $(BUILD_DIR)/$(@F).bin -O binary $(PRELIM_OBJCOPY_FLAGS)
	$(CROSS)ld -r -b binary -o $(BUILD_DIR)/$(@F).elf.1 $(BUILD_DIR)/$(@F).bin
	$(CROSS)objcopy $(BUILD_DIR)/$(@F).elf.1 $@ -O binary $(OBJCOPY_FLAGS)
	@if [ $(MATCHING) -ne 0 ]; then \
		$(N64CRC) $@;\
	else \
		python3 tools/invalidate_checksum.py $@;\
	fi


all: $(BUILD_DIR)/$(TARGET).z64

clean:
	rm -r $(BUILD_DIR)/

extract:
	python3 Asset_Extractor.py

bank_files: $(BUILD_DIR)/assets/assets.marker

# test rules
test_misc: assets/misc/bank_7/59/level.bin
test_geo: assets/geo/bank_7/1/geo.bin
test_image: assets/image/bank_7/452/image.i4.bin
test_anim: assets/anim/bank_0/1/anim.bin
test_images: $(IMAGE_BUILD)

print: ; $(info files are: $(IMAGE_BUILD) ) @true