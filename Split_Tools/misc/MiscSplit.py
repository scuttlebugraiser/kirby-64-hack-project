# Splits up level settings block, and collision file types of misc banks
# the basis classes here are mostly the same ones used in the kirby decomp plugin

# imports
import os, sys, struct, shutil, math
from collections import namedtuple
from dataclasses import dataclass
from pathlib import Path

# if the path is from the same directory, these imports will work
# otherwise it will use an absolute import as if it was from the parent
try:
    from LevelDataTypes import *
    import ParticleDat as PD
except:
    from misc.LevelDataTypes import *
    import misc.ParticleDat as PD

# to import utility.py, we need to add parent dir to path
file = Path(os.path.dirname(__file__))
sys.path.append(str(file.parent))
from Utility import *

# -------------------------------------------------------------------------------
# Generic Processing Funcs
# -------------------------------------------------------------------------------


def denorm(a):
    return (abs(a) < 0.000000001 and abs(a) > 0) or math.isnan(a)


# -------------------------------------------------------------------------------
# Misc Blocks
# -------------------------------------------------------------------------------

# for selecting proper misc type of imp bin
class misc_bin(BinProcess, BinWrite):
    _vert_sig = (0x270F, 0x270F, 0x270F)
    # create the proper subclass given the signature of the file
    # to skip this, call __new__ on the subclass directly
    def __new__(self, file):
        self.file = file
        self.symbols = (
            dict()
        )  # symbol names for ptrs to addresses so I can write them to file with the correct name
        sig_obj = self.upt(self, 4, ">3H", 6)
        if sig_obj == self._vert_sig:
            feature = "obj"
        else:
            sig_obj = self.upt(self, 16, ">3H", 6)
            if sig_obj == self._vert_sig:
                feature = "level"
            # the first 5 members of floats are always floats for particle dats
            # because this is __new__ self is not implicitly pass to class methods
            # I have to more directly use methods isntead because of this, so I will
            # fine tune which parts of the file I will extract
            else:
                Header = self.upt(self, 0, ">2L", 8)
                try:
                    floats = self.upt(self, Header[1] + 12, ">12f", 48)
                    q = [denorm(a) for a in floats[:5]]
                    if any(q):
                        feature = "particle_tex"
                    else:
                        feature = "particle"
                except:
                    print("error reading header")
                    feature = "particle_tex"
        subclass_map = {
            subclass.feature: subclass for subclass in self.__subclasses__()
        }
        subclass = subclass_map[feature]
        instance = super(misc_bin, subclass).__new__(subclass)
        return instance

    def write_pad(self, file, name):
        padding = self.pads.get(name)
        self.add_header_data(f"u16 pad_{name}", static = True)
        if padding:
            file.write(f"static u16 pad_{name} = 0x{padding:X};\n\n")

    # check for 4 byte alignment padding and add it if it exists
    def check_padding(self, start, end, length, size, name, up_type=(">H", 2)):
        delta = (end - start) - (length * size)
        if delta > up_type[1]:
            dat = [
                self.upt(start + (length * size) + (x * size), *up_type)
                for x in range(0, delta, size)
            ]
            self.pads[name] = dat
            return dat
        elif delta == up_type[1]:
            dat = self.upt(start + (length * size), *up_type)
            self.pads[name] = dat
            return dat

    # if its a tuple return second index, otherwise return val
    # for pointers which take a tuple of (cast, name) or just name
    def get_ptr_name(self, name):
        if type(name) == tuple:
            return name[1]
        return name

    # if a ptr is passed, then auto add it to ptr dict
    def __setattr__(self, name, value, ptr=None):
        self.__dict__[self.get_ptr_name(name)] = value
        if ptr:
            self.AddPtr(name, ptr)

    # adds a symbol to a dict of ptrs, when writing via dict, if ptr arg is passed will search for name from void *ptr; value
    def AddPtr(self, name, ptr):
        self.symbols[ptr] = name

    def decode_from_ptr(self, ptr, format, len, name, num):
        self.__setattr__(
            name, [self.upt(a * len + ptr, format, len) for a in range(num)], ptr
        )

    # used for tri cells
    def WriteArrDelim(self, file, values, name, delim, floats=None):
        self.WriteLocComment(file, values)
        file.write(f"{name} = {{\n")
        l = []
        for v in values:
            if floats:
                l.append(f"0x{v:f}")
            else:
                l.append(f"0x{v:X}")
            if delim(v):
                file.write(f"\t{{{', '.join(l)}}},\n")
                l = []
        if l:
            file.write(f"\t{{{', '.join(l)}}},\n")
        file.write("};\n\n")

    # used for triangles only
    def WriteStructArr(self, file, values, structure, name, static=False):
        self.WriteLocComment(file, values)
        self.add_header_data(f"struct {name}[{len(values)}]", static = True)
        file.write(f"{'static '* bool(static)}struct {name}[{len(values)}] = {{\n")
        for v in values:
            w = []
            for off, name, v in zip(structure.keys(), structure.values(), v):
                try:
                    arr = name[2]
                except:
                    arr = 0
                if arr:
                    w.append(f"{{{', '.join([hex(a) for a in v])}}}")
                else:
                    w.append(f"{v}")
            file.write(f"\t{{{', '.join(w)}}},\n")
        file.write("};\n\n")


# for node data, not used here at all really
class node_cls(misc_bin):
    feature = "node"

    def __init__(self, num, node_header):
        self.header = []
        self.index = num
        self.node_header = node_header
        self.decode_node()

    # add the node num to the name of the var in the ptr dict
    def __setattr__(self, name, value, ptr=None):
        self.__dict__[name] = value
        if ptr:
            self.AddPtr(f"{name}_{self.index}", ptr)

    def decode_node(self):
        PathHeader = self.node_header[1] + (self.index * 16)
        self.__setattr__("PathHeader", self.upt(PathHeader, ">3L2H", 16), PathHeader)

        kirb = namedtuple(
            "kirb_node",
            "node entrance_act warp pad1 shade pad2 WarpFlag opt1 opt2 opt3 opt4 pad3",
        )
        cam = namedtuple(
            "cam_node",
            """Profile pad LockX LockY LockZ pad PanHiLo PanLo PanYaw pad pad FocusX FocusY FocusZ
		NearClip FarClip CamR CamYaw CamRadius FOV CamPhi
		CamXLock CamYLock CamZLock CamYawLock CamPitchLock """,
            rename=True,
        )
        # do kirby without dict unpacking but manually since it is the only one that really needs it in this class
        k_up = lambda x, y, z: self.upt(self.PathHeader[0] + z, x, y)
        self.__setattr__(
            "KirbyNode",
            kirb._make(
                (
                    *k_up(">2H", 4, 0),
                    k_up(">4B", 4, 4),
                    k_up(">B", 1, 8),
                    k_up(">3B", 3, 9),
                    *k_up(">4H", 8, 12),
                    *k_up(">3f", 12, 0x14),
                )
            ),
            self.PathHeader[0],
        )
        cam_node = self.upt(self.PathHeader[0] + 0x20, ">10BH25f", 0x70)
        # fix up the array so that it properly groups the bounds together
        # is this even faster than list comp? it looks prettier though, and that is what matters
        self.__setattr__(
            "CamNode",
            cam._make((*cam_node[:16], *zip(cam_node[16::2], cam_node[17::2]))),
            self.PathHeader[0] + 0x20,
        )

        self.__setattr__(
            "PathFooter",
            self.upt(self.PathHeader[1], ">2HfLf2L", 0x18),
            self.PathHeader[1],
        )
        self.__setattr__(
            "PathMatrix",
            [
                self.upt(self.PathFooter[3] + 12 * i, ">3f", 12)
                for i in range(self.PathFooter[1] + (self.PathFooter[0] >> 8))
            ],
            self.PathFooter[3],
        )
        self.Num_Pts = self.PathFooter[1]
        self.__setattr__(
            "PathBounds",
            self.upt(self.PathFooter[5], ">%df" % self.Num_Pts, 4 * self.Num_Pts),
            self.PathFooter[5],
        )
        # curl is a currently unknown data section that is only sometimes used
        # it likely has to do with paths that are constantly turning
        if self.PathFooter[6]:
            self.__setattr__(
                "PathCurl",
                [
                    self.upt(self.PathFooter[6] + 20 * i, ">5f", 20)
                    for i in range(self.Num_Pts - 1)
                ],
                self.PathFooter[6],
            )
        self.num_connections = self.PathHeader[3]
        conns = max(1, self.num_connections)
        self.__setattr__(
            "NodeConnector",
            [self.upt(self.PathHeader[2] + 4 * i, ">4B", 0x4) for i in range(conns)],
            self.PathHeader[2],
        )
        self.is_loop = self.PathHeader[4]


# for i/o of level bin data
class level_bin(misc_bin):
    feature = "level"

    def __init__(self, file):
        self.header = []
        self.child_symbols = []
        self.pads = dict()
        self.main_header = self.upt(0, ">3L", 12)
        # get collision, I do this weird method because I only want to call the new on collision bin
        # not do the full inheritance, the result is I also have to manually call __init__
        self.collision = super(misc_bin, collision_bin).__new__(collision_bin)
        self.collision.__init__(file)
        # make the collision header the same object as this header, for ordering
        self.collision.header = self.header
        # get node data
        super().__setattr__(
            "NodeHdr", self.upt(self.main_header[1], ">4L", 16), self.main_header[1]
        )
        self.path_nodes = {}
        for i in range(self.NodeHdr[0]):
            node = super(misc_bin, node_cls).__new__(node_cls)
            node.__init__(i, self.NodeHdr)
            self.path_nodes[i] = node
        # node traversal and dists relys on all nodes
        self.decode_node_relations()
        # get entities
        self.entities = []
        x = 0
        start = self.main_header[2]
        entity = namedtuple(
            "entity", "node bank id action res_flag spawn_flag eep pos rot scale"
        )
        vec3f = namedtuple("vec3", "x y z")
        if start:
            self.AddPtr("Entity_0", start)
            while True:
                sig = self.upt(start + x * 0x2C, ">L", 4)
                if sig == 0x99999999:
                    break
                ent = self.upt(start + x * 0x2C, ">6BH9f", 0x2C)
                pos, rot, scale = [
                    vec3f._make(ent[3 * i + 7 : 3 * i + 10]) for i in range(3)
                ]
                self.entities.append(entity._make([*ent[:7], pos, rot, scale]))
                x += 1
        [self.add_header_child(node) for node in list(self.path_nodes.values())]

    def decode_node_relations(self):
        num = self.NodeHdr[0]
        start = self.NodeHdr[2]
        self.__setattr__(
            "NodeTraversals",
            [
                self.upt(start + i * num, ">%dB" % num, num, iter=True)
                for i in range(num)
            ],
            start,
        )
        # number of floats is the unique number of distances needed to represent all potential paths
        # we can also just take the max from the node traversals
        x = 1
        for node in self.NodeTraversals:
            for t in node:
                if t & 0x7F > x:
                    x = t & 0x7F
        start = self.NodeHdr[3]
        self.__setattr__(
            "NodeDistances",
            self.upt(start, ">%df" % (x + 1), 4 * (x + 1), iter=True),
            start,
        )
        # now get padding in NodeRelations
        traversal_pad = self.check_padding(
            self.NodeHdr[2],
            self.NodeHdr[3],
            num**2,
            1,
            "NodeTraversals",
            up_type=(">B", 1),
        )
        if traversal_pad:
            self.NodeTraversals.append(traversal_pad)
        dist_pad = self.check_padding(
            self.NodeHdr[3],
            self.NodeHdr[1],
            x + 1,
            4,
            "NodeDistances",
            up_type=(">f", 4),
        )
        if dist_pad:
            self.NodeDistances = (*self.NodeDistances, dist_pad)

    def Write(self, file):
        file.write(f"{level_data_includes}\n")
        # main header
        self.WriteDictStruct(
            self.main_header,
            level_header,
            file,
            "",
            "LevelHeader LvlHeader",
            symbols=self.symbols,
            static=True
        )
        self.collision.WriteCollision(file)
        self.WriteNodes(file)
        self.WriteEntities(file)

    def WriteEntities(self, file):
        for j, e in enumerate(self.entities):
            self.WriteDictStruct(e, EntityStruct, file, "", f"Entity Entity_{j}", static=True)
        # end terminator
        if self.entities:
            file.write(f"u32 ent_term = ARR_TERMINATOR;")

    def WriteNodes(self, file):
        # path nodes
        for j, node in self.path_nodes.items():
            pts = node.PathFooter[1]
            self.write_arr(
                file,
                f"f32 PathMatrix_{j}[{pts + (node.PathFooter[0] >> 8)}]",
                node.PathMatrix,
                self.format_arr,
                length=3,
                static=True
            )
            self.write_arr(
                file, f"f32 PathBounds_{j}", node.PathBounds, self.format_arr, static=True, length=pts
            )
            if node.PathFooter[6]:
                self.write_arr(
                    file,
                    f"f32 PathCurl_{j}[{pts - 1}]",
                    node.PathCurl,
                    self.format_arr,
                    length=5,
                    static=True
                )
            self.WriteDictStruct(
                node.PathFooter,
                Path_Footer,
                file,
                "",
                f"PathNodeFooter PathFooter_{j}",
                symbols=self.symbols,
                static=True
            )
        # cam nodes
        for j, node in self.path_nodes.items():
            self.WriteDictStruct(
                node.KirbyNode,
                Kirby_Settings_Node,
                file,
                "",
                f"KirbyNode KirbyNode_{j}",
                symbols=self.symbols,
                static=True
            )
            self.WriteDictStruct(
                node.CamNode, Camera_Node, file, "", f"CameraNode CamNode_{j}", static=True
            )
        # node connections
        for j, node in self.path_nodes.items():
            if node.NodeConnector:
                self.write_arr(
                    file,
                    f"struct Node_Connectors NodeConnector_{j}",
                    node.NodeConnector,
                    self.format_arr,
                    static=True,
                    length=len(node.NodeConnector)
                )
            else:
                file.write("// no node connectors\n\n")
        # node traversal & node distances

        # turn into single long list
        node_traversals = []
        for node_t in self.NodeTraversals:
            node_traversals.extend(node_t)
        self.write_arr(
            file,
            f"u8 NodeTraversals",
            node_traversals,
            self.format_arr,
            length=len(node_traversals),
            static=True
        )
        self.write_arr(
            file,
            f"f32 NodeDistances",
            self.NodeDistances,
            self.format_arr,
            length=len(self.NodeDistances),
            static=True
        )

        # path node headers
        for j, node in self.path_nodes.items():
            self.WriteDictStruct(
                node.PathHeader,
                PathHeader,
                file,
                "",
                f"PathNodeHeader PathHeader_{j}",
                symbols=self.symbols,
                static=True
            )
        # node header
        self.WriteDictStruct(
            self.NodeHdr,
            NodeHeader,
            file,
            "",
            f"NodeHeader NodeHdr",
            symbols=self.symbols,
            static=True
        )


# -------------------------------------------------------------------------------
# Collision file
# -------------------------------------------------------------------------------

# for collision, this is independent, but can also be used for the collision part of a level bin
class collision_bin(misc_bin):
    feature = "obj"
    # in all files, col hdr ptr is at offset 0
    def __init__(self, file, start=0x0):
        self.header = []
        # some data has alignment padding of 0x9999, this is a dict of that data
        self.pads = dict()
        col_header = namedtuple(
            "col_header",
            """triangles num_tris vertices num_vertices
		normals num_normals tri_cells num_tri_cells norm_cells num_norm_cells norm_cell_root
		dyn_geo_groups dyn_geo_indices water_box_data num_water_boxes water_normals num_water_normals
		""",
        )
        if not start:
            start = self.upt(0, ">L", 4)
        super().__setattr__(
            "Col_Header", col_header._make(self.upt(start, ">17L", 68)), ptr=start
        )
        self.col_header_ptr = start
        self.decode_col()

    def decode_col(self):
        # get data from header, set ptrs for all
        col_hd = self.Col_Header
        super().__setattr__(
            "Triangles",
            [
                (
                    self.upt(a * 20 + col_hd.triangles, ">3H", 6),
                    *self.upt(a * 20 + col_hd[0] + 6, ">7H", 14),
                )
                for a in range(col_hd.num_tris)
            ],
            col_hd.triangles,
        )
        self.decode_from_ptr(col_hd.vertices, ">3h", 6, "Vertices", col_hd.num_vertices)
        self.check_padding(
            col_hd.vertices, col_hd.triangles, col_hd.num_vertices, 6, "Vertices"
        )
        self.decode_from_ptr(col_hd.normals, ">4f", 16, "Normals", col_hd.num_normals)
        self.decode_from_ptr(
            col_hd.tri_cells, ">H", 2, "Tri_Cells", col_hd.num_tri_cells
        )
        self.check_padding(
            col_hd.tri_cells, col_hd.norm_cells, col_hd.num_tri_cells, 2, "Tri_Cells"
        )
        self.decode_from_ptr(
            col_hd.norm_cells, ">4H", 8, "Norm_Cells", col_hd.num_norm_cells
        )
        # water
        if self.Col_Header.water_box_data:
            water = namedtuple(
                "water",
                "num_norms normals active has_flow flow_dir flow_spd pos1 pos2 pos3 pos4",
            )
            super().__setattr__(
                "Water_Box_Data",
                [
                    water._make(
                        self.upt(
                            a * 0x18 + self.Col_Header.water_box_data, ">2H4B4f", 0x18
                        )
                    )
                    for a in range(self.Col_Header.num_water_boxes)
                ],
                self.Col_Header.water_box_data,
            )
            self.decode_from_ptr(
                self.Col_Header.water_normals,
                ">4f",
                16,
                f"WaterNormals",
                self.Col_Header.num_water_normals,
            )

        # dyn geo, this doesn't have a defined length, I just keep going until I see the next group
        dyn_geo_groups = self.Col_Header.dyn_geo_groups
        dyn_geo_indices = self.Col_Header.dyn_geo_indices
        # size is next group start - this group start
        dyn_geo_groups_siz = (dyn_geo_indices - dyn_geo_groups) // 6
        dyn_geo_indices_siz = (self.col_header_ptr - dyn_geo_indices) // 2

        # sometimes, the ptr is just NULL'd out but the data still exists
        # scan for that condition
        if not dyn_geo_groups:
            last_end = self.Col_Header.norm_cells + col_hd.num_norm_cells * 8
            if self.Col_Header.water_box_data:
                print("check for water condition when missing dyn geo group")
            if last_end < self.col_header_ptr:
                dyn_geo_groups = last_end
                dyn_geo_groups_siz = (self.col_header_ptr - dyn_geo_groups) // 6

        if dyn_geo_groups:
            self.decode_from_ptr(
                dyn_geo_groups, ">3H", 6, "Dyn_Geo_Groups", dyn_geo_groups_siz
            )
            self.check_padding(
                dyn_geo_groups, dyn_geo_indices, dyn_geo_groups_siz, 6, "Dyn_Geo_Groups"
            )
        if dyn_geo_indices:
            self.decode_from_ptr(
                dyn_geo_indices, ">H", 2, "Dyn_Geo_Indices", dyn_geo_indices_siz
            )
            self.check_padding(
                dyn_geo_indices,
                self.col_header_ptr,
                dyn_geo_indices_siz,
                2,
                "Dyn_Geo_Indices",
            )

    def Write(self, file):
        # main header
        value = self.symbols.get(self.col_header_ptr, None)
        if value:
            file.write(f"static struct CollisionHeader *ColHeaderPtr = &{value};\n\n")
        else:
            file.write(
                f"static struct CollisionHeader *ColHeaderPtr = 0x{self.col_header_ptr:X};\n\n"
            )
        self.WriteCollision(file)

    def WriteCollision(self, file):
        # vertices, just a list
        self.write_arr(
            file,
            f"s16 Vertices[{len(self.Vertices)}]",
            self.Vertices,
            self.format_arr,
            length=3,
            static=True
        )
        self.write_pad(file, "Vertices")
        # triangles, as a struct
        self.WriteStructArr(
            file, self.Triangles, Triangles, "CollisionTriangle Triangles", static=True
        )
        # normals, as a list
        self.write_arr(file, "struct Normal Normals", self.Normals, self.format_arr, static=True, length=len(self.Normals))
        # tri cells as regular arr
        self.write_arr(file, "u16 Tri_Cells", self.Tri_Cells, self.format_arr, static=True, length=len(self.Tri_Cells))
        self.write_pad(file, "Tri_Cells")
        # tri cells, as a delimited list, not matching as of now
        # self.WriteArrDelim(
        # file, self.Tri_Cells, "u16 Tri_Cells[]", lambda x: x > 0x8000
        # )
        # norm cells, a list
        self.write_arr(
            file, "struct NormalGroup Norm_Cells", self.Norm_Cells, self.format_arr, static=True, length=len(self.Norm_Cells)
        )
        # water data if it exists struct WaterData
        if hasattr(self, "Water_Box_Data"):
            self.write_arr(
                file, f"struct Normal WaterNormals", self.WaterNormals, self.format_arr, static=True, length=len(self.WaterNormals)
            )
            self.WriteStructArr(
                file, self.Water_Box_Data, Water_Quads, "WaterData Water_Box_Data", static=True
            )
        # dyn geo groups and indices, both lists, but only if they exist
        if hasattr(self, "Dyn_Geo_Groups"):
            self.write_arr(
                file,
                "struct DynamicGeometryNode Dyn_Geo_Groups",
                self.Dyn_Geo_Groups,
                self.format_arr,
                static=True,
                length=len(self.Dyn_Geo_Groups)
            )
            self.write_pad(file, "Dyn_Geo_Groups")
        if hasattr(self, "Dyn_Geo_Indices"):
            self.write_arr(
                file,
                "u16 Dyn_Geo_Indices",
                self.Dyn_Geo_Indices,
                self.format_arr,
                length=len(self.Dyn_Geo_Indices),
                static=True
            )
            self.write_pad(file, "Dyn_Geo_Indices")
        # write col header
        self.WriteDictStruct(
            self.Col_Header,
            collision_header,
            file,
            "",
            "CollisionHeader Col_Header",
            symbols=self.symbols,
            static=True
        )


# -------------------------------------------------------------------------------
# Particle files
# -------------------------------------------------------------------------------

# for both particle textures and particle dat files
# majority of stuff is inside the ParticleDat file classes

# struct for the header
# struct ParticleHeader{
# /*0x0*/ u32 num;
# union{
# /*0x4*/ struct ParticleTex *Offsets[]; //len is num
# /*0x4*/ struct ParticleDat *Offsets[]; //len is num
# };
# };

# particle data itself is bytecode. I am representing this with macros
# Many cmds are MSB defined, but some only use the upper nybble
# I won't focus on representing these super accurately until I know
# what they actually do. So I will focus on readability first.

# not finished yet
class particle_bin(misc_bin):
    feature = "particle"

    def __init__(self, file):
        # particle dat
        self.Header = self.extract_dict(0, ParticleHeader)
        # header contains ptrs to each dat
        particles = {}
        start = self.Header[1][0]
        last = start
        for ptr in self.Header[1]:
            # init the particle
            particles[ptr] = PD.Particle(file, ptr)
            # get the data
            particles[ptr].FormatDat(file)
            # check for vestigial data (or padding) and add it to byte array
            if ptr > last:
                particles[last].Vestigial(ptr, file)
                last = ptr
        self.particles = particles

    def Write(self, file):
        self.WriteDictStruct(
            self.Header, ParticleHeader_W, file, "", f"ParticleHeader Header"
        )
        for i, (k, v) in enumerate(self.particles.items()):
            # comment on location
            file.write(f"//0x{k:X}\n")
            self.WriteDictStruct(
                v.ParticleInit, ParticleInit_W, file, "", f"ParticleInit PD_{i}"
            )
            self.WriteByteArray(file, v.dat, i)

    # byte array in particle data is all u8 cmds, followed by arguments which
    # can be floats, u32s, u16s, u8s etc.
    def WriteByteArray(self, file, dat, i):
        # write the type
        file.write(f"u8 ParticleDat PD_Dat_{i}[] = {{\n")
        x = 0
        val = ""
        macro_set = ""
        while x < len(dat):
            cmd = dat[x]
            if not cmd:
                x += 1
                continue
            # it is arguments to the prev cmd
            if type(cmd) != int:
                cmds = [f"0x{a:X}" if type(a) == int else f"{a:f}" for a in cmd]
                if val:
                    val = [val, *cmds]
                else:
                    val = cmds
                val = ", ".join(val)
                file.write(f"\t{macro_set}({val}),\n")
                x += 1
                val = ""
                macro_set = ""
                continue
            # it is a cmd
            else:
                # macro without args, or args are the lower of cmd (implicitly)
                if macro_set:
                    file.write(f"\t{macro_set}({val}), \n")
                    macro_set = ""
                # 0xC0 and 0xD0 use their lower bits as an arg
                if cmd & 0xF0 == 0xC0 or cmd & 0xF0 == 0xD0:
                    macro_set = Particle_Macros.get(cmd & 0xF0)
                    val = f"0x{cmd&0xF:X}, "  # add lower as first arg to macro
                    x += 1
                    continue
                # if cmd is under 0x80, then only the upper bit is used as cmd
                # lower 5 bits are the only arg except for 0x20 and 0x40
                elif cmd < 0x80:
                    macro_set = Particle_Macros.get(cmd & 0xE0)
                    val = f"0x{cmd&0x1F:X}"
                    x += 1
                    continue
                # macros between 0x80 and 0x99 use lower 3 bits as arg
                elif cmd < 0x99:
                    macro_set = Particle_Macros.get(cmd & 0xF8)
                    val = f"0x{cmd&0x7:X}"
                    x += 1
                    continue
                else:
                    val = Particle_Macros.get(cmd)
                if val:
                    macro_set = val
                    val = ""
                else:
                    macro_set = f"0x{cmd:X}"
                    val = ""
                    # print(macro_set)
                x += 1
        # close up array
        file.write("};\n")


# for now, only extract the init structs
class particle_tex(misc_bin):
    feature = "particle_tex"

    def __init__(self, file):
        self.Header = self.extract_dict(0, ParticleHeader)
        particles = {}
        start = self.Header[1][0]
        last = start
        for ptr in self.Header[1]:
            # I get the init struct only for now
            particles[ptr] = PD.TextureBin(file, ptr)
            # check for vestigial data (or padding) and add it to byte array
            if ptr > last:
                particles[last].Vestigial(ptr, file)
                last = ptr
        self.particles = particles

    def Write(self, file):
        self.WriteDictStruct(
            self.Header, ParticleHeader_W, file, "", f"ParticleHeader Header"
        )
        for i, (k, v) in enumerate(self.particles.items()):
            # comment on location
            file.write(f"//0x{k:X}\n")
            self.WriteDictStruct(v.dat, ParticleTex_W, file, "", f"ParticleTex PT_{i}")


# just generic getters for testing
def GetAllParticles(Kirb: "binary ROM file"):
    for a in particle_textures:
        GetSingleMisc(*a, Kirb)
    for a in particle_data:
        GetSingleMisc(*a, Kirb)


# getting some debug info on particle dats
def DebugParticles():
    l = [a for a in PD.unk2]
    l.sort()
    lf = [f"0x{a:X}" for a in PD.unk2]
    bits = 0
    for a in l:
        bits = bits | a
    print(lf, f"bits = 0x{bits:X}")


# -------------------------------------------------------------------------------
# Main
# -------------------------------------------------------------------------------


def GetSingleMisc(bank, index, Kirb, path=None):
    ptrs = GetPointers(bank, index, "Misc", Kirb)
    if not path:
        name = f"assets/misc/bank_{bank}/{index}"
    else:
        name = path
    Path(name).mkdir(exist_ok=True, parents=True)
    WriteBin(Kirb, *ptrs, str(name) + "/misc")
    stem = "misc" # change this later??
    with open(Path(name) / (f"{stem}.bin"), "rb") as f:
        block = misc_bin(f.read())
        # write files based on what type of file I read, which is contained within the class variable
    if block.feature == "level":
        stem = "level"
    with open(Path(name) / (f"{stem}.c"), "w", newline='') as f:
        f.write(f"//start 0x{ptrs[0]:X} - end 0x{ptrs[1]:X}\n\n")
        block.Write(f)
    if block.feature == "level":
        with open(Path(name) / (f"{stem}.h"), "w", newline='') as f:
            block.write_header(f)
    print(f"Misc bank {bank} index {index} file wrote")


# exports all geo block data from an entire bank
def ExportMiscBank(bank, Kirb: "binary ROM file"):
    for index in LoopOverBank(bank, Kirb, block_type="Misc"):
        GetSingleMisc(bank, index, Kirb)


# later write this to grab all misc files
def main(Kirb: "binary ROM file"):
    # GetAllParticles(Kirb)
    # GetSingleMisc(7, 65, Kirb)
    # for i in range(8):
    ExportMiscBank(7, Kirb)


if __name__ == "__main__":
    Kirby = open(sys.argv[1], "rb")
    Kirb = Kirby.read()
    main(Kirb)
    print("Misc Bank Exports finished")
