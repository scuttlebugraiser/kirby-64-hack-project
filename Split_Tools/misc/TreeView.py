# python tool meant for use in interpreter to analyze and debug collision cells
# not really necessary anymore as collision cells have been fully analyzed

from Kirby_Editor import *
from treelib import Node, Tree
import struct


class Normal(Node):
    def __init__(self, LS, child, parent, tree):
        Norm = LS.Get("Normals")
        NC = LS.Get("Normal_Cells")
        TC = LS.Get("Triangle_Cells")
        T = LS.Get("Triangles")
        V = LS.Get("Vertices")
        self.NC = [NC[child]]
        self.child = child
        self.Normal = Norm[NC[child][0]]
        x = 0
        PNM = []
        while True:
            try:
                PNM.extend(LS.Get("Path_Node_Matrix_%d" % x))
                x += 1
            except:
                break
        L = 1
        for tc in TC[1:]:
            if NC[child][3] == L:
                self.TriCell = tc
                break
            if tc == 0x9999:
                print(self.NC)
                break
            if type(tc) == tuple:
                L += len(tc)
            else:
                L += 1
        if type(self.TriCell) == tuple:
            tris = [T[a & 0x7FFF] for a in self.TriCell]
        else:
            tris = [T[self.TriCell & 0x7FFF]]
        verts = []
        Ygroups = []
        for t in tris:
            for a in t[0]:
                verts.append(V[a])
            Ygroups.append(max([V[a][1] for a in t[0]]))
        self.Hsort = Ygroups.copy()
        self.Hsort.sort()
        self.Hsort = Ygroups == self.Hsort
        self.xSpan = [self.comp2dec(v[0]) for v in verts]
        self.ySpan = [self.comp2dec(v[1]) for v in verts]
        self.zSpan = [self.comp2dec(v[2]) for v in verts]
        convert = lambda x: [self.comp2dec(a) for a in x]
        self.verts = [convert(a) for a in verts]
        dot = lambda x, y: sum([a * b for a, b in zip(x, y)])
        self.NegOff = dot(self.Normal, self.verts[0]) + self.Normal[3]
        self.size = len(self.TriCell)
        self.xSpan.sort()
        self.ySpan.sort()
        self.zSpan.sort()
        self.xSpan = [self.xSpan[0], self.xSpan[-1]]
        self.ySpan = [self.ySpan[0], self.ySpan[-1]]
        self.zSpan = [self.zSpan[0], self.zSpan[-1]]
        self.ColTypes = []
        self.ArrayInd = []
        self.Part = []
        w = [0, 1, 0]
        s = [1, 0, 1]
        if dot(w, self.Normal) == 0:
            self.Wall = True
        else:
            self.Wall = False
        if dot(s, self.Normal) != 1 and self.Wall == False:
            self.slope = True
        else:
            self.slope = False
        for t in tris:
            if t[7] not in self.ColTypes:
                self.ColTypes.append(t[7])
            if t[3] not in self.ArrayInd:
                self.ArrayInd.append(t[3])
            if t[4] not in self.Part:
                self.Part.append(t[4])
        if parent:
            P = tree.get_node(parent)
            Pn = P.data
            negdot = Pn.Normal[3]
            norm = Pn.Normal[0:3]
            self.plane = dot(norm, self.Normal[0:3])
            Pos1 = [self.xSpan[0], self.ySpan[0], self.zSpan[0]]
            Pos3 = [self.xSpan[1], self.ySpan[1], self.zSpan[1]]
            self.Avg = [(a + b) * 0.5 for a, b in zip(Pos1, Pos3)]
            self.All = True
            self.offset = 0
            self.Break = 0
            for v in self.verts:
                q = dot(norm, v) + negdot
                if q < -0.01:
                    self.All = False
                    self.offset = q
                    self.Break = v
                    break
            self.Path = True
            for v in PNM:
                a = [v[0], v[1] + 20, v[2]]
                q = dot(norm, v) + negdot
                w = dot(norm, a) + negdot
                if q < -0.01:
                    self.Path = False
                    break
            for v in self.verts:
                if v in Pn.verts:
                    self.Connected = [v, Pn.verts[Pn.verts.index(v)], norm]
                    break
            else:
                self.Connected = False
            if self.All:
                self.out = self.All == (NC[Pn.child][1] == child)
            else:
                self.out = self.All != (NC[Pn.child][2] == child)
            if self.Path:
                self.Path = self.Path == (NC[Pn.child][1] == child)
            else:
                self.Path = self.Path != (NC[Pn.child][2] == child)
            if not self.out:
                self.Broken = self.offset
                self.BV = self.verts
            else:
                self.Broken = "True"
                self.BV = "True"
        else:
            self.out = "Root"
            self.Avg = [0, 0, 0]
            self.All = "Root"
            self.Adjust = "Root"
            self.offset = "Root"
            self.Break = "Root"
            self.BV = "Root"
            self.Broken = "Root"
            self.plane = "Root"
            self.Path = "Root"
            self.Connected = "Root"
        self.WP = [self.Path, self.Wall]

    def comp2dec(self, num):
        n = struct.unpack(">h", struct.pack(">H", num))
        return n[0]


def ViewTree(LS):
    print(LS.LS)
    NC = LS.Get("Normal_Cells")
    tree = Tree()
    Rstack = []
    root = LS.Get("Collision_Header")[40]
    # parent node
    tree.create_node(str(NC[root]), str(NC[root]), data=Normal(LS, root, 0, tree))
    P = str(NC[root])
    LChild = NC[root][1]
    if LChild == 0:
        LChild = NC[root][2]
    while True:
        if LChild != 0:
            tree.create_node(
                str(NC[LChild]),
                str(NC[LChild]),
                parent=P,
                data=Normal(LS, LChild, P, tree),
            )
            P = str(NC[LChild])
            if NC[LChild][2]:
                Rstack.append(NC[LChild])
            LChild = NC[LChild][1]
        # reached furthest point left, go back in stack
        else:
            if Rstack:
                LChild = Rstack[-1][2]
                P = str(Rstack[-1])
                Rstack.pop(-1)
            else:
                if NC[LChild] == NC[0]:
                    break
                if NC[LChild][2]:
                    Rstack.append(NC[LChild])
                else:
                    break
    return tree


def ShowTrees(T):
    print("Default Tree")
    T.show(line_type="ascii-em")
    # print("ColTypes")
    # T.show(line_type="ascii-em",data_property="ColTypes")
    print("Normal")
    T.show(line_type="ascii-em", data_property="Normal")
    print("Comparison")
    T.show(line_type="ascii-em", data_property="out")
    print("slopes")
    T.show(line_type="ascii-em", data_property="slope")
    # print("TriCell")
    # T.show(line_type="ascii-em",data_property="TriCell")
    # print("size")
    # T.show(line_type="ascii-em",data_property="size")
    # print("WP")
    # T.show(line_type="ascii-em",data_property="WP")


# Good correlation:
# All verts in front, very very good
# Tris sorted by height, very very good
# Shares no verts with parent, good
# In front of all pathing points, ok
# In front of all pathing points with range of y=20, ok

# No correlation:
# tri cell size
# distance from parent
# col type
# array index
# coplanar with parent
# highest vert in front
# verts in front with y offset
# average pos/kirby size projected in front
# average pos in front
# slopes with other slopes


# usage
# import importlib
# from Kirby_Editor import *
# sys.path.insert(0,"C:\\Users\\issa\\desktop\\kirby")
# tv = importlib.import_module('TreeView')
# AddPaths()
# stage = LS(7,70)#levels = GetAllLevels()
# T = tv.ViewTree(stage)
# tv.ShowTrees(T)
# reload module after changing
# importlib.reload(tv)
