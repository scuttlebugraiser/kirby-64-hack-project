# -------------------------------------------------------------------------------
# Level Settings Structs/data
# -------------------------------------------------------------------------------

HS = 0x80057DB0

# type dicts for writing to file only
# values are (data type, then name, is arr (only as needed))

level_data_includes = """#include <ultra64.h>
#include "level_settings_structs.h"
#include "stages.h"
#include "level.h"
"""


level_header = {
    0x0: ("u32", "*CollisionHeader", "ptr"),
    0x4: ("u32", "*NodeHeader", "ptr"),
    0x8: ("u32", "*Entities[]", "ptr"),
    0xC: ("u32", "Pad"),
}

collision_header = {
    0x0: ("struct Triangle", "*Triangles[]", "ptr no&"),
    0x4: ("u32", "Num_Triangles"),
    0x8: ("struct Vertices", "*Vertices[]", "ptr no& [0]"),
    0xC: ("u32", "Num_Vertices"),
    0x10: ("f32", "*Tri_Normas[4][]", "ptr no&"),
    0x14: ("u32", "Num_Tri_Norms"),
    0x18: ("u16", "*Triangle_Cells[]", "ptr no&"),
    0x1C: ("u16", "Num_Tri_Cells"),
    0x20: ("u16", "*Tri_Norm_Cells[]", "ptr no&"),
    0x24: ("u16", "Num_Tri_Norm_Cells"),
    0x28: ("u32", "Norm_Cell_Root"),
    0x2C: ("u16", "*Dyn_Geo_Groups[3][]", "ptr no&"),
    0x30: ("u16", "*Dyn_Geo_Indices[]", "ptr no&"),
    0x34: ("struct WaterData", "*Water_Box_Data[]", "ptr no&"),
    0x38: ("u32", "Num_Water_Boxes"),
    0x3C: ("f32", "*Water_Normals[4][]", "ptr no&"),
    0x40: ("u32", "Num_Water_Normals"),
}

Triangles = {
    0x0: ("u16", "Vert_IDs[3]", "arr"),
    0x6: ("u16", "Norm_ID"),
    0x8: ("u16", "Norm_Type"),
    0xA: ("u16", "Dyn_Geo_ID"),
    0xC: ("u16", "Paticle_ID"),
    0xE: ("u16", "Stop"),
    0x10: ("u16", "Col_Param"),
    0x12: ("u16", "Col_Type"),
}

Water_Quads = {
    0x0: ("u16", "Number Normals"),
    0x2: ("u16", "Normals Array Offset"),
    0x4: ("u8", "Water Box Active"),
    0x5: ("u8", "Activate Water Flow"),
    0x6: ("u8", "Water Flow Direction"),
    0x7: ("u8", "Water Flow Speed"),
    0x8: ("f32", "Pos1"),
    0xC: ("f32", "Pos2"),
    0x10: ("f32", "Pos3"),
    0x14: ("f32", "Pos4"),
}

NodeHeader = {
    0x0: ("u32", "Num Path Nodes"),
    0x4: ("struct PathHeader", "*Path_Headers[]", "ptr"),
    0x8: ("u8", "*Node_Traversals[]", "ptr"),
    0xC: ("", "*Node_Dists[]", "ptr"),
}

PathHeader = {
    0x0: ["u32", "Kirby_Node", "ptr"],
    0x4: ["u32", "Path_Footer", "ptr"],
    0x8: ["u32", "Node_Connector", "ptr no&"],
    0xC: ["u16", "Num Connections"],
    0xE: ["u16", "Self Connected"],
}

Camera_Node = {
    0x0: ("u8", "Profile View"),
    0x1: ("u8", "Unused"),
    0x2: ("u8", "Lock H pos"),
    0x3: ("u8", "Lock Y pos"),
    0x4: ("u8", "Lock Z pos?"),
    0x5: ("u8", "Unused2"),
    0x6: ("u8", "Not Camera Pan Phi Above/Below"),
    0x7: ("u8", "Not Camera Pan Phi Below"),
    0x8: ("u8", "Camera Pan Theta"),
    0x9: ("u8", "unused3"),
    0xA: ("u16", "unused4"),
    0xC: ("f32", "Focus X pos"),
    0x10: ("f32", "Focus Y pos"),
    0x14: ("f32", "Focus Z pos"),
    0x18: ("f32", "Near Clip Plane"),
    0x1C: ("f32", "Far Clip Plane"),
    0x20: ("f32", "Cam Phi Rot[2]", "arr"),
    0x28: ("f32", "Cam Theta Rot[2]", "arr"),
    0x30: ("f32", "Cam Radius[2]", "arr"),
    0x38: ("f32", "FOV pair[2]", "arr"),
    0x40: ("f32", "Cam Y offset[2]", "arr"),
    0x48: ("f32", "Cam X Pos Lock Bounds[2]", "arr"),
    0x50: ("f32", "Cam Y Pos Lock Bounds[2]", "arr"),
    0x58: ("f32", "Cam Z Pos Lock Bounds[2]", "arr"),
    0x60: ("f32", "Cam Yaw Lock Bounds[2]", "arr"),
    0x68: ("f32", "Cam Pitch Lock Bounds[2]", "arr"),
}

Kirby_Settings_Node = {
    0x0: ("u16", "node"),
    0x2: ("u16", "entrance_act"),
    0x4: ("u8", "warp[4]", "arr"),
    0x8: ("u8", "pad1"),
    0x9: ("u8", "Shade[3]", "arr"),
    0xC: ("u16", "pad2"),
    0xE: ("u16", "Flags"),
    0x10: ("u16", "opt1"),
    0x12: ("u16", "opt2"),
    0x14: ("f32", "opt3"),
    0x18: ("f32", "opt4"),
    0x1C: ("f32", "pad3"),
}


Path_Footer = {
    0x0: ("u16", "Has_Curl"),
    0x2: ("u16", "Num_Pts"),
    0x4: ("f32", "Force"),
    0x8: ("u32", "*Path_Matrix", "ptr no& [0]"),
    0xC: ("f32", "Node_Length"),
    0x10: ("u32", "*Path_Bounds", "ptr no&"),
    0x14: ("u32", "*Path_Curl(?)", "ptr no&"),
}


EntityStruct = {
    0x00: ("u8", "Node Num"),
    0x01: ("u8", "Bank"),
    0x02: ("u8", "ID"),
    0x03: ("u8", "Action"),
    0x04: ("u8", "Res_Flag"),
    0x05: ("u8", "Spawn_Flag"),
    0x06: ("u16", "Eeprom"),
    0x08: ("f32", "Location", "arr"),
    0x14: ("f32", "Rot", "arr"),
    0x20: ("f32", "Scale", "arr"),
}

# -------------------------------------------------------------------------------
# Particle files
# -------------------------------------------------------------------------------

# values are func->type, name, func->len, arr = None

ParticleInit = {
    0x0: (">H", "Flag_Unk", 2),  # not sure what this means really
    0x2: (">H", "TexIndex", 2),  # src file is likely next misc = particle tex
    0x4: (">H", "Persistance", 2),  # 0 to stay forever
    0x6: (">H", "unk2", 2),  # something to do with transform
    0x8: (">H", "unk3", 2),  # only one byte is used? read only at start
    0xA: (">H", "unk4", 2),  # bitflag
    0xC: (">12f", "Transform", 48),  # bitflag
}

ParticleHeader = {0x0: (">L", "Num", 4), 0x4: (">{}L", "Offsets", 4, lambda x: x[0])}

ParticleTex = {
    0x0: (">L", "numFrames", 4),
    0x4: (">L", "Fmt", 4),
    0x8: (">L", "Siz", 4),
    0xC: (">L", "width", 4),
    0x10: (">L", "height", 4),
    0x14: (">L", "unk", 4),
    0x18: (">{}L", "*Offsets[]", 4, lambda x: x[0] * (1 + TexDouble(x))),
}

# for particle textures, in some conditions there are extra offsets from what is specified.
# this is for palettes, or mip maps, or potentially other conditions yet to be known
def TexDouble(x):
    return (x[5] == 0) or (x[1] == 2)


# repeats of dicts for writing
ParticleInit_W = {
    0x0: ("u16", "Flag_Unk"),
    0x2: ("u16", "TexIndex"),
    0x4: ("u16", "Persistance"),  # 0 to stay forever
    0x6: ("u16", "unk2"),  # something to do with transform
    0x8: ("u16", "unk3"),  # only one byte is used? read only at start
    0xA: ("u16", "unk4"),  # bitflag
    0xC: ("f32", "Transform", "arr"),  # bitflag
}

ParticleTex_W = {
    0x0: ("u32", "numFrames"),
    0x4: ("u32", "Fmt"),
    0x8: ("u32", "Siz"),
    0xC: ("u32", "width"),
    0x10: ("u32", "height"),
    0x14: ("u32", "unk"),
    0x18: ("u32", "*Offsets[]", "arr"),
}


ParticleHeader_W = {0x0: ("u32", "Num"), 0x4: ("u32", "*Offsets[]", "arr")}

# this dict will be mapping the particle dat cmds to macro names
Particle_Macros = {
    0x00: "LowerSetLoop",
    0x20: "LowerSetLoop_OB",
    0x40: "LowerSetLoop_SetB_40",
    0x80: "BitTransformSet_80",
    0x88: "BitTransformAdd_88",
    0x90: "BitTransform2Set_90",
    0x98: "BitTransform2Add_98",
    0xA0: "FloatSet1_A0",
    0xA1: "SetUnk6_A1",
    0xA2: "JumpSet_A2",
    0xA3: "SetUnk6_A3",
    0xA4: "Unk_A4",
    0xA5: "Unk_A5",
    0xA6: "Unk_A6",
    0xA7: "Unk_A7",
    0xA8: "AddTransformRNG_A8",
    0xA9: "Unk_A9",
    0xAA: "Unk_AA",
    0xAB: "MultiplyTransform_AB",
    0xAC: "AddTransformRNG_AC",
    0xAD: "Flag_0x80",
    0xAE: "Flag_n0x80",
    0xAF: "Flag_n0x40",
    0xB0: "Flag_n0x20o0x40",
    0xB1: "Flag_0x60",
    0xB2: "Flag_0x200",
    0xB3: "Flag_n0x400",
    0xB4: "Flag_0x400",
    0xB5: "Flag_0x100",
    0xB6: "Flag_n0x100",
    0xE2: "Flag_0x8",
    0xB7: "Unk_B7",
    0xB8: "Unk_B8",
    0xB9: "Unk_B9",
    0xBA: "Unk_BA",
    0xBB: "Unk_BB",
    0xBC: "Unk_BC",
    0xBD: "Unk_BD",
    0xBE: "Unk_BE",
    0xBF: "Unk_BF",
    0xC0: "Unk_C0",
    0xD0: "Unk_D0",
    0xE0: "Unk_E0",
    0xE3: "Unk_E3",
    0xFA: "repeat_N_FA",
    0xFB: "return_N_FB",
    0xFC: "loop_FC",
    0xFD: "return_loop_FD",
    0xFF: "break_FF",
}

# list of files for quick extracting

particle_data = [
    (0, 1),
    (0, 3),
    (1, 1),
    (2, 1),
    (3, 7),
    (3, 18),
    (3, 20),
    (4, 1),
    (7, 1),
    (7, 3),
    (7, 5),
    (7, 7),
    (7, 9),
    (7, 11),
    (7, 13),
    (7, 15),
    (7, 17),
    (7, 19),
    (7, 21),
    (7, 23),
    (7, 25),
    (7, 27),
    (7, 29),
    (7, 31),
    (7, 33),
    (7, 35),
    (7, 37),
    (7, 39),
    (7, 41),
    (7, 43),
    (7, 45),
    (7, 47),
    (7, 49),
    (7, 51),
    (7, 53),
    (7, 55),
]
particle_textures = [
    (0, 2),
    (1, 2),
    (2, 2),
    (3, 8),
    (3, 19),
    (3, 21),
    (4, 2),
    (7, 2),
    (7, 4),
    (7, 6),
    (7, 8),
    (7, 10),
    (7, 12),
    (7, 14),
    (7, 16),
    (7, 18),
    (7, 20),
    (7, 22),
    (7, 24),
    (7, 26),
    (7, 28),
    (7, 30),
    (7, 32),
    (7, 34),
    (7, 36),
    (7, 38),
    (7, 40),
    (7, 42),
    (7, 44),
    (7, 46),
    (7, 48),
    (7, 50),
    (7, 52),
    (7, 54),
    (7, 56),
]
