import struct, math, os, sys
from pathlib import Path

# if the path is from the same directory, these imports will work
# otherwise it will use an absolute import as if it was from the parent
try:
    from LevelDataTypes import *
except:
    from misc.LevelDataTypes import *

# to import utility.py, we need to add parent dir to path
file = Path(os.path.dirname(__file__))
sys.path.append(str(file.parent))
from Utility import *

unk2 = set()

# particle dat data retrieval, each class is one particle
class Particle(BinProcess):
    def __init__(self, file, ptr):
        self.DatLen = 0
        self.file = file
        self.ptr = ptr
        self.ParticleInit = self.extract_dict(ptr, ParticleInit)
        q = self.ParticleInit[6][5]
        if self.denorm(q):
            # if self.ParticleInit[0] == 5:
            print(
                q, struct.pack(">f", q).hex(), self.ParticleInit[0], self.ParticleInit
            )
            global unk2
            unk2.add(self.ParticleInit[3])

    def denorm(self, a):
        return (abs(a) < 0.000000001 and abs(a) > 0) or math.isnan(a)

    def Pad(self, pos, rom):
        loop = 4 - (pos % 4)
        if loop == 4:
            loop = 0
        [self.dat.append(self.upt(pos + i, ">B", 1)) for i in range(loop)]
        return loop

    def FormatDat(self, rom):
        start = self.ptr + 0x3C
        dat = []
        while 1:
            cmd = self.upt(start, ">B", 1)
            dat.append(cmd)
            self.ParticleDat = []  # values contained within cmd
            # end of particle data bytecode cmds
            if cmd == 0xFF or cmd == 0xFE:
                start += 1
                break
            # cmds over 0x7F continue the loop
            if cmd >= 0x80:
                switch = cmd & 0xF8
                if switch >= 0x99:
                    TruncSwitch = switch & 0xF0
                    if TruncSwitch == 0xC0 or TruncSwitch == 0xD0:
                        switch = TruncSwitch
                    else:
                        switch = cmd
                start = self.ExecFunc(switch, cmd, start + 1, rom)
                if self.ParticleDat:
                    dat.append(self.ParticleDat)
            # cmds under 0x80 act as end of frames, or blocking cmds for processing
            else:
                start += 1
                low = cmd & 0x1F
                if cmd & 0x20 or (cmd & 0xC0 == 0x40):
                    start += self.OneByte(cmd, start, rom)
                if self.ParticleDat:
                    dat.append(self.ParticleDat)
        self.dat = dat
        start += self.Pad(start, rom)
        self.DatLen = start - (self.ptr + 0x3C)

    # if the length of data does not butt up to next struct, there are bytes I missed
    def Vestigial(self, TrueEnd, rom):
        end = (self.ptr + 0x3C) + self.DatLen
        if TrueEnd > end:
            amt = (TrueEnd - end) // 4
            Ex = self.upt(end, ">%dB" % (amt * 4), amt * 4)
            self.dat.append(Ex)
            self.DatLen += len(Ex)

    # execute the particle cmd and gather the data, and advance the ptr
    def ExecFunc(self, switch, cmd, start, rom):
        Funcs = {
            0x80: self.BitFloatTransform,
            0x88: self.BitFloatTransform,
            0x90: self.BitFloatTransform,
            0x98: self.BitFloatTransform,
            0xA0: self.MSBHalfOneFloat,
            0xA1: self.OneByte,
            0xA2: self.OneFloat,
            0xA3: self.OneFloat,
            0xA4: self.TwoByte,
            0xA5: self.TwoByte,
            0xA6: self.TwoHalf,
            0xA7: self.OneByte,
            0xA8: self.ThreeFloat,
            0xA9: self.OneFloat,
            0xAA: self.OneFloat,
            0xAB: self.OneFloat,
            0xAC: self.MSBHalfTwoFloat,
            0xB7: self.OneByte,
            0xB8: self.OneByteOneFloat,
            0xB9: self.TwoByte,
            0xBA: self.OneFloat,
            0xBB: self.OneFloat,
            0xBC: self.TwoByte,
            0xBD: self.TwoFloat,
            0xBE: self.ThreeFloat,
            0xBF: self.OneByte,
            0xC0: self.BitByteTransform,
            0xD0: self.BitByteTransform,
            0xE0: self.FourByte,
            0xE3: self.OneByte,
            0xFA: self.OneByte,
        }
        start += Funcs.get(switch, self.NoDat)(cmd, start, rom)
        return start

    # start of cmds
    def MSBHalfOneFloat(self, cmd, pos, rom):
        param = self.upt(pos, ">B", 1)
        self.ParticleDat.append(param)
        adv = 1
        if param & 0x80:
            self.ParticleDat.append((self.upt(pos + adv, ">B", 1)))
            adv += 1
        self.ParticleDat.append(self.upt(pos + adv, ">f", 4))
        return adv + 4

    def MSBHalfTwoFloat(self, cmd, pos, rom):
        param = self.upt(pos, ">B", 1)
        self.ParticleDat.append(param)
        adv = 1
        if param & 0x80:
            self.ParticleDat.append((self.upt(pos + adv, ">B", 1)))
            adv += 1
        self.ParticleDat.append(self.upt(pos + adv, ">f", 4))
        self.ParticleDat.append(self.upt(pos + adv + 4, ">f", 4))
        return adv + 8

    def BitFloatTransform(self, cmd, pos, rom):
        a, b, c = cmd & 1, cmd & 2, cmd & 4
        adv = 0
        for val in (a, b, c):
            if val:
                self.ParticleDat.append(self.upt(pos + adv, ">f", 4))
                adv += 4
        return adv

    def BitByteTransform(self, cmd, pos, rom):
        a, b, c, d = cmd & 1, cmd & 2, cmd & 4, cmd & 8
        adv = 1
        param = self.upt(pos, ">B", 1)
        self.ParticleDat.append(param)
        if param & 0x80:
            self.ParticleDat.append((self.upt(pos + 1, ">B", 1)))
            adv += 1
        for val in (a, b, c, d):
            if val:
                self.ParticleDat.append(self.upt(pos + adv, ">B", 1))
                adv += 1
        return adv

    def NoDat(self, cmd, pos, rom):
        return 0

    def OneFloat(self, cmd, pos, rom):
        self.ParticleDat.append(self.upt(pos, ">f", 4))
        return 4

    def TwoFloat(self, cmd, pos, rom):
        self.ParticleDat.extend(self.upt(pos, ">2f", 8))
        return 8

    def ThreeFloat(self, cmd, pos, rom):
        self.ParticleDat.extend(self.upt(pos, ">3f", 12))
        return 12

    def OneByte(self, cmd, pos, rom):
        self.ParticleDat.append(self.upt(pos, ">B", 1))
        return 1

    def TwoByte(self, cmd, pos, rom):
        self.ParticleDat.extend(self.upt(pos, ">2B", 2))
        return 2

    def FourByte(self, cmd, pos, rom):
        self.ParticleDat.extend(self.upt(pos, ">4B", 4))
        return 4

    def OneHalf(self, cmd, pos, rom):
        self.ParticleDat.append(self.upt(pos, ">H", 2))
        return 2

    def TwoHalf(self, cmd, pos, rom):
        self.ParticleDat.extend(self.upt(pos, ">2H", 4))
        return 4

    def OneByteOneFloat(self, cmd, pos, rom):
        self.ParticleDat.extend(self.upt(pos, ">Bf", 5))
        return 5


# particle tex data retrieval, each class is one particle texture
class TextureBin(BinProcess):
    def __init__(self, file, ptr):
        self.file = file
        self.dat = self.extract_dict(ptr, ParticleTex)
        offs = self.dat[6]
        siz = int(self.dat[3] * self.dat[4] * (4 * (2 ** self.dat[2])) / 8)
        # palettes. always 0x20 long??. Important that this cond is checked first
        if self.dat[1] == 2:  # CI fmt is defined as 2 in gbi
            self.bins = [(a, a + siz) for a in offs[: -int(len(offs) / 2)]]
            self.bins += [(a, a + 0x20) for a in offs[int(len(offs) / 2) :]]
        # mip maps
        elif self.dat[5] == 0:
            self.bins = [(a, a + siz) for a in offs[: -int(len(offs) / 2)]]
            self.bins += [(a, a + int(siz * 0.25)) for a in offs[int(len(offs) / 2) :]]
        else:
            self.bins = (*((a, a + siz) for a in offs),)  # what a syntax

    def Vestigial(self, start, rom):
        Tstart = self.dat[6][0]
        end = start + 0x18 + 4 * len(self.bins)
        if Tstart > end:
            amt = (Tstart - end) // 4
            Ex = self.upt(end, ">%dL" % (amt), amt * 4)
            self.dat.append(Ex)
