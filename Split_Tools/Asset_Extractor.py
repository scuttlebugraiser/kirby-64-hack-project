# using various tools located in their respectively named folders
# extract the binary assets and convert them to .c as necessary

# imports from python
import sys, struct, shutil, json
from pathlib import Path
from functools import partial
import multiprocessing as mp

# my tools
import gfx.SplitGeo as SplitGeo
import gfx.TexManage as TexManage
import misc.MiscSplit as MiscSplit
import audio.AudioSplit as AudioSplit
import anims.AnimSplit as AnimSplit
from Utility import *

# -------------------------------------------------------------------------------
# Pathing, file getters
# -------------------------------------------------------------------------------


def GetRom():
    Kirby = open("Kirby64.z64", "rb")
    Kirb = Kirby.read()
    return Kirb


def GetJS():
    jsPath = Path("gfx") / "TxDat" / "KirbTextures_FromGeo.json"
    if jsPath.exists():
        with open(jsPath, "r") as jsF:
            return json.load(jsF)
    else:
        return None


# -------------------------------------------------------------------------------
# Misc Extraction
# -------------------------------------------------------------------------------

# loop over one bank, and get all files of that type
def GetMiscBank(bank, Kirb: "binary ROM file"):
    p = mp.Pool(mp.cpu_count() - 1)
    indices = LoopOverBank(bank, Kirb, block_type="Misc")
    miscs = partial(GetMiscIndex, bank, Kirb)
    p.map(miscs, indices)


def GetMiscIndex(bank, Kirb: "binary ROM file", index):
    root_path = Path("assets") / "misc" / (f"bank_{bank}")
    binary = root_path / str(index) / "misc.bin"
    if binary.exists():
        return
    try:
        MiscSplit.GetSingleMisc(bank, index, Kirb, path=root_path / str(index))
    except:
        print(f"misc {bank} {index} unable to be exported")


# -------------------------------------------------------------------------------
# Geo Extraction
# -------------------------------------------------------------------------------

# loop over one bank, and get all files of that type
def GetGeoBank(bank, Kirb: "binary ROM file"):
    p = mp.Pool(mp.cpu_count() - 1)
    indices = LoopOverBank(bank, Kirb, block_type="Geo_Block")
    geo = partial(GetGeoIndex, bank, Kirb)
    p.map(geo, indices)


def GetGeoIndex(bank, Kirb: "binary ROM file", index):
    root_path = Path("assets") / "geo" / (f"bank_{bank}")
    binary = root_path / str(index) / "geo.bin"
    if binary.exists():
        return
    SplitGeo.GetSingleGeo(bank, index, Kirb, path=root_path / str(index))


# -------------------------------------------------------------------------------
# Texture Extraction
# -------------------------------------------------------------------------------

# export from the JS, unless the JS does not exist
def GetTexBank(bank, Kirb: "binary ROM file"):
    js = GetJS()
    root_path = Path("assets") / "image" / (f"bank_{bank}") / "{}"
    if js:
        TexManage.ExportBankjs(
            bank, Kirb, js=js[f"Bank_{bank}"], path=root_path, tx_name="image"
        )
    else:
        TexManage.CreateGeoJS(Kirb)  # creating the JS file also makes all geo file
        # and makes all tex file, therefore frontrunning geo creation


def GetTexBankBinaries(bank, Kirb: "binary ROM file"):
    p = mp.Pool(mp.cpu_count() - 1)
    indices = LoopOverBank(bank, Kirb, block_type="Image")
    img = partial(GetTextureBinary, bank, Kirb)
    p.map(img, indices)


def GetTextureBinary(bank, Kirb: "binary ROM file", index):
    root_path = Path("assets") / "image" / (f"bank_{bank}")
    binary = root_path / str(index) / "block.bin"
    if binary.exists():
        return
    ptrs = GetPointers(bank, index, "Image", Kirb)
    (root_path / str(index)).mkdir(exist_ok=True, parents=True)
    WriteBin(Kirb, *ptrs, str(root_path / str(index) / "block"))
    print(f"texture bin bank {bank} index {index} extracted")


# -------------------------------------------------------------------------------
# Anim Extraction
# -------------------------------------------------------------------------------


# loop over one bank, and get all files of that type
def GetAnimBank(bank, Kirb: "binary ROM file"):
    p = mp.Pool(mp.cpu_count() - 1)
    indices = LoopOverBank(bank, Kirb, block_type="Anim")
    anims = partial(GetAnimIndex, bank, Kirb)
    p.map(anims, indices)


def GetAnimIndex(bank, Kirb: "binary ROM file", index):
    root_path = Path("assets") / "anim" / (f"bank_{bank}")
    binary = root_path / str(index) / "anim.bin"
    if binary.exists():
        return
    AnimSplit.GetSingleAnim(bank, index, Kirb, path=root_path / str(index))


# -------------------------------------------------------------------------------
# Main
# -------------------------------------------------------------------------------


def ExportBank(Kirb, bank):
    # extract misc
    GetMiscBank(bank, Kirb)
    # extract geo
    GetGeoBank(bank, Kirb)
    # extract textures
    GetTexBankBinaries(bank, Kirb)
    GetTexBank(bank, Kirb)
    # extract anims
    GetAnimBank(bank, Kirb)


def main():
    # get rom
    Kirb = GetRom()
    # clear assets (optional)
    p = Path("assets")
    if p.exists():
        shutil.rmtree(Path("assets"))
    p.mkdir(exist_ok=True, parents=True)
    for i in range(8):
        # extract all
        ExportBank(Kirb, i)
    # extract audio data
    AudioSplit.main(Kirb)  # extracts all


if __name__ == "__main__":
    main()
