import struct
from dataclasses import dataclass

# -------------------------------------------------------------------------------
# Data / Variables
# -------------------------------------------------------------------------------

HS = 0x80057DB0

# -------------------------------------------------------------------------------
# Binary data reading/writing
# -------------------------------------------------------------------------------


def is_arr(val):
    if type(val) == str:
        return False
    if hasattr(val, "__iter__"):
        return True
    return False


# holds bank index, I need this to not be an iterable for generic funcs to work
class BankIndex:
    def __init__(self, bank, index):
        self.bank = bank
        self.index = index

    def __hash__(self):
        return hash((self.bank, self.index))

    # export the class as a tuple for convenience
    # used for textures outgoing to the json
    def exp(self):
        return (self.bank, self.index)

    def __str__(self):
        return f"BankIndex(bank={self.bank} index={self.index})"


# just a base class that holds some binary processing
# this class is focused on reading data
class BinProcess:
    # unpack type
    def upt(self, offset, type, length, iter=False):
        a = struct.unpack(type, self.file[offset : offset + length])
        if iter:
            return a
        if len(a) == 1:
            return a[0]
        else:
            return a

    @staticmethod
    def seg2phys(num):
        if num >> 24 == 4:
            return num & 0xFFFFFF
        else:
            return num

    # get pairs in range num or until stopping signal
    # used for tex scrolls and anims
    def get_BI_pairs(self, start, num=9999, stop=(0, 0)):
        pairs = []
        for i in range(num):
            BI = self.upt(start + 4 * i, ">HH", 4)
            pairs.append(BankIndex(*BI))
            if stop == BI:
                break
        return pairs

    # get list of pointers, stop at ARR_TERMINATOR
    def get_referece_list(self, start, stop=0):
        x = 0
        start = self.seg2phys(start)
        ref = []
        r = self.upt(start, ">L", 4)
        while r != stop:
            x += 4
            ref.append(r)
            r = self.upt(start + x, ">L", 4)
        ref.append(r)
        return ref

    # using a list of labels, find which symbol corresponds to pointer
    def get_symbol_from_locations(self, symbol_dict, pointer, size=8):
        # throw out non pointers
        if pointer == 0:
            return 0
        closest = None
        chosen_str = None
        for format_str, symbols in symbol_dict.items():
            for sym in symbols:
                if not closest:
                    closest = sym if sym < pointer else None
                    chosen_str = format_str
                    continue
                if sym > pointer:
                    continue
                if (pointer - sym) < (pointer - closest):
                    closest = sym
                    chosen_str = format_str
        if not closest:
            closest = min([sym for sym in symbols for symbols in symbol_dict.values()])
        return chosen_str.format(f"{closest:X}", f"[{(pointer - closest) // size}]")

    # turn dict into an array. usually to be fed into a named tuple
    def extract_dict(self, start, dict):
        a = []
        for k, v in dict.items():
            try:
                # if a function is used for member 4, then call with current results
                if callable(v[3]):
                    # variable length structs are always at the end, and should be arrays
                    # since upt sometimes is not a list and sometimes is, I will
                    # force this result to be a list
                    num = v[3](a)
                    a.append(
                        self.upt(start + k, v[0].format(num), v[2] * num, iter=True)
                    )
                else:
                    a.append(self.upt(start + k, v[0], v[2]))
            except:
                a.append(self.upt(start + k, v[0], v[2]))
        return a


# this class writes bin data out from within the class
class BinWrite:
    # when writing a struct that had a ptr, write the loc
    # do this by searching the class for ptr stored within the cls
    def WriteLocComment(self, file, dat):
        try:
            for k, v in self.__dict__.items():
                if v == dat:
                    name = k
                    break
            if hasattr(self, "feature"):
                # if it is a node cls, then add node num suffix
                if self.feature == "node":
                    name += f"_{self.index}"
            # just let it fail if it wasn't found
            for k, v in self.ptrs.items():
                if v == name:
                    loc = k
            file.write(f"//{name} - 0x{loc:X}\n")
        except:
            pass

    # write header file
    def write_header(self, file):
        # gather headers from child objects into this header
        for obj in self.child_symbols:
            ind = self.header.index(obj)
            self.header = [*self.header[:ind], *obj.header, *self.header[ind+1:]]
        for extern in self.header:
            file.write(extern)

    def add_header_child(self, child):
        if hasattr(child, "header"):
            self.child_symbols.append(child)
            self.header.append(child) # reserve spot
    
    # add header data
    def add_header_data(self, label, static=False):
        if hasattr(self, "header"):
            var_type = "extern"*(not static) + "static"*(static)
            self.header.append(f"{var_type} {label};\n")

    # format arr data
    def format_arr(self, vals, prefix = ""):
        buf = []
        for index, item in enumerate(vals):
            if type(item) == float:
                buf.append(str(item))
            elif type(item) == str:
                # puke emoji
                buf.append("{2}{0}{1}".format(prefix, item, "\n\t"*(index > 0)))
            else:
                buf.append(hex(item))
        return ', '.join(buf)

    # format arr ptr data into string
    def format_arr_symbols(self, arr, BI=None, symbols: dict = None):
        vals = []
        for item in arr:
            if value := symbols.get(item, None):
                vals.append(f"&{value}")
            else:
                vals.append("NULL")
        return f"{', '.join( vals )}"

    # format arr ptr data into string
    def format_arr_ptr(self, arr, BI=None, ptr_format="0x", prefix = ""):
        vals = []
        for item in arr:
            if item == 0x99999999 or item == (0x9999, 0x9999):
                vals.append(f"ARR_TERMINATOR")
            elif BI:
                vals.append("BANK_INDEX(0x{:X}, 0x{:X})".format(item.bank, item.index))
            elif item:
                # ptrs have to be in bank 4 (really applied to entry pts)
                if type(item) == int and item & 0x04000000 == 0x04000000:
                    vals.append(f"{ptr_format}{item:X}")
                # item may already have been converted to a str, acc for that case
                elif type(item) == str:
                    vals.append(f"{prefix}{item}")
                else:
                    vals.append(f"0x{item:X}")
            else:
                vals.append("NULL")
        return ',\n\t'.join(vals)

    # given an array, create a recursive set of lines until no more
    # iteration is possible
    def format_iter(self, arr, func, **kwargs):
        if is_arr(arr[0]):
            arr = [f"{{{self.format_iter(a, func, **kwargs)}}}" for a in arr]
        return func(arr, **kwargs)

    # write generic array, use recursion to unroll all loops
    def write_arr(self, file, name, arr, func, length=None, static=False, **kwargs):
        self.add_header_data(f"{name}[{self.write_truthy(length)}]", static=static)
        file.write(f"{'static '* bool(static)}{name}[{self.write_truthy(length)}] = {{\n\t")
        # use array formatter func
        file.write(self.format_iter(arr, func, **kwargs))
        file.write("\n};\n\n")

    # write if val exists else return nothing
    def write_truthy(self, val):
        if val:
            return val
        return ""

    # sort a dict by keys, useful for making sure DLs are in mem order
    # instead of being in referenced order
    def SortDict(self, dictionary):
        return {k: dictionary[k] for k in sorted(dictionary.keys())}

    # write a struct from a python dictionary
    def WriteDictStructArray(
        self,
        Data: list,
        Prototype_dict: dict,
        file: "filestream write",
        comment: str,
        name: str,
        symbols: dict = None,
        align: str = "",
        length: int = 0,
        static: bool = False
    ):
        self.WriteLocComment(file, Data)
        self.add_header_data(f"struct {name}[{length}]", static = static)
        file.write(f"{align + ' '* bool(align)}{'static '* bool(static)}struct {name}[{length}] = {{\n")
        for dat in Data:
            file.write("{")
            self.WriteStructContents(dat, Prototype_dict, file, comment, name, symbols=symbols, align=align, static=static)
            file.write("},\n")
        file.write("};\n\n")
    
    # write a struct from a python dictionary
    def WriteDictStruct(
        self,
        Data: list,
        Prototype_dict: dict,
        file: "filestream write",
        comment: str,
        name: str,
        symbols: dict = None,
        align: str = "",
        static: bool = False
    ):
        self.WriteLocComment(file, Data)
        self.add_header_data(f"struct {name}", static = static)
        file.write(f"{align + ' '* bool(align)}{'static '* bool(static)}struct {name} = {{\n")
        self.WriteStructContents(Data, Prototype_dict, file, comment, name, symbols=symbols, align=align, static=static)
        file.write("};\n\n")
    
    def WriteStructContents(
        self,
        Data: list,
        Prototype_dict: dict,
        file: "filestream write",
        comment: str,
        name: str,
        symbols: dict = None,
        align: str = "",
        static: bool = False
    ):
        for x, y, z in zip(Prototype_dict.values(), Data, Prototype_dict.keys()):
            # x is (data type, string name, is arr/ptr etc.)
            # y is the actual variable value
            # z is the struct hex offset
            try:
                arr = "arr" in x[2] and hasattr(y, "__iter__")
            except:
                arr = 0
            # use symbols if given the flag and given a symbol dict
            try:
                ptrs = "ptr" in x[2] and symbols
            except:
                ptrs = 0
            if ptrs:
                value = symbols.get(y)
                # ptr targets that are arrays don't need &
                # screen via no& in formatting dict
                ampersand = "&" * ("no&" not in x[2])
                # mutli dim arrays sometimes want a target, screen same as above
                index = "[0]" * ("[0]" in x[2])
                if value == "NULL":
                    file.write(f"\t/* 0x{z:X} {x[1]}*/\t{value},\n")
                # a tuple value includes the type or cast
                elif is_arr(value):
                    file.write(
                        f"\t/* 0x{z:X} {x[1]}*/\t{value [0]} {ampersand}{value[1]}{index},\n"
                    )
                elif value:
                    file.write(f"\t/* 0x{z:X} {x[1]}*/\t{ampersand}{value}{index},\n")
                else:
                    if arr:
                        value = ", ".join([f"&{symbols.get(a)}" for a in y])
                        file.write(f"\t/* 0x{z:X} {x[1]}*/\t{{{value}}},\n")
                    else:
                        file.write(f"\t/* 0x{z:X} {x[1]}*/\t0x{y:X},\n")
                continue
            if "f32" in x[0] and arr:
                value = ", ".join([f"{a}" for a in y])
                file.write(f"\t/* 0x{z:X} {x[1]}*/\t{{{value}}},\n")
                continue
            if "f32" in x[0]:
                file.write(f"\t/* 0x{z:X} {x[1]}*/\t{y},\n")
            elif arr:
                value = ", ".join([hex(a) for a in y])
                file.write(f"\t/* 0x{z:X} {x[1]}*/\t{{{value}}},\n")
            else:
                file.write(f"\t/* 0x{z:X} {x[1]}*/\t0x{y:X},\n")


# unpack word, unpack list, used in audio files cause old
UPW = lambda x: struct.unpack(">L", x)[0]
UPL = lambda x, y, z: struct.unpack(">%dB" % z, x[y : y + z])

# -------------------------------------------------------------------------------
# File I/O
# -------------------------------------------------------------------------------

# extract a portion of the rom into a binary file
def WriteBin(Kirb: "binary ROM file", start: "ptr", end: "ptr", name: "str"):
    dat = Kirb[start:end]
    f = open(name + ".bin", "wb")
    f.write(dat)
    f.close()


# -------------------------------------------------------------------------------
# Kirby ROM specific data getting
# -------------------------------------------------------------------------------

# search the file table for the file location of a BI pair and specific file type
def GetPointers(bank: int, index: int, DatType: str, Kirb: "binary ROM file"):
    BP = 0x000783D4  # bank pointer entry
    BPs = UPW(Kirb[BP + bank * 4 : BP + bank * 4 + 4]) - HS
    types = {"Geo_Block": 0, "Image": 2, "Anim": 4, "Misc": 6}
    off = types[DatType] * 4
    if DatType == "Geo_Block":
        scale = 2
    else:
        scale = 1
    Table = UPW(Kirb[BPs + off : BPs + off + 4]) - HS
    Offset = UPW(Kirb[BPs + off + 4 : BPs + off + 8])
    if Offset == 0xFFFFFFFF:
        Offset = 0
    Start = (
        UPW(Kirb[Table + index * 4 * scale : Table + index * 4 * scale + 4]) + Offset
    )
    End = (
        UPW(Kirb[Table + 4 + index * 4 * scale : Table + index * 4 * scale + 8])
        + Offset
    )
    return [Start, End]


# check if a block exists
def BlockExists(bank, index, Kirb: "binary ROM file", block_type="Geo_Block"):
    if not index:
        return None
    ptrs = GetPointers(bank, index, block_type, Kirb)
    return all(ptrs) and (ptrs[1] > ptrs[0]) and all([p < 0x80000000 for p in ptrs])


# a generator that loops the bank until it no longer exists
def LoopOverBank(bank, Kirb: "binary ROM file", start=1, block_type="Geo_Block"):
    x = start
    # check if block exists
    while BlockExists(bank, x, Kirb, block_type=block_type):
        yield x
        x += 1


# get length of bank of files, used when not iterating but doing comparison checks
def BankLen(bank, Kirb: "binary ROM file", start=1, block_type="Geo_Block"):
    x = start
    # check if block exists
    while BlockExists(bank, x, Kirb, block_type=block_type):
        x += 1
    return x
