import math


def vfov_to_hfov(vert_fov, aspect_ratio):
    v = math.radians(vert_fov)
    return math.degrees(2*math.atan(math.tan(v/2)*aspect_ratio))

def hfov_to_vfov(horiz_fov, aspect_ratio):
    h = math.radians(horiz_fov)
    return math.degrees(2*math.atan(math.tan(h/2)/aspect_ratio))


common = [15, 30, 33, 45, 56, 60, 66, 75, 90]



for c in common:
    print(f"vfov: {c} - hfov: {vfov_to_hfov(c, 300/172)}")

print("\n\n")

for c in common:
    print(f"vfov: {c} - hfov: {vfov_to_hfov(c, 320/240)}")