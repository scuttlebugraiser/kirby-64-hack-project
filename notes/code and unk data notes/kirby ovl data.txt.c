Kirby is in ovl3:
****************************************************************************
[Overlay Ranges
asm range:
0x80151100-0x80190310 (0x3F210)
data range:
0x80190310-0x80197bf0 (0x78E0)
bss range:
0x80197bf0-0x80198870 (0xC80)
]
****************************************************************************
[Important structs:
// 0x8012E7C0
// TODO: populate with actual values
struct KirbyState {
	u32 actionChange;			// 0x0
	u8 unk4;
	u8 action;					// 0x5
	u8 previousAction;			// 0x6
	u8 unk7;
	u8 unk8;
	u8 unk9;
	u8 unkA;
	u8 unkB;
	s8 abilityState;				// 0xC
	u8 unkD;
	u8 unkE;
	u8 unkF;
	u32 unk10;
	u8 unk14;
	u8 unk15;
	u8 unk16;
	u8 unk17;
	u8 unk18;
	u8 unk19;
	u8 unk1A;
	u8 unk1B;
	u32 floatTimer;				// 0x1C
	s32 turnDirection;			// 0x20
	u32 unk24;
	u32 unk28;
	u32 unk2C;
	u32 unk30;
	u32 isTurning;				// 0x34
	f32 unk38;
	u32 unk3C;
	u32 unk40;
	u32 unk44;
	u32 unk48;
	u32 unk4C;
	u32 unk50;
	u32 unk54;
	u32 unk58;
	u16 unk5C;
	u16 unk5E;
	u32 unk60;
	u16 unk64;
	u16 unk66;
	u16 unk68;
	u16 unk6A;
	u32 unk6C;
	u32 unk70;
	u32 unk74;
	f32 unk78;
	f32 unk7C;
	f32 unk80;
	u32 unk84;
	u32 unk88;
	u32 unk8C;
	u32 ability;					// 0x90
	u32 currentInhale;			// 0x94
	u32 firstInhale;				// 0x98
	u32 secondInhale;			// 0x9C
	u32 abilityInUse;			// 0xA0
	u32 unkA4;
	u32 inhaledEntityData;		// 0xA8 (First 4 bytes of Entity struct)
	u32 isHoldingEntity;		// 0xAC
	u8 isInhaling;				// 0xB0
	u8 isInhalingBlock;			// 0xB1
	u8 unkB2;
	u8 numberInhaling;			// 0xB3
	u16 numberInhaled;			// 0xB4
	u16 unkB6;
	u32 unkB8;
	u32 unkBC;
	u32 unkC0;
	u32 unkC4;
	u32 unkC8;
	u32 unkCC;
	u16 jumpHeight;				// 0xD0
	u16 isFullJump;				// 0xD2
	u16 damageType;				// 0xD4
	u16 damageFlashTimer;		// 0xD6
	u16 damagePaletteTimer;	// 0xD8
	u16 damagePaletteIndex;	// 0xDA
	u8 isTakingDamage;			// 0xDC
	u8 droppedAbility;			// 0xDD
	u16 abilityDropTimer;		// 0xDE
	u32 hpAfterDamage;			// 0xE0
	u32 ceilingCollisionNext;	// 0xE4
	u32 floorCollisionNext;	// 0xE8
	u32 rightCollisionNext;	// 0xEC
	u32 leftCollisionNext;		// 0xF0
	u32 levelCollisionFlags;	// 0xF4
	u32 verticalCollision;		// 0xF8
	u32 horizontalCollision;	// 0xFC
	u16 ceilingType;				// 0x100
	u16 floorType;				// 0x102
	u32 unk104;
	u16 unk108;
	u16 unk10A;
	u32 unk10C;
	u32 unk110;
	u32 unk114;
	u32 unk118;
	u32 unk11C;
	u32 unk120;
	u32 unk124;
	u32 unk128;
	u32 unk12C;
	u32 unk130;
	u32 unk134;
	u32 unk138;
	u32 unk13C;
	u32 unk140;
	f32 unk144;
	f32 unk148;
	f32 unk14C;
	u8 unk150;
	u8 unk151;
	u8 unk152;
	u8 unk153;
	u32 unk154;
	f32 unk158;
	u32 unk15C;
	u32 unk160;
	u32 unk164;
	u32 unk168;
	u32 unk16C;
	u32 unk170;
	Vec3f vel;					// 0x74
	u32 unk180;
	u32 unk184;
	u32 unk188;
	u32 unk18C;
	u32 unk190;
	u32 unk194;
	u32 unk198;
	u32 unk19C;
	u32 unk1A0;
	u32 unk1A4;
	struct PosState;					//PositionState goes from here until the end
};
gPosState:
Positions used for collision calculations and other stuff at 0x8012E968
struct PosState {
/*0x0*/  u32    Padding;
/*0x4*/  f32[3] Kirby_Ground_Pos; //Position of kirby at feet/back
/*0x10*/ f32[3] Char_Size; //this is prob for size of char (for dedede segments?)
/*0x1c*/ f32[3] Facing_Vec; //Constat with mag, last member seems to be total H dist or something
/*0X28*/ f32[3] Kirby_Head_Pos; //Position of kirby at head/face
/*0x34*/ f32[2] Kirby_Ground_Path; //the X/Z pos of kirby feet
/*0x40*/ f32[2] Kirby_Head_Path; //the X/Z pos of kirby head
/*0x44*/ f32[2] Kirby_Path_Y; //Y pos of head/feet respectively (assumption)
/*0x4c*/ u32    Col_Flags; //0x10000000 when grounded, 0 when not
/*0x50*/ u32    UnkPad; //0x14141414 constant
/*0x54*/ u32    UnkPad2; //0x14000000 constant
/*0x58*/ u32    VI_Timer; //the VI
};
]
****************************************************************************
[.bss documentation
0x80197bf0-0x80198870 (0xC80)

Seems to be allocated variables for each different kind of power, plus generic
data sections for all powerups:

[No specific data needed:
***********
Needle/Cutter,Rock,DoubleRock, Needle/Spark, Ice/Spark, Needle, Double Needle,
None
***********
cutter/fire, Cutter, Double Cutter:
ThrownObject
***********
Ice/Bomb, Spark, Spark/Bomb:
LightArray
***********
Inhale:
ThrownObject
UnkThing
***********
Fire/Bomb, Rock/Cutter:
UnkThing
***********
Bomb, Double Bomb:
BombState
***********
Fire/Needle:
BombState
ThrownObject
Fire Array
Leashed Object Pos
***********
Needle/Rock, Rock/Fire
ThrownObject
Leashed Object Pos
***********
]

[Specific Data:
***********
Single Fire:
u16: 0x80198830 = Length reduction timer
u16: 0x80198832 = Fire length
***********
Spark/Rock:
u32 0x8019843C: State
Vec3f 0x8019843C: Rock Pos?
***********
]

[Generic Data:
***********
Vec3f[2] 0x801982f8: Leashed Object Pos?
***********
Struct UnkThing 0x80198560[3]: Has to do with inhaled/thrown objects
***********
struct ThrownObject[4] 0x80197f64: Vars for thrown objects like firesword, cutter, bomb, etc.
***********
f32[64] 0x80198700: Light Array (An assumption) Lights something up
Always same values?
***********
f32[43] 0x8019843c: Fire Array (An assumption) Lights up in fire way
Always same values?
***********
u32 0x80198840: Control Status (-1 for normal, 0 for tracked on path)
***********
struct BombState[6] 0x80197bf0: State of each bomb projectile (can fire double bomb missles
twice at most while they're on screen, so its a max of 6 bombs at once (possible abuse?))
ends at 0x80197E00
***********
]
[.bss Strcuts

//Length 0x20
struct UnkThing {
Vec3f[2] KirbyPositions; //idk for sure really tbh
f32 unk;
u32 ptr2thing;
};

//length 0x58
struct ThrownObject {


};

//length 0x58
struct BombState {


};
]
]
]
****************************************************************************
[.data documentation
0x80190310-0x80197bf0 (0x78E0)

There is a struct of 0x68 bytes length repeated constantly at the beginning of
the section. Each struct refers to an action/animation. There are 42 of these
structs.

//length 0x68, names can be changed to whatever
struct PlayerAction {
u32 unk0;
u32 unk4;
Vec3f /* 0x8 */ PlayerPos; //struct is copied to ram and edited, .data is initialized but means nothing
Vec3f /* 0xC */ UnkSize;
/* u32 Unk20; Only in ram not in .data? */
u32 NearItem; //you are close to hitting something
u32 Unk24;
u32 *Unk28;
f32[3] /* 0x2C */HitPos; //Position of thing kirby is hitting
f32 PlayerRadius; //Size of kirby ball
Vec3f UnkFloats;
struct ActionNode UnkStruct; /* 0x48 */
};
//this follows the PlayerAction in .data but in ram its in a different spot
struct ActionNode {
struct PlayerAction *ThisAction; //pointer to the original struct
u32 Tangible; //if zero, kirby cannot be hit/collect items
u32 *Ptr24; //points to value in this struct
f32 Unk54;
u32 Unk58;
u32 Unk5C;
u32 Unk60;
u32 Unk64;
};

The array of PlayerAction structs lasts until 0x80191424.

After that is an array of floats thats 0xc0 long.

Following that are more KirbyAction structs.

These structs last until 0x801926d4, there are 46 of these structs.

Following that are tons of pointers, ints and floats in various sizes of arrays.

]
****************************************************************************