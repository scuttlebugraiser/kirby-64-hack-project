#Unk Offset
0x0000004C
#Mode
0x00000000
#Num Offsets
0x00000010
#Offsets to Start and Loop Offsets
0x00000050, 0x00000054, 0x00000058, 0x0000005C, 0x00000060, 0x00000064, 0x00000068, 0x0000006C, 0x000000CC, 0x00000124, 0x00000164, 0x000001DC, 0x00000224, 0x0000029C, 0x000002FC, 0x00000378, 
#Part Start Offsets
0x00000070, 0x000000D0, 0x00000128, 0x00000168, 0x000001E0, 0x00000228, 0x000002A0, 0x00000300, 


#Part 1 at 0x70
0x142B8000 (CMD:0A Goto Spot Block) (Scale:0) (Transform: T:(XZ) R:(XYZ), 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 
0x10100000 (CMD:08 Set Lerp 0 Block) (Scale:0) (Transform: T:(Y) , -16.00000, 
0x162B8018 (CMD:0B Goto Spot Non Block) (Scale:24) (Transform: T:(XZ) R:(XYZ), 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 
0x10100006 (CMD:08 Set Lerp 0 Block) (Scale:6) (Transform: T:(Y) , -18.00000, 
0x10100006 (CMD:08 Set Lerp 0 Block) (Scale:6) (Transform: T:(Y) , -16.00000, 
0x10100006 (CMD:08 Set Lerp 0 Block) (Scale:6) (Transform: T:(Y) , -18.00000, 
0x10100006 (CMD:08 Set Lerp 0 Block) (Scale:6) (Transform: T:(Y) , -16.00000, 
0x1C000000 (CMD:0E Loop) (Scale:0) (Transform: , 0x00000070, 



#Part 2 at 0xD0
0x11C00000 (CMD:08 Set Lerp 0 Block) (Scale:0) (Transform: S:(XYZ) , 1.00000, 1.00000, 1.00000, 
0x11C00006 (CMD:08 Set Lerp 0 Block) (Scale:6) (Transform: S:(XYZ) , 1.10000, 0.90000, 1.10000, 
0x11C00006 (CMD:08 Set Lerp 0 Block) (Scale:6) (Transform: S:(XYZ) , 1.00000, 1.00000, 1.00000, 
0x11C00006 (CMD:08 Set Lerp 0 Block) (Scale:6) (Transform: S:(XYZ) , 1.10000, 0.90000, 1.10000, 
0x11C00006 (CMD:08 Set Lerp 0 Block) (Scale:6) (Transform: S:(XYZ) , 1.00000, 1.00000, 1.00000, 
0x1C000000 (CMD:0E Loop) (Scale:0) (Transform: , 0x000000D0, 



#Part 3 at 0x128
0x15C38000 (CMD:0A Goto Spot Block) (Scale:0) (Transform: S:(XYZ) R:(XYZ), 0.00000, 0.00000, 0.00000, 1.00000, 1.00000, 1.00000, 
0x15C38018 (CMD:0A Goto Spot Block) (Scale:24) (Transform: S:(XYZ) R:(XYZ), 0.00000, 0.00000, 0.00000, 1.00000, 1.00000, 1.00000, 
0x1C000000 (CMD:0E Loop) (Scale:0) (Transform: , 0x00000128, 



#Part 4 at 0x168
0x10020000 (CMD:08 Set Lerp 0 Block) (Scale:0) (Transform: R:(X), 0.00000, 
0x0A018000 (CMD:05 Set Lerp Increment Block) (Scale:0) (Transform: R:(YZ), 0.00000, -0.11054, 0.00000, 0.03782, 
0x1202000C (CMD:09 Set Lerp 0 Non Block) (Scale:12) (Transform: R:(X), 0.00000, 
0x10018006 (CMD:08 Set Lerp 0 Block) (Scale:6) (Transform: R:(YZ), -0.52360, 0.17453, 
0x0A018006 (CMD:05 Set Lerp Increment Block) (Scale:6) (Transform: R:(YZ), 0.00000, 0.11054, 0.00000, -0.03782, 
0x10038006 (CMD:08 Set Lerp 0 Block) (Scale:6) (Transform: R:(XYZ), 1.39626, -0.69813, -0.34907, 
0x0C018006 (CMD:06 Set Lerp Increment Non Block) (Scale:6) (Transform: R:(YZ), 0.00000, -0.11054, 0.00000, 0.03782, 
0x10020006 (CMD:08 Set Lerp 0 Block) (Scale:6) (Transform: R:(X), 0.00000, 
0x1C000000 (CMD:0E Loop) (Scale:0) (Transform: , 0x00000168, 



#Part 5 at 0x1E0
0x14028000 (CMD:0A Goto Spot Block) (Scale:0) (Transform: R:(XZ), -0.34907, 0.00000, 
0x16028018 (CMD:0B Goto Spot Non Block) (Scale:24) (Transform: R:(XZ), -0.34907, 0.00000, 
0x06010000 (CMD:03 Lerp Block) (Scale:0) (Transform: R:(Y), 0.00000, 
0x06010006 (CMD:03 Lerp Block) (Scale:6) (Transform: R:(Y), 0.95993, 
0x06010006 (CMD:03 Lerp Block) (Scale:6) (Transform: R:(Y), 0.00000, 
0x06010006 (CMD:03 Lerp Block) (Scale:6) (Transform: R:(Y), -0.95993, 
0x06010006 (CMD:03 Lerp Block) (Scale:6) (Transform: R:(Y), 0.00000, 
0x1C000000 (CMD:0E Loop) (Scale:0) (Transform: , 0x000001E0, 



#Part 6 at 0x228
0x10020000 (CMD:08 Set Lerp 0 Block) (Scale:0) (Transform: R:(X), 0.00000, 
0x0A018000 (CMD:05 Set Lerp Increment Block) (Scale:0) (Transform: R:(YZ), 0.00000, 0.11054, 0.00000, 0.03782, 
0x10038006 (CMD:08 Set Lerp 0 Block) (Scale:6) (Transform: R:(XYZ), 1.39626, 0.69813, 0.34907, 
0x0C018006 (CMD:06 Set Lerp Increment Non Block) (Scale:6) (Transform: R:(YZ), 0.00000, -0.11054, 0.00000, -0.03782, 
0x10020006 (CMD:08 Set Lerp 0 Block) (Scale:6) (Transform: R:(X), 0.00000, 
0x1202000C (CMD:09 Set Lerp 0 Non Block) (Scale:12) (Transform: R:(X), 0.00000, 
0x10018006 (CMD:08 Set Lerp 0 Block) (Scale:6) (Transform: R:(YZ), -0.52360, -0.17453, 
0x0A018006 (CMD:05 Set Lerp Increment Block) (Scale:6) (Transform: R:(YZ), 0.00000, 0.11054, 0.00000, 0.03782, 
0x1C000000 (CMD:0E Loop) (Scale:0) (Transform: , 0x00000228, 



#Part 7 at 0x2A0
0x143B0000 (CMD:0A Goto Spot Block) (Scale:0) (Transform: T:(XYZ) R:(XY), 0.00000, 0.00000, -10.00000, 20.00000, 0.00000, 
0x06008000 (CMD:03 Lerp Block) (Scale:0) (Transform: R:(Z), -1.04720, 
0x163B0018 (CMD:0B Goto Spot Non Block) (Scale:24) (Transform: T:(XYZ) R:(XY), 0.00000, 0.00000, -10.00000, 20.00000, 0.00000, 
0x06008006 (CMD:03 Lerp Block) (Scale:6) (Transform: R:(Z), -1.57080, 
0x06008006 (CMD:03 Lerp Block) (Scale:6) (Transform: R:(Z), -1.04720, 
0x06008006 (CMD:03 Lerp Block) (Scale:6) (Transform: R:(Z), -0.52360, 
0x06008006 (CMD:03 Lerp Block) (Scale:6) (Transform: R:(Z), -1.04720, 
0x1C000000 (CMD:0E Loop) (Scale:0) (Transform: , 0x000002A0, 



#Part 8 at 0x300
0x14390000 (CMD:0A Goto Spot Block) (Scale:0) (Transform: T:(XYZ) R:(Y), 0.00000, 10.00000, 20.00000, 0.00000, 
0x10020000 (CMD:08 Set Lerp 0 Block) (Scale:0) (Transform: R:(X), 0.00000, 
0x06008000 (CMD:03 Lerp Block) (Scale:0) (Transform: R:(Z), -1.04720, 
0x08008006 (CMD:04 Lerp Non Block) (Scale:6) (Transform: R:(Z), 0xBF060A92, 
0x16390018 (CMD:0B Goto Spot Non Block) (Scale:24) (Transform: T:(XYZ) R:(Y), 0.00000, 10.00000, 20.00000, 0.00000, 
0x0A020006 (CMD:05 Set Lerp Increment Block) (Scale:6) (Transform: R:(X), 0.00000, 0.00000, 
0x08008006 (CMD:04 Lerp Non Block) (Scale:6) (Transform: R:(Z), 0xBF860A92, 
0x10020006 (CMD:08 Set Lerp 0 Block) (Scale:6) (Transform: R:(X), 0.00000, 
0x1202000C (CMD:09 Set Lerp 0 Non Block) (Scale:12) (Transform: R:(X), 0.00000, 
0x06008006 (CMD:03 Lerp Block) (Scale:6) (Transform: R:(Z), -1.57080, 
0x06008006 (CMD:03 Lerp Block) (Scale:6) (Transform: R:(Z), -1.04720, 
0x1C000000 (CMD:0E Loop) (Scale:0) (Transform: , 0x00000300, 


