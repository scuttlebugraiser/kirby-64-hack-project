typedef unsigned char   undefined;

typedef unsigned char    bool;
typedef unsigned char    byte;
typedef unsigned int    dword;
float2
typedef long double    longdouble;
typedef unsigned int    uint;
typedef unsigned long long    ulonglong;
typedef unsigned char    undefined1;
typedef unsigned short    undefined2;
typedef unsigned int    undefined4;
typedef unsigned long long    undefined8;
typedef unsigned short    ushort;
typedef unsigned short    word;
typedef struct uvScrollData uvScrollData, *PuvScrollData;

typedef struct color color, *Pcolor;

struct color {
    byte r;
    byte g;
    byte b;
    byte a;
};

struct uvScrollData {
    ushort field_0x0;
    byte fmt1;
    byte siz1;
    uint * textures;
    ushort stretch;
    ushort sharedOffset;
    ushort t0_w;
    ushort t0_h;
    int halve;
    float t0_xShift;
    float t0_yShift;
    float xScale;
    float yScale;
    float field_0x24;
    float field_0x28;
    pointer palettes;
    ushort flags;
    byte fmt2;
    byte siz2;
    ushort w2;
    ushort h2;
    ushort t1_w;
    ushort t1_h;
    float t1_xShift;
    float t1_yShift;
    float field_0x44;
    float field_0x48;
    int field_0x4c;
    struct color prim;
    byte primLODFrac;
    byte field_0x55;
    byte field_0x56;
    undefined field_0x57;
    struct color env;
    struct color blend;
    struct color light1;
    struct color light2;
    int field_0x68;
    int field_0x6c;
    int field_0x70;
    int field_0x74;
};

