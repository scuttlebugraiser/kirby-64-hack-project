[Level struct:
0x800be500[
0X0 U32:WORLD
0X4 U32:LEVEL
0X8 U32:AREA
0XC U32:NODE
]
MAIN STAGE TABLE PTR:
PTR IS GOTTEN BY FUNCTION 0x800F78E4
NEW LEVEL IS LOADED FOR EACH NEW AREA
ptr to level struct is loaded from WORLD*48+LEVEL*4 ->0x800d1f98(table of ptrs)+AREA*36
0x7A1E8
Basically each level has a struct of areas inside the main stage table struct
ptr to area struct is stored at 0x801290d8
]
****************************************************************************
[Main stage table

 The table at 0x000783F4 (0x800D01A4) controls how each area of a stage is loaded.
 For each stage there is an array of area structs. Each list is terminated with an empty struct.
 
 Area structure
 
  0x00: list_index geo_block_a  List-index of primary Geometry block
  0x04: list_index geo_block_b  List-index of secondary Geometry block
  0x08: u16        skybox_id    Skybox ID
  0x0A: u16		   color		BG color (loaded from color table at 800D478C)
  0x0C: int        music_id     Music ID
  0x10: bank_index level_block  Bank-index of Level settings block 
  0x14: Death Cutscene			How far the camera follows kirby as he falls
  0x16: Level Type				Int flag Assuming.
  0x18: bank_index dust_block   Bank-index of Dust particle settings
  0x1C: bank_index dust_img     Bank-index of Dust particle image
  0x20: char*      area_name    Pointer to dev ASCII level name
  
  See Resource addressing systems for details on bank_index and list_index.
  
Level Type Flags:
0: regular gameplay
1: DDD/Adeleine Boss (1-2-6, 1-3-9)
2: End of World Boss
3: End of stage (carpet with prizes)
4: Ride water on log/Log Flume(3-2-4)
5: Ride sled(5-1-3)
6: Minecart stage (4-2-2)
7: unused
8: no direct control of kirby/Ride DDD (4-4-3, 2-2-6, 5-4-3)
9: 0-2 Boss (7-1-3)
10: MiniBoss (enlarged enemy)
(game checks if this value is over 10 if so it breaks threads)
11: Crash (unused in game)
12: Crash (unused in game)
13: Crash (unused in game)
14: Crash (unused in game)

Extra notes:
10 just allows you to place a boss type entity
2 is the same, you must place boss enemy. Requires looping
path. Number of paths and shape depends on boss.
May also need other specific things to work (or has hardcoded stuff).
8 disables normal kirby, you need to place entity, or do something
so that DDD will run in and help you.
9 will go directly into 02 but crash if you don't have specific conditions
setup
]
****************************************************************************
[LOADING BLOCKS


level settings block:
bank is retrieved using function 0x800A9AA8
from addr at 0x18 ->0x800d00a4+index*4 the start and end ROM ptrs are gotten
AREA block addr is stored to 0x801290dc

geometry block:
function 0x800A9864 loads the data from the bank index pair.
ram addr of geometry block is stored at 0x800d8c80+index*4
index is stored to 0x800E02d0+(0x0->0x0->8004a7c4)*4
Basically some **var or some shit
geo block is loaded into segment 04.
]
****************************************************************************
[LEVEL SETTINGS BLOCK

This block is basically always loaded into 0x802F4A10 in memory

Main Header:

   0x00: u32 Pointer to Collision Header
   0x04: u32 Pointer to Camera&Path Header
   0x08: u32 Pointer to Entity list
   

Basic structure
 
	1. Main Header
	2. Vertices (s16 triplets)
	3. Triangles (20 bytes each)
	4. Surface Normals
	5. Triangle Groups/Cells
	6. Surface Normals Groups/Cells
	7. Water Normals (Optional)
	8. Water Quads (Optional)
	9. Destructable Geo Groups
	10. Destructable Geo Indices
	11. Collision Header (referred to by Header[0])
	12. Path nodes
	13. Camera Nodes
	14. Node Connections
	15. Node Traversal
	16. Node Distances
	17. Path Node Headers
	18. Node Header (referred to by Header[1])
	19. Entity List (referred to by Header[2])


# Collision Header:
	# 0x0: Triangles Offset
	# 0x4: Number of Triangles
	# 0x8: Vertices offset
	# 0xC: Number of Vertices
	# 0x10: Triangle Normals
	# 0x14: Number of Norms
	# 0x18: Triangle Groups (cells) Offset
	# 0x1C: Number of Triangle Groups
	# 0x20: Triangle Norms Cells
	# 0x24: Num Tri Norm Cells
	# 0x28: Norm Cells Root
	# 0x2C: Destructable Geometry Groups
	# 0x30: Destructable Geometry Indices
	# 0x34: Water Data
	# 0x38: Water Data number
	# 0x3C: Water Normals offset
	# 0x40: Water Normals number 
	
//vertices:
s16 triplets.
Ends with 9999 if padding is needed


Tri Struct:

ALWAYS BEGINS WITH
0000 0001 0002 0003 0004 0005 0006 0007 0008 0009

0x0:	u16	Vertex[3]
0x6:	u16	Normal Number
0x8:	u16	collision type (1 forward norm, 2 back norm, 4 no shadow, 8 non solid)
0xA:	u16	Destructable Group Index
0xC:	u16	Particles when broken (hammer break)
0xE:	u16	Stop Kirby from going forward
0x10:	s16	Amount to move kirby while on certain col types/Break Condition
0x12:	u16	col type 2

//COL TYPES2 (you can find them easily around 0x800FE3F8. See 0x12 load for col type kirby is over)
0==floor
1==wall ladder
2==rope
3==death floor
4==Platform you can jump through/Fall Through
5==?
6==Lava
7==?
8==warp
9==STAR BLOCK/Breakable Blocks
10==?
13==Breakable Ceiling/Breaks when launched through
16==DEDEDE hammer break
18==move kirby backwards while on top
19==move kirby forwards while on top
20==Platform with custom movement (seems to be in some sort of object list separate from normal level geometry)
All the ones used in kirby in total (levels only)
[0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 13, 14, 16, 18, 19, 20]

Break Conditions:
Same as powerup number

//tri normals

Triangle normals are stored as a F32 array of 4 values. The first three values are the surface normals while the last is the origin offset. Together the normals should solve the plane equation (see:link).

The list of normals always starts with (-1,-2,-3,-4). The actual value of the first set of normal is not known to have any significance. Redundant normals are not repeated, but are just referenced multiple times (e.g. two triangles with norm Y==1 and offset==0 share their normal).


//triangle cells.
starts with 8192
ends with 9999
is a u16 used to reference the triangle number.
msb is set to true for looping purposes, loops over connected tris (see 0x6)


//tri normal groups

The normal groups is a list that matches each normal to a triangle. It starts with (0x0,0x1,0x2,0x3) then has one cell for each tri normal.

struct:
0x0: normal index
0x2: Left Child
0x4: Right Child
0x6: tri cell

Each cell is connected to a group of tri cells. Its linked together
with other triangles based on Node?


//Water Quads
Starts with 8 bytes of data, followed by four floats for a bounding box.

First the bounding box checks to see if kirby is in range, then it checks the normals. This is so it doesn't have to check a large number of normals which is more expensive than a box col check.

It has the following structure.
Water_Quads = {
0x0: ('>H','Number Normals',2),
0x2: ('>H','Normals Array Offset',2),
0x4: ('>B','Water Box Active',1),
0x5: ('>B','Activate Water Flow',1),
0x6: ('>B','Water Flow Direction',1),
0x7: ('>B','Water Flow Speed',1),
0x8: ('>f','Pos1',4),
0xC: ('>f','Pos2',4),
0x10: ('>f','Pos3',4),
0x14: ('>f','Pos4',4)
}


//Water Floats. Works exactly like floats for other collision triangles.
Lead by a (1,2,3,4) the followed by (3*norms,origin offset).
Follows all the conventions for normals that you'd expect, including n * r = -offset.



//Destructable groups

comes in three u16s
1- Number of times it loops in list
2- Index inside Dustructable indices list (0x30 in col header)
3- Index of Layout of destroyed object.

//Destructable indices list
Each Index is a half that points to the triangle that is being destroyed.

Basic idea is that 0x2C contains the index inside the tri index list and length/num of triangles being deleted.
Then in 0x30, there is a list of all the triangles that get destroyed.

The destructable group to reference is found from 0xA in the Tri Struct

//CAMERA&PATH
referred to by Main Header[1]

Paths Header

	0x0 // number of Path&Camera Nodes
	0x4 // offsets to path nodes
	0x8 // offset 65C [string of bytes?]
	0xC // offset 668 [a few floats?]

Pathing:
Each section defined by 0x0 in the paths header contains a 3x3 matrix of floats, a Bounds Definition of floats and a footer with offsets.

3xN matrix:
Defines the positions kirby should have relative to his level H position.
0x0:	X change/H Change
0x4:	Y change/H Change?
0x8:	Z Change/H Change


0xC:	X change/H Change
0x10:	Y change/H Change?
0x14:	Z Change/H Change

0x18:	X change/H Change
0x1C:	Y change/H Change?
0x20:	Z Change/H Change

The number of points N is extended by the flag in the footer
Since its always 0x0 or 0x200 depending on the unk section it is only ever extended by 2.


Bounds Definition:
Defines the Percentages associated with each bound. Is a Nx1 matrix.
e.g. 0, .75, 1 will make the "center bound" actually be equivalent to the 75% boundary. This has the result of making kirby walk slower because his H spd is actually covering more X distance despite being the same spatial distance.
Using a value greater than 1 will cause kirby to warp to the next Path Node/Struct when 1 is reached.

0x0:	0 H%
0x4:	1/N H%
0x8:	2/N H% 
....


You should use this to give the illusion that kirby is still walking the same speed as he travels farther into the background.

Footer:
0x0: int 	Has_Curl - Flag read 0x14 (0x200 to check)
0x2: u16 	Number of sections in node
0x4: F32	Force (Constant H speed?)//used in 3-4-2 only, seems to have some condition or be hardcoded? (maybe col type)
0x8: ptr	*Path_Matrix
0xC: F32	Node Length / 10
0x10: ptr	*Path_Bounds
0x14: ptr	*Path_Curl


Path_Curl:
Seems to be a struct of 5 floats. Seen in 4:1-2.
Changing the values to something wrong makes the game crash, likely from some sort of div by zero or NaN operation.
The number of points is the num pts in the footer minus 1.
Adding curl gives the path matrix two extra points (can be varied maybe? but never done in game).
In addition to giving extra points, it seems these extra points act like handles on a curve.
The first and last point at the handles, and the middle points all act like the normal path matrix points.


CAMERA&Kirby Settings:
Each Path section/node has an equivalent camera section.
The struct is as follows.
First is a struct that determines some specifics to kirby's graphics as kirby enters the node
Certain camera values are set to 9999 or -9999 when unused, they will default to kirby's
position or something else depending on the camera modes.


Entering Actions:
When kirby enters a level, he has an initial action, this value will set that action, the destination nodes value is used.
This means if I am entering area 2, it will use the value on area 2 to set initial action. Entering actions is in the Kirby Node offset 0x2.
Byte 1:
used to set where you enter on the node
//0 - start of node (default)
//1 - end of node
//2 - appear at start of node
//3 - appear at end of node
//4 - start of node (same as 0 onwards)
Byte 2:
used to set the action
//0 - walk until end of node
//1 - stand still
//2 - jump in place
//3 - jump forward
//4 - ?
//5 - climb wall (does it even if no ladder exists) / potential to crash (skips to a different level??)
//6 - climb down wall (same as above)
//7 - climb rope (``)
//8 - climb down rope(``)
//9 - walk?
//A - jump?
//B - land from air
//C - standing (defaults onwards)

Warps:
the level you are going to. If the dest node doesn't exist, game crashes.
There are some conditional values here, like 31 (used in warp in ado fight)
that possibly stop progression until boss or shard is collected.

Kirby Settings = 

0x0:	u8[2]	[Node number,padding]
0x2:	u16		Entering Actio
0x4:	u8[4]	warp destination (world,level,area,node)
0x8:	u8		unused
0x9:	u8[3]	Shading
0xC:	u16		unused //Always zero, maybe old code.
0xE:	u16		flags (0x1 = warp, 0x10 = read 0x10)
0x10:	u16		opt 1//Read if flag&0x10
0x12:	u16		opt 2//Read if flag&0x10 & opt 1>0
0x14:	float	optional 3 //only used in 5-5-1
0x18:	float	optional 4 //only used in 5-5-1
0x1C:	unused //unused

Camera Data = 
0x0:	u16		Profile View //makes cam always at kirby's profile
0x1:	u16		10 always //I'm pretty sure this does nothing
0x2:	u8		Lock H pos //follows kirby if not
0x3:	u8		Lock Y pos //follows if not
0x4:	u8		Lock Z pos //follows if not
0x5:	u8		unused
0x6:	u8		Not Camera Pan Phi Above/Below
0x7:	u8		Not Camera Pan Phi Below //doesn't pan up
0x8:	u8		Camera Pan Theta (from cam to kirby)
0x9:	u8		unused2 //useless
0xA:	u16		unk5 (FOV?) //basically useless
0xC:	f32		Focus X pos (Stays on kirby if 9999)
0x10:	f32		Focus Y pos (Stays on kirby if 9999)
0x14:	f32		Focus Z pos (Stays on kirby if 9999)
0x18:	f32		Near Clip Plane
0x1C:	f32		Far Clip Plane
0x20:	f32[2]	Cam Phi Rot //for when following kirby
0x28:	f32[2]	Cam Theta Rot (from kirby to camera) //for when following kirby, Yaw
0x30:	f32[2]	Cam Radius //for when following kirby
0x38:	f32[2]	FOV pair //always
0x40:	f32[2]	Cam Y offset //for when following kirby, Y offset
0x48:	f32[2]	Cam X Pos Lock Bounds //Locks if -9999,9999, In range of these values it follows kirby, outside it locks at bound
0x50:	f32[2]	Cam Y Pos Lock Bounds //Locks if -9999,9999, In range of these values it follows kirby, outside it locks at bound
0x58:	f32[2]	Cam Z Pos Lock Bounds //Locks if -9999,9999, In range of these values it follows kirby, outside it locks at bound
0x60:	f32[2]	Cam Yaw Lock Bounds //Locks if -9999,9999, In range of these values it follows kirby, outside it locks at bound
0x68:	f32[2]	Cam Pitch Lock Bounds //Locks if -9999,9999, In range of these values it follows kirby, outside it locks at bound


Node Connecters:
This section follows the same number of nodes/struct convention as the previous two sections.

This section connects the nodes together. When exiting a node, this tells the game which node to go to next, this means the nodes aren't necessarily in H pos order.

0x0:	u8	Stop kirby from going backwards
0x1:	u8	unused
0x2:	u8	next node(prev node if second section is present)
0x3:	u8	Don't stop kirby from going forwards (only when single node)

(optional)
0x4:	u8	Don't stop kirby from going forwards
0x5:	u8	unused
0x6:	u8	next node
0x7:	u8	unused

This repeats for the number of connected nodes, which is max of 2.

The first is always the one used when walking to the left, the second when going right.
If the same node is connected to when going forward and back, only one node is present.

Next Section:
Node Traversal & Distances
Compares node lengths and positioning for interactions with things in different nodes.
Node Traversal is a 2d list of indices into the Node Distances array. If the first member of the float array
is zero, then a different access to the array occurs.

Array is accessed in numerical node order, and each node has its own traversal array.
The values in the array correspond to a float, that tells the game the distance from
the current node, to the dest node.
The direction is given in traversals by making the number negative. Distances is always positive

Ex. member [0][1] should mean the first compared is node zero, the second is node 1. If the second node is in front, it should be indexed with & 0x80
If the value it references is the 2nd dist, then the value will be 0x2, or 0x82.

Read by function 0x800F9828


Path&Camera Offsets:

This section holds the offsets for each node.
For every node, this is the structure.

0x0:	ptr		camera&kirb offset
0x4:	ptr		path footer offset
0x8:	ptr		node connector offset
0xC:	u16		number of connected nodes
0xE:	u16		Self Connected

A self connected loop seems to still warp even if you have a stop
kirby byte set.

Entities

   0x00: u8 Node_Num
   0x01: u8 entity_bank
   0x02: u8 entity_id
   0x03: u8 Action
   0x04: u8 Respawn Flag
   0x05: u8 Spawn Flag
   0x06: u16 Eeprom Check
   0x08: float 3f Pos
   0x14: float 3f Rotation
   0x20: float 3f Scale

0x4 Flags:
	0x1 - spawn in BG (ids [77, 64, 63, 54, 33, 28, 5, 2])
	0x2 - ? (ids [33, 8])
	0x4+ (no ents with this flag)

0x5 Flags:
	0x1 - Spawn where placed, not on path (also used for spawning underwater)
	0x2 - unk
	0x4 - 
	0x8 - 
	0x10 - 
	0x20 - 

EEP:
	this determines where an ent gets its spawn check vs the eeprom
	it does not actually determine where the saves are stored, saves
	are determined by order of shard appearance

]
***************************************************************************
[SCREEN SPACE & CAMERA & SKYBOXES

The n64's screen size is 320x240
During gameplay, there is a 10 px border on the left/right/top
and a 12 px border on the bottom, though this gets cutoff a bit on angrylion?? so idk exact amounts
(6 L, 9 T, 6 R, 12 B on angrylion and total res 313x237)
The HUD is placed at the bottom and is 45 px high and 299 px wide.
This results in the game size being 300x172(173?) (AR 1.744 or 75:43).

Camera settings are declared per node. If you have a pair, the value interpolates across kirby's traversal
of that node, this is the struct that determines camera data:

Camera Data = 
0x0:	u16		Profile View //makes cam always at kirby's profile
0x1:	u16		10 always //I'm pretty sure this does nothing
0x2:	u8		Lock H pos //follows kirby if not
0x3:	u8		Lock Y pos //follows if not
0x4:	u8		Lock Z pos //follows if not
0x5:	u8		unused
0x6:	u8		Not Camera Pan Phi Above/Below
0x7:	u8		Not Camera Pan Phi Below //doesn't pan up
0x8:	u8		Camera Pan Theta (from cam to kirby)
0x9:	u8		unused2 //useless
0xA:	u16		unk5 (FOV?) //basically useless
0xC:	f32		Focus X pos (Stays on kirby if 9999)
0x10:	f32		Focus Y pos (Stays on kirby if 9999)
0x14:	f32		Focus Z pos (Stays on kirby if 9999)
0x18:	f32		Near Clip Plane
0x1C:	f32		Far Clip Plane
0x20:	f32[2]	Cam Phi Rot //for when following kirby
0x28:	f32[2]	Cam Theta Rot (from kirby to camera) //for when following kirby, Yaw
0x30:	f32[2]	Cam Radius //for when following kirby
0x38:	f32[2]	FOV pair //always
0x40:	f32[2]	Cam Y offset //for when following kirby, Y offset
0x48:	f32[2]	Cam X Pos Lock Bounds //Locks if -9999,9999, In range of these values it follows kirby, outside it locks at bound
0x50:	f32[2]	Cam Y Pos Lock Bounds //Locks if -9999,9999, In range of these values it follows kirby, outside it locks at bound
0x58:	f32[2]	Cam Z Pos Lock Bounds //Locks if -9999,9999, In range of these values it follows kirby, outside it locks at bound
0x60:	f32[2]	Cam Yaw Lock Bounds //Locks if -9999,9999, In range of these values it follows kirby, outside it locks at bound
0x68:	f32[2]	Cam Pitch Lock Bounds //Locks if -9999,9999, In range of these values it follows kirby, outside it locks at bound

Locks refer to using absolute coordinates instead of following kirby. The absolute coordinates are defined by X/Y/Z lock bounds.
If you are following kirby (not locked), then where the camera position goes is based on the circular coordinates radius, theta and phi.
Radius is distance from kirby to camera, theta is the yaw angle from the front of kirby (his face) to the camera (90 puts you sideways) and phi is the vertical
angle (pitch) starting from pointing up and going down (120 is a good looking down from slightly above angle).
The values used for yaw/pitch are different if the camera is locked, this is because you are using absolute coords, so your coordinate system origin is no longer kirby.
Y offset just vertically offsets the camera, this works the same always.
You can pan the camera as kirby moves to forward reference his position. This works the same whether or not the camera is locked.

To calculate position while locked, use these formulas.
Y = offset - cos(phi)*radius
X = -sin(phi)*radius

For the focus, or camera target, you can either focus on kirby, or use an absolute position (focus X/Y/Z pos in struct).
Regardless of your choice, this option only works when not locked.

FOV is vertical FOV (as in all N64 games). To convert FOVS use the following formulas (H/V are shorthand for FOV in radians).

V = 2*arctan(tan(H/2)*h/w)
H = 2*arctan(tan(V/2)*w/h)

Here are some common conversions.
75:43 AR
vfov: 15 - hfov: 25.86487060640664
vfov: 30 - hfov: 50.098376002453946
vfov: 33 - hfov: 54.64640581022353
vfov: 45 - hfov: 71.6936261483083
vfov: 56 - hfov: 85.68569285305989
vfov: 60 - hfov: 90.4000274892647
vfov: 66 - hfov: 97.12027044534624
vfov: 75 - hfov: 106.46711234628165
vfov: 90 - hfov: 120.34583069105051

4:3 AR
vfov: 15 - hfov: 19.912155287310526
vfov: 30 - hfov: 39.320120804165086
vfov: 33 - hfov: 43.103214987081124
vfov: 45 - hfov: 57.82240163683314
vfov: 56 - hfov: 70.66915659851958
vfov: 60 - hfov: 75.17817893794988
vfov: 66 - hfov: 81.77705728044812
vfov: 75 - hfov: 91.30851179869055
vfov: 90 - hfov: 106.26020470831195

skybox list and the screen coords they take up
skyboxes are affected by the camera angle, but not Y position or radius.
skyboxes are treated as if they have a depth of 1, so their Y coordinate change is
equal to 86+172*(90-phi)/fov, or in other words
the skybox moves across the screen in a linear proportion phi/fov as you angle the camera.

it is possible that different layers of the skybox move at different rates, but idk

0x0: 'USE 0xA color ID'
0x1: 'White Molding Pattern',
0x2: 'Green Hills w/ Clouds 1',
0x3: 'Green Hills w/ Clouds 2',
0x4: 'Green Hills w/ Clouds 3',
0x5: 'Green Hills w/ Clouds 4',
0x6: 'Green Hills w/ Clouds 5',
0x7: 'Orange Tree Leaves Narrow',
0x8: 'Green Hills w/ Clouds 6',
0x9: 'Green Hills w/ Clouds 7 (Broken)',
0xA: 'Trees Blue Mosaic Sky 1',
0xB: 'Trees Blue Mosaic Sky 2',
0xC: 'Blue Mosaic Sky',
0xD: 'Large Trees Yellow BG'
0xE: 'Sand Dunes Blue Sky 1',
0xF: 'Sand Dunes Blue Sky 2',
0x10: 'Blue Sky Gradient 1',
0x11: 'Sand Dunes Blue Sky 3',
0x12: 'Ice Mountains Light Blue Sky 1',
0x13: 'Ice Mountains Light Blue Sky 2',
0x14: 'Sand Dunes Blue Sky Clouds',
0x15: 'Blue Sky Clouds 1',
0x16: 'Blue Sky Clouds 2',
0x17: 'Yellow BG Dark Holes',
0x18: 'Above Clouds Light Blue Sky 1',
0x19: 'Above Clouds Light Blue Sky 2',
0x1A: 'Green Tree Tops 1',
0X1B: 'Just Tree Trunks',
0x1C: 'Green Tree Tops 2',
0x1D: 'Green Tree Tops 3',
0x1E: 'Green Tree Tops and Tree Trunks',
0x1F: 'Scrolling Clouds Blue Sky 1',
0x20: 'Scrolling Clouds Blue Sky 2',
0x21: 'Scrolling Clouds Blue Sky 3',
0x22: 'Scrolling Clouds Blue Sky 4',
0x23: 'Purple Blue Sky Gradient 1',
0x24: 'Purple Blue Sky Gradient 2',
0x25: 'USE 0xA color ID',
0x26: 'Scrolling Clouds USE 0xA color ID 1',
0x27: 'Scrolling Clouds USE 0xA color ID 1',
0x28: 'USE 0xA color ID',
0x29: 'Palm Trees USE 0xA color ID',
0x2A: 'USE 0xA color ID',
0x2B: 'Tree Trunks Jungle Foliage',
0x2C: 'Static Clouds 1',
0x2D: 'Static Clouds 2',
0x2E: 'Static Clouds 3',
0x2F: 'Static Clouds 4',
0x30: 'Static Clouds 5',
0x31: 'Red Sky Gradient 1',
0x32: 'Grey Sky Gradient 1',
0x33: 'Grey Sky Gradient 2',
0x34: 'USE 0xA color ID',
0x35: 'Grey Sky Gradient 3',
0x36: 'Scrolling Eggs USE 0xA color ID 1',
0x37: 'USE 0xA color ID',
0x38: 'Scrolling Eggs USE 0xA color ID 2',
0x39: 'USE 0xA color ID',
0x3A: 'Scrolling Blue Eggs USE 0xA color ID',
0x3B: 'Scrolling Eggs USE 0xA color ID 3',
0x3C: 'Colorful Blocks and Hills 1',
0x3D: 'Colorful Blocks and Hills 2',
0x3E: 'Colorful Blocks and Hills 3',
0x3F: 'Grey Sky Gradient 4',
0x40: 'Black w/ MM vignette gfx',
0x41: 'Purple w/ Silhouette Skyscrapers'
0x42: 'Yellow Gradient Green Hills w/ Paint Doodles 1'
0x43: 'Yellow Gradient Green Hills w/ Paint Doodles 2'
0x44: 'Yellow Gradient Green Hills w/ Paint Doodles 3'
0x45: 'Yellow Gradient Green Hills w/ Paint Doodles 4'
0x46: 'Black w/ Dark Blue Spook Clouds'
0x47: 'Black w/ Vert Scrolling Dark Red Spook Clouds'
0x48: 'Demo Blue Checkerboard'

]
***************************************************************************
[gColState:
ptr to it is at 0x8012bcf8. This is like a c++ class that holds all the
data to which collision is done. Variables are stored in the class
and its used throughout many different functions making it
difficult to keep track of things in asm.

gColSTate{
0x0: StackCounter?? //Used to keep track of something
0x4: *Unk //thing 0x0 keeps track of
0x8: Kirby_Ground_Pos f32[3]
0x14: Kirby_Head_Pos f32[3] //only uses Y increase, but could change
0x20: DeltaPos f32[3] //usually just Y
0x2c: *UnkNorm2 struct Normal//only used when moving
0x30: *vColHeader struct Collision_Header
0x34: *LastHitNormal struct Normal //only used when moving
0x38: *OtherHitNormal struct Normal //only used when moving
0x40: &func_80101920 or &func_801021fc
0x44: (*func_80102364_ovl2)(void *Normal, s32 arg1)
}
]
****************************************************************************
[gPosState:
Positions used for collision calculations and other stuff at 0x8012E968

struct gPosState {
/*0x0*/  u32    Padding;
/*0x4*/  f32[3] Kirby_Ground_Pos; //Position of kirby at feet/back
/*0x10*/ f32[3] Char_Size; //this is prob for size of char (for dedede segments?)
/*0x1c*/ f32[3] Facing_Vec; //Constat with mag, last member seems to be total H dist or something
/*0X28*/ f32[3] Kirby_Head_Pos; //Position of kirby at head/face
/*0x34*/ f32[2] Kirby_Ground_Path; //the X/Z pos of kirby feet
/*0x40*/ f32[2] Kirby_Head_Path; //the X/Z pos of kirby head
/*0x44*/ f32[2] Kirby_Path_Y; //Y pos of head/feet respectively (assumption)
/*0x4c*/ u32    Col_Flags; //0x10000000 when grounded, 0 when not
/*0x50*/ u32    UnkPad; //0x14141414 constant
/*0x54*/ u32    UnkPad2; //0x14000000 constant
/*0x58*/ u32    VI_Timer; //the VI
};

constant = [20,38,2]
facing = [+-18,-+18] //first member when going forward
]
***************************************************************************
[gColPointers:
Pointers to the collision triangles/normal and other static data that kirby
is standing on top of. at 0x8012BCA0

struct gColPointers {
/*0x00*/ u32    Col_Flags; //0x10000000 when grounded, 0 when not
/*0x04*/ u32    UnkPad; //0x14 constant?
/*0x08*/ Struct Triangle *Ground_Tri;
/*0x0C*/ Struct Normal *Ground_Normal;
/*0x10*/ u32[2] UnkPad2; //0x14 followed by zero
/*0x18*/ Struct Triangle *Ceiling_Tri;
/*0x1C*/ u32[2] UnkPad4; //0x14 followed by zero
/*0x24*/ f32[][3] *Path_Node; //yea
/*0x28*/ u32[2] UnkPad5; //0x14 followed by zero
/*0x2C*/ f32[] *Something; //no idea
/*0x30*/ u32[2] UnkPad6; //0x14 followed by zero
};
]
***************************************************************************
[gLSBlockPointers:
basically pointers to data that already exist in the level settings block.
exists at 0x80129114

struct gLSBlockPointers {
/*0x00*/ Struct Node_Header *Node_Header;
/*0x04*/ u32    Num_Level_Nodes;
/*0x08*/ Struct Collision_Header *Collision_Header;
/*0x0C*/ Struct Entity_IDs[] *Entity_IDs_Start;
/*0x10*/ u32     Max_Entities; //to do with entity id spawning
/*0x18*/ u32     Unk2; //zero
/*0x1C*/ u8[]    *UnkBytes; //section in LS block of unknown use
/*0x20*/ f32[]   *UnkFloats; //section in LS block of unknown use
/*0x24*/ u32     UnkPad2; //zero
/*0x28*/ u32     *Something; //crashes when edited, has to do with images?
};
]
****************************************************************************
[KirbyState struct:
// 0x8012E7C0
struct Player {
    u32 actionChange;			// 0x0
    u8 unk4;
    u8 action;					// 0x5
    u8 previousAction;			// 0x6
    u8 unk7;
    u8 unk8;
    u8 unk9;
    u8 unkA;
    u8 unkB;
    u8 abilityState;				// 0xC
    s8 unkD;
    u8 unkE;
    u8 unkF;
    u32 unk10;
    u8 unk14;
    u8 unk15;
    u8 inactionTimer; // 0x16 used for spark/rock startup
    u8 warping; // 0x17 set to 1 when warping, 0 when done
    u8 unk18;
    u8 unk19;
    u8 unk1A;
    u8 unk1B;
    u32 floatTimer;				// 0x1C
    s32 turnDirection;			// 0x20
    u32 isKnockedBack; // knockback
    u32 unk28;
    s32 unk2C; // turn around var, also used for warps
    u32 abilityEnded; // 0x30, 0 at start, and 1 at end. set by action init func
    // u8 unk31;
    // u8 unk32;
    // u8 unk33;
    u32 isTurning;				// 0x34
    f32 unk38; // run spd?
    s32 unk3C; // projectile state (used for cutter)
    f32 unk40; // projectile speed/size
    u32 unk44; // sub action state or maybe walking state
    u32 unk48; // used in crouch slide
    // struct pointer
    void *unk4C; // virtual func
    // struct pointer
    u32 unk50;
    u32 unk54;
    u32 unk58;
    u16 unk5C;
    u16 unk5E;
    u32 unk60;
    u16 unk64;
    u16 unk66;
    u16 unk68;
    u16 unk6A;
    u32 unk6C;
    u32 unk70;
    u32 unk74;
    f32 unk78;
    f32 unk7C;
    f32 unk80;
    u32 unk84;
    u16 unk88;
    u16 unk8A;
    u32 unk8C;
    u32 ability;					// 0x90
    u32 currentInhale;			// 0x94
    u32 firstInhale;				// 0x98
    u32 secondInhale;			// 0x9C
    u32 abilityInUse;			// 0xA0
    u32 unkA4;
    u32 inhaledEntityData;		// 0xA8 (First 4 bytes of Entity struct)
    u32 isHoldingEntity;		// 0xAC
    u8 isInhaling;				// 0xB0
    u8 isInhalingBlock;			// 0xB1
    s16 numberInhaling;			// 0xB2
    s16 numberInhaled;			// 0xB4
    u16 unkB6;
    u8 unkB8;
    u8 unkB9;
    u16 unkBA;
    f32 unkBC;
    f32 unkC0;
    f32 unkC4;
    f32 unkC8;
    f32 unkCC;
    u16 jumpHeight;				// 0xD0
    u16 isFullJump;				// 0xD2
    s16 damageType;				// 0xD4
    u16 damageFlashTimer;		// 0xD6
    u16 damagePaletteTimer;	// 0xD8
    u16 damagePaletteIndex;	// 0xDA
    u8 isTakingDamage;			// 0xDC
    u8 droppedAbility;			// 0xDD
    u16 abilityDropTimer;		// 0xDE
    u16 hpAfterDamage;			// 0xE0
    u16 unkE2;
    u32 ceilingCollisionNext;	// 0xE4
    u32 floorCollisionNext;	// 0xE8
    u32 rightCollisionNext;	// 0xEC
    u32 leftCollisionNext;		// 0xF0
    u32 levelCollisionFlags;	// 0xF4
    u32 verticalCollision;		// 0xF8
    u32 horizontalCollision;	// 0xFC
    u16 ceilingType;				// 0x100
    u16 floorType;				// 0x102
    u16 unk104;
    u16 unk106;
    u16 unk108;
    u16 unk10A;
    u32 unk10C;
    u32 unk110;
    // todo: verify this
    struct KirbyState_114 *unk114;
    u32 unk118;
    u32 unk11C;
    u32 unk120;
    u32 unk124;
    u32 unk128;
    u32 unk12C;
    u32 unk130;
    u32 unk134;
    u32 unk138;
    u32 unk13C;
    u32 unk140;

    f32 unk144;
    f32 unk148;
    f32 unk14C;

    u8 unk150;
    u8 unk151;
    u8 unk152;
    u8 unk153;
    u32 unk154;
    f32 unk158;
    u32 unk15C;

    u16 unk160;
    u16 unk162;
    f32 unk164;
    f32 unk168;
    u32 unk16C;
    u32 unk170;

    f32 forwardVel;
    Vec3f vel;					// 0x178

    // ---
    // TODO: does Player actually end here???
    // ---


    // u32 unk184;
    // u32 unk188;
    // u32 unk18C;
    // u32 unk190;
    // u32 unk194;
    // u32 unk198;
    // u32 unk19C;
    // u32 unk1A0;
    // u32 unk1A4;
    struct KirbyState_184 _184;
	
	
    u32 unk1A8;					//PositionState goes from here until the end
    u32 unk1AC;
    u32 unk1B0;
    u32 unk1B4;
    u32 unk1B8;
    u32 unk1BC;
    u32 unk1C0;
    u32 unk1C4;
    u32 unk1C8;

    f32 xPos;						// 0x1CC
    f32 yPos;						// 0x1D0
    f32 zPos;						// 0x1D4

    u32 unk1D8;
    u32 unk1DC;
    u32 unk1E0;
    u32 unk1E4;
    u32 unk1E8;
    u32 unk1EC;
    u32 unk1F0;
    u32 unk1F4;
    u32 unk1F8;
    u32 unk1FC;
    u32 viTimer;					// 0x200
};
]
****************************************************************************
[POWER up list:
0: inhale
1: fire
2: rock
3: ice
4: needle
5: bomb
6: shock
7: cut
8: double fire
9: volcano (rock fire)
a: ice/fire
b: needle/fire
c: bomb/fire
d: shock/fire
e: cut/fire
f: double rock
....
15: double ice
....
1a: double needle
....
1e: double bomb
....
23: double cut
pattern is easy to see from this.
Ends at 0x23: double cut

Powerup Table is at: 0x80126EF8
This is only used to map entities to powerups, the crystals/stars
that you throw at enemies will not be affected by editing the table.
Is also not used when throwing a star at an entity, only inhaling.
]
****************************************************************************
[KIRBY ACTIONS
A list of kirby's actions as found via debugger in game. Kirbys action
state is found inside his struct at 0x8012E7C0 offset 0x4 and is a u8.

#define KIRBY_ACT_INACTIVE 0X0 //lvl select, victory carpet
#define KIRBY_ACT_STANDING 0X1
#define KIRBY_ACT_UNK 0X2
#define KIRBY_ACT_WALKING 0X3
#define KIRBY_ACT_RUNNING 0X4
#define KIRBY_ACT_JUMP_RISING 0X5
#define KIRBY_ACT_JUMP_FALLING 0X6
#define KIRBY_ACT_JUMP_LANDING 0X7
#define KIRBY_ACT_FALL_HIGH 0X8
#define KIRBY_ACT_JUMP_PUFFING 0X9
#define KIRBY_ACT_WALL_HANGING 0XA //you cannot change dirs on wall
#define KIRBY_ACT_POLE_HANGING 0XB //poles can have you change dir
#define KIRBY_ACT_CLIMB_LEDGE 0XC //UP OR DOWN
#define KIRBY_ACT_THROUGH_PLAT 0XD //UP OR DOWN
#define KIRBY_ACT_CROUCHING 0XE
#define KIRBY_ACT_SLIDING 0XF
#define KIRBY_ACT_SWALLOW_IN_MOUTH 0X10
#define KIRBY_ACT_PLACING_OVERHEAD 0X11
#define KIRBY_ACT_SWALLOWING_HELD 0X12
#define KIRBY_ACT_THROWING_UPWARDS 0X13
#define KIRBY_ACT_UNK2 0X14
#define KIRBY_ACT_BUBBLE_LAUNCH 0X15 //those green things in aqua star
#define KIRBY_ACT_HURT_KNOCKBACK 0X16 //air and grnd
#define KIRBY_ACT_DIE 0X17
#define KIRBY_ACT_INHALE 0X18
#define KIRBY_ACT_EXHALE 0X19 //(ALSO THROWING FORWARD)
#define KIRBY_ACT_POWERUP 0X1A //using any powerup it seems like
#define KIRBY_ACT_SWIMMING 0X1B //other actions underwater use same nums
#define KIRBY_ACT_ALLY_HELP 0X1C //ANYTIME A FRIEND IS HELPING YOU (DDD, WADDLE DEE CART ETC.)
//note about 0x1C, while ridding DDD, action is 0x1A to use hammer, then uses other normal actions for movement.
#define KIRBY_ACT_ENTER_WARP 0X1D //GOING INTO WARP FROM LEVEL
#define KIRBY_ACT_ENTER_LEVEL 0X1E //ENTERING LEVEL FROM WARP
#define KIRBY_ACT_FIGHT_02 0X1F //this is your action for the entire boss battle

]
****************************************************************************
[SAVE FILE STRUCT
//0x800EC9F8 = gSaveBuffer1
typedef struct {
    /* 0x0; */  u32 Furthest_World; //keeps track of bosses you killed
    /* 0x4; */  u32 Furthest_Level;
    /* 0x8; */  u32 data8;
    /* 0xC; */  u32 Cutscenes;
    /* 0x10 */  u8 percentComplete;
                u8 data11;
                u8 data12;
                u8 data13;
    /* 0x14 */  u8 data14;
                u8 data15;
                u8 data16;
                u8 data17;

                // minigames
    /* 0x18 */  u16 hundredYardHopRecord;
    /* 0x1A */  u16 bumperCropBumpRecord;
    /* 0x1C */  u16 checkerBoardChaseRecord;

                u8 Enemy_Cards;
                u8 Enemy_Cards;
    /* 0x20 */  u32 Enemy_Cards;
    /* 0x24 */  u32 Enemy_Cards;
    /* 0x28 */  u32 Enemy_Cards;
    /* 0x2C */  u32 Enemy_Cards;
    /* 0x30 */  u8 Enemy_Cards;
                u8 Enemy_Cards;
                u8 Enemy_Cards;
                u8 data33;
    /* 0x34 */  u8 Completion_Flags; //Visit_Ripple_Star is one
    /* 0x38 */  u8 Completion_Flags;
                u8 Completion_Flags;
                u8 Completion_Flags;
                u8 Completion_Flags;

                // the range in question
    /* 0x3C */  u8 Crystal_Shards[24];
    u32 checksum;
} File;
]
****************************************************************************
[Struct Bank_Pointers{
/*0x00*/	u32		GeoBlock_Table;
/*0x04*/	u32		GeoBlock_Table_Offset; //unused
/*0x08*/	u32		Img_Table;
/*0x0C*/	u32		Img_Table_Offset;
/*0x10*/	u32		Anim_Table;
/*0x14*/	u32		Anim_Table_Offset;
/*0x18*/	u32		Misc_Table;
/*0x1C*/	u32		Misc_Table_Offset;
};
]
****************************************************************************
[FILE SYSTEM POINTERS
at 0x3ff50 there is a struct full of data on where each individual file/folder
in the entire kirby file system is.
This has all the audio, and level data which seem to be the main files.

In those folders, it futher branches into audio sample data, bank files and midis.
Then in level data it has models, skyboxes, textures, particle data and animations.

Struct File_Layout{
/*0x00*/	u32		*UnkRam;
/*0x04*/	u32		*Unk; //unused
/*0x08*/	u32		*Unk2;
/*0x0C*/	u32		*Unk3;
/*0x10*/	u32		*UnkLen;
/*0x14*/	u32		*Sfx_BankFile; //0x3ff64
/*0x18*/	u32		*Sfx_SampleData;
/*0x1C*/	u32		*Sfx_SampleData2; //repeat?
/*0x20*/	u32		*Instrument_BankFile;
/*0x24*/	u32		*Instrument_SampleData;
/*0x28*/	u32		*Instrument_SampleData2; //repeat
/*0x2C*/	u32		*Midis;
/*0x30*/	u32		*Unk4;
/*0x34*/	u32[7]	*Pad; //not sure if its supposed to be part of same struct tbh
/*0x50*/	u32		*SoundMapping[2]; //0x3ffA0, affects how sfx work, not sure on exact format
/*0x58*/	u32		*SoundArgs1[2];
/*0x60*/	u32		SoundArgs2; //AssetData is the end ptr, but it is re used for asset start
/*0x64*/	u32		*AssetData; //0x3FFB4
};
]
****************************************************************************
[ASSET DATA/FILE SYSTEM

DDD is Bank 2 ID 107 (105 for kirby on DDDs back)
Adeline is Bank 2 ID 111
Waddle Dee is Bank 2 ID 96
Kirby is Bank 2 ID 7, transformations make up many of the other IDs in bank.

Each bank has all the same data, just that its separated into
different file tables.
Below is each banks assumed use.

   Bank
   0000 //boot screen
   0001 //entities
   0002 //characters/entities
   0003 //menus&minigames
   0004 //cutscenes
   0005 //basically nothing. Textures folders contains menu/HUD
   0006 //This has solid level models, basically anything movable and made for a specific level.
   0007 //levels

0x68790 to 0x6c8A0 are all dev strings

Data Tables and Data

Order stored:
Geo Blocks
Images
Anim
Misc
Bank_Pointer

The tables start at 0x6C8F0

Data starts being written at 0x4AA8F0

The list of Bank_Pointers that load all this data is at 0x000783D4 or 0x800d0184 (HS 0x80057DB0)
Each individual Bank_Pointers struct is as follows

   Bank Ram      Rom
   0000 800C47D4 0006CA24//boot screen
   0001 800C7824 0006FA74//entities
   0002 800C9090 000712E0//characters/entities
   0003 800CAED0 00073120//menus&minigames
   0004 800CC794 000749E4//cutscenes
   0005 800CCCB4 00074F04//menu/HUD
   0006 800CE220 00076470//certain bosses/entities
   0007 800D00A4 000782F4//levels

Following this data is the Main Stage Table

LOADED WITH FUNCTION 0x800A9AA8
  Misc banks
   
   (Level settings, particle images, ...)
   The function at 800A9AA8 is used to load the resource and return its RAM address.
   (800D0184 table of pointers to offset lists?)
   Loaded back to back, so end of index 1 is start of index 2.
   
   Bank  Address  Offsets
   0000  004F3290 0006CA0C
   0001  007BACA0 0006FA64
   0002  00964660 000712D0
   0003  00BD6DB0 000730C4
   0004  01116010 000749D4 #ends 11291AC
   0005  01195E50 00074EFC
   0006  0126D990 00076018
   0007  01D28720 00077F30 (level settings)

LOADED WITH FUNCTION 0X800A8BAC
   Image banks
   
   Bank-indices for images are used for addresses for G_SETTIMG commands in display lists.
   The function at 800A8BAC is used to load the resource and return its RAM address.
   Each index in the table is 4 bytes long, and they're stored back to back.
   
   Bank Address  Offsets
   0000 004B3260 0006C948//boot screen
   0001 005B5360 0006D1E4//entities
   0002 00858740 0006FE9C//characters/entities
   0003 009D8CB0 000D9268//menus&minigames
   0004 00DA5E60 00073610//cutscenes
   0005 011291B0 00074A0C//menu/HUD
   0006 01237D50 0007585C//certain bosses/entities
   0007 01BD5C80 00076B28//levels


LOADED WITH FUNCTION 0X800A9864
  Geometry block list
   
   There are multiple lists of ROM start-end address pairs for geometry blocks.
   The function at 800A9864 is used to load the resource and return its RAM address.
   Each index is 4 bytes long. Stored independently so they are not back to back.
   
   List Addresses Description
   0000 0006C8F0
   0001 0006CA44
   0002 0006FA94
   0003 00071300
   0004 00073140
   0005 00074A04
   0006 00074F24 #starts 01195E60
   0007 00076490  Level geometry
   
   
LOAD WITH FUNCTION 0X800A94F4
   Anim Bank loads
   
   This section shown inside of the geometry block loads from a bank index.
   Loaded back to back.
   
   Bank Address  Offsets
   0000 004F01B0 0006C9BC
   0001 00615C40 0006e014
   0002 008784E0 000702c4
   0003 00B76D90 00072954
   0004 00EE9780 00074118
   0005 01195E40 00074ef4
   0006 01CE5A30 00077cd0
   0007 01253100 00075ae8
   
   
LOADED WITH FUNCTION 0X800F78E4
	Main Stage Table
	0X783F4 0X800D01A4 Main stage table
	0x7A1E8 0x800d1f98 ptrs to AREAS
See KIRBY LEVELS.txt for full breakdown of each level location.
]
****************************************************************************
[Embedded Sprite/Large Textures
Certain textures are loaded via DLs, while others (mostly menus) use sprite
based code. The texture file has an 0x10 byte header in it that describes
the texture.
struct SpriteHeader{
	u8 Fmt; //f3d type
	u8 Siz; //f3d type
	u16 unk; //?
	u16 width; //not full width, but displayed width (as in a gLoadBlock)
	u16 height; //full height I guess
	u32 unk2;
	u32 *Pal; //ptr to pal within texture
};

]
****************************************************************************
[Dev strings:
ROM:
0x68790 to 0x6c8A0 are all strings
ends with VOLUME_END_BLOCK
RAM:
0x800C0540 to 0x800C4650

This data is only used for debugging or crash screens.
This leaves about 4kb of free space thats loaded from boot for free use.

The best of this is to repoint a data table so that it can be expanded for
HUGE amounts of custom data.

Repointing: Replace the table offset in the Bank_Pointers struct of respective
bank to be the location in RAM of this free space.
Relocate the original data to be the start of this bank.
Tack on to the end of it, but adding in a padding of 8 bytes of zero. This is because
the data is packed tight back to back, so you'll get invalid pointers if you don't pad.

Another set of dev strings is at 0x7e050 to 0x7e944 (0x800D5E00 - 0x800D66F4)
]
****************************************************************************
[Geo Block notes

Loaded into bank 4 with function 0X800A9864 which returns its location.
Geo blocks keep their segpointers, and gsSPSegment is used to alter the location
of seg 4 to match the location of the current processed geo block.
To find a location of a geo block in RAM, using the dma log is the easiest method
though you could also look at 0x800DF4D0 + heap offset << 2.
The heap offset is determined when the file is loaded, so I'm not really sure how to get that.

 Block structure
   1. Header
   2. Vertices
   3. Microcode
   4. G_SETTIMG references
   5. G_VTX references
   6. Entry point groups (Optional)
   7. Layout
   8. ENV VFX
   9. Texture scroll settings (Optional)
   10. Animation (Optional)

GeoBlock Header =
{
    0x00:'layout',
    0x04:'tex_scroll',
    0x08:'rendering_mode',
    0x0C:'img_refs',
    0x10:'vtx_refs',
    0x14:'Num Anims',
    0x18:'Anims',
    0x1C:'Num Layouts'
 }


There are three render modes used in levels. 0x17, 0x18 and 0x1C.
For each render mode, it interprets the Layouts differently.

In non level data, the following render modes are used:
0x13,0x14,0x17,0x18,0x1B,0x1C

Layouts essentially control how the display lists are rendered. They probably have a heiarchy
which allows for different graphical effects such as billboarding etc.

Layouts are also used to position other models. For example a model that contains other
models (such as the victory carpet) will use layouts to place them. The other models
used will not have DL pointers to them but will be later filled in via code.

Render Mode Descriptions:

0x13:
The header directly points to a single display list.
There are no layouts or entry points.

0x14:
There are no layouts, the layout pointer instead goes directly to an
entry point. The entry point works standardly.

0x1B:
Each layout points to a pair of display list pointers (or null), same layout
structure as before.
Each display list seems to have a format of the first drawing triangles,
and the second modifying those triangles and drawing them.
Possibly a switch/state machine built into the DL.
I will refer to these as DLPairs

0x1C:
Each layout points to an entry point. The difference is that these entry points
have pairs of display list pointers (or null) instead of a single DL, same layout
structure as before.

0x17:
Each layout points directly to a display list (or null), and each layout
has a rotation, translation and scale attached.

0x18:
Each layout points to a list of entry points (or null), and each layout
has a rotation, translation and scale attached.

Structure:
Every Layout has a structure as follows

Layout = {
     0x00: ('>H','Flag',2),
     0x02: ('>H','Command',2),
     0x04: ('>L','Entry Points',4),
     0x08: ('>3f','Translation Vec3f',12),
     0x14: ('>3f','Rotation Vec3f',12),
     0x20: ('>3f','Scale Vec3f',12)
}

I am assuming these are bitwise

Flags:
0x0 - nothing
0x1 - never seen?
0x2 - 5-4-4
0x4 - ?
0x8 - 
0x8000 - end ENVFX

Probably not bitwise. (or literally just depth)
Cmds:
0x0 - Start layout (crashes if used elsewhere)
0x1 - Draw display list - Parent
0x2 - Draw display list - Child no inherit transform (crashes if used on node after 0x0)
0x3 - Draw display list - Child inherit transform (crashes if used on node after 0x0)
0x4 - Draw display list - End Parent
0x5 - ?
0x6 - ?
0x7 - ?
0x12 - End Layout
0x8000 - Billboard?

Each Destructable group is its own layout.


Entry Points:

has a flag, then a ptr or a pair of ptrs
0 - start
1 - continue ?? (can sometimes start with this??)
4 - end


IMAGES:
img refs are included in the img ref section. These point to set img cmds.
Set img cmds use a bank index to load the data which is described in the above
section.
On level load, these are changed into bank indices (e.g. [BB BB][II II]) style from their
seg pointers.

these and vtx refs use an ARR_TERMINATOR to indicate the end of data.
ARR_TERMINATOR is equal to 0x99999999

Env vfx:
Uses a struct 0x2c bytes long, and is located directly after the layout section.
Is terminated by having the msb of the first struct member be true.

The header's num_layouts member does not include these values.

The struct is only read on level load, and used to initialize the env vfx.
Uses the same struct as a layout, its basically a continuation of layouts but for envfx.

Env vfx section is looked at if last layout->0x4 has its msb set e.g. its 0x80000000

struct Env_Vfx {
0x00	u32		flag;
0x02	u16		cmd;
0x04	u32		Entry Points;
0x08	Vec3f	Pos;
0x14	Vec3f	Rot;
0x20	Vec3f	Scale;
};
Types:
Unknown.


Anim:
a list of bank indices, loads from its own tables of stuff.

Data is loaded, and put into a different section.


Texture Scrolls:
All arrays use ARR_TERMINATOR in texture scrolls

Starts with a header which is an array of pointers.
The header is variable length, and is comprised of pointers to bank 4. Each pointer corresponds to one tx_scroll_hdr inside of the Texture Scrolls block.
Each tx_scroll_hdr is an array of pointers to tx_scroll objects

main_header -> tx_scroll_header[] -> tx_scroll_hdr[] ->tx_scroll

The main header is 1:1 with layouts. Each tx_scroll_header[] corresponds to
one layout struct.

tx_scroll_hdr[] places each tx_scroll in bank 0xE in ascending order.
So index 0, is at 0x0E000000 and 1 at seg E + 0x8 etc.

Access to scrolls is done via a branch gsSPDisplayList(&DL_E000000).

tx_scroll is a struct of length 0x78 bytes that contains actual scroll info.

]
****************************************************************************
[Texture Scrolls

Scrolls placement in the DL is described in the geo block. This section will
go over how to use scrolls and what combination of fields are useful.

On load, the scroll is copied to memory so that it can be edited, the scroll inside
the geo block is not touched after level initialization. This is done by function
80009A44. The scroll in memory is a struct that has a ptr to the next scroll, a gap
and then the scroll copy.

struct Animation {
	struct Animation *Animation;
	u32 unk; // maybe padding?
	struct tx_scroll virtual_scroll;
	/* 0x7C */ u32 unk1;
	u32 unk2;
	u32 unk3;
	u32 unk4;
	u32 unk5;
	/* 0x90 */struct AObj *aobj;
	struct AnimScript *anim_script; // the specific scroll for this scroll in the anim file
	f32 scale; // or cur frame
	f32 anim_rate;
	f32 unkA0;
	f32 unkA4;
}; //size 0xA8

decomp says this, but idk:

struct Animation {
  u32 unk0;
  // Is this a GObj????????????
  struct {
      u8 pad[0x40];
      f32 unk40;
      u32 unk44;
      void (*unk48)(struct Animation *a0, u32 a1, u32 a2);
  } *unk4;
  u32 unk8;
  u32 unkC;
  u32 unk10[4];
  u32 unk20[4];
  u32 unk30[4];
  u32 unk40[4];
  u32 unk50;
  u8 unk54; // verify type
  u8 unk55;
  u8 unk56;
  u8 unk57;
  u32 unk58;
  u32 unk5C;
  u32 unk60;
  u32 unk64;
  u32 unk68;
  struct AObj *aobj;
  u32 *command;
  f32 scale; // unk74
  u32 unk78;
  f32 unk7C;
  u32 unk80;
  u32 unk84;
  u32 unk88;
  u32 unk8C;
  struct Animation *unk90;
};


Where the scroll gets allocated is determined by func 0x8000888C.
The ptr to the start of the linked list of a_obj seems to be in some memory management
struct of unknown nature, which itself moves around often in memory, finding a reliable way
to locate the scrolls in memory proves troublesome.

struct tx_scroll {
    ushort field_0x0; //unused
    byte fmt1;
    byte siz1;
    uint * textures;
    ushort stretch;
    ushort sharedOffset;
    ushort t0_w;
    ushort t0_h;
    int halve;
    float t0_xShift;
    float t0_yShift;
    float xScale;
    float yScale;
    float field_0x24;
    float field_0x28;
    pointer *palettes;
    ushort flags;
    byte fmt2;
    byte siz2;
    ushort w2;
    ushort h2;
    ushort t1_w;
    ushort t1_h;
    float t1_xShift;
    float t1_yShift;
    float field_0x44;
    float field_0x48; //unused
    int field_0x4c;
    struct color prim;
    byte field_0x54; //read on struct init, bool of some sort
    byte primLODFrac;
    byte field_0x56; //unused
    undefined field_0x57; //unused
    struct color env;
    struct color blend;
    struct color light1;
    struct color light2;
    int field_0x68; //unused
    int field_0x6c; //unused
    int field_0x70; //unused
    int field_0x74; //unused
};

Basically how the scroll works is it adds in cmds to a display list in segment 0xE.
Which offset in segment 0xE is determined by the index in the tx_scroll_hdr[].

The cmds added to the display list are determined by the flags value of the struct.

flags:
0x0 - defaults to flags 0xA1 if 0 (0x80 | 0x20 | 0x01)
0x1 - use tex0 (adds G_SETTIMG)
0x2 - use tex1 (adds G_SETTIMG. if both textures, implicit load block added)
0x4 - has palettes (G_SETTIMG, implicit TLUT if also texture)
0x8 - set prim color
0x10 - ? (adds a load block (tex1 added? loads on tile 6) also adds a prim (for prim LoD multi texture maybe??))
0x20 - adds tile size coords G_SETTILESIZE uses tile 0
0x40 - adds tile size coords G_SETTILESIZE uses tile 1
0x80 - sets texture scale G_TEXTURE, uses x/y scale fields
0x100 - unused
0x200 - set prim color (same as 0x8 it seems)
0x400 - set env color
0x800 - set blend color
0x1000 - set light color 1
0x2000 - set light color 2
0x4000 - unused
0x8000 - unused

Some info about other values.

*textures is an ARR_TERMINATOR terminated array
*palettes is an ARR_TERMINATOR terminated array

*textures and *palettes can be shared between tx_scroll structs

fmt and siz are directly used in the G_SETTIMG cmd added based on flags

textures are always 32x sized (unless? maybe scales by scale coords idk though)

set tile fields:

t0_w, t0_h are the tile size coords for tex0, used with G_SETTILESIZE flags
	with flag 0x20 the coords are placed on tile 0, t0 coords are used
	with flag 0x40 the coords are placed on tile 1, t1 coords are used
	if 0 in field, defaults to 0xFFC, or 1023 units, the max size of a tile

t0_xShift, t0_yShift, t1_xShift, t1_yShift, xScale, yScale are read when the flag & 0xE0 (0x20 | 0x40 | 0x80).

if halve is 1, then some math is done based on the tile fields halve is read when the flag & 0xE0

based on the tile field values, the tile values will be moved, the condition for moving is generally
true when the scale is non zero (really abs(scale) < 1.5e-5).
the shift formula looks like this: shift = 4 * (shift*coord + shared_offset)/scale
shift is used purely for the upper corner value, and the lower corner value is shift+coord

Scrolling:
scrolling is done by editing the set tile fields mentioned above, this seems to be done by a function 8000DE30, which seems to generally handle all dynamic scroll stuff.

Values used in this are stored by funcs: 8000BFA0, 8000D35C

If field_0x54 is set, then bss_scroll fields 0x84 will be set.

The actual changing of values is done via an animation. The animation has to
be in mode 1 and the animation file will cover all texture scrolls in the file
at once. How this works is covered in animation notes.


stretch only used with flag 0x80 and textures present

useless:
field_0x0, field_0x48, field_0x68, field_0x6c, field_0x70, field_0x74 - are always 0
field_0x56, field_0x57 - are always 0 in all in game usage.
Some of these fields are used during anims, but idk what they do. They could just be
variables for the code to read idk.

texture/pal index:
this is set outside of the texture scroll creation code, so it must be specific to
the object processing, what is known is that all textures and palettes have the same
format.


]
****************************************************************************
[Animation Format:

Animations are linked directly inside of a geo block, and only make sense in
the context of that specific geo.
As a preface, you should understand layouts. Layouts are an array of transforms
and display list/entry point pointers, basically they point to pieces of a model.
The layouts form a tree like structure by allowing direct parenting in this
array. The subsequent member of the array can be either a child, sibling or
aunt (sibling of parent I guess).
All references to specific graphics data is done in reference to a layout.
For example a texture scroll for a display list in layout 4, the scroll will only apply
if that scroll is linked to layout 4, otherwise the display list pointer to scroll
data will go no where.
Animations are the same. What the animation actually affects is
the transformation or scroll data linked to a layout, and this is done by linking
a "bone" to a specific layout. All linking is done by having the referenced
data have the same index as the layout. So to link a scroll to layout 5,
the scroll will have to be in index 5 in the scroll array, same with the
animation bone.

With that all said, here is the file layout:

1: header
2: virtual offset conversions (in file ptrs that get seg2virtual called on)
3: bone ptrs (not in mode 2, and point to 5 in mode 1)
4: bone data
5: scroll anim ptrs (in mode 1 only)

header for animations:

struct Animheader {
	union {
		AnimScript bone; // mode 2
		AnimScript *anim_bones[]; // mode 0
		AnimScript **anim_scrolls[]; // mode 1
	};
	u32 mode; // 0, 1 or 2
	u32 num_virtual_offsets;
};

Following the header is an array of offsets to be converted to virtual addresses
on load. 

void *virtual_offsets[anim_header.num_virtual_offsets];

This data is only used upon initialization. I'm assuming this is
because the data is copied or it is for optimization. Not just bones are
transformed, but also pointers in data.
This sort of transformation is normal in kirby data, and is done for misc
bank data and other file types loaded in the asset system.

Following the virtual offsets, are the bone ptrs. In mode 1 we have ptrs to arrays of pointers.

For mode 2 info, see section on mode 2 animations.

AnimScript *anim_bones[]; // mode 0
AnimScript **anim_scrolls[]; // mode 1

The pointers here correspond to layouts in the geo block. The number of bones
will always match the number of layouts as mentioned in the geo block header.
For reference, the last layout with depth of 0x12 does not count for this.

In mode 1, the array points to arrays of pointers. Mode 1 is affecting
texture scrolls, and each layout can have an array of texture scrolls. So each pointer
is pointing to an array representing the scrolls, index matching same with layouts.
Rather unfortunately, it does not seem like these arrays are NULL terminated, they are
implicit upon being read in reference to scrolls.

AnimScript:
	AnimScript data is made up of arrays of bytecode. Most bytecode has a cmd type,
	a scale field (something to do with frames/length) and a transformation field.
	Then depending on the field for the transform, floats will follow to match
	the data, the number of floats matches the number of True fields.
	Each piece of the cmd is split into 4 byte chunks. The cmd type and other
	cmd data is in the first byte, and the following bytes are usually float or
	integer data input fields to the cmd.

Scroll Anims:
	Basically in mode 1, texture scrolls get linked to animations.
	In a geo block, the scroll system is as follows:
	struct TextureScroll **scroll_header[]->
	struct TextureScroll *scroll_array[]->
	struct TextureScroll scroll
	The index in the **scroll_header refers to the layout index.
	The index in the *scroll_array scroll refers to the scroll number.
	The scroll number refers goes to a specific DL pointer, which can be
	added to a DL only in the same layout index.
	To add an animation to a specific scroll, the indices are matched as follows:
	**scroll_header index is **anim_scrolls which is the array right after the header.
	*scroll_array index is at the end of the file, and is pointed to by **anim_scrolls.
	The actual animations are the same as bone animations, just different bytecode.

Cmd:
	The leading 0xFE bytes of the first word is the cmd, or in other words cmd = word >> 25.
	For this reason, cmds are generally referred to post shift.
	If the cmd < 0xF, it generally has a transform bitfield and a scale field.
	Exceptions are cmds [0x0, 0x1, 0x2, 0x7, 0xC, 0xD, 0xE, 0xF], which 
	control environmental variables about the animation rather than transforms.
	If the cmd > 0xF, then generally it has a single u32 integer data field,
	or some splitting of a u32 into smaller components.

Transform Field:
	Field of 0x01 FF 80 00 is a bitwise representation of the transfomations applied.
	XYZ ROT 012
	XYZ LOC 456
	XYZ SCL 789
	BASE/path 3
	If the cmd is 0x5 or 0x6, there are two sets of transformation data, the data
	point, and also the slope of the point (see hermite spline data).
	If the cmd is > 0xF, then changing the transform affects a different offset of
	in the struct, going oob on the transform for that cmd will halt the anim
	color bit order: prim, env, blend, light1, light2
	other bit order (cmd 16): field_0x4c, field_0x68, field_0x6c, field_0x70, field_0x74
	Important! bone data is absolute not layout relative, while blender is rest pose relative.
	If your bone starts at (0, 100, 0), and the anim starts at (0, 100, 0) the anim has
	not moved your bone.

Scale:
	This is called scale in the notes of other people who have looked at the
	animation data for other HAL games, but I'm pretty sure this is just a length
	field for number of frames (possibly changed by actor anim speed).
	Scale is the final 0x7FFFF of the first word in the anim cmd.

Blocking vs Non-Blocking:
	This is another inherited piece of terminology. Basically this means that
	the next cmd will not execute until the current cmd is finished. The point
	is that you can have transforms of different types happening at the same time
	such as a lerp on scale, and then a value set on translation, or you could
	wait for the lerp to finish before setting. Both instances have the cmds follow
	one another, but they will happen on different timings depending on if you block or not.
	A lerp cmd will edit the value over the blocking period.
	A set cmd will set the value at the end of the blocking period.
	A non-blocked cmd will set immediately, I don't know if it even cares about the scale.
	A non-blocked lerp will lerp over the scale, idk how it interacts if it gets
	overwritten by another cmd.

Cmd_list:
	0x0: stop // no args
	0x1: Goto // one arg
	0x2: Block // args is scale within arg
	0x3: Lerp Cusp Block // forms a cusp from prev pt to next instead of a smooth transition
	0x4: Lerp Cusp Non-Block
	0x5: Lerp Vel Block // two transforms, one for pt one for vel
	0x6: Lerp Vel Non-Blocks
	0x7: Set Lerp Velocity // always non blocking, sets vel at next keyframe
	0x8: Lerp Ease Block // vel is 0, should form an even ease in and out
	0x9: Lerp Ease Non-Block
	0xA: Set Block
	0xB: Set Non-Block
	0xC: Extend // no args, increase anims by scale
	0xD: UnkD  // used in mode 2, unknown what it really does, used once!(7,17)
	0xE: Loop // one arg, ptr
	0xF: SetProcessFlag // no args
	// scroll cmds from here on out.
	0x10: unk10 // no args
	0x11: unk11 // one arg per transform bit, u32
	0x12: Set Color Block // sets after block is over
	0x13: Set Color Non-Block // sets immediately
	0x14: Lerp Color Block // lerps over block period, purely linear afaik
	0x15: Lerp Color Non-Block
	0x16: Set Int Field Block // directly sets val in animation, instead of making a keyframe
	
	// this is only used in mode 2
	0x17: unk17 // 2 float args
]
****************************************************************************
[ANIMATION & TEXTURE SCROLL CASE BYTECODE

Animations are called via the object processing loop, which starts with function: func_8000E324
at least as far as you should care about for individual animations.

First these functions are called:
0x8000C3D8 (func_8000C3D8)
and 0x8000D0AC (func_8000D0AC) // which doesn't actually parse anims
// but applies their data I guess

if you have a level animation (that isnt a scroll, aka mode 0)
then nothing else seems to follow

otherwise these are called in a loop:

The function to actually parse obj animations is 0x8000D35C (func_8000D35C)
the jump table for this function is at: 0x8004074C

The function for texture scrolls is 0x8000DE30 (func_8000DE30)
the jump table for this function is at: 0x800407B4

all of them are called, but whether or not they actually execute
seems to be controlled by the type of animation you have.

useful structs:
// the offsets here seem to be 0x24 off or something idk how that happened
struct Animation {
	struct Animation *Animation;
	void *transform_aobjs; // a guess
	struct tx_scroll virtual_scroll;
	/* 0x7C */ u32 unk1;
	u32 unk2;
	u32 unk3;
	u32 unk4;
	u32 unk5;
	/* 0x90 */struct AObj *aobj;
	struct AnimScript *anim_script; // the specific scroll for this scroll in the anim file
	f32 scale; // or cur frame
	f32 anim_rate; // rate at which scale decreases every frame
	f32 unkA0;
	f32 unkA4;
}; //size 0xA8

// AObj
struct AObj {
  struct AObj *next; // it's a list but is it really the next one?
  u8 transform_index; // index
  u8 arg_type; // enum flag of some sort
  u8 unk6;
  u8 unk7;
  // these make up the values for a hermite spline, p0, p1, m0, m1
  // t is length*percent_completion
  /* 0x08 */ f32 percent_completion; // keyframe percent completion
  /* 0x0C */ f32 length; // time left until next keyframe, or just length (but negative)
  /* 0x10 */ f32 prev_pt; // p0 
  /* 0x14 */ f32 next_pt; // p1
  /* 0x18 */ f32 prev_slope; // m0
  /* 0x1C */ f32 next_slope; // m1
  /* 0x20 */ s32 unk20; // this is probably a ptr to a spline or path
};


obj animation cases (func_8000C3D8):
Anim cmd is determined by word >> 25 as described by anim format above
the function only executes if it is the first frame of the animation

the following cases are mostly the same as the general loop, so I will only
note the differences:

// this seems to just setup an aobj in preparation for later processing
Cmd 0xD UnkD:
{
	anim->anim_script++;
	// aobj would only be set by another 0xD cmd in the loop
	if (!aobj) {
		aobj = HS64_AObjNew(anim, 4);
	}
	aobj->unk20 = (s32) *anim->anim_script;
	anim->anim_script++;
}

// this has a transform field, which it uses to set some val
// not really sure what the val does, but it can affect processing
Cmd 0xF UnkF:
{
	anim->unk54 = anim_cmd.transform_field;
	tmp_scale = *anim->anim_script & 0x7FFF;
	if (anim_cmd.scale < 0){
		tmp_scale += (f32)0x4F80 0000;
	}
	anim->anim_script++;
	anim->scale += tmp_scale;
	break;
}


// no clue really, but it has no args supposedly
Cmd 0x10 Unk10:
{
	// this probably points to the array of transformation aobjs, aka bones
	if (anim->unk4->unk48 != 0) {
		cmd_transform = anim_cmd.transform_field;
		anim->unk4->unk48(anim, cmd_transform >> 8, (f32) (u32) (cmd_transform & 0xFF));
	}
	anim->anim_script++;
	anim->scale = (f32) (anim->scale + (f32) (u32) (anim_cmd & 0x7FFF));
}


// one arg per transform bit, u32
Cmd 0x11 Unk11:
{
	anim->anim_script = anim_script + 1;
	anim->scale = anim_cmd.scale;
	u32 S2 = anim_cmd.transform_field;
	u32 S1 = 4;
	while(S1 != 0xE){
		if(S2 & 1){
			if (!anim->unk4->unk48){
				anim->unk4->unk48(anim, S1, anim->anim_script);
				cur_animation->anim_script = anim_script + 1;
			}
		}
		S2 = S2 >> 1;
		S1 += 1;
	};
}



obj animation cases (func_8000D35C), this is the general runtime loop:
Anim cmd is determined by word >> 25 as described by anim format above

going into cases:
V0 = f32 anim_script[]; // where the cmd is loaded from
V1 = anim_cmd; // aka the non transform field of the anim_cmd
A0 = anim_cmd.cmd; // (also 0x58 SP)
S3 = struct *Animation *cur_animation;
S4 = 0xA;
S5 = struct AObj *field_aobjs[]; // I guess a list of aobjs, it points to stack
//and is given offset to load from during cmd parsing (maybe one per transform)

anim_scripts will be of type f32[];

Cmd 0 (0x8000DAD8) Stop:
{
	struct AObj aobj = cur_animation->aobj;
	if (aobj){
		cur_animation->unkA0 = cur_animation->scale;
	}
	do{
		// this means that the aobj has keyframe data in it
		if (aobj->arg_type){
			aobj->length = cur_animation->anim_rate + cur_animation->scale + aobj->length;
			aobj = aobj->next;
		}
	}while(aobj);
	cur_animation->unkA0 = cur_animation->scale;
	cur_animation->scale = D_800407A8; // 0xFEAAAAAA or something
}


Cmd 1(0x8000DA34) Goto:
{
	cur_animation->anim_script = anim_script + 1;
	cur_animation->anim_script = anim_script[1];
}


Cmd 2(0x8000D8E4) Block:
{
	f32 tmp_scale = anim_cmd.scale;
	if (anim_cmd.scale > 0){
		tmp_scale += (f32)0x4F80 0000; // not valid C but this should be understood
	}
	cur_animation.scale += anim_cmd.scale;
	cur_animation->anim_script = anim_script + 1;
}


Cmd 3 (0x8000D648) Lerp Cusp Block:
Cmd 4 (0x8000D648) Lerp Cusp Non-Block:
{
	f32 tmp_scale  = anim_cmd.scale
	// this seems pointless
	if (anim_cmd.scale > 0){
		tmp_scale += (f32)0x4F80 0000;
	}
	u32 S2 = anim_cmd.transform_field;
	u32 S1 = 0;
	cur_animation->anim_script = anim_script + 1;
	while(S2 != 0){
		if(S2 & 1){
			struct AObj *transform_aobj = field_aobjs[S1];
			if (!transform_aobj){
				// initializes aobj for this field
				func_800098AC(cur_animation, /* index */(S1 + 0xD) & 0xFF); 
			}
			// the tangent leaving the point is set as the linear slope from the prev pt to the next pt, making a cusp
			if (tmp_scale != 0.0f){
				transform_aobj->prev_slope = (transform_aobj->next_pt - transform_aobj->prev_pt) / tmp_scale;
			}
			transform_aobj->prev_pt = transform_aobj->next_pt;
			transform_aobj->next_pt = cur_animation->anim_script[0]; // get val at head, then increment
			cur_animation->anim_script = anim_script + 1;
			transform_aobj->length = -(cur_animation->scale + cur_animation->anim_rate);
			transform_aobj->next_slope = 0.0f;
		}
		S2 = S2 >> 1;
		S1 += 1;
	};
	if (cmd.cmd == 0x3){
		cur_animation.scale += anim_cmd.scale;
	}
}


Cmd 5 (0x8000D750) Lerp Vel Block:
Cmd 6 (0x8000D750) Lerp Vel Non-Block:
{
	f32 tmp_scale = anim_cmd.scale;
	if (anim_cmd.scale > 0){
		tmp_scale += (f32)0x4F80 0000;
	}
	u32 S2 = anim_cmd.transform_field;
	u32 S1 = 0;
	cur_animation->anim_script = anim_script + 1;
	while(S2 != 0){
		if(S2 & 1){
			struct AObj *transform_aobj = field_aobjs[S1]; // S0
			if (!transform_aobj){
				// initializes aobj for this field
				func_800098AC(cur_animation, /* index */(S1 + 0xD) & 0xFF); 
			}
			transform_aobj->next_pt = cur_animation->anim_script[0];
			cur_animation->anim_script = anim_script + 1;
			transform_aobj->prev_slope = transform_aobj->next_slope;
			transform_aobj->next_slope = cur_animation->anim_script[0];
			cur_animation->anim_script = anim_script + 1;
			transform_aobj->arg_type = 3;
			if (tmp_scale != 0.0f){
				transform_aobj->percent_completion = 1.0f / tmp_scale;
			}
			transform_aobj->length = -(cur_animation->scale + cur_animation->anim_rate);
		}
		S2 = S2 >> 1;
		S1 += 1;
	};
	if (cmd.cmd == 0x5){
		cur_animation.scale += anim_cmd.scale;
	}
}


Cmd 7 (0x8000D86C) Set Lerp Velocity:
{
	f32 tmp_scale = anim_cmd.scale;
	if (anim_cmd.scale > 0){
		tmp_scale += (f32)0x4F80 0000;
	}
	u32 S2 = anim_cmd.transform_field;
	u32 S1 = 0;
	cur_animation->anim_script = anim_script + 1;
	while(S2 != 0){
		if(S2 & 1){
			struct AObj *transform_aobj = field_aobjs[S1]; // S0
			if (!transform_aobj){
				// initializes aobj for this field
				func_800098AC(cur_animation, /* index */(S1 + 0xD) & 0xFF); 
			}
			transform_aobj->next_slope = cur_animation->anim_script[0];
		}
		S2 = S2 >> 1;
		S1 += 1;
	};
}


Cmd 8 (0x8000D540) Lerp Ease Block:
Cmd 9 (0x8000D540) Lerp Ease Non-Block:
{
	f32 tmp_scale = anim_cmd.scale;
	if (anim_cmd.scale > 0){
		tmp_scale += (f32)0x4F80 0000;
	}
	u32 S2 = anim_cmd.transform_field;
	u32 S1 = 0;
	cur_animation->anim_script = anim_script + 1;
	while(S2 != 0){
		if(S2 & 1){
			struct AObj *transform_aobj = field_aobjs[S1]; // S0
			if (!transform_aobj){
				// initializes aobj for this field
				func_800098AC(cur_animation, /* index */(S1 + 0xD) & 0xFF); 
			}
			transform_aobj->prev_pt = transform_aobj->next_pt;
			transform_aobj->next_pt = cur_animation->anim_script[0];
			cur_animation->anim_script = anim_script + 1;
			transform_aobj->prev_slope = transform_aobj->next_slope;
			transform_aobj->next_slope = 0.0f;
			if (tmp_scale != 0.0f){
				transform_aobj->percent_completion = 1.0f / tmp_scale;
			}
			transform_aobj->length = -(cur_animation->scale + cur_animation->anim_rate);
		}
		S2 = S2 >> 1;
		S1 += 1;
	};
	if (cmd.cmd == 0x8){
		cur_animation.scale += anim_cmd.scale;
	}
}


Cmd 0xA (0x8000D920) Set Block:
Cmd 0xB (0x8000D920) Set Non-Block:
{
	f32 tmp_scale = anim_cmd.scale;
	if (anim_cmd.scale > 0){
		tmp_scale += (f32)0x4F80 0000;
	}
	u32 S2 = anim_cmd.transform_field;
	u32 S1 = 0;
	cur_animation->anim_script = anim_script + 1;
	while(S2 != 0){
		if(S2 & 1){
			struct AObj *transform_aobj = field_aobjs[S1]; // S0
			if (!transform_aobj){
				// initializes aobj for this field
				func_800098AC(cur_animation, /* index */(S1 + 0xD) & 0xFF); 
			}
			transform_aobj->prev_pt = transform_aobj->next_pt;
			transform_aobj->next_pt = cur_animation->anim_script[0];
			cur_animation->anim_script = anim_script + 1;
			transform_aobj->arg_type = 1;
			transform_aobj->percent_completion = tmp_scale;
			transform_aobj->length = -(cur_animation->scale + cur_animation->anim_rate);
			transform_aobj->next_slope = 0.0f;
		}
		S2 = S2 >> 1;
		S1 += 1;
	};
	if (cmd.cmd == 0xA){
		cur_animation.scale += anim_cmd.scale;
	}
}


Cmd 0xC (0x8000DA4C) Extend:
{
	f32 tmp_scale = anim_cmd.scale;
	if (anim_cmd.scale > 0){
		tmp_scale += (f32)0x4F80 0000;
	}
	u32 S2 = anim_cmd.transform_field;
	u32 S1 = 0;
	cur_animation->anim_script = anim_script + 1;
	while(S2 != 0){
		if(S2 & 1){
			struct AObj *transform_aobj = field_aobjs[S1]; // S0
			if (!transform_aobj){
				// initializes aobj for this field
				func_800098AC(cur_animation, /* index */(S1 + 0xD) & 0xFF); 
			}
			transform_aobj->length = tmp_scale + transform_aobj->length;
		}
		S2 = S2 >> 1;
		S1 += 1;
	};
	if (cmd.cmd == 0xA){
		cur_animation.scale += anim_cmd.scale;
	}
}


Cmd 0xD (0x8000DDE0) UnkD:
{
	// does nothing??
}


Cmd 0xE (0x8000DA14) Loop:
{
	cur_animation->anim_script = cur_animation->anim_script[1];
	cur_animation->unkA0 = -cur_animation->scale;
}


Cmd 0xF (0x8000DDE0):
{
	// does nothing??
}


Cmd 0x10 (0x8000DDE0):
{
	// does nothing??
}


Cmd 0x11 (0x8000DDE0):
{
	// does nothing??
}


Cmd 0x12 (0x8000DB2C) Set Color Block:
Cmd 0x13 (0x8000DB2C) Set Color Non-Block:
{
	// only difference between this and other set cmd is the index into the aobj
	// but if the aobj for this transform already exists then it will act the same
	f32 tmp_scale = anim_cmd.scale;
	if (anim_cmd.scale > 0){
		tmp_scale += (f32)0x4F80 0000;
	}
	u32 S2 = anim_cmd.transform_field;
	u32 S1 = 0;
	cur_animation->anim_script = anim_script + 1;
	while(S2 != 0){
		if(S2 & 1){
			struct AObj *transform_aobj = field_aobjs[S1]; // S0
			if (!transform_aobj){
				// initializes aobj for this field
				func_800098AC(cur_animation, /* index */(S1 + 0x25) & 0xFF); 
			}
			transform_aobj->prev_pt = transform_aobj->next_pt;
			transform_aobj->next_pt = cur_animation->anim_script[0];
			cur_animation->anim_script = anim_script + 1;
			transform_aobj->arg_type = 1;
			transform_aobj->percent_completion = tmp_scale;
			transform_aobj->length = -(cur_animation->scale + cur_animation->anim_rate);
		}
		S2 = S2 >> 1;
		S1 += 1;
	};
	if (cmd.cmd == 0xA){
		cur_animation.scale += anim_cmd.scale;
	}
}


Cmd 0x14 (0x8000DC1C) Lerp Color Block:
Cmd 0x15 (0x8000DC1C) Lerp Color Non-Block:
{
	f32 tmp_scale = anim_cmd.scale;
	if (anim_cmd.scale > 0){
		tmp_scale += (f32)0x4F80 0000;
	}
	u32 S2 = anim_cmd.transform_field;
	u32 S1 = 0;
	cur_animation->anim_script = anim_script + 1;
	while(S2 != 0){
		if(S2 & 1){
			struct AObj *transform_aobj = field_aobjs[S1]; // S0
			if (!transform_aobj){
				// initializes aobj for this field
				func_800098AC(cur_animation, /* index */(S1 + 0x25) & 0xFF); 
			}
			transform_aobj->prev_pt = transform_aobj->next_pt;
			transform_aobj->next_pt = cur_animation->anim_script[0];
			cur_animation->anim_script = anim_script + 1;
			transform_aobj->arg_type = 2;
			if (tmp_scale != 0.0f){
				transform_aobj->percent_completion = 1.0f / tmp_scale;
			}
			transform_aobj->length = -(cur_animation->scale + cur_animation->anim_rate);
		}
		S2 = S2 >> 1;
		S1 += 1;
	};
	if (cmd.cmd == 0xA){
		cur_animation.scale += anim_cmd.scale;
	}
}



Cmd 0x16 (0x8000DD14) Set Int Field Block:
{
	// sets color fields
	f32 tmp_scale = anim_cmd.scale; // f16
	if (anim_cmd.scale > 0){
		tmp_scale += (f32)0x4F80 0000;
	}
	cur_animation->scale = tmp_scale;
	cur_animation->anim_script = anim_script + 1;
	u32 S2 = anim_cmd.transform_field;
	if (S2 & 1) {
		cur_animation->anim_script = anim_script + 1;
		cur_animation->unk54 = cur_animation->anim_script[0];
	}
	if (S2 & 2) {
		cur_animation->anim_script = anim_script + 1;
		cur_animation->unk74 = cur_animation->anim_script[0];
	}
	if (S2 & 4) {
		cur_animation->anim_script = anim_script + 1;
		cur_animation->unk70 = cur_animation->anim_script[0];
	}
	if (S2 & 8) {
		cur_animation->anim_script = anim_script + 1;
		cur_animation->unk7C = cur_animation->anim_script[0];
	}
	if (S2 & 0x10) {
		cur_animation->anim_script = anim_script + 1;
		cur_animation->unk78 = cur_animation->anim_script[0];
	}
}


For func_8000DE30, which is called after func_8000D35C (which sets up the aobjs):

f32 field_set;
if (aobj->arg_type != 0){
	if ((-FLT_MAX / 3.0f) != cur_animation->scale) {
		aobj->length = (f32) (aobj->length + cur_animation->anim_rate);
	}
	u8 aobj_index = aobj->transform_index;
	if (aobj->transform_index < 24){
		switch(aobj->arg_type){
			// set to this in lerp color
			case 2:
				field_set = aobj->prev_pt + (aobj->length * aobj->prev_slope);
				break;
			// set to this in lerp step
			case 3:
				AxB2 = aobj->percent_completion * (aobj->length)^2;
				A2xB3 = (aobj->length)^3 * (aobj->percent_completion)^2;
				A2xB3mAxB2 = A2xB3 - AxB2;
				A3xB3x2 = 2.0f * A2xB3 * aobj->percent_completion;
				A2xB2x3 = 3.0f * (aobj->length)^2 * (aobj->percent_completion)^2;
				field_set = (aobj->prev_pt * ((A3xB3x2 - A2xB2x3) + 1.0f)) + (aobj->next_pt * (A2xB2x3 - A3xB3x2)) + (aobj->prev_slope * ((A2xB3mAxB2 - AxB2) + aobj->length)) + (aobj->next_slope * A2xB3mAxB2);
				break;
	// Formula in full with aobj->percent_completion as A and aobj->length as B and aobj->prev_pt as C and aobj->next_pt as D aobj->prev_slope as E and aobj->next_slope as F
	// AB should be the equivalent of time t parametrically for a curve mapped [0-t]
	// C(2*t^3 - 3t^2 + 1) + D(3t^2 - 2*t^3) + E(t^2*B - 2t*B + B) + F(t^2*B - 2t*B)

	// this is called a hermite spline, C=p0, D=p1, E=m0*A, F=m1*A
	// m coefficients are pre multiplied by A, which I guess is a small optimization, saving a mul
			// set to this in set cmds
			case 1:
				if (aobj->percent_completion <= aobj->length) {
					field_set = aobj->next_pt;
				} else {
					field_set = aobj->prev_pt;
				}
				break;
		}
		// for limb transforms, transform_index should start at 0xD
		switch(aobj->transform_index){
			case 0xD:
				cur_animation->unk80 = (s16) (u32) field_set;
				break;
			case 0xE:
				cur_animation->unk1C = field_set;
				break;
			case 0xF:
				cur_animation->unk20 = field_set;
				break;
			case 0x10:
				cur_animation->unk24 = field_set;
				break;
			case 0x11:
				cur_animation->unk28 = field_set;
				break;
			case 0x12:
				cur_animation->unk82 = (s16) (u32) field_set;
				break;
			case 0x13:
				cur_animation->unk44 = field_set;
				break;
			case 0x14:
				cur_animation->unk48 = field_set;
				break;
			case 0x15:
				cur_animation->unk84 = field_set;
				break;
			case 0x16:
				cur_animation->unk88 = field_set;
				break;
		}
		// color transforms
		else{
			if (aobj->arg_type != 1) {
				if (aobj->arg_type == 2) {
					var_v1 = (s32) (aobj->length * aobj->percent_completion * 256.0f);
					if (var_v1 < 0) {
						var_v1 = 0;
					}
					if (var_v1 >= 0x101) {
						var_v1 = 0x100;
					}
					sp34 = 0;
					sp38 = 0;
					temp_a2 = 0x100 - var_v1;
					unksp39 = (u8) aobj->prev_pt;
					unksp3B = aobj->unk11;
					unksp35 = (u8) aobj->next_pt;
					unksp37 = aobj->unk15;
					sp38 = (temp_a2 * sp38) + (sp34 * var_v1);
					temp_t9 = (u8) sp38;
					sp38 = 0;
					sp44 = temp_t9;
					sp45 = unksp3A;
					unksp39 = aobj->unk12;
					unksp3B = aobj->unk13;
					unksp35 = aobj->unk16;
					unksp37 = aobj->unk17;
					sp38 = (temp_a2 * sp38) + (sp34 * var_v1);
					sp46 = (u8) sp38;
					sp47 = unksp3A;
				}
			} else {
				// wtf
				var_a2 = aobj + 0x10;
				if (aobj->percent_completion <= aobj->length) {
					var_a2 = aobj + 0x14;
				}
				sp44 = (s32) *var_a2;
			}
			// color fields, starts at 0x25 or 37
			switch (aobj->transform_index) {
			case 37:
				cur_animation->unk58 = (s32) sp44;
				break;
			case 38:
				cur_animation->unk60 = (s32) sp44;
				break;
			case 39:
				cur_animation->unk64 = (s32) sp44;
				break;
			case 40:
				cur_animation->unk68 = (s32) sp44;
				break;
			case 41:
				cur_animation->unk6C = (s32) sp44;
				break;
			}
		}
	}
}
aobj = aobj->next; // should be the next transform field that was initialized
]
****************************************************************************
[Mode 2 Animations

Mode 2 animations seem to contain more than just anim cmds, it has a bunch of
spline or path data inside of it as well, and some other struct data.

Mode 2 animations seem to be used in cutscenes. Possibly as moving collision anims.

Functions used:
	- func_8001DB54 // during processing for spline data
	- func_8001E104 // during processing for spline data
	- func_800A94F4 // on initialization
	
	- func_80010988 // calls the two below
	- func_8000FE64 // to read the anim_script data, scroll is unk70 in arg0
	- func_8001074C // to read the anim_script data, scroll is unk70 in arg0

func_8000FE64 jump table is at 0x800408f8:

The case code is basically the same as in func_8000D35C, I will note the differences.
Aobj indices start at 0x19, the field edited by splines is the transform of the
anim_script cmd bit + 0x19 and that gets parsed by the aobj update code.

There are also cases for anims that aren't in other formats:

case 0xD(0x800105B8) UnkD:
// 0xD seems to hold pointer data, up to 2 args, this is only used once (7,17)
{
	u32 S2 = anim_cmd.transform_field;
	cur_animation->anim_script = anim_script + 1;
	if (S2 & 0x8) {
		if(aobj_sp8c == NULL){
			// initialize aobj
			aobj_sp8c =func_80009978(cur_animation, 0x1C);
		}
		aobj_sp8c->unk20 = arg0->unk70[0] // this should be the anim script
		aobj_sp8c->unk70 = arg0->unk70 + 1 // this should be the anim script
	}
	if (S2 & 0x80) {
		if(aobj_sp9c == NULL){
			// initialize aobj
			aobj_sp9c =func_80009978(cur_animation, 0x1C);
		}
		aobj_sp9c->unk20 = arg0->unk70[0] // this should be the anim script
		aobj_sp9c->unk70 = arg0->unk70 + 1 // this should be the anim script
	}
}

case 0x17(0x800106A0) Unk17:
// sets some variables in the unknown struct
// 2 float args
{
	f32 anim_script[] = arg0->unk70;
	anim_script += 1;
	arg0->unk70 = anim_script;
	arg0->unk74 = arg0->unk74 + anim_cmd.scale;
	anim_script += 1;
	arg0->unk70 = anim_script;
	arg0->unk28 = anim_script[0];
	anim_script += 1;
	arg0->unk70 = anim_script; // this should be the anim script
	arg0->unk2C = anim_script[0];
}


]
****************************************************************************
[Misc Bank Contents:

Bank 6,7...
These contain Level Settings or Collision Blocks (not exclusively).
Level settings blocks are as described at the start. Collision Blocks are
the same, but only have the collision part with no path or entity data.

Bank 7/4/3/2/1/0:
Contains particle texture data and particle image format specs
Bank 5:
Nothing

Bank 0/1/2/7/6:
A mysterious format with a large header, and then repeated structs containing
byte data and float arrays of length 11.

Seems to be particle data of some sort.

Bank 3:
Contains some header and then all 0xFF padding?? No known info of use.
Always 0x200 bytes long??

MISC BANK 3 CONTENTS
Bank 3 has a unique misc data type that is not present in any other bank.
The data appears to have a short amount of bytes, followed by padding to
fill it out to 0x200 length. The padding is insiginificant.
The data itself is unremarkable in nature at first, but appears to affect
the HUD in the menus.
Currently theorised to be palette data for HUD textures.

]
****************************************************************************
[Misc Particles Formatting
Misc Bank 0-2 contains particle textures after a list of offsets.
format:
struct ParticleHeader{
	/*0x0*/ u32 num;
	union{
		/*0x4*/ struct ParticleTex *Offsets[]; //len is num
		/*0x4*/ struct ParticleDat *Offsets[]; //len is num
	};
};
struct ParticleTex{
	/*0x0*/ u32 numFrames;
	/*0x4*/ u32 Fmt; //f3d convention
	/*0x8*/ u32 Siz; //f3d convention
	/*0xC*/ u32 width; //or swapped
	/*0x10*/ u32 height; //or swapped
	/*0x14*/ u32 unk; //often 1
	/*0x18*/ u32 *Offsets[]; //len is numFrames
	//extra stuff like second set of offsets or palette
};

when unk is not 1, it seems to affect the number of textures. possibly a mip map or something??
when it is 0, has second set of textures 25% of the size of the first set
mip mapping overruled by palettes potentially? Unknown condition that occurs
when both are present, but current example is that mipmap offsets don't exist

when tex type is CI, seems to have palette ptrs at end


Misc Bank 0-1 contains float/int data and no particle textures.
//same as one for images.
//small hop dust is index 8 in 0-1
struct ParticleHeader{
	/*0x0*/ u32 num;
	union{
		/*0x4*/ struct ParticleTex *Offsets[]; //len is num
		/*0x4*/ struct ParticleDat *Offsets[]; //len is num
	};
};
//Always present and only read on init
struct ParticleInit{
	/*0x0*/ u16 Flag_Unk; //not sure
	/*0x2*/ u16 TexIndex; //src file is likely next misc = particle tex
	/*0x4*/ u16 Persistance; //0 to stay forever
	/*0x6*/ u16 unk2; //something to do with transform
	/*0x8*/ u16 unk3; //only one byte is used? read only at start
	/*0xA*/ u16 unk4; //bitflag
	/*0x10*/ f32 Transform[12]; //len 12, has multiple variations
	/*0x3C*/ struct ParticleDat Dat;
};

//read during run time, variable length
struct ParticleDat{
//idk how to even represent this in proper C lol
//see next section for how this works.
};
Generic notes. Upon init, this struct is read and put into a diff struct
that links together and does transformations to particles. Data is read via
index call when particle is spawned (e.g. small jump -> 0-8 -> header ->init)


Transform:
Seems to have multiple different formats depending on the structure before it.
First 5 are always floats.

transform 6 - Flag_Unk == (1 or 5 (only sometimes?)), unk2 has a value ['0x1E0', '0x12C', '0xF0', '0x258', '0x3C', '0x1E'] bits = 0x3FE - turns into u8[4]

transform 7 - Flag_Unk == (8 or 1), unk2 has a value ['0x1', '0x8', '0xA', '0xC', '0xF', '0x10', '0x12', '0x14', '0x19', '0x1E', '0x20', '0x24', '0x28', '0x12C', '0x32', '0x3C', '0x258', '0x64', '0xF0', '0x78'] bits = 0x3FF - turns into u8[4]

transform 8 - always float
transform 9 - always float

transform 10 - Flag_Unk & 2 && unk4 & 1, unk2 has a value ['0x32', '0x64'] bits = 0x76 - turns into u8[4]

transform 11 - Flag_Unk & 2 && !(unk2 &0xF), unk2 has a value ['0x78', '0x32', '0x64', '0x14'] bits = 0x7E - turns into u8[4]

transform 12 - Flag_Unk == (7, 6, 5, 1) - turns into float, otherwise u8[4]


bitflag notes:
Flag_Unk:
seems to change the format of the transform

unk4:
unk4 & 0x800 causes runtime struct to go to next member and stay there.
unk4 & 0x1 sets transform 10 to bytes

*****unk6[0]*****(read at 0x8009C548)

see decomp for code tbh.
The rest of the data is a packed byte array. not guaranteed to be aligned.
It is interpreted bytecode essentially, sort of like a behavior script.
Has cmd, then byte data.


for more on function, see next section.

//runtime struct that holds particle info. unk length (maybe not static?)
//very very not accurate data members right now
struct ParticleTransform{
	/*0x0*/ struct *ParticleTransform; //linked list?
	/*0x4*/ u16 unk; //not read/not important?
	/*0x6*/ u16 unk2; //0xA of particle dat OR'd with 0x10
	/*0x8*/ u8 unk3; //
	/*0x9*/ u8 unk4; //
	/*0xA*/ u8 unk5; //part of the initial spawn arg
	/*0xB*/ u8 TexIndex; //Texture index, must be index inside of particle tex file
	/*0xC*/ u16 unk6; //0x6 of  particle dat, lifetime per particle?
	/*0xE*/ u16 Persistance; //0x4 of  particle dat
	/*0x10*/ struct ParticleDat *Dat; //Points to Dat of ParticleInit
	/*0x14*/ f32 Transform[13]; //not necessarily in the same order
	/*0x48*/ u32 *unk8; //used as an arg to func 0x800A0558 but checked null first
	/*0x4C*/ u32 *unk9; //checked null, crashes when not
	/*0x50*/ f32 unk10[2]; //rot vel? seems to take radians arg
	/*0x58*/ u32 unk11[8]; //not read during loop or some conditions?
};


Transform notes (as seen in ParticleTransform struct):
X origin (or H pos on path?)
Y origin
Z origin (or ortho to path?)

Z rotation base (maxes at +\- 90 deg?)
Up direction (only determines up/down based on sign??)
X rotation base (maxes at +\- 90 deg?)

X scale base(X seemed to have no effect?)
Y scale base
Z scale base(only appeared to make it less sparse?)

(all of these are negative for some reason)
radius (negative is compliment of circle formed??)
angle? (phi angle, definitely in radians)
num particles? (no idea what sign means here)

next one seemed calculated. Maybe overflow so odd number of particles
stays at consistant pos between frames.
Seems to be calculated more often when number is negative.

]
****************************************************************************
[PARTICLE DATA CASE BYTECODE
run from function 0x8009C4E0

some ex. to study, dash (0-0), small hop (0-8), wall hit (0-30), slide (0-31),
enter water (0-16), swim (0-1E)
particle chosen is given by func 0x800A19EC(particle file (5 is 0?), index)

basic processing psuedo code, accum is cur loc in particle dat:
[
	loop:
	cmd = accum[0]
	if(cmd < 0x80){
		V0 = cmd & 0x1F;
		if(cmd & 0x20){
			SP_94 = (V0 << 8) + accum[0];
			accum++
		}else{
			SP_94 = V0;
		}
		V0 = cmd&0xC0;
		if(V0 == 0x40){
			unk_0B = accum[0];
			accum++
		}
		if(SP_94){
			goto loop;
		}else{
			apply_transforms(); //really just guessing, idk the full implications of this
		}
	}else{
		v0 = cmd & 0xF8;
		if(V0 > 0x99){
			if(V0 & 0xF0 == (0xC0 || 0xD0)){
			cmd = v0;
			}
		}
		cmd -= 0x80;
		switch(cmd){
			/*details below*/
		}
	}
]
particle data uses bytecode plus a static init struct to control the transformations.
The following are the conditions for each case in the code.
all struct mentions are the runtime particle struct ParticleTransform

cmds for stuff over 0x80:[
0x80 (set (translate??) transform):
checks bits 1, 2, 3. performs function for each one present. Increments by 4.
(0x24,0x28,0x2C) of struct updated based on bit

0x88(add (translate??) transform):
repeat of above, but struct update is added not set

0x90 (set (rotate??) transform):
same as above but diff transform
(0x30,0x34,0x38) of struct updated based on bit

0x98 (add (rotate??) transform):
same as above but diff transform
(0x30,0x34,0x38) of struct updated based on bit


0x99 ():
unk

0x9F ():
unk



0xA0 (set ?? and ??):
Uses 1 byte normally, and 2 bytes if byte 1 &0x80.
Then struct value 0x48 by next 4 as float.
//not sure on significance of this yet
if (arg0->unk12 == 1) {
	arg0->unk12 = 0U;
	arg0->unk44 = (f32) arg0->unk48;
}

0xA1 (set byte 0x6):
arg0->unk6 = (u16) temp_s1->unk0;
increments by 1

0xA2 (jump and set??):
increments by 4
not sure, sets next value to be based on a0+4 which not sure on what it is.

0xA3 (set ?? 0x40):
increments by 4, does something else to unk6

0xA4 (??????????):
increments by 2, does other shit idk.

0xA5 (???????????):
increments by 2, does other shit idk.

0xA6 (???????????):
increments by 4, does other shit idk.
data is two halfs

0xA7 (???????????):
increments by 1, does other shit idk.

0xA8 (add transform modulate by random):
increments by 12.
Sets (0x24,0x28,0x2C) of struct based on formula (f32) (arg0->unk2C + ((2.0f * sp80 * random_f32()) - sp80));
where sp80 is data

0xA9 (???????????):
increments by 4, does other shit idk.

0xAA (???????????):
increments by 4, does other shit idk.

0xAB (multiply transform):
increments by 4
multiplies (0x30,0x34,0x38) by read float data.

0xAC (set transform and modulate random?):
increments by 8 + 1 + bitwise 0x80 on 1 for 2
sets 0x12, then 0x48, then arg0->unk48 = (f32) (arg0->unk48 + (sp80 * random_f32()));
then some other stuff

0xAD (set unk6 flag 0x80):
increments by 0

0xAE (set unk6 flag & ~0x80):
increments by 0

0xAF (set unk6 flag & ~0x40):
increments by 0
does some other stuff too

0xB0 (set unk6 flag & 0xFFDF | 0x40):
increments by 0
does some other stuff too

0xB1 (set unk6 flag | 0x60):
increments by 0

0xB2 (set unk6 flag | 0x200):
increments by 0

0xB3 (set unk6 flag & 0xFBFF):
increments by 0

0xB4 (set unk6 flag | 0x400):
increments by 0

0xB5 (set unk6 flag | 0x100):
increments by 0

0xB6 (set unk6 flag & ~0x100):
increments by 0

0xB7 (????????):
increments by 1
takes data &D_800D6A14 as arg to something

0xB8 (????????):
increments by 5
takes data &D_800D6A14 as arg to something
1 byte, followed by float

0xB9 (????????):
increments by 2
idk

0xBA (????????):
increments by 4
lots of shit

0xBB (????????):
increments by 4
similar to last

0xBC (????????):
increments by 2
does some stuff

0xBD (????????):
increments by 8
does some stuff

0xBE (multiply transform ??):
increments by 12
multiplies (0x30,0x34,0x38) by read float data.

0xBF (?????????):
increments by 1
sets unk6

0xC0 (?????????):
checks first 4 bits of num.
increments by 1 for each (+1 always, +2 if byte 1 &0x80)
sets (0x50,0x51,0x52,0x53) bytes of struct

0xD0 (?????????):
basically same as above but slightly different logic to whats being edited.

0xE0 (?????????):
inc by 4, 4 bytes of data, idk logic

0xE2 (set unk6 flag | 0x8):
inc by 0

0xE3 (set unkC):
inc by 1

0xE4 ():

0xE6 ():

0xEB ():

0xED ():

0xEE ():
unk

0xF0 ():

0xF6 ():

0xF7 ():

0xF5 ():
unk


0xFA (setup return repeatN):
inc by 1
sets var in struct to return to (0x20)
repeats by byte 1 value

0xFB (goto return repeatN):
goes to var in struct to return to (0x20)
repeats by byte 1 value set in 0xFA

0xFC (setup return loop):
sets var in struct to return to (0x1E)

0xFD (goto return loop):
goes to var in struct to return to (0x1E)

0xFE ():
unk

0xFF (???????????):
sets v0 to something, most likely a break
]
less than 0x80:
if it fails to loop, then it executes the rest of the function logic, then
returns, only to have the function called again. I imagine this works as an
apply transformations of sorts.
Other cmds have certain uses. 0x40 reads 1 byte advances and loops, so does
0x20. Otherwise the bottom 5 bits act as an end loop.

Significance of bottom 5 (aka SP_94) bits during transform:
value is stored to unk10 of some unk struct, not sure on purpose


]
****************************************************************************
[LIST OF PARTICLE DATA FILES
list of data for testing and for later porting to makefile/linker
data is almost always followed by a texture file. Maybe it is adhoc
behavior to keep the textures associated with data?

particle_data = [
	(0,1),
	(0,3),
	(1,1),
	(2,1),
	(3,7),
	(3,18),
	(3,20),
	(4,1),
	(7,1),
	(7,3),
	(7,5),
	(7,7),
	(7,9),
	(7,11),
	(7,13),
	(7,15),
	(7,17),
	(7,19),
	(7,21),
	(7,23),
	(7,25),
	(7,27),
	(7,29),
	(7,31),
	(7,33),
	(7,35),
	(7,37),
	(7,39),
	(7,41),
	(7,43),
	(7,45),
	(7,47),
	(7,49),
	(7,51),
	(7,53),
	(7,55),
]

particle_textures = [
	(0,2),
	(1,2),
	(2,2),
	(3,8),
	(3,19),
	(3,21),
	(4,2),
	(7,2),
	(7,4),
	(7,6),
	(7,8),
	(7,10),
	(7,12),
	(7,14),
	(7,16),
	(7,18),
	(7,20),
	(7,22),
	(7,24),
	(7,26),
	(7,28),
	(7,30),
	(7,32),
	(7,34),
	(7,36),
	(7,38),
	(7,40),
	(7,42),
	(7,44),
	(7,46),
	(7,48),
	(7,50),
	(7,52),
	(7,54),
	(7,56),
]
both of the above particle data files are 100% matching using the ParticleDat.py
tool.
]
****************************************************************************
[Audio Locations & Formats:

No audio data seems to be present in any of the file system data listed above.

String/Table data starts at 0x68790 ends around 0x7a318

File system data starts at 0x4AA8F0 and ends at the rom end

Another set of dev strings is at 0x7e050 to 0x7e944

Inside the main stage tables, the music ID is read by function 0x800F64B0
It feeds the music ID to function 0x800a75b0 or 0x800b9df8 depending on some hardcoded values. (see https://github.com/farisawan-2000/kirby64/blob/master/src/ovl2/ovl2.c#L155
)

In function 0x800a75b0 it basically checks if the music track is playing (or loaded),
if not it then calls function 0x80020914 based on some data around 0x800c0000.

I have no idea what the function after that does but it seems to just do some simple
logic to setup values.

After all that, function 0x800f8560 is called with new values. Then 0x800bb98c is called.

From there a bunch of stuff happens, I have no idea what any of the values are.

Looking for compressed audio chunk IDs:
No results for any of the common ones or ones listed in programming manuals.

Looking for compressed midi events:
Tempo: FF51 (tempo)
Loop start: FF2E (loop num) FF
Loop end: FF2D (loop count) FF (delta from loop start)
End: FF2F

Found several results for loop 0 around 0x250000 to 0x2A0000
Found end track all over, but very dense in same area as loop
Similar story for tempo cmds.
Looking for loop end cmds I found them all very near the loop start ones, with
correct deltas (assuming delta is from loop start start to loop end end.)

Midi data is loaded into 0x80058FF0 (unsure if constant, tested on 1-1-1, though other levels have same spot)

Midi locations:

0x0D - 0x0268554
header struct:
00000044 0000068E 00000B74 0000125C 0000144F 0000154E 00001683 000017CA 00001B17 00001EB0 0000286B 00002B1D 00002C4F 00002EEE 00002FA9 0000301C 00000180 02FF5105

The offset is specified in bytes from the begining of the file to the begining of
the track
division value is taken from input midi.
division from midi spec:
The third word, <division>, specifies the meaning of the delta-times. It has two formats, one for metrical
time, and one for time-code-based time:
bit 15 bits 14 thru 8 bits 7 thru 0
0 ticks per quarter-note
1 negative SMPTE format ticks per frame

0x00250320 contains midi offsets, table ends at 0x250518.
This table is read in code at 0x800203A8, function 0x8001FD64 which is probably an audio thread or something else huge.

value 0xD = 0x00018234, 0x00003361 OR 0x0002582, 0x0018234. Not exactly sure
midi 0xD = 0x18230(4 off?) offset from table start, 0x1803C from table end.

From manual:

//sequences is the entire midi file, this is a way to collect all of the midis in one file
typedef struct {
	u16 version; /* Should be 0x5331 in kirby*/
	s16 seqCount;
	ALSeqData seqArray[];
} ALCSeqHdr; //not a name from the manual

typedef struct {
	u8 *offset;
	s32 seqLen;
} ALSeqData;

//this is a singular midi header
//the rest of the data is mostly normal midi with some small changes
typedef struct {
	 u32 trackOffset[16];
	 u32 division;
} ALCMidiHdr;

The offsets represent the position of the start of the sequence from the
beginning of the file.
MIDI ends at 0x2a8ca1
]
****************************************************************************
[Audio Samples
starts at 0x2A8CB0 and ends at 0x4AA8F0 (actual end 0x49F58C)

Pointer to data start is at 0x3ff70 alongside midi file start:
0x3ff60: 00003214 003E1400 003E6BC0 003E6BC0
0x3ff70: 002A8CB0 002B1510 002B1510 00250320


***0x2A8CB0***
a bank file as described in the programming manual, begins with the 
bank file header.

typedef struct {
	s16		revision;
	s16		bankCount;
	s32		bankArray[1]; //actually ALBank
} ALBankFile;

typedef struct {
	s16		instCount;
	u8		flags;
	u8		pad;
	s32		sampleRate;
	s32		percussion;
	s32		instArray[1];
} ALBank;

structs from manual now.

typedef struct {
	u8		volume;
	u8		pan;
	u8		priority;
	u8		flags;
	u8		tremType;
	u8		tremRate;
	u8		tremDepth;
	u8		tremDelay;
	u8		vibType;
	u8		vibRate;
	u8		vibDepth;
	u8		vibDelay;
	s16		bendRange;
	s16		soundCount;
	s32		soundArray[1]; //actually ALSound
} ALInstrument;

typedef struct Sound_s {
	s32		envelope;
	s32		keyMap;
	s32		wavetable;
	u8		samplePan;
	u8		sampleVolume;
	u8		flags
} ALSound;

typedef struct {
	s32		attackTime;
	s32		decayTime;
	s32		releaseTime;
	s16		attackVolume;
	s16		decayVolume;
} ALEnvelope;

typedef struct {
	u8		velocityMin;
	u8		velocityMax;
	u8		keyMin;
	u8		keyMax;
	u8		keyBase;
	u8		detune;
} ALKeyMap;

enum {AL_ADPCM_WAVE = 0,
 AL_RAW16_WAVE};
 
typedef struct {
	s32		order;
	s32		npredictors;
	s16		book[1]; /* Must be 8-byte aligned */
} ALADPCMBook;

typedef struct {
	u32		start;
	u32		end;
	u32		count;
	ADPCM_STATE		state;
} ALADPCMloop;

typedef struct {
	u32		start;
	u32		end;
	u32		count;
} ALRawLoop;

typedef struct {
	ALADPCMloop		*loop;
	ALADPCMBook		*book;
} ALADPCMWaveInfo;

typedef struct {
	ALRawLoop	*loop;
} ALRAWWaveInfo;

typedef struct {
	s32		base;
	s32		len;
	u8		type;
	u8		flags;
	union {
		ALADPCMWaveInfo adpcmWave;
		ALRAWWaveInfo rawWave;
	} waveInfo;
} ALWaveTable;

***0x2B1510***
looks like byte data, aka the samples themselves.

table repeats at 
0x003E1400
with next effects at 0x003E6BC0

These are the sound effects, the previous ones are instruments.

Last Sound Effect ends at: 0x49F58C
]
****************************************************************************
[Mystery data after sounds but before files

note: These notes are me figuring out what the data is, kind of messy

starts at 0x49F58C and ends at 0x4AA8F0

at 0x3ffA0: 0049F590 004A0340 004A0340 004A3B60 004A3B60 004AA8F0

0x4AA8F0 - The file start of the file system that has been documented far above.

0x049F590 - Data (loaded when the game starts)

0x04A0340 - Offset table of file, followed by byte data? (loaded when the game starts)

0x04A3B60 - Offset table of file, followed by byte data again (loaded when the game starts)

The offsets referenced inside of the file are within 0x20 bytes of each other and not aligned at all,
this means its probably not an image or sound.

One chunk looks like this:
6000307F 40003540 01674004 808F4002 00007F80 0A5EC050 000864C0 40034209 7AC070

Chunks seem to always start with 0x60

Tables at 0x04A0340/0x04A3B60:
On load, all but the first value are converted to virtual addresses. First value is likely array length.
0x4a0340 loaded into 0x80071BD0.
Table values are read by function 0x8002e30 and used as an argument to function 0x80023d5C.
Values are later read by 0x80024750.
One specific chunk of data is read everytime kirby takes a step, so it is audio related or animation related.

Changing the values of that byte struct results in (collecting star bit sound):
changing the 0x60 changes the pitch of the sound.
changing all to zero drastically changes the sound.
100% sound arguments.

Data at 0x049F590:
Loaded into 0x80070E20 at game start.
Values are read whenever kirby starts running.
Values are read whenver kirby hits a wall.
Values are read whenever mr poppy bros throws a bomb and when it explodes.
read by function 0x80024750 around 0x80024a30 in the function.
Is made up by 3 floats, and a struct of 4 bytes.
struct Mystery{
u8 unk0;
u8 unk1;
u8 unk2;
u8 unk3;
Vec3f unk4;
}
Changing these values changes how the sound effects sound. Likely a mapping of sfx to sound args.
]
****************************************************************************
[Sound Args and Sound Mappings

Sound Args:
This data is in two locations, from the main file table at 0x3FF50 there is:
/*0x58*/	u32		*SoundArgs1[2];
/*0x60*/	u32		*SoundArgs2; //AssetData is the end ptr, but it is re used for asset start
/*0x64*/	u32		*AssetData; //0x3FFB4

File starts with the header:
typedef struct {
	u32 SfxArgCount;
	u8 *SfxArgs[];
} SoundArgsHeader

Each sfx args ptr is a byte array. The byte arrays determine how to manipulate sound samples
to form a given sound effect. The exact argument of each byte is yet to be determined.

Sound Mappings:
This data is in one location, found from the main file table at 0x3FF50:
/*0x50*/	u32		*SoundMapping[2]; //0x3ffA0, affects how sfx work, not sure on exact format

This mapping file tells the game which sound args to use (maybe?) for an actual sound call.
This also can affect how the sound actually sounds rather than the samples used.

Not sure on exact purpose and relation between this and sound args.
]
****************************************************************************
[Unallowed entities on boss stage (not sure if constant):
gordo
shotzo
propellor
bronto burt
ghost knight
]
****************************************************************************
[Making mini boss stages.

22 in stage struct should be 10.
Use a entity type inside of bank 8
Use an entity type of door (usually in bank 5)
Search in another boss level to find door entity to use.
Whether or not crystal shard and mini types spawn is based on entity type.
You can only have one boss or they all instantly despawn?
]
****************************************************************************
[Code Overlay Data and ROM locations

There are 20 overlays in total in the game, each can be loaded at various points
by the game, based on where you are in the game, and stage flags
Format is OVL: (ROM start, end), (RAM start, end), "desc"

Overlays = {
	0: (0x1000, 0x39e30), (0x80000400, 0x80039230), "main, threads, crash screens",
	1: (0x43790, 0x7ec10), (0x8009b540, 0x800f61a0), "object processor, particles, animations",
	2: (0x7ec10, 0xb1b40), (0x800f61a0, 0x8012eaf0), "collision processing, levels etc.",
	//different game states
	3: (0xb1b40, 0xf8630), (0x80151100, 0x80198870), "kirby",
	4: (0xf8630, 0x103bb0), (0x80151100, 0x8015c730), "menus",
	5: (0x103bb0, 0x135490), (0x8015c740, 0x8018ee60), "menus",
	6: (0x135490, 0x13e8f0), (0x80151100, 0x8015a560), "unk",
	//idk
	7: (0x13e8f0, 0x174740), (0x80198880, 0x801ce6d0), "unk",
	//bosses
	8: (0x174740, 0x17ecb0), (0x801d0c60, 0x801db1d0), "unk?",
	9: (0x17ecb0, 0x1cbf50), (0x801d0c60, 0x8021df00), "entities/minibosses",
	10: (0x1cbf50, 0x1e5aa0), (0x801db1e0, 0x801f4db0), "Adeleine?",
	11: (0x1e5aa0, 0x1eb520), (0x801db1e0, 0x801e0c60), "whispy woods",
	12: (0x1eb520, 0x1f3160), (0x801db1e0 , 0x801e2e20), "pix",
	13: (0x1f3160, 0x1fddd0), (0x801db1e0, 0x801e5e50), "acro",
	14: (0x1fddd0, 0x205d40), (0x801db1e0, 0x801e3150), "magman",
	15: (0x205d40, 0x211490), (0x801db1e0, 0x801e6930), "HR-H/HR-R",
	16: (0x211490, 0x2263d0), (0x801db1e0, 0x801f0120), "miracle matter",
	17: (0x2263d0, 0x2308c0), (0x801db1e0, 0x801e56d0), "zero two",
	//gfx backend and such
	18: (0x2308c0, 0x23e630), (0x8021df20, 0x8022bd40), "unk",
	19: (0x23e630, 0x2501c0), (0x8021df20, 0x8022fb50), "unk",
	20: (0x2501c0, 0x250320), (0x80300000, 0x80300230), "tamper check",
}


]
****************************************************************************
[Game Engine Objects
In most HAL games, there is a system of objects used for controlling each specific
type of data. They follow the nomenclature of <X>Obj, where <X> is the type of data.

AObj is an animation object. Really this contains the spline data and transform
properties of an animation. For example if you're editing a rotation, the X, Y and Z
value will have their own AObj, and the AObj will contain info from one val to the next.

// AObj
struct AObj {
  struct AObj *next; // it's a list but is it really the next one?
  u8 transform_index; // index
  u8 arg_type; // enum flag of some sort
  u8 unk6;
  u8 unk7;
  // these make up the values for a hermite spline, p0, p1, m0, m1
  // t is length*percent_completion
  f32 percent_completion; // keyframe percent completion
  f32 length; // time left until next keyframe, or just length (but negative)
  f32 prev_pt; // p0 
  f32 next_pt; // p1
  f32 prev_slope; // m0
  f32 next_slope; // m1
  s32 unk20;
};

D objects are one of the runtime data structs the game uses to process entities gfx.
there can be multiple D objects for a single entity as seen in game

struct DObj {
	/* 0x00 */ struct DObj *unk0; //next?
	/* 0x04 */ void (*unk04)(struct GObj *); //GObj contains ptr to this at 0x3C
	/* 0x08 */ struct DObj *unk8;
	/* 0x0C */ u32 unkC;
	/* 0x10 */ struct DObj *unk10; //child?
	/* 0x14 */ u32 unk14;
	/* 0x18 */ struct DObj *unk18; //
	/* 0x1C */ Vec3f Pos;
	/* 0x28 */ void (*unk28) void; //
	/* 0x2C */ u32 unk2C;
	/* 0x30 */ Vec3f Rot;
	/* 0x3C */ void (*unk3C) void; //
	/* 0x40 */ Vec3f Scale;
	/* 0x4C */ u32 unk4C;
	/* 0x50 */ u32 unk50;
	/* 0x54 */ u8 unk54[4];
	/* 0x58 */ void (*unk58) void; //
	/* 0x5C */ u32 unk5C;
	/* 0x60 */ u32 unk60;
	/* 0x64 */ u32 unk64;
	/* 0x68 */ u32 unk68;
	/* 0x6C */ u32 unk6C;
	/* 0x70 */ u32 unk70; //anim related
	/* 0x74 */ Vec3f unk74; //anim related
	/* 0x80 */ void (*unk80) void;
	/* 0x84 */ u32 unk84;
};
]
****************************************************************************
[Vtables

kirby uses vtables for entities everywhere, making it basically impossible
to really debug code by statically looking at functions, or by pulling a
stack trace
because all ents are processed in a row each frame, there is no
place to get an entry without already knowing the func needed to process
Instead, it is better to identify which vtables correspond to which general
group of entities/actions.
This will be done by isolating entities, and monitoring the virtual function
processing loop

Functions are called via 0x800a447c:
// executes the virtual function at index arg0
void call_virtual_function(u32 arg0, u32 arg1, VTABLE callback) {
    // __thiscall? But rarely any of these functions use D_8004A7C4...
    if (arg0 < arg1) callback[arg0](D_8004A7C4);
}

There are vtables in ovl9 data (0x8021BAB0 - 8021DF00) some float data aswell.
These are the vtables associated with entities.

Kirby Vtables (ovl3_5) vtbl_80196990, D_80196A10, D_80196AE8, D_80196B88
These contain functions for kirbys power ups and actions

Certain entities that are level/very specific don't seem to use vtables
this is stuff like some solid plats, or ado
]
*************************************************************************
[obj attr arrays

each attr of an object is stored in array of type, and accessed by ID.
e.g. all speeds are in one array, all bools are in a different etc.

Absolute speed seems to calculated based on action and stored to 0x800E6850
direction/floor dependent speed and stored to 0x800E64D0

Direction (+1 or -1) is stored in 0x800E6A10

Env flags (u32) are in 0x800E8AE0
# based on observation
underwater entry is 0x1
underwater is 0x2
below surface is 0x4
water plunge is 0xC0

action is 0x800DDFD0

?? obj ptr is at 0x800E0490

Y speed is 0x800E3210

unk f32, 0x800E3750, 0x800E3C90, 0x800E6690

?? timer is 0x800E9720 (maybe anim/action related)

scale X, Y, Z is 0x800E4550, 0x800E4710, 0x800E48D0

pos X, Y, Z is 0x800E2B10, 0x800E2CD0, 0x800E2E90
next pos X, Y, Z is 0x800E25D0, 0x800E2790, 0x800E2950

angle X, Y, Z is 0x800E4010, 0x800E41D0, 0x800E4390
]
****************************************************************************
[Kirby Ent Vtable Map
#I will be documenting some ents as I find them

Ent_vtable_map = {
	0: (call = 0x80215464, vtable = 0x8021CCE8) #NZ
	1: (call = 0x801D0FAC, vtable = 0x8021BAE8) #rocky
	2: (call = 0x801F3408, vtable = 0x8021C3D0) #bronto burt
	3: (call = 0x8020A598, vtable = 0x8021C994) #skud
	4: (call = 0x801D3698, vtable = 0x8021BB40) #gordo
	5: (call = 0x801E8A38, vtable = 0x8021C008) #shotzo
	6: (call = 0x801D5080, vtable = 0x8021BB68) #spark-i
	7: (call = 0x80218248, vtable = 0x8021CDAC) #bouncy
	8: (call = 0x802175C4, vtable = 0x8021CD7C) #glunk
	#slushy no vtable?
	10: (call = 0x8020A9B8, vtable = 0x8021C9C0) #chilly
	11: (call = 0x801FA80C, vtable = 0x8021C5AC) #propellor
	12: (call = 0x8020B498, vtable = 0x8021C9FC) #glom
	#mahall
	14: (call = 0x80216184, vtable = 0x8021CD2C) #poppy bros jr.
	#no ent 15
	#splinter, gobblin
	18: (call = 0x801F4A14, vtable = 0x8021C414) #kany
	#bivolt
	20: (call = 0x801F6C00, vtable = 0x8021C4A4) #sir kibble
	21: (call = 0x8020BC98, vtable = 0x8021CA24) #gabon
	#mariel
	23: (call = 0x8020D618, vtable = 0x8021CA88) #large I3
	24: (call = 0x8020E178, vtable = 0x8021CAB8) #snipper
	#blowfish
	26: (call = 0x801F7B90, vtable = 0x8021C4DC) #bonehead
	27: (call = 0x801B2D90, vtable = 0x801CD60C) #squibbly
	28: (call = 0x801FCD1C, vtable = 0x8021C69C) #bobo
	29: (call = 0x801FB408, vtable = 0x8021C5C8) #bo
	30: (call = 0x80218A58, vtable = 0x8021CDE0) #punc
	31: (call = 0x801F9610, vtable = 0x8021C560) #mite
	32: (call = 0x801FA80C, vtable = 0x8021C5AC) #sandman
	33: (call = 0x801B58A8, vtable = 0x801CD6D0) #flopper
	34: (call = 0x801F889C, vtable = 0x8021C520) #kapar
	#maw
	36: (call = 0x80211B1C, vtable = 0x8021CB88) #drop
	37: (call = 0x801B9498, vtable = 0x801CD754) #pedo
	38: (call = 0x801D78F0, vtable = 0x8021BC20) #noo
	#tick
	40: (call = 0x801DB62C, vtable = 0x8021BCD0) #cairn
	#no 41
	#pompey
	43: (call = 0x8021C6D0, vtable = 0x801FD41C) #hack
	#burnis
	45: (call = 0x801FF2D0, vtable = 0x8021C738) #fishbone
	#frigis
	47: (call = 0x801D98B8, vtable = 0x8021BC8C) #sawyer
	48: (call = 0x802002F4, vtable = 0x8021C770) #turbite
	49: (call = 0x801DA7A0, vtable = 0x8021BCB0) #plug
	50: (call = 0x801DBF48, vtable = 0x8021BD4C) #ghost knight
	51: (call = 0x80201008, vtable = 0x8021C794) #zoos
	52: (call = 0x801E2970, vtable = 0x8021BEA0) #kakti
	#rockn, chacha
	55: (call = 0x802144F8, vtable = 0x8021CCA0) #galbo
	56: (call = 0x80201F94, vtable = 0x8021C7C0) #bumber
	57: (call = 0x80203500, vtable = 0x8021C810) #scarfy
	58: (call = 0x801E5A74, vtable = 0x8021BF1C) #Nruff
	#emp, magoo
	61: (call = 0x801EBE1C, vtable = 0x8021C0C4) #yariko
	#62 is butterfly
	63: (call = 0x801E9D18, vtable = 0x8021C054) #wall shotzo
	#no vtable for keke
	65: (call = 0x801E7C88, vtable = 0x8021BF64) #sparky
	#ignus (rocks)
	#flora, putt, pteran
	70: (call = 0x8020488C, vtable = 0x8021C854) #mumbies
	71: (call = 0x80205738, vtable = 0x8021C884) #pupa
	#mopoo, zebon
}

Boss_vtable_map = {
	
	1: (call = 0x80225FA8, vtable = 0x8022AD10) #bouncy boss
	2: (call = 0x80221440, vtable = 0x8022ABA8) #kakti boss
	3: (call = 0x8022383C, vtable = 0x8022AC2C) #fishbone boss
	4: (call = 0x80220424, vtable = 0x8022AB8C) #spark-i boss
	
	8: (call = 0x80220AA8, vtable = 0x8022AB9C) #blowfish boss
	
	15: (call = 0x802218E4, vtable = 0x8022ABC0) #sawyer boss
	
}


# kirby uses (ovl3_5) vtbl_80196990, D_80196A10, D_80196AE8, D_80196B88
Kirby_vtable_map = {
	"unk1": (call = 0x8016C510, vtable = 0x80196990), # action init
	"unk2": (call = 0x8016C510, vtable = 0x80196A10), # extension of prev
	"unk3": (call = 0x8016C558, vtable = 0x80196AE8), # kirby actions & powerups run
	"unk4": (call = 0x8016C558, vtable = 0x80196B88), # just an extension of prev
}

# since kirby is so important, I will label each func to an action/power
# init can be monitored at BP 8016C53C
kirby_init_func_map = {
	# action init, vtable 0x80196990 & 0x80196A10
	"stand": (id = 0x00, func = 0x8016CA8C),
	"walk": (id = 0x01, func = 0x8016D3A8),
	"run": (id = 0x02, func = 0X8016DA14),
	"jump": (id = 0x03, func = 0x8016DDE8),
	"falling": (id = 0x06, func = 0x8016EF5C),
	"soft land": (id = 0x07, func = 0x8016F6DC),
	"fast falling": (id = 0x08, func = 0x8016F80C),
	"crouch": (id = 0x09, func = 0x80170AC4),
	"fall through plat": (id = 0x0A, func = 0z801702F0),
	"swallow": (id = 0x0B, func = 0x80170794),
	"float": (id = 0x0C, func = 0x8016FD88), # occurs each puff up
	"ladder": (id = 0x0D, func = 0x801712F8), 
	"vine": (id = 0x0E, func = 0x80171E00), 
	"take out power": (id = 0x11, func = 0x80174504),
	"put away power": (id = 0x12, func = 0x801747F0),
	"throw obj up": (id = 0x13, func = 0x80174A30),
	"dmg/knockback": (id = 0x14, func = 0X80174C10),
	"die": (id = 0x16, func = 0x80176490),
	"swim": (id = 0x17, func = 0x80176860),
	"inhale": (id = 0x18, func = 0x80172AE4),
	"crouch slide": (id = 0x19, func = 0x801736BC),
	"air puff": (id = 0x1A, func = 0x80173CB4),
	"throw object fw": (id = 0x1B, func = 0x80173EC0),
	# powerups
	"fire": (id = 0x23, func = 0x80179370),
	"rock": (id = 0x24, func = 0x80179C28),
	"ice": (id = 0x25, func = 0x8017B068),
	"needle": (id = 0x26, func = 0x8017B78C),
	"bomb": (id = 0x27, func = 0x8017BD68),
	"spark": (id = 0x28, func = 0x8017BF34),
	"cutter": (id = 0x29, func = 0x8017C418), # double cutter too
	"fire/fire": (id = 0x2A, func = 0x8017CF60),
	"fire/rock": (id = 0x2B, func = 0x8017D8E8),
	"fire/ice": (id = 0x2C, func = 0x8017E074),
	"fire/needle": (id = 0x2D, func = 0x8017E284),
	"fire/bomb": (id = 0x2E, func = 0x8017EA0C),
	"rock/rock": (id = 0x2F, func = 0x8017F1C0),
	"rock/needle": (id = 0x30, func = 0x80180818),
	"rock/bomb": (id = 0x31, func = 0x80181014),
	"rock/ice": (id = 0x32, func = 0x80181110),
	"ice/needle": (id = 0x33, func = 0x80181F64),
	"ice/bomb": (id = 0x34, func = 0x80181F64),
	"ice/spark": (id = 0x35, func = 0x80182D9C),
	"ice/ice": (id = 0x36, func = 0x801835AC),
	"needle/needle": (id = 0x37, func = 0x80183E38),
	"needle/bomb": (id = 0x38, func = 0x80184538),
	"bomb/bomb": (id = 0x39, func = 0x80184B24),
	"spark/fire": (id = 0x3A, func = 0x80184CA4),
	"spark/rock": (id = 0x3B, func = 0x801856A4),
	"spark/needle": (id = 0x3C, func = 0x80185788),
	"spark/bomb": (id = 0x3D, func = 0x80185A9C),
	"spark/spark": (id = 0x3E, func = 0x80186750),
	"cutter/fire": (id = 0x3F, func = 0x80186E30), # occurs on subsequent re-use of power. e.g. take out sword, swing sword, walk with sword
	"cutter/rock": (id = 0x40, func = 0x80189914),
	"cutter/ice": (id = 0x41, func = 0x8018CC54),
	"cutter/needle": (id = 0x42, func = 0x8018DDCC),
	"cutter/bomb": (id = 0x43, func = 0x8018E164),
	"cutter/spark": (id = 0x44, func = 0x8018E164),
	"cutter/cutter": (id = 0x44, func = 0x8018E164), # same as single cutter
	# misc/auto
	"warp": (id = 0x46, func = 0x80156050), 
	"boss beat": (id = 0x48, func = 0x8015A44C), 
	"minecart": (id = 0x4D, func = 0x80158C40), 
	
}

#actions at BP 8016C5C4
kirby_run_func_map = {
	# powerups/actions run, vtable 0x80196AE8 & 0x80196B88
	"stand": (id = 0x00, func = 0x8017CAF8),
	"walk": (id = 0x01, func = 0x8016D81C),
	"run": (id = 0x02, func = 0x8016DD0C),
	"jump": (id = 0x03, func = 0x8016E15C),
	"fall": (id = 0x05, func = 0x8016F240),
	"soft land": (id = 0x06, func = 0x8016F7C8),
	"fast fall": (id = 0x07, func = 0x8016FB58),
	"crouch": (id = 0x08, func = 0x8016FFF8),
	"go through plat": (id = 0x09, func = 0x80170638),
	"swallow": (id = 0x0A, func = 0x80170A24),
	"float": (id = 0x0B, func = 0x80170D88),
	"ladder": (id = 0x0C, func = 0x801717F0),
	"vine": (id = 0x0D, func = 0x80172234),
	"take out power": (id = 0x10, func = 0x801746E0),
	"put back power": (id = 0x11, func = 0x8017499C),
	"throw power up": (id = 0x12, func = 0x80174B7C),
	"knockback/dmg": (id = 0x13, func = 0x80175754),
	"swim": (id = 0x15, func = 0x80176DE0),
	"inhale": (id = 0x16, func = 0x80173260),
	"crouch slide": (id = 0x17, func = 0x80173AF4),
	"spit out object": (id = 0x18, func = 0x80173E40),
	"throw power fw": (id = 0x19, func = 0x80174144),
	"air puff": (id = 0x1B, func = 0x8017782C),
	
	# powerups start
	"fire": (id = 0x21, func = 0x8017982C),
	"rock": (id = 0x22, func = 0x8017A390),
	"ice": (id = 0x23, func = 0x8017B3C4),
	"needle": (id = 0x24, func = 0x8017B8F4),
	"bomb": (id = 0x25, func = 0x8017BEF4),
	"cutter": (id = 0x27, func = 0x8017CAF8),
	"spark": (id = 0x26, func = 0x8017C1FC),
	"fire/fire": (id = 0x28, func = 0x8017D430),
	"fire/rock": (id = 0x29, func = 0x8017DBB8),
	"fire/ice": (id = 0x2A, func = 0x8017E1EC),
	"fire/needle": (id = 0x2B, func = 0x8017E54C),
	"fire/bomb": (id = 0x2C, func = 0x8017EDDC),
	"rock/rock": (id = 0x2D, func = 0x8017F988),
	"rock/needle": (id = 0x2E, func = 0x80180B58),
	"rock/bomb": (id = 0x2F, func = 0x801810D0),
	"rock/ice": (id = 0x30, func = 0x801815F4),
	"ice/needle": (id = 0x31, func = 0x80181CFC),
	"ice/bomb": (id = 0x32, func = 0x8018271C),
	"ice/spark": (id = 0x33, func = 0x80183428),
	"ice/ice": (id = 0x34, func = 0x80183A1C),
	"needle/needle": (id = 0x35, func = 0x80183FF4),
	"needle/bomb": (id = 0x36, func = 0x801848A4),
	"bomb/bomb": (id = 0x37, func = 0x80184C64),
	"spark/fire": (id = 0x38, func = 0x80185224),
	"spark/rock": (id = 0x39, func = 0x80185748),
	"spark/needle": (id = 0x3A, func = 0x801859688),
	"spark/bomb": (id = 0x3B, func = 0x80186248),
	"spark/spark": (id = 0x3C, func = 0x80186A20),
	"cutter/fire": (id = 0x3D, func = 0x80188238),
	"cutter/rock": (id = 0x3E, func = 0x8018B228),
	"cutter/ice": (id = 0x3F, func = 0x8018D4C8),
	"cutter/needle": (id = 0x40, func = 0x8018DFB4),
	"cutter/bomb": (id = 0x41, func = 0x8018E3B0),
	"cutter/spark": (id = 0x42, func = 0x8018F368),
	
	# misc acts
	"warp": (id = 0x43, func = 0x80156594),
	
}

]
****************************************************************************
[Shiftable Assets Build Map

Headersize is 0x80057DB0 for file related overlays

Filetables start at 0x6C8F0:
    filestables are described in ASSET DATA/FILE SYSTEM section
    before filetables are dev strings, can eat into that for extra space if needed
    

There is a small binary chunk containing unknown data after filetables. This chunk starts at 0x800D00C4 (0x78314). This can also be represented as C .data.

Pointers to BankHeader which contains pointers to filetables is at 0x800D0184 (0x783D4)

After BankHeader filetables, is the stage table.

Stagetable ends at 0x800d20e8 (0x7A338), then is more misc C data.

There is data for other .c files starting at 0x800D5660 (0x7D8B0).

Stagetable references strings, these strings are at 0x800D5E00 to 0x800d6680 (0x7e050) to (0x7E8D0).

Assets start at 0x4AA8F0

In order to compile, this order will be used.

Segment start{
    Binary until 0x6C8F0
    Filetables
    misc C data
    Pointers to filetables and stagetable
    misc C data
    Binary from 0x7D8B0 to 0x7e050
    Dev strings (.rodata)
    Binary from 0x7E8D0 to 0x4AA8F0
}

In order to have a shiftable asset system, the filetables need to be able to change size.
Everything uses pointers in the file system, so there is no issue with with moving the
filetable, though the rest of the data needs to be at the same exact location.

0x68790 to 0x6c8A0 are all dev strings, this area will be used to store the expanded
filetable.
Then the filetable will be padded to 0x78314 (0x800D00C4), this will be done by changing the size of the extracted file "until_0x6C8F0.bin".

]
****************************************************************************