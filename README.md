# Kirby 64 Hacking Project

This project is based on the [Kirby64 hacking project by ShyGoo](https://github.com/shygoo/k64cs-project) and the [Kirby64 Decomp project by Someone2639](https://github.com/farisawan-2000/kirby64/). This project mostly aims to completely understand the inner workings of specific files that are especially pertinent for ROM hacking.
This project is mostly a solo endeavor by me,  and the above projects were used for reference. Currently this repo is in a state where file replacement should be possible very soon. There is also a plethora of notes, information and tools for extracting and analyzing assets within the Kirby 64 ROM.

## Directory
* The `notes` folder will contain my personal notes used during debugging and has most of the info with my current understanding of the Kirby64 ROM. This should be the most up to date info on how the game works from any available resource.
* `Blender Tools` contains a mostly defunct blender plugin for importing/exporting level data assets. For more up to date blender tools see [this fast64 fork](https://github.com/jesusyoshi54/fast64-KCS/tree/KCS).
* `Split_Tools` contains all the tooling needed to extract all game assets, and build a (almost) matching ROM with the extracted content.

## Dependencies

You will need an up to date version of python (I use 3.8.9, probably even as early as 3.6 will work). You will also need to have a linux terminal, either through WSL or via Linux OS. Once you have a working Linux install, you should be able to install the dependencies by running this in the terminal ``sudo apt update && sudo apt install git gcc-mips-linux-gnu && git clone``.
You will also need a copy of a US kirby64 ROM. The file should be in the `Split_Tools` folder and have the filename `Kirby64.z64`.
You will need the following python packages installed:

* pypng ``pip3 install pypng``
* bitstring ``pip3 install bitstring``

## Extracting

In CMD or terminal, run ``python3 AssetExtractor.py`` or ``make extract`` in the `Split_Tools` folder. Use `python3` if you are using Linux.

## Building
Extract all the assets using the extraction instructions first. Then, in terminal, run `make all -j` in the `Split_Tools` folder. The output ROM should be in the `build` folder.

## Importing
Importing model files is handled by fast64. Visit [this fork](https://github.com/jesusyoshi54/fast64-KCS) and follow the instructions in the README (likely none as of now) and you will be able to import and export level geometry.

## TODO
* convert animations to .c
* convert particle files to a compilable .c
* convert collision misc files to a compilable .c format
* create a method to convert background images to bins with sprite headers without using a json at compile time

# Issues
There is currently still some binary files that are not auto extracted. Asset tables cannot be shifted, and the checksum is not defeated when building a non-matching ROM.

# Contributing
You can contribute by joining our [discord](https://discord.gg/7GaDCQ2), or by using gitlab/github features for collaboration.